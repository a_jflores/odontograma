<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<div class="row bg-title">
    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
        <h4 class="page-title">Pacientes</h4> </div>
    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
        <button class="right-side-toggle waves-effect waves-light btn-info btn-circle pull-right m-l-20"><i class="ti-settings text-white"></i></button>
        <ol class="breadcrumb">
            <li><a href="${pageContext.request.contextPath}/index.jsp">Dashboard</a></li>
            <li class="active">Pacientes</li>
        </ol>
    </div>
</div>
<!-- /row -->
<div class="row">
    <div class="col-sm-12">
        <div class="white-box">
            <div class="row">
                <div class="col-sm-12 col-xs-12 col-md-10">
                    <h3 class="box-title m-b-0">Lista Pacientes</h3>
                    <p class="text-muted m-b-15">Pacientes</p>
                </div>
                <div class="col-sm-12 col-xs-12 col-md-2 text-right">
                    <a href="javascript:crear()" class="btn btn-info btn-rounded">Nuevo Paciente</a>
                </div>
            </div>
            <br>
            <div class="table-responsive">
                <table id="data_pacientes" class="display" style="width:100%">
                    <thead>
                        <tr>
                            <th>Id</th>
                            <th>Nombres</th>
                            <th>Nacionalidad</th>
                            <th>Documento</th>
                            <th>Teléfono</th>
                            <th>Operaciones</th>
                        </tr>
                    </thead>
                    <tbody>

                    </tbody>
                </table>
            </div>

            <div id="modal_crearPaciente" class="modal fade in" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button id="btn_cerrar_modal" type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                            <center>
                                <h3 class="box-title m-b-0" id="myModalLabel">Nuevo Paciente</h3>
                            </center>
                        </div>
                        <div class="modal-body">
                            <form data-toggle="validator" id="pacienteFormNew" method="POST">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Nombres y Apellidos</label>
                                            <div class="input-group">
                                                <div class="input-group-addon"><i class="ti-user"></i></div>
                                                <input name="nombres" data-error="Campo requerido" type="text" class="form-control" pattern="[a-zA-ZàáâäãåąčćęèéêëėįìíîïłńòóôöõøùúûüųūÿýżźñçčšžÀÁÂÄÃÅĄĆČĖĘÈÉÊËÌÍÎÏĮŁŃÒÓÔÖÕØÙÚÛÜŲŪŸÝŻŹÑßÇŒÆČŠŽ∂ð ,.'-]{2,254}" placeholder="Nombres y Apellidos" required>
                                            </div>
                                            <div class="help-block with-errors"></div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Documento de indentidad</label>
                                            <div class="input-group">
                                                <div class="input-group-addon"><i class="ti-file"></i></div>
                                                <input name="documento" data-error="Campo requerido" type="text" class="form-control" placeholder="Dni" required>
                                            </div>
                                            <div class="help-block with-errors"></div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Nacionalidad</label>
                                            <div class="input-group">
                                                <div class="input-group-addon"><i class="fa fa-flag-o"></i></div>
                                                <select name="nacionalidad" id="new_nacionalidadPaciente" class="form-control" style="width: 100%">

                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Teléfono/Celular</label>
                                            <div class="input-group">
                                                <div class="input-group-addon"><i class="ti-mobile"></i></div>
                                                <input name="telefono" data-error="Campo requerido" type="text" class="form-control" placeholder="Contacto" required>
                                            </div>
                                            <div class="help-block with-errors"></div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Domicilio</label>
                                            <div class="input-group">
                                                <div class="input-group-addon"><i class="ti-home"></i></div>
                                                <input name="domicilio" data-error="Error, el domicilio es inválido" type="text" class="form-control" placeholder="Domicilio">
                                            </div>
                                            <div class="help-block with-errors"></div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Email</label>
                                            <div class="input-group">
                                                <div class="input-group-addon"><i class="ti-email"></i></div>
                                                <input name="email" data-error="Error, el email es inválido" type="email" class="form-control" placeholder="Email">
                                            </div>
                                            <div class="help-block with-errors"></div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label>Fecha de nacimiento</label>
                                            <div class="input-group">
                                                <div class="input-group-addon"><i class="ti-calendar"></i></div>
                                                <input name="fecha_nac" type="date" class="form-control">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label>Género</label>
                                            <div class="input-group">
                                                <div class="input-group-addon"><i class="ti-layers"></i></div>
                                                <select name="sexo" class="form-control">
                                                    <option value="" selected>Sellecione una opción</option>
                                                    <option value="M">Masculino</option>
                                                    <option value="F">Femenino</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label>Estado civil</label>
                                            <div class="input-group">
                                                <div class="input-group-addon"><i class="ti-heart"></i></div>
                                                <select name="estado_civil" class="form-control">
                                                    <option value="" selected>Sellecione una opción</option>
                                                    <option value="S">Soltero</option>
                                                    <option value="C">Casado</option>
                                                    <option value="V">Viudo</option>
                                                    <option value="D">Divorciado</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12 text-center">
                                        <button id="btn_new" type="submit" class="btn btn-success btn-rounded waves-effect waves-light m-r-10">Nuevo</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>

            <div id="modal_paciente" class="modal fade in" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                            <center>
                                <h3 class="box-title m-b-0" id="myModalLabel">Paciente</h3>
                            </center>
                        </div>
                        <div class="modal-body">
                            <strong>Nombres</strong>
                            <p id="nombrePaciente"></p>
                            <strong>Documento</strong>
                            <p id="documentoPaciente"></p>
                            <strong>Nombres</strong>
                            <p id="nacionalidadPaciente"></p>
                            <strong>Nombres</strong>
                            <p id="telefonoPaciente"></p>
                        </div>
                    </div>
                </div>
            </div>

            <div id="modal_editarPaciente" class="modal fade in" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button id="btn_cerrar_modal" type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                            <center>
                                <h3 class="box-title m-b-0" id="myModalLabel">Editar Paciente</h3>
                            </center>
                        </div>
                        <div class="modal-body">
                            <form data-toggle="validator" id="pacienteFormUpdate" method="POST">
                                <input type="hidden" id="update_idPaciente" name="id_paciente"/>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Nombres y Apellidos</label>
                                            <div class="input-group">
                                                <div class="input-group-addon"><i class="ti-user"></i></div>
                                                <input name="nombres" id="update_nombrePaciente" data-error="Campo requerido" type="text" class="form-control" pattern="[a-zA-ZàáâäãåąčćęèéêëėįìíîïłńòóôöõøùúûüųūÿýżźñçčšžÀÁÂÄÃÅĄĆČĖĘÈÉÊËÌÍÎÏĮŁŃÒÓÔÖÕØÙÚÛÜŲŪŸÝŻŹÑßÇŒÆČŠŽ∂ð ,.'-]{2,254}" placeholder="Nombres y Apellidos" required>
                                            </div>
                                            <div class="help-block with-errors"></div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Documento de indentidad</label>
                                            <div class="input-group">
                                                <div class="input-group-addon"><i class="ti-file"></i></div>
                                                <input name="documento" id="update_documentoPaciente" data-error= "Campo requerido" type="text" class="form-control" placeholder="Dni" required>
                                            </div>
                                            <div class="help-block with-errors"></div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Nacionalidad</label>
                                            <div class="input-group">
                                                <div class="input-group-addon"><i class="fa fa-flag-o"></i></div>
                                                <select name="nacionalidad" id="update_nacionalidadPaciente" class="form-control" style="width: 100%">

                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Teléfono/Celular</label>
                                            <div class="input-group">
                                                <div class="input-group-addon"><i class="ti-mobile"></i></div>
                                                <input name="telefono" id="update_telefonoPaciente" data-error="Campo requerido" type="text" class="form-control" placeholder="Contacto" required>
                                            </div>
                                            <div class="help-block with-errors"></div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Domicilio</label>
                                            <div class="input-group">
                                                <div class="input-group-addon"><i class="ti-home"></i></div>
                                                <input name="domicilio" id="update_domicilioPaciente" data-error="Error, el domicilio es inválido" type="text" class="form-control" placeholder="Domicilio">
                                            </div>
                                            <div class="help-block with-errors"></div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Email</label>
                                            <div class="input-group">
                                                <div class="input-group-addon"><i class="ti-email"></i></div>
                                                <input name="email" id="update_emailPaciente" data-error="Error, el email es inválido" type="email" class="form-control" placeholder="Email">
                                            </div>
                                            <div class="help-block with-errors"></div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label>Fecha de nacimiento</label>
                                            <div class="input-group">
                                                <div class="input-group-addon"><i class="ti-calendar"></i></div>
                                                <input name="fecha_nac" id="update_fechanacimientoPaciente" type="date" class="form-control">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label>Género</label>
                                            <div class="input-group">
                                                <div class="input-group-addon"><i class="ti-layers"></i></div>
                                                <select name="sexo" id="update_sexoPaciente" class="form-control">
                                                    <option value="" selected>Sellecione una opción</option>
                                                    <option value="M">Masculino</option>
                                                    <option value="F">Femenino</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label>Estado civil</label>
                                            <div class="input-group">
                                                <div class="input-group-addon"><i class="ti-heart"></i></div>
                                                <select name="estado_civil" id="update_estadocivilPaciente" class="form-control">
                                                    <option value="" selected>Sellecione una opción</option>
                                                    <option value="S">Soltero</option>
                                                    <option value="C">Casado</option>
                                                    <option value="V">Viudo</option>
                                                    <option value="D">Divorciado</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12 text-center">
                                        <button id="btn_update" type="submit" class="btn btn-success btn-rounded waves-effect waves-light m-r-10">Actualizar</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>
