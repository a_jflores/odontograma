<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="es">
    <head>
        <jsp:include page="/templates/head.jsp"/>
    </head>
    <body class="fix-header">
        <div class="preloader">
            <svg class="circular" viewBox="25 25 50 50">
            <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10"/>
            </svg>
        </div>
        <div id="wrapper">
            <jsp:include page="/templates/header.jsp" flush="true"/> 
            <jsp:include page="/templates/navbar.jsp" flush="true"/>
            <div id="page-wrapper">
                <div class="container-fluid">
                    <jsp:include page="/views/Pacientes/pacientes.jsp" flush="true"/>
                    <jsp:include page="/templates/right-sidebar.jsp" flush="true"/>
                </div>
                <jsp:include page="/templates/footer.jsp" flush="true"/>
            </div>
        </div>
        <jsp:include page="/templates/scripts.jsp" flush="true"/>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.0/js/i18n/es.js" defer></script>
        <script>
            $(document).ready(function () {
                
                $.fn.select2.defaults.set('language', 'es');
                $.fn.select2.defaults.set("theme", "bootstrap");

                $.extend($.fn.dataTable.defaults, {
                    responsive: true
                });

                $("#data_pacientes").DataTable({
                    "processing": true,
                    "language": {
                        "url": "//cdn.datatables.net/plug-ins/1.10.24/i18n/Spanish.json"
                    },
                    "ajax": {
                        "url": "pacientes.do?op=listarJson",
                        "type": "POST",
                        "dataSrc": ""
                    },
                    "columns": [
                        {"data": "codigoPaciente"},
                        {"data": "nombrePaciente"},
                        {"data": "nacionalidadPaciente"},
                        {"data": "documentoPaciente"},
                        {"data": "telefonoPaciente"},
                        {
                            "data": null,
                            "className": "dt-center",
                            "render": function (data) {
                                return  '<a href="javascript:ver(' + data.codigoPaciente + ')" class="text-inverse p-r-10" data-toggle="tooltip" title="" data-original-title="Visualizar"><i class="fa fa-eye"></i></a>' +
                                        '<a href="javascript:editar(' + data.codigoPaciente + ')" class="text-inverse p-r-10" data-toggle="tooltip" title="" data-original-title="Editar"><i class="ti-marker-alt text-info"></i></a>' +
                                        '<a href="javascript:eliminar(' + data.codigoPaciente + ')" class="text-inverse" title="" data-toggle="tooltip" data-original-title="Eliminar"><i class="ti-trash text-danger"></i></a>';
                            },
                            "orderable": false
                        }
                    ],
                    "order": [[0, "desc"]]
                });

                $.ajax({
                    url: 'historias.do?op=paises',
                    Type: 'GET',
                    dataType: 'json',
                    success: function (data) {
                        $.each(data, function (key, registro) {
                            $("#update_nacionalidadPaciente").append('<option value=' + registro.nacionalidadPais + '>' + registro.nombrePais + '</option>');
                        });
                    },
                    error: function () {
                        console.log("No se ha podido obtener la información");
                    }
                });

                $.ajax({
                    url: 'historias.do?op=paises',
                    Type: 'GET',
                    dataType: 'json',
                    success: function (data) {
                        $.each(data, function (key, registro) {
                            $("#new_nacionalidadPaciente").append('<option value=' + registro.nacionalidadPais + '>' + registro.nombrePais + '</option>');
                        });
                        for (var i in data) {
                            if (data[i].nacionalidadPais == 'PE') {
                                $('#new_nacionalidadPaciente').val(data[i].nacionalidadPais);
                            }
                        }
                    },
                    error: function () {
                        console.log("No se ha podido obtener la información");
                    }
                });

                $("#update_nacionalidadPaciente").select2({
                    minimumInputLength: 2
                });

                $("#new_nacionalidadPaciente").select2({
                    minimumInputLength: 2,
                    placeholder: "Selecciona un País"
                });

            });
            function ver(id)
            {
                $.ajax({
                    url: 'pacientes.do?op=verJson&id=' + id,
                    Type: 'GET',
                    dataType: 'json',
                    success: function (data) {
                        console.log(data);
                        $("#modal_paciente").modal("toggle");
                        $("#nombrePaciente").text(data.nombrePaciente);
                        $("#documentoPaciente").text(data.documentoPaciente);
                        $("#nacionalidadPaciente").text(data.nacionalidadPaciente);
                        $("#telefonoPaciente").text(data.telefonoPaciente);
                    },
                    error: function () {
                        console.log("No se ha podido obtener la información");
                    }
                });
            }

            function crear()
            {
                $("#modal_crearPaciente").modal("show");
                $('#pacienteFormNew').validator().on('submit', function (e) {
                    if (e.isDefaultPrevented()) {
                        console.log("no válido");
                    } else {
                        e.preventDefault();
                        $.ajax({
                            data: $('#pacienteFormNew').serialize(),
                            url: "pacientes.do?op=crearJson",
                            type: "POST",
                            success: function (data)
                            {
                                console.log(data);
                                $('#data_pacientes').DataTable().ajax.reload();
                                $("#modal_crearPaciente").modal("hide");
                                $('#pacienteFormNew').trigger("reset");
                                $.toast({
                                    heading: 'Éxito',
                                    text: 'Nuevo Paciente creado',
                                    position: 'bottom-right',
                                    icon: 'success',
                                    hideAfter: 4000,
                                    stack: 6
                                });
                            },
                            error: function (data)
                            {
                                console.log(data);
                            }
                        });
                    }
                });
            }

            function editar(id)
            {
                $.ajax({
                    url: 'pacientes.do?op=verJson&id=' + id,
                    dataType: 'json',
                    success: function (data) {
                        console.log(data);
                        $('#update_sexoPaciente option:first-child').attr("selected", "selected");
                        $("#modal_editarPaciente").modal("show");
                        $("#update_idPaciente").val(data.codigoPaciente);
                        $("#update_nombrePaciente").val(data.nombrePaciente);
                        $("#update_documentoPaciente").val(data.documentoPaciente);
                        $('#update_nacionalidadPaciente').val(data.nacionalidadPaciente);
                        $('#update_nacionalidadPaciente').trigger('change');
                        $("#update_telefonoPaciente").val(data.telefonoPaciente);
                        $("#update_domicilioPaciente").val(data.domicilioPaciente);
                        $("#update_emailPaciente").val(data.emailPaciente);
                        $("#update_fechanacimientoPaciente").val(data.fechanacimientoPaciente);
                        if (data.sexoPaciente == undefined) {
                            $('#update_sexoPaciente option:first-child').prop("selected", "selected");
                        } else {
                            $("#update_sexoPaciente option[value='" + data.sexoPaciente + "']").prop("selected", "selected");
                        }
                        if (data.sexoPaciente == undefined) {
                            $('#update_estadocivilPaciente option:first-child').prop("selected", "selected");
                        } else {
                            $("#update_estadocivilPaciente option[value='" + data.estadocivilPaciente + "']").prop("selected", "selected");
                        }
                    },
                    error: function () {
                        console.log("No se ha podido obtener la información");
                    }
                });
            }

            $('#pacienteFormUpdate').validator().on('submit', function (e) {
                if (e.isDefaultPrevented()) {
                    console.log("no válido");
                } else {
                    e.preventDefault();
                    $.ajax({
                        data: $('#pacienteFormUpdate').serialize(),
                        type: "POST",
                        url: "pacientes.do?op=editarJson",
                        success: function (data)
                        {
                            console.log(data);
                            $('#data_pacientes').DataTable().ajax.reload(null, false);
                            $("#modal_editarPaciente").modal("hide");
                            $.toast({
                                heading: 'Éxito',
                                text: 'Paciente actualizado',
                                position: 'bottom-right',
                                icon: 'success',
                                hideAfter: 4000,
                                stack: 6
                            });
                        },
                        error: function (data)
                        {
                            $.toast({
                                heading: 'Érror',
                                text: 'Ocurrió un error',
                                position: 'bottom-right',
                                icon: 'error',
                                hideAfter: 4000,
                                stack: 6
                            });
                        }
                    });
                }

            });

            function eliminar(id)
            {
//        var token = $("meta[name='csrf-token']").attr("content");
//        $.ajax({
//            url: 'pacientes.do?op=eliminarPaciente&id=' + id,
//            type: "PUT",
//            dataType: 'json',
//            data: {
//                "id_paciente": id,
//                "_token": token
//            },
//            success: function (data) {
//                console.log(data);
//                tablaPaciente.ajax.reload();
//            },
//            error: function () {
//                console.log("No se ha podido eliminar");
//            }
//        });
            }
        </script>
    </div>
</body>
</html>

