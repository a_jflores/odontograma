$(document).ready(function () {
    var id_paciente = $("#id_paciente").val();
    var id_historia_clinica = $("#id_historia_clinica").val();

    $.fn.select2.defaults.set('language', 'es');
    $.fn.select2.defaults.set("theme", "bootstrap");
    $("#nacionalidad,#recomendacion").select2({
        minimumInputLength: 2
    });

    $("#select2_odontogramas").select2({
        minimumInputLength: 1
    });

    $.extend($.fn.dataTable.defaults, {
        responsive: true
    });

    $("a[data-toggle=\"tab\"]").on("shown.bs.tab", function (e) {
        $($.fn.dataTable.tables(true)).DataTable()
                .columns.adjust()
                .responsive.recalc();
    });

    $('#modal_crearPresupuesto').on('shown.bs.modal', function () {
        $($.fn.dataTable.tables(true)).DataTable()
                .columns.adjust()
                .responsive.recalc();
    });

    $("#data_citas").DataTable({
        "language": {
            "url": "//cdn.datatables.net/plug-ins/1.10.24/i18n/Spanish.json"
        },
        "deferRender": true,
        "processing": true,
        "ajax": {
            "url": "cita.do?op=listarCitas&id=" + id_paciente,
            "dataSrc": ""
        },
        "columns": [
            {"data": "idCita"},
            {"data": "tratamiento.nombreTratamiento"},
            {"data": "medico", "render": function (data) {
                    return data.nombresMedico + ' ' + data.apellidosMedico;
                }
            },
            {"data": "fechaCita"},
            {
                "data": "estadoCita",
                "render": function (data) {
                    return data === 'A' ?
                            '<span class="label label-success label-rouded">Atendido</span>' :
                            '<span class="label label-danger label-rouded">Pendiente</span>';
                }
            }
        ],
        "order": [[0, "desc"]]
    });

    $("#data_odontogramas").DataTable({
        "processing": true,
        "language": {
            "url": "//cdn.datatables.net/plug-ins/1.10.24/i18n/Spanish.json"
        },
        "ajax": {
            "url": "historias.do?op=listarOdontogramasJson&id=" + id_historia_clinica,
            "dataSrc": ""
        },
        "columns": [
            {
                "data": "codigoOdontograma",
                "width": "10%"
            },
            {
                "data": "date_created",
                "width": "23%",
                render: function (data, type) {
                    var dateSplit = data.split(' ');
                    var newdate = dateSplit[0].split('-');
                    var newhour = dateSplit[1].split('.');
                    return type === "display" || type === "filter" ?
                            newdate[2] + '/' + newdate[1] + '/' + newdate[0] + ' ' + newhour[0] : data;
                }
            },
            {"data": "observaciones"},
            {
                "data": null,
                "className": "dt-center",
                "render": function (data) {
                    return  '<a href="javascript:verOdontograma(' + data.codigoOdontograma + ')" class="text-inverse p-r-10" data-toggle="tooltip" title="" data-original-title="Visualizar"><i class="fa fa-eye"></i></a>' +
                            '<a href="javascript:editarOdontograma(' + data.codigoOdontograma + ')" class="text-inverse p-r-10" data-toggle="tooltip" title="" data-original-title="Editar"><i class="ti-marker-alt text-info"></i></a>' +
                            '<a href="javascript:eliminarOdontograma(' + data.codigoOdontograma + ')" class="text-inverse" title="" data-toggle="tooltip" data-original-title="Eliminar"><i class="ti-trash text-danger"></i></a>';
                },
                "orderable": false
            }

        ],
        "order": [[0, "desc"]]
    });

    $("#data_radiografias").DataTable({
        "processing": true,
        "language": {
            "url": "//cdn.datatables.net/plug-ins/1.10.24/i18n/Spanish.json"
        },
        "ajax": {
            "url": "examenradiografico.do?op=listarJson&id=" + id_historia_clinica,
            "dataSrc": ""
        },
        "columns": [
            {"data": "id_examen_radiografico"},
            {
                "data": "foto",
                'sortable': false,
                'searchable': false,
                'render': function (data) {
                    return '<img src="' + data + '" height="50px" width="50px" />';
                }
            },
            {"data": "observaciones"},
            {
                "data": null,
                "className": "dt-center",
                "render": function (data) {
                    return  '<a href="javascript:verRadiografia(' + data.id_examen_radiografico + ')" class="text-inverse p-r-10" data-toggle="tooltip" title="" data-original-title="Visualizar"><i class="fa fa-eye"></i></a>' +
                            '<a href="javascript:editarRadiografia(' + data.id_examen_radiografico + ')" class="text-inverse p-r-10" data-toggle="tooltip" title="" data-original-title="Editar"><i class="ti-marker-alt text-info"></i></a>' +
                            '<a href="javascript:eliminarRadiografia(' + data.id_examen_radiografico + ')" class="text-inverse" title="" data-toggle="tooltip" data-original-title="Eliminar"><i class="ti-trash text-danger"></i></a>';
                },
                "orderable": false
            }
        ],
        "order": [[0, "desc"]]
    });

    $("#data_videos").DataTable({
        "processing": true,
        "language": {
            "url": "//cdn.datatables.net/plug-ins/1.10.24/i18n/Spanish.json"
        },
        "ajax": {
            "url": "video.do?op=listarJson&id=" + id_historia_clinica,
            "dataSrc": ""
        },
        "columns": [
            {"data": "id_video"},
            {"data": "nombre"},
            {"data": "observaciones"},
            {
                "data": null,
                "className": "dt-center",
                "render": function (data) {
                    return  '<a href="javascript:verVideo(' + data.id_video + ')" class="text-inverse p-r-10" data-toggle="tooltip" title="" data-original-title="Visualizar"><i class="fa fa-eye"></i></a>' +
                            '<a href="javascript:editarVideo(' + data.id_video + ')" class="text-inverse p-r-10" data-toggle="tooltip" title="" data-original-title="Editar"><i class="ti-marker-alt text-info"></i></a>' +
                            '<a href="javascript:eliminarVideo(' + data.id_video + ')" class="text-inverse" title="" data-toggle="tooltip" data-original-title="Eliminar"><i class="ti-trash text-danger"></i></a>';
                },
                "orderable": false
            }
        ],
        "order": [[0, "desc"]]
    });

    $("#data_presupuestos").DataTable({
        "processing": true,
        "language": {
            "url": "//cdn.datatables.net/plug-ins/1.10.24/i18n/Spanish.json"
        },
        "ajax": {
            "url": "presupuesto.do?op=listarJson&id=" + id_historia_clinica,
            "dataSrc": ""
        },
        dom: 'Bfrtip',
        buttons: [{
                extend: 'pdfHtml5',
                text: 'PDF',
                pageSize: 'A4',
                exportOptions: {
                    columns: [0, 1, 2, 3, 4]
                },
                customize: function (doc) {
                    var cols = [];
                    cols[0] = {text: 'Left part', alignment: 'left', margin: [20]};
                    cols[1] = {text: 'Right part', alignment: 'right', margin: [0, 0, 20]};
                    var objFooter = {};
                    objFooter['columns'] = cols;
                    doc['footer'] = objFooter;
                }
            }],
        "columns": [
            {"data": "id_presupuesto"},
            {"data": "asunto"},
            {
                "data": "fecha",
                render: function (data, type) {
                    var dateSplit = data.split(' ');
                    var newdate = dateSplit[0].split('-');
                    var newhour = dateSplit[1].split('.');
                    return type === "display" || type === "filter" ?
                            newdate[2] + '/' + newdate[1] + '/' + newdate[0] + ' ' + newhour[0] : data;
                }
            },
            {
                "data": "total",
                "render": $.fn.dataTable.render.number(',', '.', 2, "S/ ")
            },
            {"data": "tipo_pago"},
            {
                "data": "estado",
                "render": function (data) {
                    return data === 'proceso' ?
                            '<span class="label label-info label-rouded">Proceso</span>' :
                            '<span class="label label-danger label-rouded">Anulado</span>';
                }
            },
            {
                "data": null,
                "className": "dt-center",
                "render": function (data) {
                    var options = "";
                    if (data.estado == 'anulado') {
                        options = '<select class="form-control" onchange="selectOpcion(value,' + data.id_presupuesto + ');">' +
                                '<option value="" selected disabled>Seleccione</option>' +
                                '<option value="4">Activar</option>' +
                                '</select>';
                    } else if (data.estado == 'proceso') {
                        options = '<select class="form-control" onchange="selectOpcion(value,' + data.id_presupuesto + ');">' +
                                '<option value="" selected disabled>Seleccione </option>' +
                                '<option value="1">Pagar</option>' +
                                '<option value="2">Anular</option>' +
                                '<option value="5">Editar</option>' +
                                '<option value="3">Imprimir</option>' +
                                '</select>';
                    } else if (data.estado == 'pagado') {
                        options = '<select class="form-control" onchange="selectOpcion(value,' + data.id_presupuesto + ');">' +
                                '<option value="" selected disabled>Seleccione</option>' +
                                '<option value="2">Anular</option>' +
                                '<option value="3">Imprimir</option>' +
                                '</select>';
                    }
                    return options;
                },
                "orderable": false
            }
        ],
        "order": [[0, "desc"]]
    });

    $("#data_servicio").DataTable({
        "processing": true,
        "searching": false,
        "lengthChange": false,
        "language": {
            "url": "//cdn.datatables.net/plug-ins/1.10.24/i18n/Spanish.json"
        },
        "ajax": {
            "url": "dientedetalle.do?op=obtenerAnomalia&id=" + this.value,
            "dataSrc": ""
        },
        "columns": [
            {"data": "diente.numero"},
            {"data": "hallazgo.nombre"},
            {
                "data": null,
                "render": function (data) {
                    return  '<select name="row-1-office">' +
                            '<option value="Edinburgh" selected="selected">Extracción</option>' +
                            '</select>';
                },
                "orderable": false
            },
            {
                "data": null,
                "render": function (data) {
                    return  '<input min="1" max="99999" type="number" value="1"/>';
                },
                "orderable": false
            },
            {
                "data": null,
                "render": function (data) {
                    return  '<input min="1" max="99999" type="number" value="1"/>';
                },
                "orderable": false
            },
            {
                "data": null,
                "render": function (data) {
                    return  '<input min="1" max="99999" type="number" value="1"/>';
                },
                "orderable": false
            },
            {
                "data": null,
                "className": "dt-center",
                "render": function (data) {
                    return '<a id="eliminarServicio" href="javascript:void(0);" data-toggle="tooltip" data-original-title="Eliminar"><i class="ti-trash text-danger"></i></a>';
                },
                "orderable": false
            }
        ],
        "order": [[0, "desc"]]
    });

    $('#data_servicio tbody').on('click', 'tr', function () {
        fila_servicio = $('#data_servicio').DataTable().row(this).index();
    });

    $('#update_data_servicio tbody').on('click', 'tr', function () {
        update_fila_servicio = $('#update_data_servicio').DataTable().row(this).index();
    });

    var data_precio = [];

    $.ajax({
        url: 'tratamiento.do?op=listar_json',
        Type: 'GET',
        cache: false,
        dataType: 'json',
        success: function (data) {

            for (var item in data) {
                data_precio.push({"id": data[item].idTratamiento, "nombre": data[item].nombreTratamiento, "precio": data[item].precioTratamiento});
            }
        },
        error: function () {
            console.log("No se ha podido obtener la información");
        }
    });

    var tabla_servicio;
    var update_tabla_servicio;

    $('#select2_odontogramas').on('change', function () {
        $("#data_servicio").DataTable().destroy();
        tabla_servicio = $("#data_servicio").DataTable({
            "processing": true,
            "searching": false,
            "lengthChange": false,
            "language": {
                "url": "//cdn.datatables.net/plug-ins/1.10.24/i18n/Spanish.json"
            },
            "ajax": {
                "url": "dientedetalle.do?op=obtenerAnomalia&id=" + this.value,
                "dataSrc": ""
            },
            "columns": [
                {"data": "diente.numero"},
                {"data": "hallazgo.nombre"},
                {
                    "data": null,
                    "render": function () {
                        var sel = '<select required class="select2_tratamiento" name="tratamiento" style="width: 100%">' +
                                '<option value="" selected disabled>Selecciona una opción</option>';

                        for (var i = 0; i < data_precio.length; ++i) {
                            sel += "<option  value = '" + data_precio[i].id + "' >" + data_precio[i].nombre + "</option>";
                        }

                        sel += "</select>";
                        return sel;
                    },
                    "orderable": false
                },
                {
                    "data": null,
                    "className": "dt-center",
                    "render": function (data) {
                        return  '<input name="cantidad" size="4" class="form-control form-control-width" type="text" value="1" required="true"/>';
                    },
                    "orderable": false
                },
                {
                    "data": null,
                    "render": function (data) {
                        return  '<input name="precio" size="6" class="form-control form-control-width" type="text" value="0.00" required="true"/>';
                    },
                    "orderable": false
                },
                {
                    "data": null,
                    "render": function (data) {
                        return  '<input name="sumatotal" size="6" class="form-control form-control-width" type="text" value="0.00" readonly="true" required="true"/>';
                    },
                    "orderable": false
                },
                {
                    "data": null,
                    "className": "dt-center",
                    "render": function (data) {
                        return '<a id="eliminarServicio" href="javascript:void(0);" data-toggle="tooltip" data-original-title="Eliminar"><i class="ti-trash text-danger"></i></a>';
                    },
                    "orderable": false
                }
            ],
            //"order": [[0, "desc"]],
            "drawCallback": function (settings) {
                var api = this.api();
                var rows = api.rows().nodes();
                var suma_total = 0;

                for (var i = 0; i < rows.length; i++)
                {
                    suma_total += parseFloat(api.cell(i, 5).nodes().to$().find('input').val());
                }
                $("#total").val(suma_total.toFixed(2));

                $('.select2_tratamiento').select2({
                    minimumInputLength: 1
                });

                $('.select2_tratamiento').on('select2:select select2:unselecting', function () {
                    var a = parseFloat(api.cell(fila_servicio, 3).nodes().to$().find('input').val());
                    api.cell(fila_servicio, 4).nodes().to$().find('input').val(data_precio[(this.value - 1)].precio);
                    var b = parseFloat(api.cell(fila_servicio, 4).nodes().to$().find('input').val());
                    api.cell(fila_servicio, 5).nodes().to$().find('input').val(a * b);
                });

                $('input').not('#asunto').keyup(function () {
                    var a = parseFloat(api.cell(fila_servicio, 3).nodes().to$().find('input').val());
                    var b = parseFloat(api.cell(fila_servicio, 4).nodes().to$().find('input').val());
                    api.cell(fila_servicio, 5).nodes().to$().find('input').val(a * b);
                });
            }
        });
    });

    $("#update_data_servicio").DataTable();
    $('#update_select2_odontogramas').on('change', function () {
        $("#update_data_servicio").DataTable().destroy();
        update_tabla_servicio = $("#update_data_servicio").DataTable({
            "processing": true,
            "searching": false,
            "lengthChange": false,
            "language": {
                "url": "//cdn.datatables.net/plug-ins/1.10.24/i18n/Spanish.json"
            },
            "ajax": {
                "url": "dientedetalle.do?op=obtenerAnomalia&id=" + this.value,
                "dataSrc": ""
            },
            "columns": [
                {"data": "diente.numero"},
                {"data": "hallazgo.nombre"},
                {
                    "data": null,
                    "render": function () {
                        var sel = '<select required class="select2_tratamiento" name="tratamiento" style="width: 100%">' +
                                '<option value="" selected disabled>Selecciona una opción</option>';

                        for (var i = 0; i < data_precio.length; ++i) {
                            sel += "<option  value = '" + data_precio[i].id + "' >" + data_precio[i].nombre + "</option>";
                        }

                        sel += "</select>";
                        return sel;
                    },
                    "orderable": false
                },
                {
                    "data": null,
                    "className": "dt-center",
                    "render": function (data) {
                        return  '<input name="cantidad" size="4" class="form-control form-control-width" type="text" value="1" required="true"/>';
                    },
                    "orderable": false
                },
                {
                    "data": null,
                    "render": function (data) {
                        return  '<input name="precio" size="6" class="form-control form-control-width" type="text" value="0.00" required="true"/>';
                    },
                    "orderable": false
                },
                {
                    "data": null,
                    "render": function (data) {
                        return  '<input name="sumatotal" size="6" class="form-control form-control-width" type="text" value="0.00" readonly="true" required="true"/>';
                    },
                    "orderable": false
                },
                {
                    "data": null,
                    "className": "dt-center",
                    "render": function (data) {
                        return '<a id="eliminarServicio" href="javascript:void(0);" data-toggle="tooltip" data-original-title="Eliminar"><i class="ti-trash text-danger"></i></a>';
                    },
                    "orderable": false
                }
            ],
            //"order": [[0, "desc"]],
            "drawCallback": function (settings) {
                var api = this.api();
                var rows = api.rows().nodes();
                var suma_total = 0;

                for (var i = 0; i < rows.length; i++)
                {
                    suma_total += parseFloat(api.cell(i, 5).nodes().to$().find('input').val());
                }
                $("#update_total").val(suma_total.toFixed(2));

                $('.select2_tratamiento').select2({
                    minimumInputLength: 1
                });

                $('.select2_tratamiento').on('select2:select select2:unselecting', function () {
                    var a = parseFloat(api.cell(update_fila_servicio, 3).nodes().to$().find('input').val());
                    api.cell(update_fila_servicio, 4).nodes().to$().find('input').val(data_precio[(this.value - 1)].precio);
                    var b = parseFloat(api.cell(update_fila_servicio, 4).nodes().to$().find('input').val());
                    api.cell(update_fila_servicio, 5).nodes().to$().find('input').val(a * b);
                });

                $('input').not('#update_asunto').keyup(function () {
                    var a = parseFloat(api.cell(update_fila_servicio, 3).nodes().to$().find('input').val());
                    var b = parseFloat(api.cell(update_fila_servicio, 4).nodes().to$().find('input').val());
                    api.cell(update_fila_servicio, 5).nodes().to$().find('input').val(a * b);
                });
            }
        });
    });

    $('#data_servicio tbody').on('click', '#eliminarServicio', function () {
        $('#data_servicio').DataTable()
                .row($(this).parents('tr'))
                .remove()
                .draw();
    });

    $('#update_data_servicio tbody').on('click', '#eliminarServicio', function () {
        $('#update_data_servicio').DataTable()
                .row($(this).parents('tr'))
                .remove()
                .draw();
    });

    $('#data_servicio').on('select2:select select2:unselecting', '.select2_tratamiento', function () {
        tabla_servicio.draw();
    });

    $('#update_data_servicio').on('select2:select select2:unselecting', '.select2_tratamiento', function () {
        update_tabla_servicio.draw();
    });

    $("#data_servicio").on("keyup", "input[type='text']", function () {
        tabla_servicio.draw();
    });

});

function verOdontograma(id)
{

}

function editarOdontograma(id)
{
    location.href = "historias.do?op=obtenerOdontograma&id=" + id;
}

function eliminarOdontograma(id)
{

}

function crearRadiografia()
{
    $('#modal_crearRadiografia').on('hidden.bs.modal', function () {
        $(this).find('radiografiaFormNew').trigger('reset');
    });
    $("#modal_crearRadiografia").modal("show");
}

$('#radiografiaFormNew').validator().on('submit', function (e) {
    if (e.isDefaultPrevented()) {
        console.log("no válido");
    } else {
        e.preventDefault();
        var formData = new FormData($('#radiografiaFormNew')[0]);
        $.ajax({
            url: "examenradiografico.do?op=agregarRadiografia",
            type: "POST",
            data: formData,
            contentType: false,
            processData: false,
            beforeSend: function () {
                $('#btn_new_radiografia').button('loading');
            },
            success: function (data)
            {
                console.log(data);
                $('#radiografiaFormNew').trigger('reset');
                $('#data_radiografias').DataTable().ajax.reload();
                $("#modal_crearRadiografia").modal("hide");
                $('#radiografiaFormNew').trigger("reset");
                $.toast({
                    heading: 'Éxito',
                    text: data,
                    position: 'bottom-right',
                    icon: 'success',
                    hideAfter: 4000,
                    stack: 6
                });
            },
            error: function (data)
            {
                console.log(data);
                $.toast({
                    heading: 'Error',
                    text: data.responseText,
                    position: 'bottom-right',
                    icon: 'error',
                    hideAfter: 4000,
                    stack: 6
                });
            },
            complete: function () {
                $("#btn_new_radiografia").button('reset');
            }
        });
    }
});

function verRadiografia(id)
{
    $.ajax({
        url: 'examenradiografico.do?op=verJson&id=' + id,
        Type: 'GET',
        dataType: 'json',
        success: function (data) {
            console.log(data);
            $("#img_radiografia").prop('src', data.foto);
            $("#href_radiografia").attr('href', data.foto);
            $("#href_radiografia").click();
        },
        error: function () {
            console.log("No se ha podido obtener la información");
        }
    });
}

function editarRadiografia(id)
{
    $.ajax({
        url: 'examenradiografico.do?op=verJson&id=' + id,
        dataType: 'json',
        success: function (data) {
            console.log(data);
            $("#modal_editarRadiografia").modal("show");
            $("#update_id_examen_radiografico").val(data.id_examen_radiografico);
            $("#update_observaciones").val(data.observaciones);
        },
        error: function () {
            console.log("No se ha podido obtener la información");
        }
    });
}

$('#radiografiaFormEdit').validator().on('submit', function (e) {
    if (e.isDefaultPrevented()) {
        console.log("no válido");
    } else {
        e.preventDefault();
        var formData = new FormData($('#radiografiaFormEdit')[0]);
        $.ajax({
            url: "examenradiografico.do?op=editarRadiografia",
            type: "POST",
            data: formData,
            contentType: false,
            processData: false,
            beforeSend: function () {
                $('#btn_update_radiografia').button('loading');
            },
            success: function (data)
            {
                console.log(data);
                $('#radiografiaFormEdit').trigger('reset');
                $('#data_radiografias').DataTable().ajax.reload();
                $("#modal_editarRadiografia").modal("hide");
                $('#radiografiaFormEdit').trigger("reset");
                $.toast({
                    heading: 'Éxito',
                    text: data,
                    position: 'bottom-right',
                    icon: 'success',
                    hideAfter: 4000,
                    stack: 6
                });
            },
            error: function (data)
            {
                console.log(data.responseText);
                $.toast({
                    heading: 'Error',
                    text: data.responseText,
                    position: 'bottom-right',
                    icon: 'error',
                    hideAfter: 4000,
                    stack: 6
                });
            },
            complete: function () {
                $('#btn_update_radiografia').button('loading');
            }
        });
    }
});

function eliminarRadiografia(id)
{
    swal({
        title: "¿Eliminar?",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Yes, eliminar!",
        cancelButtonText: "No, cancelar!",
        closeOnConfirm: false,
        showLoaderOnConfirm: true,
        closeOnCancel: false
    }, function (isConfirm) {
        if (isConfirm) {
            $.ajax({
                url: 'examenradiografico.do?op=eliminarJson&id=' + id,
                success: function (data) {
                    console.log(data);
                    $('#data_radiografias').DataTable().ajax.reload();
                    swal.close();
                    $.toast({
                        heading: 'Éxito',
                        text: 'Radiografía eliminada',
                        position: 'bottom-right',
                        icon: 'success',
                        hideAfter: 4000,
                        stack: 6
                    });
                },
                error: function () {
                    console.log("No se ha podido eliminar");
                    swal.close();
                    $.toast({
                        heading: 'Error',
                        text: 'No se pudo eliminar',
                        position: 'bottom-right',
                        icon: 'error',
                        hideAfter: 4000,
                        stack: 6
                    });
                }
            });
        } else {
            swal("Cancelado", "Intenta de nuevo :)", "error");
        }
    });
}

function crearVideo()
{
    $('#modal_crearVideo').on('hidden.bs.modal', function () {
        $(this).find('videoFormNew').trigger('reset');
    });
    $("#modal_crearVideo").modal("show");
}

$('#videoFormNew').validator().on('submit', function (e) {
    if (e.isDefaultPrevented()) {
        console.log("no válido");
    } else {
        e.preventDefault();
        var formData = new FormData($('#videoFormNew')[0]);
        $.ajax({
            url: "video.do?op=crearJson",
            type: "POST",
            data: formData,
            contentType: false,
            processData: false,
            beforeSend: function () {
                $('#btn_new_video').button('loading');
            },
            success: function (data)
            {
                console.log(data);
                $('#videoFormNew').trigger('reset');
                $('#data_videos').DataTable().ajax.reload();
                $("#modal_crearVideo").modal("hide");
                $('#videoFormNew').trigger("reset");
                $.toast({
                    heading: 'Éxito',
                    text: data,
                    position: 'bottom-right',
                    icon: 'success',
                    hideAfter: 4000,
                    stack: 6
                });
            },
            error: function (data)
            {
                console.log(data.responseText);
                $.toast({
                    heading: 'Error',
                    text: data.responseText,
                    position: 'bottom-right',
                    icon: 'error',
                    hideAfter: 4000,
                    stack: 6
                });
            },
            complete: function () {
                $("#btn_new_video").button('reset');
            }
        });
    }
});

function verVideo(id)
{
    var player = videojs('video-js');
    $('#ver_video').on('hidden.bs.modal', function () {
        player.reset();
    });
    $("#ver_video").modal('show');
    $.ajax({
        url: 'video.do?op=verJson&id=' + id,
        Type: 'GET',
        dataType: 'json',
        success: function (data) {
            console.log(data);
            player.src({type: 'video/mp4', src: data.ruta});
            player.responsive(true);
            player.fluid(true);
            player.language('es');
        },
        error: function () {
            console.log("No se ha podido obtener la información");
        }
    });
}

function editarVideo(id)
{
    $.ajax({
        url: 'video.do?op=verJson&id=' + id,
        dataType: 'json',
        success: function (data) {
            console.log(data);
            $("#modal_editarVideo").modal("show");
            $("#update_id_video").val(data.id_video);
            $("#update_nombre").val(data.nombre);
            $("#update_observaciones_video").val(data.observaciones);
        },
        error: function () {
            console.log("No se ha podido obtener la información");
        }
    });
}

$('#videoFormEdit').validator().on('submit', function (e) {
    if (e.isDefaultPrevented()) {
        console.log("no válido");
    } else {
        e.preventDefault();
        var formData = new FormData($('#videoFormEdit')[0]);
        $.ajax({
            url: "video.do?op=editarJson",
            type: "POST",
            data: formData,
            contentType: false,
            processData: false,
            beforeSend: function () {
                $('#btn_update_video').button('loading');
            },
            success: function (data)
            {
                console.log(data);
                $('#videoFormEdit').trigger('reset');
                $('#data_videos').DataTable().ajax.reload();
                $("#modal_editarVideo").modal("hide");
                $('#videoFormEdit').trigger("reset");
                $.toast({
                    heading: 'Éxito',
                    text: data,
                    position: 'bottom-right',
                    icon: 'success',
                    hideAfter: 4000,
                    stack: 6
                });
            },
            error: function (data)
            {
                console.log(data.responseText);
                $.toast({
                    heading: 'Error',
                    text: data.responseText,
                    position: 'bottom-right',
                    icon: 'error',
                    hideAfter: 4000,
                    stack: 6
                });
            },
            complete: function () {
                $('#btn_update_video').button('loading');
            }
        });
    }
});

function eliminarVideo(id)
{
    swal({
        title: "¿Eliminar?",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Yes, eliminar!",
        cancelButtonText: "No, cancelar!",
        closeOnConfirm: false,
        showLoaderOnConfirm: true,
        closeOnCancel: false
    }, function (isConfirm) {
        if (isConfirm) {
            $.ajax({
                url: 'video.do?op=eliminarJson&id=' + id,
                success: function (data) {
                    console.log(data);
                    $('#data_videos').DataTable().ajax.reload();
                    swal.close();
                    $.toast({
                        heading: 'Éxito',
                        text: 'Video eliminado',
                        position: 'bottom-right',
                        icon: 'success',
                        hideAfter: 4000,
                        stack: 6
                    });
                },
                error: function () {
                    console.log("No se ha podido eliminar");
                    swal.close();
                    $.toast({
                        heading: 'Error',
                        text: 'No se pudo eliminar',
                        position: 'bottom-right',
                        icon: 'error',
                        hideAfter: 4000,
                        stack: 6
                    });
                }
            });
        } else {
            swal("Cancelado", "Intenta de nuevo :)", "error");
        }
    });
}

function crearPresupuesto()
{
    $('#modal_crearPresupuesto').on('hidden.bs.modal', function () {
        $(this).find('presupuestoFormNew').trigger('reset');
    });
    $("#modal_crearPresupuesto").modal("show");
}

$('#modal_editarPresupuesto').on('hidden.bs.modal', function () {
    $(this).find('presupuestoEditNew').trigger('reset');
    $('#data_presupuestos').DataTable().ajax.reload();
});

$('#presupuestoFormNew').validator().on('submit', function (e) {
    if (e.isDefaultPrevented()) {
        console.log("no válido");
    } else {
        e.preventDefault();
        var cells = [];
        var rows = $("#data_servicio").dataTable().fnGetNodes();
        var rows_length = rows.length;
        for (var i = 0; i < rows_length; i++)
        {
            cells.push({
                "pieza": $(rows[i]).find("td:eq(0)").html(),
                "hallazgo": $(rows[i]).find("td:eq(1)").html(),
                "id_tratamiento": $(rows[i]).find("td option:selected").eq(0).val(),
                "cantidad": $(rows[i]).find("td input").eq(0).val(),
                "precio": $(rows[i]).find("td input").eq(1).val(),
                "subtotal": $(rows[i]).find("td input").eq(2).val()
            });
        }
        console.log(cells);

        var id_historia_clinica = $("#presupuesto_id_historia_clinica").val();
        var id_medico = $("#presupuesto_id_medico option:selected").val();
        var asunto = $("#asunto").val();
        var id_odontograma = $("#select2_odontogramas option:selected").val();
        var tipo_pago = $("#tipo_pago option:selected").val();
        var total = $("#total").val();

        $.ajax({
            data: {
                "id_historia_clinica": id_historia_clinica,
                "id_medico": id_medico,
                "asunto": asunto,
                "id_odontograma": id_odontograma,
                "tipo_pago": tipo_pago,
                "total": total,
                "array_detalle_presupuesto": JSON.stringify(cells)
            },
            url: "presupuesto.do?op=crearJson",
            type: "POST",
            success: function (data)
            {
                console.log(data);
                $('#data_presupuestos').DataTable().ajax.reload();
                $("#modal_crearPresupuesto").modal("hide");
                $('#presupuestoFormNew').trigger("reset");
                $('#select2_odontogramas').val('').trigger('change');
                $.toast({
                    heading: 'Éxito',
                    text: data,
                    position: 'bottom-right',
                    icon: 'success',
                    hideAfter: 4000,
                    stack: 6
                });
            },
            error: function (data)
            {
                console.log(data.responseText);
                $.toast({
                    heading: 'Error',
                    text: data.responseText,
                    position: 'bottom-right',
                    icon: 'error',
                    hideAfter: 4000,
                    stack: 6
                });
            }
        });
    }
});

$('#presupuestoFormEdit').validator().on('submit', function (e) {
    if (e.isDefaultPrevented()) {
        console.log("no válido");
    } else {
        e.preventDefault();
        var update_cells = [];
        var rows = $("#update_data_servicio").dataTable().fnGetNodes();
        var rows_length = rows.length;
        for (var i = 0; i < rows_length; i++)
        {
            update_cells.push({
                "pieza": $(rows[i]).find("td:eq(0)").html(),
                "hallazgo": $(rows[i]).find("td:eq(1)").html(),
                "id_tratamiento": $(rows[i]).find("td option:selected").eq(0).val(),
                "cantidad": $(rows[i]).find("td input").eq(0).val(),
                "precio": $(rows[i]).find("td input").eq(1).val(),
                "subtotal": $(rows[i]).find("td input").eq(2).val()
            });
        }
        console.log(update_cells);

        var id_presupuesto = $("#update_id_presupuesto").val();
        var id_medico = $("#update_presupuesto_id_medico option:selected").val();
        var asunto = $("#update_asunto").val();
        var id_odontograma = $("#update_select2_odontogramas option:selected").val();
        var tipo_pago = $("#update_tipo_pago option:selected").val();
        var total = $("#update_total").val();

        $.ajax({
            data: {
                "id_presupuesto": id_presupuesto,
                "id_medico": id_medico,
                "asunto": asunto,
                "id_odontograma": id_odontograma,
                "tipo_pago": tipo_pago,
                "total": total,
                "update_array_detalle_presupuesto": JSON.stringify(update_cells)
            },
            url: "presupuesto.do?op=editarJson",
            type: "POST",
            success: function (data)
            {
                console.log(data);
                $('#data_presupuestos').DataTable().ajax.reload();
                $("#modal_editarPresupuesto").modal("hide");
                $('#presupuestoFormEdit').trigger("reset");
                $('#update_select2_odontogramas').val('').trigger('change');
                $.toast({
                    heading: 'Éxito',
                    text: data,
                    position: 'bottom-right',
                    icon: 'success',
                    hideAfter: 4000,
                    stack: 6
                });
            },
            error: function (data)
            {
                console.log(data.responseText);
                $.toast({
                    heading: 'Error',
                    text: data.responseText,
                    position: 'bottom-right',
                    icon: 'error',
                    hideAfter: 4000,
                    stack: 6
                });
            }
        });
    }
});

function selectOpcion(value, id_presupuesto) {
    if (value == 1) {
        $.toast({
            heading: 'Holi',
            text: 'ESTAMOS TRABAJANDO :)',
            position: 'bottom-right',
            icon: 'error',
            hideAfter: 4000,
            stack: 6
        });
    } else if (value == 2) {
        $.ajax({
            data: {"id": id_presupuesto},
            url: "presupuesto.do?op=anularEstado",
            type: "POST",
            success: function (data)
            {
                console.log(data);
                $('#data_presupuestos').DataTable().ajax.reload();
                $.toast({
                    heading: 'Éxito',
                    text: data,
                    position: 'bottom-right',
                    icon: 'success',
                    hideAfter: 4000,
                    stack: 6
                });
            },
            error: function (data)
            {
                console.log(data.responseText);
                $.toast({
                    heading: 'Error',
                    text: data.responseText,
                    position: 'bottom-right',
                    icon: 'error',
                    hideAfter: 4000,
                    stack: 6
                });
            }
        });
    } else if (value == 3) {
        location.href = "detallepresupuesto.do?op=reporte&id=" + id_presupuesto;
    } else if (value == 4) {
        $.ajax({
            data: {"id": id_presupuesto},
            url: "presupuesto.do?op=activarEstado",
            type: "POST",
            success: function (data)
            {
                console.log(data);
                $('#data_presupuestos').DataTable().ajax.reload();
                $.toast({
                    heading: 'Éxito',
                    text: data,
                    position: 'bottom-right',
                    icon: 'success',
                    hideAfter: 4000,
                    stack: 6
                });
            },
            error: function (data)
            {
                console.log(data.responseText);
                $.toast({
                    heading: 'Error',
                    text: data.responseText,
                    position: 'bottom-right',
                    icon: 'error',
                    hideAfter: 4000,
                    stack: 6
                });
            }
        });
    } else if (value == 5) {

        $('#update_data_servicio tbody').on('click', 'tr', function () {
            update_fila_servicio = $('#update_data_servicio').DataTable().row(this).index();
        });

        $("#update_data_servicio").on("keyup", "input[type='text']", function () {
            update_tabla_servicio.draw();
        });

        $.ajax({
            url: 'presupuesto.do?op=obtenerJson&id=' + id_presupuesto,
            dataType: 'json',
            success: function (data) {
                console.log(data);
                $("#modal_editarPresupuesto").modal("show");
                $("#update_id_presupuesto").val(data.id_presupuesto);
                $("#update_presupuesto_id_medico").val(data.medico.idMedico);
                $("#update_asunto").val(data.asunto);
                $("#update_select2_odontogramas").val(data.odontograma.codigoOdontograma);
                $("#update_tipo_pago").val(data.tipo_pago);
                $("#update_total").val(data.total);
            },
            error: function () {
                console.log("No se ha podido obtener la información");
            }
        });

        var data_precio = [];

        $.ajax({
            url: 'tratamiento.do?op=listar_json',
            Type: 'GET',
            cache: false,
            dataType: 'json',
            success: function (data) {

                for (var item in data) {
                    data_precio.push({"id": data[item].idTratamiento, "nombre": data[item].nombreTratamiento, "precio": data[item].precioTratamiento});
                }
            },
            error: function () {
                console.log("No se ha podido obtener la información");
            }
        });

        $("#update_data_servicio").DataTable();
        $("#update_data_servicio").DataTable().destroy();
        update_tabla_servicio = $("#update_data_servicio").DataTable({
            "processing": true,
            "searching": false,
            "lengthChange": false,
            "language": {
                "url": "//cdn.datatables.net/plug-ins/1.10.24/i18n/Spanish.json"
            },
            "ajax": {
                "url": "detallepresupuesto.do?op=obtenerJson&id=" + id_presupuesto,
                "dataSrc": ""
            },
            "columns": [
                {"data": "pieza"},
                {"data": "hallazgo"},
                {
                    "data": null,
                    "render": function (data) {
                        var sel = '<select required class="update_select2_tratamiento" name="tratamiento" style="width: 100%">';

                        for (var i = 0; i < data_precio.length; i++) {
                            if (data.tratamiento.idTratamiento == data_precio[i].id) {
                                sel += "<option  value = '" + data_precio[i].id + "' selected>" + data_precio[i].nombre + "</option>";
                            } else {
                                sel += "<option  value = '" + data_precio[i].id + "' >" + data_precio[i].nombre + "</option>";
                            }
                        }

                        sel += "</select>";

                        return sel;
                    },
                    "orderable": false
                },
                {
                    "data": null,
                    "className": "dt-center",
                    "render": function (data) {
                        return  '<input name="cantidad" size="4" class="form-control form-control-width" type="text" value="' + data.cantidad + '" required="true"/>';
                    },
                    "orderable": false
                },
                {
                    "data": null,
                    "render": function (data) {
                        return  '<input name="precio" size="6" class="form-control form-control-width" type="text" value="' + data.precio + '" required="true"/>';
                    },
                    "orderable": false
                },
                {
                    "data": null,
                    "render": function (data) {
                        return  '<input name="sumatotal" size="6" class="form-control form-control-width" type="text" value="' + data.subtotal + '" readonly="true" required="true"/>';
                    },
                    "orderable": false
                },
                {
                    "data": null,
                    "className": "dt-center",
                    "render": function (data) {
                        return '<a id="eliminarServicio" href="javascript:void(0);" data-toggle="tooltip" data-original-title="Eliminar"><i class="ti-trash text-danger"></i></a>';
                    },
                    "orderable": false
                }
            ],
            //"order": [[0, "desc"]],
            "drawCallback": function (settings) {
                var api = this.api();
                var rows = api.rows().nodes();
                var suma_total = 0;

                for (var i = 0; i < rows.length; i++)
                {
                    suma_total += parseFloat(api.cell(i, 5).nodes().to$().find('input').val());
                }
                $("#update_total").val(suma_total.toFixed(2));

                $('.update_select2_tratamiento').select2({
                    minimumInputLength: 1
                });

                $('.update_select2_tratamiento').on('select2:select select2:unselecting', function () {
                    var a = parseFloat(api.cell(update_fila_servicio, 3).nodes().to$().find('input').val());
                    api.cell(update_fila_servicio, 4).nodes().to$().find('input').val(data_precio[(this.value - 1)].precio);
                    var b = parseFloat(api.cell(update_fila_servicio, 4).nodes().to$().find('input').val());
                    api.cell(update_fila_servicio, 5).nodes().to$().find('input').val(a * b);
                });

                $('input').not('#update_asunto').keyup(function () {
                    var a = parseFloat(api.cell(update_fila_servicio, 3).nodes().to$().find('input').val());
                    var b = parseFloat(api.cell(update_fila_servicio, 4).nodes().to$().find('input').val());
                    api.cell(update_fila_servicio, 5).nodes().to$().find('input').val(a * b);
                });
            }
        });


//                    $.ajax({
//                        data: {"id": id_presupuesto},
//                        url: "presupuesto.do?op=editarJson",
//                        type: "POST",
//                        success: function (data)
//                        {
//                            console.log(data);
//                            $('#data_presupuestos').DataTable().ajax.reload();
//                            $.toast({
//                                heading: 'Éxito',
//                                text: data,
//                                position: 'bottom-right',
//                                icon: 'success',
//                                hideAfter: 4000,
//                                stack: 6
//                            });
//                        },
//                        error: function (data)
//                        {
//                            console.log(data.responseText);
//                            $.toast({
//                                heading: 'Error',
//                                text: data.responseText,
//                                position: 'bottom-right',
//                                icon: 'error',
//                                hideAfter: 4000,
//                                stack: 6
//                            });
//                        }
//                    });
    }
    return false;
}