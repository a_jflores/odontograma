<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="java.util.*"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="es" ng-app="app">
    <head>
        <jsp:include page="/templates/head.jsp" flush="true" />
        <link href="${pageContext.request.contextPath}/css/estilosOdontograma.css" rel="stylesheet" type="text/css"/>
        <script src="${pageContext.request.contextPath}/scripts/angular.js"></script>
        <script type="text/javascript" src="${pageContext.request.contextPath}/scripts/modulos/app.js"></script>
        <script type="text/javascript" src="${pageContext.request.contextPath}/scripts/directivas/opcionescanvas.js"></script>
    </head>
    <body class="fix-header">
        <div class="preloader">
            <svg class="circular" viewBox="25 25 50 50">
            <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10" />
            </svg>
        </div>
        <div id="wrapper">
            <jsp:include page="/templates/header.jsp" flush="true" /> 
            <jsp:include page="/templates/navbar.jsp" flush="true" />
            <div id="page-wrapper">
                <div class="container-fluid">
                    <div class="row bg-title">
                        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                            <h4 class="page-title">Odontograma</h4>
                            <input type="hidden" id="id_odontograma" value="${odontograma.codigoOdontograma}"/>
                            <input type="hidden" id="id_historia_clinica" value="${odontograma.historiaClinica.codigoHistoriaClinica}"/>
                            <input type="hidden" id="id_estado_anterior" value="${anterior}"/>
                            <input type="hidden" id="conteo_diente_detalle" value="${conteoDienteDetalle}"/>
                            <input type="hidden" id="observaciones" value="${odontograma.observaciones}"/>
                        </div>
                        <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                            <button class="right-side-toggle waves-effect waves-light btn-info btn-circle pull-right m-l-20"><i class="ti-settings text-white"></i></button>
                            <ol class="breadcrumb">
                                <li><a href="${pageContext.request.contextPath}/index.jsp">Dashboard</a></li>
                                <li class="active">Odontograma</li>
                            </ol>
                        </div>
                    </div>
                    <button id="pintar" style="display:none">Pintar</button>
                    <button id="test" style="display:none">Test</button>
                    <div id="lista-usuarios"></div>
                    <ul style="display:none;">
                        <c:forEach items="${requestScope.listaDienteDetalle}" var="detalle">
                            <li>${detalle.id_detalleDiente}</li>
                            </c:forEach>
                    </ul>
                    <div>
                        <odontogramageneral></odontogramageneral>
                    </div>
                    <jsp:include page="/templates/right-sidebar.jsp" flush="true"/>
                </div>
                <jsp:include page="/templates/footer.jsp" flush="true"/>
            </div>
        </div>
        <jsp:include page="/templates/scripts.jsp" flush="true"/>
        <!-- Angular Controladores-->
        <script type="text/javascript" src="${pageContext.request.contextPath}/scripts/controladores/controller.js"></script>
        <!--Angular Directives-->
        <script type="text/javascript" src="${pageContext.request.contextPath}/scripts/directivas/canvasodontograma.js"></script>

        <script type="text/javascript" src="${pageContext.request.contextPath}/scripts/directivas/odontogramaGeneral.js"></script>
        <script type="text/javascript" src="${pageContext.request.contextPath}/scripts/directivas/odontogramaUp.js"></script>
        <script type="text/javascript" src="${pageContext.request.contextPath}/scripts/directivas/odontogramaDown.js"></script>
        <script type="text/javascript" src="${pageContext.request.contextPath}/scripts/directivas/placaUp.js"></script>
        <script type="text/javascript" src="${pageContext.request.contextPath}/scripts/directivas/placaDown.js"></script>

        <script type="text/javascript" src="${pageContext.request.contextPath}/scripts/directivas/diente-18.js"></script>
        <script type="text/javascript" src="${pageContext.request.contextPath}/scripts/directivas/diente-17.js"></script>
        <script type="text/javascript" src="${pageContext.request.contextPath}/scripts/directivas/diente-16.js"></script>
        <script type="text/javascript" src="${pageContext.request.contextPath}/scripts/directivas/diente-15.js"></script>
        <script type="text/javascript" src="${pageContext.request.contextPath}/scripts/directivas/diente-14.js"></script>
        <script type="text/javascript" src="${pageContext.request.contextPath}/scripts/directivas/diente-13.js"></script>
        <script type="text/javascript" src="${pageContext.request.contextPath}/scripts/directivas/diente-12.js"></script>
        <script type="text/javascript" src="${pageContext.request.contextPath}/scripts/directivas/diente-11.js"></script>
        <script type="text/javascript" src="${pageContext.request.contextPath}/scripts/directivas/diente-21.js"></script>
        <script type="text/javascript" src="${pageContext.request.contextPath}/scripts/directivas/diente-22.js"></script>
        <script type="text/javascript" src="${pageContext.request.contextPath}/scripts/directivas/diente-23.js"></script>
        <script type="text/javascript" src="${pageContext.request.contextPath}/scripts/directivas/diente-24.js"></script>
        <script type="text/javascript" src="${pageContext.request.contextPath}/scripts/directivas/diente-25.js"></script>
        <script type="text/javascript" src="${pageContext.request.contextPath}/scripts/directivas/diente-26.js"></script>
        <script type="text/javascript" src="${pageContext.request.contextPath}/scripts/directivas/diente-27.js"></script>
        <script type="text/javascript" src="${pageContext.request.contextPath}/scripts/directivas/diente-28.js"></script>

        <script type="text/javascript" src="${pageContext.request.contextPath}/scripts/directivas/diente-48.js"></script>
        <script type="text/javascript" src="${pageContext.request.contextPath}/scripts/directivas/diente-47.js"></script>
        <script type="text/javascript" src="${pageContext.request.contextPath}/scripts/directivas/diente-46.js"></script>
        <script type="text/javascript" src="${pageContext.request.contextPath}/scripts/directivas/diente-45.js"></script>
        <script type="text/javascript" src="${pageContext.request.contextPath}/scripts/directivas/diente-44.js"></script>
        <script type="text/javascript" src="${pageContext.request.contextPath}/scripts/directivas/diente-43.js"></script>
        <script type="text/javascript" src="${pageContext.request.contextPath}/scripts/directivas/diente-42.js"></script>
        <script type="text/javascript" src="${pageContext.request.contextPath}/scripts/directivas/diente-41.js"></script>
        <script type="text/javascript" src="${pageContext.request.contextPath}/scripts/directivas/diente-31.js"></script>
        <script type="text/javascript" src="${pageContext.request.contextPath}/scripts/directivas/diente-32.js"></script>
        <script type="text/javascript" src="${pageContext.request.contextPath}/scripts/directivas/diente-33.js"></script>
        <script type="text/javascript" src="${pageContext.request.contextPath}/scripts/directivas/diente-34.js"></script>
        <script type="text/javascript" src="${pageContext.request.contextPath}/scripts/directivas/diente-35.js"></script>
        <script type="text/javascript" src="${pageContext.request.contextPath}/scripts/directivas/diente-36.js"></script>
        <script type="text/javascript" src="${pageContext.request.contextPath}/scripts/directivas/diente-37.js"></script>
        <script type="text/javascript" src="${pageContext.request.contextPath}/scripts/directivas/diente-38.js"></script>
        
        <script type="text/javascript" src="${pageContext.request.contextPath}/scripts/directivas/diente-55.js"></script>
        <script type="text/javascript" src="${pageContext.request.contextPath}/scripts/directivas/diente-54.js"></script>
        <script type="text/javascript" src="${pageContext.request.contextPath}/scripts/directivas/diente-53.js"></script>
        <script type="text/javascript" src="${pageContext.request.contextPath}/scripts/directivas/diente-52.js"></script>
        <script type="text/javascript" src="${pageContext.request.contextPath}/scripts/directivas/diente-51.js"></script>
        <script type="text/javascript" src="${pageContext.request.contextPath}/scripts/directivas/diente-61.js"></script>
        <script type="text/javascript" src="${pageContext.request.contextPath}/scripts/directivas/diente-62.js"></script>
        <script type="text/javascript" src="${pageContext.request.contextPath}/scripts/directivas/diente-63.js"></script>
        <script type="text/javascript" src="${pageContext.request.contextPath}/scripts/directivas/diente-64.js"></script>
        <script type="text/javascript" src="${pageContext.request.contextPath}/scripts/directivas/diente-65.js"></script>

        <script type="text/javascript" src="${pageContext.request.contextPath}/scripts/directivas/diente-85.js"></script>
        <script type="text/javascript" src="${pageContext.request.contextPath}/scripts/directivas/diente-84.js"></script>
        <script type="text/javascript" src="${pageContext.request.contextPath}/scripts/directivas/diente-83.js"></script>
        <script type="text/javascript" src="${pageContext.request.contextPath}/scripts/directivas/diente-82.js"></script>
        <script type="text/javascript" src="${pageContext.request.contextPath}/scripts/directivas/diente-81.js"></script>
        <script type="text/javascript" src="${pageContext.request.contextPath}/scripts/directivas/diente-71.js"></script>
        <script type="text/javascript" src="${pageContext.request.contextPath}/scripts/directivas/diente-72.js"></script>
        <script type="text/javascript" src="${pageContext.request.contextPath}/scripts/directivas/diente-73.js"></script>
        <script type="text/javascript" src="${pageContext.request.contextPath}/scripts/directivas/diente-74.js"></script>
        <script type="text/javascript" src="${pageContext.request.contextPath}/scripts/directivas/diente-75.js"></script>
    </body>
</html>

<script>
    $(document).ready(function () {

        var data_pintar = [];
        var data_ortodontico_fijo = [];
        var data_ortodontico_fijo_rojo = [];
        var data_ortodontico_remo_azul = [];
        var data_ortodontico_remo_rojo = [];
        var data_protesis_remo_azul = [];
        var data_protesis_remo_rojo = [];
        var data_edentulo_total = [];
        var data_protesis_total_azul = [];
        var data_protesis_total_rojo = [];
        var data_puente_total_azul = [];
        var data_puente_total_rojo = [];
        var id_odontograma = document.getElementById("id_odontograma").value;
        var conteo_diente_detalle = document.getElementById("conteo_diente_detalle").value;
        var saltarEachCirculo = null;

        $.ajax({
            url: 'historias.do?op=obtenerDienteDetalle&id=' + id_odontograma,
            dataType: 'json',
            success: function (respuesta) {
                for (var key in respuesta) {
                    data_pintar.push({
                        cara: respuesta[key].id_cara,
                        diente: respuesta[key].id_diente,
                        estado: respuesta[key].id_hallazgo,
                        pareja: respuesta[key].pareja
                    });
                }
                respuesta.sort(function (a, b) {
                    return a.pareja - b.pareja;
                });
                var j = 0;
                for (var i in respuesta) {
                    if (respuesta[i].id_hallazgo == 37) {
                        data_ortodontico_fijo.push({
                            pareja: respuesta[i].pareja,
                            id: parseInt(j),
                            diente: respuesta[i].id_diente,
                            estado: respuesta[i].id_hallazgo
                        });
                        j++;
                    }
                }
                var j = 0;
                for (var i in respuesta) {
                    if (respuesta[i].id_hallazgo == 38) {
                        data_ortodontico_fijo_rojo.push({
                            pareja: respuesta[i].pareja,
                            id: parseInt(j),
                            diente: respuesta[i].id_diente,
                            estado: respuesta[i].id_hallazgo
                        });
                        j++;
                    }
                }
                var j = 0;
                for (var i in respuesta) {
                    if (respuesta[i].id_hallazgo == 39) {
                        data_ortodontico_remo_azul.push({
                            pareja: respuesta[i].pareja,
                            id: parseInt(j),
                            diente: respuesta[i].id_diente,
                            estado: respuesta[i].id_hallazgo
                        });
                        j++;
                    }
                }
                var j = 0;
                for (var i in respuesta) {
                    if (respuesta[i].id_hallazgo == 40) {
                        data_ortodontico_remo_rojo.push({
                            pareja: respuesta[i].pareja,
                            id: parseInt(j),
                            diente: respuesta[i].id_diente,
                            estado: respuesta[i].id_hallazgo
                        });
                        j++;
                    }
                }
                var j = 0;
                for (var i in respuesta) {
                    if (respuesta[i].id_hallazgo == 47) {
                        data_protesis_remo_azul.push({
                            pareja: respuesta[i].pareja,
                            id: parseInt(j),
                            diente: respuesta[i].id_diente,
                            estado: respuesta[i].id_hallazgo
                        });
                        j++;
                    }
                }
                var j = 0;
                for (var i in respuesta) {
                    if (respuesta[i].id_hallazgo == 48) {
                        data_protesis_remo_rojo.push({
                            pareja: respuesta[i].pareja,
                            id: parseInt(j),
                            diente: respuesta[i].id_diente,
                            estado: respuesta[i].id_hallazgo
                        });
                        j++;
                    }
                }
                var j = 0;
                for (var i in respuesta) {
                    if (respuesta[i].id_hallazgo == 55) {
                        data_edentulo_total.push({
                            pareja: respuesta[i].pareja,
                            id: parseInt(j),
                            diente: respuesta[i].id_diente,
                            estado: respuesta[i].id_hallazgo
                        });
                        j++;
                    }
                }
                var j = 0;
                for (var i in respuesta) {
                    if (respuesta[i].id_hallazgo == 56) {
                        data_protesis_total_azul.push({
                            pareja: respuesta[i].pareja,
                            id: parseInt(j),
                            diente: respuesta[i].id_diente,
                            estado: respuesta[i].id_hallazgo
                        });
                        j++;
                    }
                }
                var j = 0;
                for (var i in respuesta) {
                    if (respuesta[i].id_hallazgo == 57) {
                        data_protesis_total_rojo.push({
                            pareja: respuesta[i].pareja,
                            id: parseInt(j),
                            diente: respuesta[i].id_diente,
                            estado: respuesta[i].id_hallazgo
                        });
                        j++;
                    }
                }
                var j = 0;
                for (var i in respuesta) {
                    if (respuesta[i].id_hallazgo == 58) {
                        data_puente_total_azul.push({
                            pareja: respuesta[i].pareja,
                            id: parseInt(j),
                            diente: respuesta[i].id_diente,
                            estado: respuesta[i].id_hallazgo
                        });
                        j++;
                    }
                }
                var j = 0;
                for (var i in respuesta) {
                    if (respuesta[i].id_hallazgo == 59) {
                        data_puente_total_rojo.push({
                            pareja: respuesta[i].pareja,
                            id: parseInt(j),
                            diente: respuesta[i].id_diente,
                            estado: respuesta[i].id_hallazgo
                        });
                        j++;
                    }
                }
            },
            error: function () {
                console.log("No se ha podido obtener la información");
            }
        });

        $("#pintar").on("click", getDienteDetalle);
        function getDienteDetalle() {
            var countMarcado = document.getElementsByClassName('marcado');

            for (var key = 0; key < conteo_diente_detalle; key++) {
                saltarEachCirculo = false;
                if (data_pintar[key].estado == 13 || data_pintar[key].estado == 14 || data_pintar[key].estado == 15) {
                    $('#diente' + data_pintar[key].diente + ' path').each(function () {
                        if (saltarEachCirculo == false) {
                            pintar(data_pintar[key].estado, this);
                            saltarEachCirculo = true;
                        }
                    });
                } else if (data_pintar[key].estado == 16 || data_pintar[key].estado == 17 || data_pintar[key].estado == 18) {
                    $('#diente' + data_pintar[key].diente + ' path').each(function () {
                        if (saltarEachCirculo == false) {
                            pintar(data_pintar[key].estado, this);
                            saltarEachCirculo = true;
                        }
                    });
                } else if (data_pintar[key].estado == 19 || data_pintar[key].estado == 20 || data_pintar[key].estado == 21) {
                    $('#diente' + data_pintar[key].diente + ' line').each(function () {
                        if (saltarEachCirculo == false) {
                            pintar(data_pintar[key].estado, this);
                            saltarEachCirculo = true;
                        }
                    });
                } else if (data_pintar[key].estado == 22) {
                    $('#diente' + data_pintar[key].diente + ' text').each(function () {
                        if (saltarEachCirculo == false) {
                            pintar(data_pintar[key].estado, this);
                            saltarEachCirculo = true;
                        }
                    });
                } else if (data_pintar[key].estado == 23 || data_pintar[key].estado == 24 || data_pintar[key].estado == 25 || data_pintar[key].estado == 26 || data_pintar[key].estado == 27) {
                    $('#' + data_pintar[key].diente + ' path').each(function () {
                        if (saltarEachCirculo == false) {
                            pintar(data_pintar[key].estado, this);
                            saltarEachCirculo = true;
                        }
                    });
                } else if (data_pintar[key].estado == 28) {
                    $('#' + data_pintar[key].diente + ' path').each(function () {
                        if (saltarEachCirculo == false) {
                            pintar(data_pintar[key].estado, this);
                            saltarEachCirculo = true;
                        }
                    });
                } else if (data_pintar[key].estado == 29) {
                    $('#' + data_pintar[key].diente + ' path').each(function () {
                        if (saltarEachCirculo == false) {
                            for (var i = 1; i <= 15; i++) {
                                if (data_pintar[key].diente == i) {
                                    $('svg[id=' + i + ']').find('.diastema2').attr({class: 'marcadoAzul marcado', estado: data_pintar[key].estado});
                                    $('svg[id=' + (i + 1) + ']').find('.diastema1').attr({class: 'marcadoAzul', estado: data_pintar[key].estado});
                                }
                            }
                            for (var i = 17; i <= 31; i++) {
                                if (data_pintar[key].diente == i) {
                                    $('svg[id=' + i + ']').find('.diastema2').attr({class: 'marcadoAzul marcado', estado: data_pintar[key].estado});
                                    $('svg[id=' + (i + 1) + ']').find('.diastema1').attr({class: 'marcadoAzul', estado: data_pintar[key].estado});
                                }
                            }
                            for (var i = 33; i <= 41; i++) {
                                if (data_pintar[key].diente == i) {
                                    $('svg[id=' + i + ']').find('.diastema2').attr({class: 'marcadoAzul marcado', estado: data_pintar[key].estado});
                                    $('svg[id=' + (i + 1) + ']').find('.diastema1').attr({class: 'marcadoAzul', estado: data_pintar[key].estado});
                                }
                            }
                            for (var i = 43; i <= 51; i++) {
                                if (data_pintar[key].diente == i) {
                                    $('svg[id=' + i + ']').find('.diastema2').attr({class: 'marcadoAzul marcado', estado: data_pintar[key].estado});
                                    $('svg[id=' + (i + 1) + ']').find('.diastema1').attr({class: 'marcadoAzul', estado: data_pintar[key].estado});
                                }
                            }
                            saltarEachCirculo = true;
                        }

                    });
                } else if (data_pintar[key].estado == 30 || data_pintar[key].estado == 31) {
                    $('#diente' + data_pintar[key].diente + ' path').each(function () {
                        if (saltarEachCirculo == false) {
                            pintar(data_pintar[key].estado, this);
                            saltarEachCirculo = true;
                        }
                    });
                } else if (data_pintar[key].estado == 32) {
                    $('#up' + data_pintar[key].diente + ' ellipse').each(function () {
                        if (saltarEachCirculo == false) {
                            for (var i = 1; i <= 52; i++) {
                                if (data_pintar[key].diente == i) {
                                    $('svg[id=up' + i + ']').find('.fusion1').attr({class: 'fusion1Azul marcado', estado: data_pintar[key].estado});
                                    $('svg[id=up' + i + ']').find('.fusion2').attr({class: 'fusion2Azul', estado: data_pintar[key].estado});
                                    $('svg[id=up' + (i + 1) + ']').find('.fusion3').attr({class: 'fusion3Azul', estado: data_pintar[key].estado});
                                    $('svg[id=up' + (i + 1) + ']').find('.fusion4').attr({class: 'fusion4Azul', estado: data_pintar[key].estado});
                                }
                            }
                            saltarEachCirculo = true;
                        }

                    });
                } else if (data_pintar[key].estado == 33) {
                    $('#up' + data_pintar[key].diente + ' circle').each(function () {
                        if (saltarEachCirculo == false) {
                            for (var i = 1; i <= 52; i++) {
                                if (data_pintar[key].diente == i) {
                                    $('svg[id=up' + i + ']').find('.geminacion').attr({class: 'geminacionAzul marcado', estado: data_pintar[key].estado});
                                }
                            }
                            saltarEachCirculo = true;
                        }
                    });
                } else if (data_pintar[key].estado == 34) {
                    $('#up' + data_pintar[key].diente + ' circle').each(function () {
                        if (saltarEachCirculo == false) {
                            for (var i = 1; i <= 52; i++) {
                                if (data_pintar[key].diente == i) {
                                    $('svg[id=up' + i + ']').find('.clavija').attr({class: 'clavijaAzul marcado', estado: data_pintar[key].estado});
                                }
                            }
                            saltarEachCirculo = true;
                        }
                    });
                } else if (data_pintar[key].estado == 35) {
                    $('#up' + data_pintar[key].diente + ' circle').each(function () {
                        if (saltarEachCirculo == false) {
                            for (var i = 1; i <= 52; i++) {
                                if (data_pintar[key].diente == i) {
                                    $('svg[id=up' + i + ']').find('.supernumerario2').attr({class: 'supernumerario2Azul marcado', estado: data_pintar[key].estado});
                                    $('svg[id=up' + i + ']').find('.supernumerario3').attr({class: 'supernumerario3Azul', estado: data_pintar[key].estado});
                                    $('svg[id=up' + (i + 1) + ']').find('.supernumerario1').attr({class: 'supernumerario1Azul', estado: data_pintar[key].estado});
                                    $('svg[id=up' + (i + 1) + ']').find('.supernumerario4').attr({class: 'supernumerario4Azul', estado: data_pintar[key].estado});
                                }
                            }
                            saltarEachCirculo = true;
                        }
                    });
                } else if (data_pintar[key].estado == 36) {
                    $('#up' + data_pintar[key].diente + ' path').each(function () {
                        if (saltarEachCirculo == false) {
                            for (var i = 1; i <= 52; i++) {
                                if (data_pintar[key].diente == i) {
                                    $('svg[id=up' + i + ']').find('.transposicion1').attr({class: 'transposicion1Azul marcado', estado: data_pintar[key].estado});
                                    $('svg[id=up' + i + ']').find('.transposicion2').attr({class: 'transposicion2Azul', estado: data_pintar[key].estado});
                                    $('svg[id=up' + (i + 1) + ']').find('.transposicion3').attr({class: 'transposicion3Azul', estado: data_pintar[key].estado});
                                    $('svg[id=up' + (i + 1) + ']').find('.transposicion4').attr({class: 'transposicion4Azul', estado: data_pintar[key].estado});
                                }
                            }
                            saltarEachCirculo = true;
                        }
                    });
                } else if (data_pintar[key].estado == 37) {
                    $('#up' + data_pintar[key].diente + ' path').each(function () {
                        if (saltarEachCirculo == false) {
                            for (var i = 0; i < data_ortodontico_fijo.length; i++) {
                                if (i % 2 == 0) {
                                    $('svg[id=up' + data_ortodontico_fijo[i].diente + ']').find('.ortodontico_fijo2').attr({class: 'ortodontico_fijo2Azul marcado', estado: data_pintar[key].estado});
                                } else {
                                    $('svg[id=up' + data_ortodontico_fijo[i].diente + ']').find('.ortodontico_fijo1').attr({class: 'ortodontico_fijo1Azul marcado', estado: data_pintar[key].estado});
                                }
                            }
                            for (var j = 0; j < (data_ortodontico_fijo.length); j = j + 2) {
                                for (var i = (data_ortodontico_fijo[j].diente); i < (data_ortodontico_fijo[j + 1].diente - 1); i++) {
                                    $('svg[id=up' + (i + 1) + ']').find('.ortodontico_fijo3').attr({class: 'ortodontico_fijo3Azul', estado: data_pintar[key].estado});
                                }
                            }
                            saltarEachCirculo = true;
                        }
                    });
                } else if (data_pintar[key].estado == 38) {
                    $('#up' + data_pintar[key].diente + ' path').each(function () {
                        if (saltarEachCirculo == false) {
                            for (var i = 0; i < data_ortodontico_fijo_rojo.length; i++) {
                                if (i % 2 == 0) {
                                    $('svg[id=up' + data_ortodontico_fijo_rojo[i].diente + ']').find('.ortodontico_fijo2').attr({class: 'ortodontico_fijo2Rojo marcado', estado: data_pintar[key].estado});
                                } else {
                                    $('svg[id=up' + data_ortodontico_fijo_rojo[i].diente + ']').find('.ortodontico_fijo1').attr({class: 'ortodontico_fijo1Rojo marcado', estado: data_pintar[key].estado});
                                }
                            }
                            for (var j = 0; j < (data_ortodontico_fijo_rojo.length); j = j + 2) {
                                for (var i = (data_ortodontico_fijo_rojo[j].diente); i < (data_ortodontico_fijo_rojo[j + 1].diente - 1); i++) {
                                    $('svg[id=up' + (i + 1) + ']').find('.ortodontico_fijo3').attr({class: 'ortodontico_fijo3Rojo', estado: data_pintar[key].estado});
                                }
                            }
                            saltarEachCirculo = true;
                        }
                    });
                } else if (data_pintar[key].estado == 39) {
                    $('#up' + data_pintar[key].diente + ' path').each(function () {
                        if (saltarEachCirculo == false) {
                            for (var i = 0; i < data_ortodontico_remo_azul.length; i++) {
                                if (i % 2 == 0) {
                                    $('svg[id=up' + data_ortodontico_remo_azul[i].diente + ']').find('.ortodontico_remo').attr({class: 'ortodonticoAzul marcado', estado: data_pintar[key].estado});
                                } else {
                                    $('svg[id=up' + data_ortodontico_remo_azul[i].diente + ']').find('.ortodontico_remo').attr({class: 'ortodonticoAzul marcado', estado: data_pintar[key].estado});
                                }
                            }
                            for (var j = 0; j < (data_ortodontico_remo_azul.length); j = j + 2) {
                                for (var i = (data_ortodontico_remo_azul[j].diente); i < (data_ortodontico_remo_azul[j + 1].diente - 1); i++) {
                                    $('svg[id=up' + (i + 1) + ']').find('.ortodontico_remo').attr({class: 'ortodonticoAzul', estado: data_pintar[key].estado});
                                }
                            }
                            saltarEachCirculo = true;
                        }
                    });
                } else if (data_pintar[key].estado == 40) {
                    $('#up' + data_pintar[key].diente + ' path').each(function () {
                        if (saltarEachCirculo == false) {
                            for (var i = 0; i < data_ortodontico_remo_rojo.length; i++) {
                                if (i % 2 == 0) {
                                    $('svg[id=up' + data_ortodontico_remo_rojo[i].diente + ']').find('.ortodontico_remo').attr({class: 'ortodonticoRojo marcado', estado: data_pintar[key].estado});
                                } else {
                                    $('svg[id=up' + data_ortodontico_remo_rojo[i].diente + ']').find('.ortodontico_remo').attr({class: 'ortodonticoRojo marcado', estado: data_pintar[key].estado});
                                }
                            }
                            for (var j = 0; j < (data_ortodontico_remo_rojo.length); j = j + 2) {
                                for (var i = (data_ortodontico_remo_rojo[j].diente); i < (data_ortodontico_remo_rojo[j + 1].diente - 1); i++) {
                                    $('svg[id=up' + (i + 1) + ']').find('.ortodontico_remo').attr({class: 'ortodonticoRojo', estado: data_pintar[key].estado});
                                }
                            }
                            saltarEachCirculo = true;
                        }
                    });
                } else if (data_pintar[key].estado == 41) {
                    $('#up' + data_pintar[key].diente + ' path').each(function () {
                        if (saltarEachCirculo == false) {
                            $('svg[id=up' + data_pintar[key].diente + ']').find('.puente_1').attr({class: 'puente_1Azul marcado', estado: data_pintar[key].estado});
                            saltarEachCirculo = true;
                        }
                    });
                } else if (data_pintar[key].estado == 42) {
                    $('#up' + data_pintar[key].diente + ' path').each(function () {
                        if (saltarEachCirculo == false) {
                            $('svg[id=up' + data_pintar[key].diente + ']').find('.puente_2').attr({class: 'puente_2Azul marcado', estado: data_pintar[key].estado});
                            saltarEachCirculo = true;
                        }
                    });
                } else if (data_pintar[key].estado == 43) {
                    $('#up' + data_pintar[key].diente + ' path').each(function () {
                        if (saltarEachCirculo == false) {
                            $('svg[id=up' + data_pintar[key].diente + ']').find('.pilar').attr({class: 'pilarAzul marcado', estado: data_pintar[key].estado});
                            saltarEachCirculo = true;
                        }
                    });
                } else if (data_pintar[key].estado == 44) {
                    $('#up' + data_pintar[key].diente + ' path').each(function () {
                        if (saltarEachCirculo == false) {
                            $('svg[id=up' + data_pintar[key].diente + ']').find('.puente_1').attr({class: 'puente_1Rojo marcado', estado: data_pintar[key].estado});
                            saltarEachCirculo = true;
                        }
                    });
                } else if (data_pintar[key].estado == 45) {
                    $('#up' + data_pintar[key].diente + ' path').each(function () {
                        if (saltarEachCirculo == false) {
                            $('svg[id=up' + data_pintar[key].diente + ']').find('.puente_2').attr({class: 'puente_2Rojo marcado', estado: data_pintar[key].estado});
                            saltarEachCirculo = true;
                        }
                    });
                } else if (data_pintar[key].estado == 46) {
                    $('#up' + data_pintar[key].diente + ' path').each(function () {
                        if (saltarEachCirculo == false) {
                            $('svg[id=up' + data_pintar[key].diente + ']').find('.pilar').attr({class: 'pilarRojo marcado', estado: data_pintar[key].estado});
                            saltarEachCirculo = true;
                        }
                    });
                } else if (data_pintar[key].estado == 47) {
                    $('#up' + data_pintar[key].diente + ' path').each(function () {
                        if (saltarEachCirculo == false) {
                            for (var i = 0; i < data_protesis_remo_azul.length; i++) {
                                if (i % 2 == 0) {
                                    $('svg[id=up' + data_protesis_remo_azul[i].diente + ']').find('.protesis_remo').attr({class: 'protesisRemovibleAzul marcado', estado: data_pintar[key].estado});
                                } else {
                                    $('svg[id=up' + data_protesis_remo_azul[i].diente + ']').find('.protesis_remo').attr({class: 'protesisRemovibleAzul marcado', estado: data_pintar[key].estado});
                                }
                            }
                            for (var j = 0; j < (data_protesis_remo_azul.length); j = j + 2) {
                                for (var i = (data_protesis_remo_azul[j].diente); i < (data_protesis_remo_azul[j + 1].diente - 1); i++) {
                                    $('svg[id=up' + (i + 1) + ']').find('.protesis_remo').attr({class: 'protesisRemovibleAzul', estado: data_pintar[key].estado});
                                }
                            }
                            saltarEachCirculo = true;
                        }
                    });
                } else if (data_pintar[key].estado == 48) {
                    $('#up' + data_pintar[key].diente + ' path').each(function () {
                        if (saltarEachCirculo == false) {
                            for (var i = 0; i < data_protesis_remo_rojo.length; i++) {
                                if (i % 2 == 0) {
                                    $('svg[id=up' + data_protesis_remo_rojo[i].diente + ']').find('.protesis_remo').attr({class: 'protesisRemovibleRojo marcado', estado: data_pintar[key].estado});
                                } else {
                                    $('svg[id=up' + data_protesis_remo_rojo[i].diente + ']').find('.protesis_remo').attr({class: 'protesisRemovibleRojo marcado', estado: data_pintar[key].estado});
                                }
                            }
                            for (var j = 0; j < (data_protesis_remo_rojo.length); j = j + 2) {
                                for (var i = (data_protesis_remo_rojo[j].diente); i < (data_protesis_remo_rojo[j + 1].diente - 1); i++) {
                                    $('svg[id=up' + (i + 1) + ']').find('.protesis_remo').attr({class: 'protesisRemovibleRojo', estado: data_pintar[key].estado});
                                }
                            }
                            saltarEachCirculo = true;
                        }
                    });
                } else if (data_pintar[key].estado == 49) {
                    $('#placa_up' + data_pintar[key].diente + ' path').each(function () {
                        if (saltarEachCirculo == false) {
                            $('svg[id=placa_up' + data_pintar[key].diente + ']').find('.extruido').attr({class: 'extruidoAzul marcado', estado: data_pintar[key].estado});
                            saltarEachCirculo = true;
                        }
                    });
                } else if (data_pintar[key].estado == 50) {
                    $('#placa_up' + data_pintar[key].diente + ' path').each(function () {
                        if (saltarEachCirculo == false) {
                            $('svg[id=placa_up' + data_pintar[key].diente + ']').find('.intruido').attr({class: 'intruidoAzul marcado', estado: data_pintar[key].estado});
                            saltarEachCirculo = true;
                        }
                    });
                } else if (data_pintar[key].estado == 51) {
                    $('#placa_up' + data_pintar[key].diente + ' path').each(function () {
                        if (saltarEachCirculo == false) {
                            for (var i = 1; i <= 8; i++) {
                                if (data_pintar[key].diente == i)
                                {
                                    $('svg[id=placa_up' + data_pintar[key].diente + ']').find('.giroversion_1').attr({class: 'giroversion1Azul marcado', estado: data_pintar[key].estado});
                                }
                            }
                            for (var i = 9; i <= 16; i++) {
                                if (data_pintar[key].diente == i)
                                {
                                    $('svg[id=placa_up' + data_pintar[key].diente + ']').find('.giroversion_2').attr({class: 'giroversion2Azul marcado', estado: data_pintar[key].estado});
                                }
                            }
                            for (var i = 17; i <= 24; i++) {
                                if (data_pintar[key].diente == i)
                                {
                                    $('svg[id=placa_up' + data_pintar[key].diente + ']').find('.giroversion_1').attr({class: 'giroversion1Azul marcado', estado: data_pintar[key].estado});
                                }
                            }
                            for (var i = 25; i <= 32; i++) {
                                if (data_pintar[key].diente == i)
                                {
                                    $('svg[id=placa_up' + data_pintar[key].diente + ']').find('.giroversion_2').attr({class: 'giroversio2nAzul marcado', estado: data_pintar[key].estado});
                                }
                            }
                            for (var i = 33; i <= 37; i++) {
                                if (data_pintar[key].diente == i)
                                {
                                    $('svg[id=placa_up' + data_pintar[key].diente + ']').find('.giroversion_1').attr({class: 'giroversion1Azul marcado', estado: data_pintar[key].estado});
                                }
                            }
                            for (var i = 38; i <= 42; i++) {
                                if (data_pintar[key].diente == i)
                                {
                                    $('svg[id=placa_up' + data_pintar[key].diente + ']').find('.giroversion_2').attr({class: 'giroversion2Azul marcado', estado: data_pintar[key].estado});
                                }
                            }
                            for (var i = 43; i <= 47; i++) {
                                if (data_pintar[key].diente == i)
                                {
                                    $('svg[id=placa_up' + data_pintar[key].diente + ']').find('.giroversion_1').attr({class: 'giroversion1Azul marcado', estado: data_pintar[key].estado});
                                }
                            }
                            for (var i = 48; i <= 52; i++) {
                                if (data_pintar[key].diente == i)
                                {
                                    $('svg[id=placa_up' + data_pintar[key].diente + ']').find('.giroversion_2').attr({class: 'giroversion2Azul marcado', estado: data_pintar[key].estado});
                                }
                            }
                            saltarEachCirculo = true;
                        }
                    });
                } else if (data_pintar[key].estado == 52) {
                    $('#placa_up' + data_pintar[key].diente + ' path').each(function () {
                        if (saltarEachCirculo == false) {
                            for (var i = 1; i <= 8; i++) {
                                if (data_pintar[key].diente == i)
                                {
                                    $('svg[id=placa_up' + data_pintar[key].diente + ']').find('.giroversion_2').attr({class: 'giroversion2Azul marcado', estado: data_pintar[key].estado});
                                }
                            }
                            for (var i = 9; i <= 16; i++) {
                                if (data_pintar[key].diente == i)
                                {
                                    $('svg[id=placa_up' + data_pintar[key].diente + ']').find('.giroversion_1').attr({class: 'giroversion1Azul marcado', estado: data_pintar[key].estado});
                                }
                            }
                            for (var i = 17; i <= 24; i++) {
                                if (data_pintar[key].diente == i)
                                {
                                    $('svg[id=placa_up' + data_pintar[key].diente + ']').find('.giroversion_2').attr({class: 'giroversion2Azul marcado', estado: data_pintar[key].estado});
                                }
                            }
                            for (var i = 25; i <= 32; i++) {
                                if (data_pintar[key].diente == i)
                                {
                                    $('svg[id=placa_up' + data_pintar[key].diente + ']').find('.giroversion_1').attr({class: 'giroversion1Azul marcado', estado: data_pintar[key].estado});
                                }
                            }
                            for (var i = 33; i <= 37; i++) {
                                if (data_pintar[key].diente == i)
                                {
                                    $('svg[id=placa_up' + data_pintar[key].diente + ']').find('.giroversion_2').attr({class: 'giroversion2Azul marcado', estado: data_pintar[key].estado});
                                }
                            }
                            for (var i = 38; i <= 42; i++) {
                                if (data_pintar[key].diente == i)
                                {
                                    $('svg[id=placa_up' + data_pintar[key].diente + ']').find('.giroversion_1').attr({class: 'giroversion1Azul marcado', estado: data_pintar[key].estado});
                                }
                            }
                            for (var i = 43; i <= 47; i++) {
                                if (data_pintar[key].diente == i)
                                {
                                    $('svg[id=placa_up' + data_pintar[key].diente + ']').find('.giroversion_2').attr({class: 'giroversion2Azul marcado', estado: data_pintar[key].estado});
                                }
                            }
                            for (var i = 48; i <= 52; i++) {
                                if (data_pintar[key].diente == i)
                                {
                                    $('svg[id=placa_up' + data_pintar[key].diente + ']').find('.giroversion_1').attr({class: 'giroversion1Azul marcado', estado: data_pintar[key].estado});
                                }
                            }
                            saltarEachCirculo = true;
                        }
                    });
                } else if (data_pintar[key].estado == 53) {
                    $('#placa_up' + data_pintar[key].diente + ' path').each(function () {
                        if (saltarEachCirculo == false) {
                            for (var i = 1; i <= 8; i++) {
                                if (data_pintar[key].diente == i)
                                {
                                    $('svg[id=placa_up' + data_pintar[key].diente + ']').find('.migracion_1').attr({class: 'migracion1Azul marcado', estado: data_pintar[key].estado});
                                }
                            }
                            for (var i = 9; i <= 16; i++) {
                                if (data_pintar[key].diente == i)
                                {
                                    $('svg[id=placa_up' + data_pintar[key].diente + ']').find('.migracion_2').attr({class: 'migracion2Azul marcado', estado: data_pintar[key].estado});
                                }
                            }
                            for (var i = 17; i <= 24; i++) {
                                if (data_pintar[key].diente == i)
                                {
                                    $('svg[id=placa_up' + data_pintar[key].diente + ']').find('.migracion_1').attr({class: 'migracion1Azul marcado', estado: data_pintar[key].estado});
                                }
                            }
                            for (var i = 25; i <= 32; i++) {
                                if (data_pintar[key].diente == i)
                                {
                                    $('svg[id=placa_up' + data_pintar[key].diente + ']').find('.migracion_2').attr({class: 'migracion2Azul marcado', estado: data_pintar[key].estado});
                                }
                            }
                            for (var i = 33; i <= 37; i++) {
                                if (data_pintar[key].diente == i)
                                {
                                    $('svg[id=placa_up' + data_pintar[key].diente + ']').find('.migracion_1').attr({class: 'migracion1Azul marcado', estado: data_pintar[key].estado});
                                }
                            }
                            for (var i = 38; i <= 42; i++) {
                                if (data_pintar[key].diente == i)
                                {
                                    $('svg[id=placa_up' + data_pintar[key].diente + ']').find('.migracion_2').attr({class: 'migracion2Azul marcado', estado: data_pintar[key].estado});
                                }
                            }
                            for (var i = 43; i <= 47; i++) {
                                if (data_pintar[key].diente == i)
                                {
                                    $('svg[id=placa_up' + data_pintar[key].diente + ']').find('.migracion_1').attr({class: 'migracion1Azul marcado', estado: data_pintar[key].estado});
                                }
                            }
                            for (var i = 48; i <= 52; i++) {
                                if (data_pintar[key].diente == i)
                                {
                                    $('svg[id=placa_up' + data_pintar[key].diente + ']').find('.migracion_2').attr({class: 'migracion2Azul marcado', estado: data_pintar[key].estado});
                                }
                            }
                            saltarEachCirculo = true;
                        }
                    });
                } else if (data_pintar[key].estado == 54) {
                    $('#placa_up' + data_pintar[key].diente + ' path').each(function () {
                        if (saltarEachCirculo == false) {
                            for (var i = 1; i <= 8; i++) {
                                if (data_pintar[key].diente == i)
                                {
                                    $('svg[id=placa_up' + data_pintar[key].diente + ']').find('.migracion_2').attr({class: 'migracion2Azul marcado', estado: data_pintar[key].estado});
                                }
                            }
                            for (var i = 9; i <= 16; i++) {
                                if (data_pintar[key].diente == i)
                                {
                                    $('svg[id=placa_up' + data_pintar[key].diente + ']').find('.migracion_1').attr({class: 'migracion1Azul marcado', estado: data_pintar[key].estado});
                                }
                            }
                            for (var i = 17; i <= 24; i++) {
                                if (data_pintar[key].diente == i)
                                {
                                    $('svg[id=placa_up' + data_pintar[key].diente + ']').find('.migracion_2').attr({class: 'migracion2Azul marcado', estado: data_pintar[key].estado});
                                }
                            }
                            for (var i = 25; i <= 32; i++) {
                                if (data_pintar[key].diente == i)
                                {
                                    $('svg[id=placa_up' + data_pintar[key].diente + ']').find('.migracion_1').attr({class: 'migracion1Azul marcado', estado: data_pintar[key].estado});
                                }
                            }
                            for (var i = 33; i <= 37; i++) {
                                if (data_pintar[key].diente == i)
                                {
                                    $('svg[id=placa_up' + data_pintar[key].diente + ']').find('.migracion_2').attr({class: 'migracion2Azul marcado', estado: data_pintar[key].estado});
                                }
                            }
                            for (var i = 38; i <= 42; i++) {
                                if (data_pintar[key].diente == i)
                                {
                                    $('svg[id=placa_up' + data_pintar[key].diente + ']').find('.migracion_1').attr({class: 'migracion1Azul marcado', estado: data_pintar[key].estado});
                                }
                            }
                            for (var i = 43; i <= 47; i++) {
                                if (data_pintar[key].diente == i)
                                {
                                    $('svg[id=placa_up' + data_pintar[key].diente + ']').find('.migracion_2').attr({class: 'migracion2Azul marcado', estado: data_pintar[key].estado});
                                }
                            }
                            for (var i = 48; i <= 52; i++) {
                                if (data_pintar[key].diente == i)
                                {
                                    $('svg[id=placa_up' + data_pintar[key].diente + ']').find('.migracion_1').attr({class: 'migracion1Azul marcado', estado: data_pintar[key].estado});
                                }
                            }
                            saltarEachCirculo = true;
                        }
                    });
                } else if (data_pintar[key].estado == 55) {
                    $('#' + data_pintar[key].diente + ' path').each(function () {
                        if (saltarEachCirculo == false) {
                            for (var i = 0; i < data_edentulo_total.length; i++) {
                                if (i % 2 == 0) {
                                    $('svg[id=' + data_edentulo_total[i].diente + ']').find('.edentulo_total').attr({class: 'edentulo_totalAzul marcado', estado: data_pintar[key].estado});
                                } else {
                                    $('svg[id=' + data_edentulo_total[i].diente + ']').find('.edentulo_total').attr({class: 'edentulo_totalAzul marcado', estado: data_pintar[key].estado});
                                }
                            }
                            for (var j = 0; j < (data_edentulo_total.length); j = j + 2) {
                                for (var i = (data_edentulo_total[j].diente); i < (data_edentulo_total[j + 1].diente - 1); i++) {
                                    $('svg[id=' + (i + 1) + ']').find('.edentulo_total').attr({class: 'edentulo_totalAzul', estado: data_pintar[key].estado});
                                }
                            }
                            saltarEachCirculo = true;
                        }
                    });
                } else if (data_pintar[key].estado == 56) {
                    $('#' + data_pintar[key].diente + ' path').each(function () {
                        if (saltarEachCirculo == false) {
                            for (var i = 0; i < data_protesis_total_azul.length; i++) {
                                if (i % 2 == 0) {
                                    $('svg[id=' + data_protesis_total_azul[i].diente + ']').find('.protesis_total').attr({class: 'protesis_totalAzul marcado', estado: data_pintar[key].estado});
                                } else {
                                    $('svg[id=' + data_protesis_total_azul[i].diente + ']').find('.protesis_total').attr({class: 'protesis_totalAzul marcado', estado: data_pintar[key].estado});
                                }
                            }
                            for (var j = 0; j < (data_protesis_total_azul.length); j = j + 2) {
                                for (var i = (data_protesis_total_azul[j].diente); i < (data_protesis_total_azul[j + 1].diente - 1); i++) {
                                    $('svg[id=' + (i + 1) + ']').find('.protesis_total').attr({class: 'protesis_totalAzul', estado: data_pintar[key].estado});
                                }
                            }
                            saltarEachCirculo = true;
                        }
                    });
                } else if (data_pintar[key].estado == 57) {
                    $('#' + data_pintar[key].diente + ' path').each(function () {
                        if (saltarEachCirculo == false) {
                            for (var i = 0; i < data_protesis_total_rojo.length; i++) {
                                if (i % 2 == 0) {
                                    $('svg[id=' + data_protesis_total_rojo[i].diente + ']').find('.protesis_total').attr({class: 'protesis_totalRojo marcado', estado: data_pintar[key].estado});
                                } else {
                                    $('svg[id=' + data_protesis_total_rojo[i].diente + ']').find('.protesis_total').attr({class: 'protesis_totalRojo marcado', estado: data_pintar[key].estado});
                                }
                            }
                            for (var j = 0; j < (data_protesis_total_rojo.length); j = j + 2) {
                                for (var i = (data_protesis_total_rojo[j].diente); i < (data_protesis_total_rojo[j + 1].diente - 1); i++) {
                                    $('svg[id=' + (i + 1) + ']').find('.protesis_total').attr({class: 'protesis_totalRojo', estado: data_pintar[key].estado});
                                }
                            }
                            saltarEachCirculo = true;
                        }
                    });
                } else if (data_pintar[key].estado == 58) {
                    $('#up' + data_pintar[key].diente + ' path').each(function () {
                        if (saltarEachCirculo == false) {
                            for (var i = 0; i < data_puente_total_azul.length; i++) {
                                if (i % 2 == 0) {
                                    $('svg[id=up' + data_puente_total_azul[i].diente + ']').find('.puente_2').attr({class: 'puente_2Azul marcado', estado: data_pintar[key].estado});
                                } else {
                                    $('svg[id=up' + data_puente_total_azul[i].diente + ']').find('.puente_1').attr({class: 'puente_1Azul marcado', estado: data_pintar[key].estado});
                                }
                            }
                            for (var j = 0; j < (data_puente_total_azul.length); j = j + 2) {
                                for (var i = (data_puente_total_azul[j].diente); i < (data_puente_total_azul[j + 1].diente - 1); i++) {
                                    $('svg[id=up' + (i + 1) + ']').find('.puente_total').attr({class: 'puente_totalAzul', estado: data_pintar[key].estado});
                                }
                            }
                            saltarEachCirculo = true;
                        }
                    });
                } else if (data_pintar[key].estado == 59) {
                    $('#' + data_pintar[key].diente + ' path').each(function () {
                        if (saltarEachCirculo == false) {
                            for (var i = 0; i < data_puente_total_rojo.length; i++) {
                                if (i % 2 == 0) {
                                    $('svg[id=up' + data_puente_total_rojo[i].diente + ']').find('.puente_2').attr({class: 'puente_2Rojo marcado', estado: data_pintar[key].estado});
                                } else {
                                    $('svg[id=up' + data_puente_total_rojo[i].diente + ']').find('.puente_1').attr({class: 'puente_1Rojo marcado', estado: data_pintar[key].estado});
                                }
                            }
                            for (var j = 0; j < (data_puente_total_rojo.length); j = j + 2) {
                                for (var i = (data_puente_total_rojo[j].diente); i < (data_puente_total_rojo[j + 1].diente - 1); i++) {
                                    $('svg[id=up' + (i + 1) + ']').find('.puente_total').attr({class: 'puente_totalRojo', estado: data_pintar[key].estado});
                                }
                            }
                            saltarEachCirculo = true;
                        }
                    });
                } else {
                    $('#' + data_pintar[key].diente + ' path').each(function () {
                        if ($(this).attr('value') == data_pintar[key].cara) {
                            pintar(data_pintar[key].estado, this);
                        }
                    });
                }
            }

            if (countMarcado.length == conteo_diente_detalle) {
                console.log(countMarcado.length);
                $.toast({
                    heading: 'Éxito',
                    text: 'Datos cargados correctamente',
                    position: 'bottom-right',
                    icon: 'success',
                    hideAfter: 4000,
                    stack: 6
                })
                return true;
            } else {
                console.log("Faltan datos");
                swal({
                    title: "Error",
                    text: "Actualice la página por favor",
                    type: "error",
                    allowEscapeKey: false,
                    showCancelButton: false,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "Actualizar!",
                }, function (isConfirm) {
                    if (isConfirm) {
                        location.reload();
                    }
                });
                return true;
            }

        }
    });
</script>