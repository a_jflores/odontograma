<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!doctype html>
<html lang="es">
    <head>
        <jsp:include page="/templates/head.jsp" flush="true"/>
        <link href="https://vjs.zencdn.net/7.11.4/video-js.css" rel="stylesheet" />
        <script src="https://vjs.zencdn.net/7.11.4/video.min.js"></script>
        <script src="https://vjs.zencdn.net/7.11.4/lang/es.js"></script>
    </head>
    <style>
        .form-control-width {
            width: auto;
        }
    </style>
    <body class="fix-header">
        <div class="preloader">
            <svg class="circular" viewBox="25 25 50 50">
            <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10" />
            </svg>
        </div>
        <div id="wrapper">
            <jsp:include page="/templates/header.jsp" flush="true"/> 
            <jsp:include page="/templates/navbar.jsp" flush="true"/>
            <div id="page-wrapper">
                <div class="container-fluid">
                    <jsp:include page="/views/HistoriaClinica/historia.jsp" flush="true"/>
                    <jsp:include page="/templates/right-sidebar.jsp" flush="true"/>
                </div>
                <jsp:include page="/templates/footer.jsp" flush="true"/>
            </div>
        </div>
        <jsp:include page="/templates/scripts.jsp" flush="true"/>
        <script>
            <c:if test="${not empty exito}">
            $.toast({
                heading: 'Éxito',
                text: '${exito}',
                position: 'bottom-right',
                icon: 'success',
                hideAfter: 4000,
                stack: 6
            });
                <c:set var="exito" value="" scope="session"/>
            </c:if>
            <c:if test="${not empty fracaso}">
            $.toast({
                heading: 'Error',
                text: '${fracaso}',
                position: 'bottom-right',
                icon: 'error',
                hideAfter: 4000,
                stack: 6
            });
                <c:set var="fracaso" value="" scope="session"/>
            </c:if>
        </script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.0/js/i18n/es.js" defer></script>
        <script src="https://cdn.datatables.net/buttons/1.2.2/js/dataTables.buttons.min.js"></script>
        <script src="https://cdn.datatables.net/buttons/1.2.2/js/buttons.flash.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.min.js"></script>
        <script src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/pdfmake.min.js"></script>
        <script src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/vfs_fonts.js"></script>
        <script src="https://cdn.datatables.net/buttons/1.2.2/js/buttons.html5.min.js"></script>
        <script src="https://cdn.datatables.net/buttons/1.2.2/js/buttons.print.min.js"></script>
        <script src="${pageContext.request.contextPath}/views/HistoriaClinica/verHistoria.js"></script>
    </body>
</html>