<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<div class="row bg-title">
    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
        <h4 class="page-title">Historias Clínicas</h4> </div>
    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
        <button class="right-side-toggle waves-effect waves-light btn-info btn-circle pull-right m-l-20"><i class="ti-settings text-white"></i></button>
        <ol class="breadcrumb">
            <li><a href="${pageContext.request.contextPath}/index.jsp">Dashboard</a></li>
            <li class="active">Historias</li>
        </ol>
    </div>
</div>
<!-- /row -->
<div class="row">
    <div class="col-sm-12">
        <div class="white-box">
            <div class="row">
                <div class="col-sm-12 col-xs-12 col-md-10">
                    <h3 class="box-title m-b-0">Historias Clínicas</h3>
                    <p class="text-muted m-b-15">Historias</p>
                </div>
                <div class="col-sm-12 col-xs-12 col-md-2 text-right">
                    <button type="button" class="btn btn-info btn-rounded" data-toggle="modal" data-target="#add-history">Nueva Historia Clínica</button>
                </div>
            </div>
            <br>
            <div id="add-history" class="modal fade in" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                            <center>
                                <h3 class="box-title m-b-0" id="myModalLabel">Nueva Historia Clínica</h3>
                            </center>
                        </div>
                        <div class="modal-body">
                            <form action="${pageContext.request.contextPath}/historias.do">
                                <input type="hidden" name="op" value="insertar"/>
                                <div class="no-bg-addon">
                                    <div class="form-group">
                                        <label>Buscar paciente:</label>
                                        <br>
                                        <br>
                                        <select id="paciente" name="id_paciente" style="width: 100%">
                                            <c:forEach items="${requestScope.listaPacientes}" var="paciente">
                                                <option value="${paciente.codigoPaciente}">${paciente.nombrePaciente}</option>
                                            </c:forEach>
                                        </select>
                                    </div>
                                    <button type="submit" class="btn btn-block btn-success btn-md btn-rounded"> <i class="fa fa fa-save"></i> Guardar</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <div class="table-responsive">
                <table id="data_historias" class="display" style="width:100%">
                    <thead>
                        <tr>
                            <th>Id</th>
                            <th>Nombres y Apellidos</th>
                            <th>Documento</th>
                            <th>Teléfono</th>
                            <th>Nacionalidad</th>
                            <th>Fecha</th>
                            <th>Operaciones</th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
</div>
