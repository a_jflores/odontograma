<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<style>
    .nav-tab-vertical {
        border-color:transparent;
        width:100%;
    }

    .nav-tab-vertical>li {
        float: none;
        margin-bottom: 0px;
        margin-right: 0px;
    }

    .nav-tab-vertical > li > a { 
        color:black;
    }

    .dropdown-menu>.active>a {
        background-color: #2cabe3;
    }

    .nav-tab-vertical > li.active > a,
    .nav-tab-vertical > li.active > a:focus,
    .nav-tab-vertical > li.active > a:hover {
        background-color:#2cabe3;
        color:#000;
    }

    .table_update {
        border-bottom: none;
    }

    .table_update>tbody>tr>td, .table_update>tbody>tr>th, .table_update>tfoot>tr>td, .table_update>tfoot>tr>th, .table_update>thead>tr>td, .table_update>thead>tr>th {
        padding: 8px;
        line-height: 1.42857143;
        vertical-align: middle;
        border-top: 1px solid #ddd;
    }

    .table_update-bordered, .table_update>tbody>tr>td, .table_update>tbody>tr>th, .table_update>tfoot>tr>td, .table_update>tfoot>tr>th, .table_update>thead>tr>td, .table_update>thead>tr>th {
        border-top: 0px solid #1769ba;
    }

    .table_update>thead>tr>th {
        vertical-align: bottom;
        border-bottom: 1px solid #245c95;
    }

    .text-ident{
        text-indent: 20px;
        padding: 1px 1px 1px 1px;
    }

    .waves-effect {
        overflow: visible;
    }

    .mailbox-alert {
        width:200px;
    }

    @media (max-width: 767px){
        .mailbox-alert {
            width:100%;
        }
    }

</style>

<div class="row bg-title">
    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
        <h4 class="page-title">Paciente</h4> </div>
    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
        <button class="right-side-toggle waves-effect waves-light btn-info btn-circle pull-right m-l-20"><i class="ti-settings text-white"></i></button>
        <ol class="breadcrumb">
            <li><a href="${pageContext.request.contextPath}/index.jsp">Dashboard</a></li>
            <li class="active">Paciente</li>
        </ol>
    </div>
</div>

<div class="row">
    <div class="col-md-3 col-xs-12">
        <div class="white-box">
            <div class="row">
                <div class="col-md-8">
                    <img style="background-size: 100px 100px;width:150px; height:150px;" alt="user" class="img-responsive thumbnail" src="${pageContext.request.contextPath}/plugins/images/users/1.jpg">
                </div>
                <div class="col-md-4">
                    <c:if test="${not empty anamnesis.codigoAnamnesis && 
                                  (anamnesis.alerta_diabetes == 's' 
                                  || anamnesis.alerta_hipertension == 's'
                                  || anamnesis.alerta_hemorragia == 's'
                                  || anamnesis.alerta_alergia == 's'
                                  || anamnesis.alerta_desmayo == 's'
                                  || anamnesis.alerta_hepatica == 's'
                                  || anamnesis.alerta_fiebre_reumatica == 's'
                                  || anamnesis.alerta_angina == 's'
                                  || anamnesis.alerta_miocardio == 's'
                                  || anamnesis.alerta_embarazo == 's')}"> 
                          <ul class="nav tabs-vertical">
                              <li class="dropdown">
                                  <a class="dropdown-toggle waves-effect waves-light" data-toggle="dropdown" href="#"> <i class="fa fa-bell"></i>
                                      <span class="notify"> <span class="heartbit"></span> <span class="point"></span> </span>
                                  </a>
                                  <ul class="dropdown-menu mailbox-alert animated bounceInDown">
                                      <li>
                                          <div class="drop-title">Alerta médica</div>
                                      </li>
                                      <c:if test="${anamnesis.alerta_diabetes == 's'}">
                                          <li>
                                              <div class="text-ident">
                                                  <h5>Diabetes</h5>
                                              </div>
                                          </li>
                                      </c:if>
                                      <c:if test="${anamnesis.alerta_hipertension == 's'}">
                                          <li>
                                              <div class="text-ident">
                                                  <h5>Hipertensión arterial</h5>
                                              </div>
                                          </li>
                                      </c:if>
                                      <c:if test="${anamnesis.alerta_hemorragia == 's'}">
                                          <li>
                                              <div class="text-ident">
                                                  <h5>Hemorragia</h5>
                                              </div>
                                          </li>
                                      </c:if>
                                      <c:if test="${anamnesis.alerta_alergia == 's'}">
                                          <li>
                                              <div class="text-ident">
                                                  <h5>Alergia</h5>
                                              </div>
                                          </li>
                                      </c:if>
                                      <c:if test="${anamnesis.alerta_desmayo == 's'}">
                                          <li>
                                              <div class="text-ident">
                                                  <h5>Desmayo</h5>
                                              </div>
                                          </li>
                                      </c:if>
                                      <c:if test="${anamnesis.alerta_hepatica == 's'}">
                                          <li>
                                              <div class="text-ident">
                                                  <h5>Hepática</h5>
                                              </div>
                                          </li>
                                      </c:if>
                                      <c:if test="${anamnesis.alerta_fiebre_reumatica == 's'}">
                                          <li>
                                              <div class="text-ident">
                                                  <h5>Fiebre reumática</h5>
                                              </div>
                                          </li>
                                      </c:if>
                                      <c:if test="${anamnesis.alerta_angina == 's'}">
                                          <li>
                                              <div class="text-ident">
                                                  <h5>Angina</h5>
                                              </div>
                                          </li>
                                      </c:if>
                                      <c:if test="${anamnesis.alerta_miocardio == 's'}">
                                          <li>
                                              <div class="text-ident">
                                                  <h5>Miocardio</h5>
                                              </div>
                                          </li>
                                      </c:if>
                                      <c:if test="${anamnesis.alerta_embarazo == 's' }">
                                          <li>
                                              <div class="text-ident">
                                                  <h5>Embarazo</h5>
                                              </div>
                                          </li>
                                      </c:if>
                                  </ul>
                              </li>
                          </ul>
                    </c:if>
                </div>
            </div>
            <div class="box">
                <div class="row">
                    <input type="hidden" id="id_paciente" value="${historia.paciente.codigoPaciente}"/>
                    <input type="hidden" id="id_historia_clinica" value="${historia.codigoHistoriaClinica}"/>
                    <div class="col-md-12"><strong>Nombres y Apellidos</strong>
                        <p>${historia.paciente.nombrePaciente}</p>
                    </div>
                    <div class="col-md-12"><strong>Nacionalidad</strong>
                        <p>${historia.paciente.nacionalidadPaciente}</p>
                    </div>
                    <div class="col-md-12"><strong>Teléfono</strong>
                        <p>${historia.paciente.telefonoPaciente}</p>
                    </div>
                    <div class="col-md-12"><strong>Edad</strong>
                        <c:if test="${not empty historia.paciente.fechanacimientoPaciente}">
                            <p>${historia.paciente.edadPaciente}</p>
                        </c:if>
                    </div>
                </div>

                <hr>
                <ul class="nav nav-tab-vertical">
                    <li class="tab"><a data-toggle="tab" href="#anamnesis"><i class="fa fa-users"></i> Anamnesis</a></li>
                    <li class="tab"><a data-toggle="tab" href="#examen_clinico"><i class="fa fa-fire-extinguisher"></i> Examen Clinico</a></li>
                    <li class="tab active"><a data-toggle="tab" href="#odontograma"><i class="fa fa-life-ring"></i> Odontograma</a></li>
                    <!--                    <li class="tab"><a data-toggle="tab" href="#" data-original-title="" title=""><i class="fa fa-picture-o"></i> Periodontograma</a></li>
                                        <li class="tab"><a data-toggle="tab" href="#" data-original-title="" title=""><i class="fa fa-heart-o"></i> Diagnostico</a></li>-->
                    <li class="tab"><a data-toggle="tab" href="#examen_radiografico"><i class="fa fa-photo"></i> Examen Radiográfico</a></li>
                    <li class="tab"><a data-toggle="tab" href="#video_tratamiento"><i class="fa fa-video-camera"></i> Video Tratamiento</a></li>
                    <li class="tab"><a data-toggle="tab" href="#presupuesto"><i class="fa fa-usd"></i> Presupuesto</a></li>
                    <!--                    <li class="tab"><a data-toggle="tab" href="#" data-original-title="" title=""><i class="fa fa-usd"></i> Plan De Tratamiento</a></li>
                                        <li class="tab"><a data-toggle="tab" href="#" data-original-title="" title=""><i class="fa fa-keyboard-o"></i> Evolucion</a></li>
                                        <li class="tab"><a data-toggle="tab" href="#" data-original-title="" title=""><i class="fa fa-clipboard"></i> Receta</a></li>-->
                    <li class="tab"><a data-toggle="tab" href="#citas" data-original-title="" title=""><i class="fa fa-calendar-minus-o"></i> Citas</a></li>
                </ul>
            </div>
        </div>
    </div>
    <div class="col-md-9 col-xs-12">
        <div class="white-box">
            <div class="tab-content">
                <div id="anamnesis" class="tab-pane">
                    <ul class="nav nav-tabs tabs customtab">
                        <li class="active tab">
                            <a href="#datos_personales" data-toggle="tab"> <span class="visible-xs"><i class="fa fa-home"></i></span> <span class="hidden-xs">Datos personales</span> </a>
                        </li>
                        <li class="tab">
                            <a href="#cuestionario" data-toggle="tab"> <span class="visible-xs"><i class="fa fa-user"></i></span> <span class="hidden-xs">Cuestionario</span> </a>
                        </li>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane active" id="datos_personales">
                            <div class="row">
                                <div class="col-sm-12 col-xs-12">
                                    <c:if test="${not empty requestScope.listaErrores}">
                                        <div class="alert alert-danger">
                                            <ul>
                                                <c:forEach items="${requestScope.listaErrores}" var="error">
                                                    <li>${error}</li>
                                                    </c:forEach>
                                            </ul>
                                        </div>
                                    </c:if>
                                    <form data-toggle="validator" action="${pageContext.request.contextPath}/pacientes.do" method="POST">
                                        <input type="hidden" name="op" value="modificar"/>
                                        <input type="hidden" name="id_historia_clinica" value="${historia.codigoHistoriaClinica}"/>
                                        <input type="hidden" name="id_paciente" value="${historia.paciente.codigoPaciente}"/>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label>Nombres y Apellidos</label>
                                                    <div class="input-group">
                                                        <div class="input-group-addon"><i class="ti-user"></i></div>
                                                        <input data-error="Campo requerido" name="nombres" type="text" value="${historia.paciente.nombrePaciente}" class="form-control" pattern="[a-zA-ZàáâäãåąčćęèéêëėįìíîïłńòóôöõøùúûüųūÿýżźñçčšžÀÁÂÄÃÅĄĆČĖĘÈÉÊËÌÍÎÏĮŁŃÒÓÔÖÕØÙÚÛÜŲŪŸÝŻŹÑßÇŒÆČŠŽ∂ð ,.'-]{2,254}" placeholder="Nombres y Apellidos" required>
                                                    </div>
                                                    <div class="help-block with-errors"></div>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label>Documento de indentidad</label>
                                                    <div class="input-group">
                                                        <div class="input-group-addon"><i class="ti-file"></i></div>
                                                        <input name="documento" data-error= "Campo requerido" type="text" value="${historia.paciente.documentoPaciente}" class="form-control" placeholder="Dni" required>
                                                    </div>
                                                    <div class="help-block with-errors"></div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label>Nacionalidad</label>
                                                    <div class="input-group">
                                                        <div class="input-group-addon"><i class="fa fa-flag-o"></i></div>
                                                        <select id="nacionalidad" name="nacionalidad" class="form-control" style="width: 100%">
                                                            <c:forEach items="${requestScope.listaPaises}" var="pais">
                                                                <option value="${pais.nacionalidadPais}" ${pais.nacionalidadPais == historia.paciente.nacionalidadPaciente ? 'selected="selected"' : ''}>${pais.nombrePais}</option>
                                                            </c:forEach>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label>Teléfono/Celular</label>
                                                    <div class="input-group">
                                                        <div class="input-group-addon"><i class="ti-mobile"></i></div>
                                                        <input name="telefono" data-error="Campo requerido" type="text" value="${historia.paciente.telefonoPaciente}" class="form-control" placeholder="Contacto" required>
                                                    </div>
                                                    <div class="help-block with-errors"></div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label>Domicilio</label>
                                                    <div class="input-group">
                                                        <div class="input-group-addon"><i class="ti-home"></i></div>
                                                        <input data-error="Error, el domicilio es inválido" name="domicilio" type="text" value="${historia.paciente.domicilioPaciente}" class="form-control" placeholder="Domicilio">
                                                    </div>
                                                    <div class="help-block with-errors"></div>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label>Email</label>
                                                    <div class="input-group">
                                                        <div class="input-group-addon"><i class="ti-email"></i></div>
                                                        <input data-error="Error, el email es inválido" name="email" type="email" value="${historia.paciente.emailPaciente}" class="form-control" placeholder="Email">
                                                    </div>
                                                    <div class="help-block with-errors"></div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label>Fecha de nacimiento</label>
                                                    <div class="input-group">
                                                        <div class="input-group-addon"><i class="ti-calendar"></i></div>
                                                        <input name="fecha_nac" type="date" value="${historia.paciente.fechanacimientoPaciente}" class="form-control" placeholder="Enter email">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label>Género</label>
                                                    <div class="input-group">
                                                        <div class="input-group-addon"><i class="ti-layers"></i></div>
                                                        <select name="sexo" class="form-control">
                                                            <c:if test = "${empty historia.paciente.sexoPaciente}">
                                                                <option value="" selected>Selecione una opción</option>
                                                                <option value="M">Masculino</option>
                                                                <option value="F">Femenino</option>
                                                            </c:if>
                                                            <c:if test = "${not empty historia.paciente.sexoPaciente}">
                                                                <option value="M" ${historia.paciente.sexoPaciente == 'M' ? 'selected="selected"' : ''}>Masculino</option>
                                                                <option value="F" ${historia.paciente.sexoPaciente == 'F' ? 'selected="selected"' : ''}>Femenino</option>
                                                            </c:if>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label>Estado civil</label>
                                                    <div class="input-group">
                                                        <div class="input-group-addon"><i class="ti-heart"></i></div>
                                                        <select name="estado_civil" class="form-control">
                                                            <c:if test = "${empty historia.paciente.estadocivilPaciente}">
                                                                <option value="" selected>Selecione una opción</option>
                                                                <option value="S">Soltero</option>
                                                                <option value="C">Casado</option>
                                                                <option value="V">Viudo</option>
                                                                <option value="D">Divorciado</option>
                                                            </c:if>
                                                            <c:if test = "${not empty historia.paciente.estadocivilPaciente}">
                                                                <option value="S" ${historia.paciente.estadocivilPaciente == 'S' ? 'selected="selected"' : ''}>Soltero</option>
                                                                <option value="C" ${historia.paciente.estadocivilPaciente == 'C' ? 'selected="selected"' : ''}>Casado</option>
                                                                <option value="V" ${historia.paciente.estadocivilPaciente == 'V' ? 'selected="selected"' : ''}>Viudo</option>
                                                                <option value="D" ${historia.paciente.estadocivilPaciente == 'D' ? 'selected="selected"' : ''}>Divorciado</option>
                                                            </c:if>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label>Recomendado por:</label>
                                                    <div class="input-group">
                                                        <div class="input-group-addon"><i class="ti-star"></i></div>
                                                        <select id="recomendacion" name="recomendacion" class="form-control" style="width: 100%">
                                                            <c:if test = "${empty historia.paciente.recomendacionPaciente}">
                                                                <option value="0">Selecione una opción</option>
                                                            </c:if>
                                                            <c:if test = "${historia.paciente.recomendacionPaciente == '0'}">
                                                                <option value="0">Sin recomendación</option>
                                                            </c:if>
                                                            <c:forEach items="${requestScope.listaRecomendaciones}" var="recomendacion">
                                                                <option value="${recomendacion.codigoPaciente}" ${recomendacion.codigoPaciente == historia.paciente.recomendacionPaciente ? 'selected="selected"' : ''}>${recomendacion.nombrePaciente}</option>
                                                            </c:forEach>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label>Hospedaje</label>
                                                    <div class="input-group">
                                                        <div class="input-group-addon"><i class="mdi mdi-home-modern"></i></div>
                                                        <input name="hospedaje" value="${historia.paciente.hospedajePaciente}" type="text" class="form-control" placeholder="Hospedaje">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12 text-center">
                                                <button type="submit" class="btn btn-success btn-rounded waves-effect waves-light m-r-10"><i class="fa fa-edit m-r-5"></i>Guardar</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane" id="cuestionario">
                            <div class="row">
                                <div class="col-lg-12">
                                    <c:choose>
                                        <c:when test="${not empty anamnesis.codigoAnamnesis}">
                                            <form id="formeditAnamnesis" action="${pageContext.request.contextPath}/anamnesis.do" method="POST">
                                                <input type="hidden" name="op" value="cuestionario_1_update"/>
                                                <input type="hidden" name="id_historia_clinica" value="${historia.codigoHistoriaClinica}"/>
                                                <input type="hidden"name="id_paciente" value="${historia.paciente.codigoPaciente}"/>
                                                <table id="" class="table table_update">
                                                    <thead>
                                                        <tr>
                                                            <th>Cuestionario</th>
                                                            <th>Sí</th>
                                                            <th>No</th>
                                                            <th>Detalle</th>
                                                            <th>Alerta</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <tr>
                                                            <td>Diabetes</td>
                                                            <td><input type="radio" value="s" name="diabetes" ${anamnesis.diabetes == 's' ? 'checked' : ''}></td>
                                                            <td><input type="radio" value="n" name="diabetes" ${anamnesis.diabetes == 'n' ? 'checked' : ''}></td>
                                                            <td><textarea name="detalle_diabetes" rows="2" class="form-control">${anamnesis.detalle_diabetes}</textarea></td>
                                                            <td><input type="checkbox" name="alerta_diabetes" value="s" ${anamnesis.alerta_diabetes == 's' ? 'checked' : ''}/></td>
                                                        </tr>
                                                        <tr>
                                                            <td>Hipertensión arterial</td>
                                                            <td><input type="radio" value="s" name="hipertension_arterial" ${anamnesis.hipertension == 's' ? 'checked' : ''}></td>
                                                            <td><input type="radio" value="n" name="hipertension_arterial" ${anamnesis.hipertension == 'n' ? 'checked' : ''}></td>
                                                            <td><textarea name="detalle_hipertension_arterial" rows="2" class="form-control">${anamnesis.detalle_hipertension}</textarea></td>
                                                            <td><input type="checkbox" name="alerta_hipertension_arterial" value="s" ${anamnesis.alerta_hipertension == 's' ? 'checked' : ''}/></td>
                                                        </tr>
                                                        <tr>
                                                            <td>Hemorragia</td>
                                                            <td><input type="radio" value="s" name="hemorragia" ${anamnesis.hemorragia == 's' ? 'checked' : ''}></td>
                                                            <td><input type="radio" value="n" name="hemorragia" ${anamnesis.hemorragia == 'n' ? 'checked' : ''}></td>
                                                            <td><textarea name="detalle_hemorragia" rows="2" class="form-control">${anamnesis.detalle_hemorragia}</textarea></td>
                                                            <td><input type="checkbox" name="alerta_hemorragia" value="s" ${anamnesis.alerta_hemorragia == 's' ? 'checked' : ''}/></td>
                                                        </tr>
                                                        <tr>
                                                            <td>Alergias</td>
                                                            <td><input type="radio" value="s" name="alergia" ${anamnesis.alergia == 's' ? 'checked' : ''}></td>
                                                            <td><input type="radio" value="n" name="alergia" ${anamnesis.alergia == 'n' ? 'checked' : ''}></td>
                                                            <td><textarea name="detalle_alergia" rows="2" class="form-control">${anamnesis.detalle_alergia}</textarea></td>
                                                            <td><input type="checkbox" name="alerta_alergia" value="s" ${anamnesis.alerta_alergia == 's' ? 'checked' : ''}/></td>
                                                        </tr>
                                                        <tr>
                                                            <td>Síncope o desmayo</td>
                                                            <td><input type="radio" value="s" name="desmayo" ${anamnesis.desmayo == 's' ? 'checked' : ''}></td>
                                                            <td><input type="radio" value="n" name="desmayo" ${anamnesis.desmayo == 'n' ? 'checked' : ''}></td>
                                                            <td><textarea name="detalle_desmayo" rows="2" class="form-control">${anamnesis.detalle_desmayo}</textarea></td>
                                                            <td><input type="checkbox" name="alerta_desmayo" value="s" ${anamnesis.alerta_desmayo == 's' ? 'checked' : ''}/></td>
                                                        </tr>
                                                        <tr>
                                                            <td>Eenfermedad hepática</td>
                                                            <td><input type="radio" value="s" name="hepatica" ${anamnesis.hepatica == 's' ? 'checked' : ''}></td>
                                                            <td><input type="radio" value="n" name="hepatica" ${anamnesis.hepatica == 'n' ? 'checked' : ''}></td>
                                                            <td><textarea name="detalle_hepatica" rows="2" class="form-control">${anamnesis.detalle_hepatica}</textarea></td>
                                                            <td><input type="checkbox" name="alerta_hepatica" value="s" ${anamnesis.alerta_hepatica == 's' ? 'checked' : ''}/></td>
                                                        </tr>
                                                        <tr>
                                                            <td>Fiebre reumática</td>
                                                            <td><input type="radio" value="s" name="fiebre_reumatica" ${anamnesis.fiebre_reumatica == 's' ? 'checked' : ''}></td>
                                                            <td><input type="radio" value="n" name="fiebre_reumatica" ${anamnesis.fiebre_reumatica == 'n' ? 'checked' : ''}></td>
                                                            <td><textarea name="detalle_fiebre_reumatica" rows="2" class="form-control">${anamnesis.detalle_fiebre_reumatica}</textarea></td>
                                                            <td><input type="checkbox" name="alerta_fiebre_reumatica" value="s" ${anamnesis.alerta_fiebre_reumatica == 's' ? 'checked' : ''}/></td>
                                                        </tr>
                                                        <tr>
                                                            <td>Angina de pecho</td>
                                                            <td><input type="radio" value="s" name="angina_pecho" ${anamnesis.angina == 's' ? 'checked' : ''}></td>
                                                            <td><input type="radio" value="n" name="angina_pecho" ${anamnesis.angina == 'n' ? 'checked' : ''}></td>
                                                            <td><textarea name="detalle_angina_pecho" rows="2" class="form-control">${anamnesis.detalle_angina}</textarea></td>
                                                            <td><input type="checkbox" name="alerta_angina_pecho" value="s" ${anamnesis.alerta_angina == 's' ? 'checked' : ''}/></td>
                                                        </tr>
                                                        <tr>
                                                            <td>Infarto o miocardio</td>
                                                            <td><input type="radio" value="s" name="miocardio" ${anamnesis.miocardio == 's' ? 'checked' : ''}></td>
                                                            <td><input type="radio" value="n" name="miocardio" ${anamnesis.miocardio == 'n' ? 'checked' : ''}></td>
                                                            <td><textarea name="detalle_miocardio" rows="2" class="form-control">${anamnesis.detalle_miocardio}</textarea></td>
                                                            <td><input type="checkbox" name="alerta_miocardio" value="s" ${anamnesis.alerta_miocardio == 's' ? 'checked' : ''}/></td>
                                                        </tr> 
                                                        <tr>
                                                            <td>Embarazo</td>
                                                            <td><input type="radio" value="s" name="embarazo" ${anamnesis.embarazo == 's' ? 'checked' : ''}></td>
                                                            <td><input type="radio" value="n" name="embarazo" ${anamnesis.embarazo == 'n' ? 'checked' : ''}></td>
                                                            <td><textarea name="detalle_embarazo" rows="2" class="form-control">${anamnesis.detalle_embarazo}</textarea></td>
                                                            <td><input type="checkbox" name="alerta_embarazo" value="s" ${anamnesis.alerta_embarazo == 's' ? 'checked' : ''}/></td>
                                                        </tr> 
                                                    </tbody>
                                                </table>
                                                <div class="row text-center m-t-10">
                                                    <div class="col-md-12">
                                                        <button type="submit" class="btn btn-rounded btn-success"><i class="fa fa-edit m-r-5"></i>Actualizar</button>
                                                    </div>
                                                </div>
                                            </form>
                                        </c:when>    
                                        <c:otherwise>
                                            <form id="formeditAnamnesis" action="${pageContext.request.contextPath}/anamnesis.do" method="POST">
                                                <input type="hidden" name="op" value="cuestionario_1_insertar"/>
                                                <input type="hidden" name="id_historia_clinica" value="${historia.codigoHistoriaClinica}"/>
                                                <input type="hidden" name="id_paciente" value="${historia.paciente.codigoPaciente}"/>
                                                <table id="" class="table table_update">
                                                    <thead>
                                                        <tr>
                                                            <th>Cuestionario</th>
                                                            <th>Sí</th>
                                                            <th>No</th>
                                                            <th>Detalle</th>
                                                            <th>Alerta</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <tr>
                                                            <td>Diabetes</td>
                                                            <td><input type="radio" value="s" name="diabetes"></td>
                                                            <td><input type="radio" value="n" name="diabetes" checked></td>
                                                            <td><textarea name="detalle_diabetes" rows="2" class="form-control"></textarea></td>
                                                            <td><input type="checkbox" name="alerta_diabetes" value="s"/></td>
                                                        </tr>
                                                        <tr>
                                                            <td>Hipertensión arterial</td>
                                                            <td><input type="radio" value="s" name="hipertension_arterial"></td>
                                                            <td><input type="radio" value="n" name="hipertension_arterial" checked></td>
                                                            <td><textarea name="detalle_hipertension_arterial" rows="2" class="form-control"></textarea></td>
                                                            <td><input type="checkbox" name="alerta_hipertension_arterial" value="s"/></td>
                                                        </tr>
                                                        <tr>
                                                            <td>Hemorragia</td>
                                                            <td><input type="radio" value="s" name="hemorragia"></td>
                                                            <td><input type="radio" value="n" name="hemorragia" checked></td>
                                                            <td><textarea name="detalle_hemorragia" rows="2" class="form-control"></textarea></td>
                                                            <td><input type="checkbox" name="alerta_hemorragia" value="s"/></td>
                                                        </tr>
                                                        <tr>
                                                            <td>Alergias</td>
                                                            <td><input type="radio" value="s" name="alergia"></td>
                                                            <td><input type="radio" value="n" name="alergia" checked></td>
                                                            <td><textarea name="detalle_alergia" rows="2" class="form-control"></textarea></td>
                                                            <td><input type="checkbox" name="alerta_alergia" value="s"/></td>
                                                        </tr>
                                                        <tr>
                                                            <td>Síncope o desmayo</td>
                                                            <td><input type="radio" value="s" name="desmayo"></td>
                                                            <td><input type="radio" value="n" name="desmayo" checked></td>
                                                            <td><textarea name="detalle_desmayo" rows="2" class="form-control"></textarea></td>
                                                            <td><input type="checkbox" name="alerta_desmayo" value="s"/></td>
                                                        </tr>
                                                        <tr>
                                                            <td>Eenfermedad hepática</td>
                                                            <td><input type="radio" value="s" name="hepatica"></td>
                                                            <td><input type="radio" value="n" name="hepatica" checked></td>
                                                            <td><textarea name="detalle_hepatica" rows="2" class="form-control"></textarea></td>
                                                            <td><input type="checkbox" name="alerta_hepatica" value="s"/></td>
                                                        </tr>
                                                        <tr>
                                                            <td>Fiebre reumática</td>
                                                            <td><input type="radio" value="s" name="fiebre_reumatica"></td>
                                                            <td><input type="radio" value="n" name="fiebre_reumatica" checked></td>
                                                            <td><textarea name="detalle_fiebre_reumatica" rows="2" class="form-control"></textarea></td>
                                                            <td><input type="checkbox" name="alerta_fiebre_reumatica" value="s"/></td>
                                                        </tr>
                                                        <tr>
                                                            <td>Angina de pecho</td>
                                                            <td><input type="radio" value="s" name="angina_pecho"></td>
                                                            <td><input type="radio" value="n" name="angina_pecho" checked></td>
                                                            <td><textarea name="detalle_angina_pecho" rows="2" class="form-control"></textarea></td>
                                                            <td><input type="checkbox" name="alerta_angina_pecho" value="s"/></td>
                                                        </tr>
                                                        <tr>
                                                            <td>Infarto o miocardio</td>
                                                            <td><input type="radio" value="s" name="miocardio"></td>
                                                            <td><input type="radio" value="n" name="miocardio" checked></td>
                                                            <td><textarea name="detalle_miocardio" rows="2" class="form-control"></textarea></td>
                                                            <td><input type="checkbox" name="alerta_miocardio" value="s"/></td>
                                                        </tr> 
                                                        <tr>
                                                            <td>Embarazo</td>
                                                            <td><input type="radio" value="s" name="embarazo"></td>
                                                            <td><input type="radio" value="n" name="embarazo" checked></td>
                                                            <td><textarea name="detalle_embarazo" rows="2" class="form-control"></textarea></td>
                                                            <td><input type="checkbox" name="alerta_embarazo" value="s"/></td>
                                                        </tr> 
                                                    </tbody>
                                                </table>
                                                <div class="row text-center m-t-10">
                                                    <div class="col-md-12">
                                                        <button type="submit" class="btn btn-rounded btn-success"><i class="fa fa-edit m-r-5"></i>Guardar</button>
                                                    </div>
                                                </div>
                                            </form>
                                        </c:otherwise>
                                    </c:choose>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div id="examen_clinico" class="tab-pane">
                    <ul class="nav nav-tabs tabs customtab">
                        <li class="active tab">
                            <a href="#signos_vitales" data-toggle="tab"> <span class="visible-xs"><i class="fa fa-home"></i></span> <span class="hidden-xs">Signos vitales</span> </a>
                        </li>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane active" id="signos_vitales">
                            <div class="row">
                                <div class="col-sm-12 col-xs-12">
                                    <c:choose>
                                        <c:when test="${not empty examen.codigoExamenFisico}">
                                            <form data-toggle="validator" action="${pageContext.request.contextPath}/historias.do" method="POST">
                                                <input type="hidden" name="op" value="modificar_examen_fisico"/>
                                                <input type="hidden" name="id_examen_fisico" value="${examen.codigoExamenFisico}"/>
                                                <input type="hidden" name="id_historia_clinica" value="${historia.codigoHistoriaClinica}"/>
                                                <div class="row">
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label>Peso</label>
                                                            <div class="input-group">
                                                                <div class="input-group-addon"><i class="mdi mdi-weight-kilogram"></i></div>
                                                                <input id="peso" name="peso" data-error="Campo requerido" pattern="[0-9]+([\.,][0-9]+)?" value="${examen.peso}" class="form-control" placeholder="Peso" onkeyup="calcular_imc();" required>
                                                                <span class="input-group-addon">Kg</span>
                                                            </div>
                                                            <div class="help-block with-errors"></div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label>Talla</label>
                                                            <div class="input-group">
                                                                <div class="input-group-addon"><i class="mdi mdi-ruler"></i></div>
                                                                <input id="talla" name="talla" data-error= "Campo requerido" pattern="[0-9]+([\.,][0-9]+)?" value="${examen.talla}" class="form-control" placeholder="Talla" onkeyup="calcular_imc();" required>
                                                                <span class="input-group-addon">m</span>
                                                            </div>
                                                            <div class="help-block with-errors"></div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label>IMC</label>
                                                            <div class="input-group">
                                                                <div class="input-group-addon"><i class="ti-file"></i></div>
                                                                <input id="imc" name="imc" data-error= "Campo requerido" type="text" value="${examen.imc}" class="form-control" placeholder="Índice de masa corporal" readonly>
                                                                <span class="input-group-addon">Kg/m2</span>
                                                            </div>
                                                            <div class="help-block with-errors"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label>Presión arterial máxima</label>
                                                            <div class="input-group">
                                                                <div class="input-group-addon"><i class="fa fa-stethoscope"></i></div>
                                                                <input id="presion_arterial_maxima" name="presion_arterial_maxima" data-error= "Campo requerido" pattern="[0-9]+([\.,][0-9]+)?" value="${examen.presion_arterial_maxima}" class="form-control" placeholder="Presión arterial máxima" onkeyup="calcular_frecuencia_respiratoria();" required>
                                                                <span class="input-group-addon">mmHg</span>
                                                            </div>
                                                            <div class="help-block with-errors"></div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label>Presión arterial mínima</label>
                                                            <div class="input-group">
                                                                <div class="input-group-addon"><i class="fa fa-stethoscope"></i></div>
                                                                <input id="presion_arterial_minima" name="presion_arterial_minima" data-error= "Campo requerido" pattern="[0-9]+([\.,][0-9]+)?" value="${examen.presion_arterial_minima}" class="form-control" placeholder="Presión arterial mínima" onkeyup="calcular_frecuencia_respiratoria();" required>
                                                                <span class="input-group-addon">mmHg</span>
                                                            </div>
                                                            <div class="help-block with-errors"></div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label>Frecuencia respiratoria</label>
                                                            <div class="input-group">
                                                                <div class="input-group-addon"><i class="fa fa-stethoscope"></i></div>
                                                                <input id="frecuencia_respiratoria" name="frecuencia_respiratoria" data-error="Campo requerido" type="text" value="${examen.frecuencia_respiratoria}" class="form-control" placeholder="Frecuencia respiratoria" readonly>
                                                                <span class="input-group-addon">mmHg</span>
                                                            </div>
                                                            <div class="help-block with-errors"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label>Pulso</label>
                                                            <div class="input-group">
                                                                <div class="input-group-addon"><i class="mdi mdi-heart-pulse"></i></div>
                                                                <input name="pulso" data-error="Campo requerido" type="number" value="${examen.pulso}" class="form-control" placeholder="Pulso cardíaco" required>
                                                                <span class="input-group-addon">lat/min</span>
                                                            </div>
                                                            <div class="help-block with-errors"></div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label>Grupo sanguíneo</label>
                                                            <div class="input-group">
                                                                <div class="input-group-addon"><i class="fa fa-heart"></i></div>
                                                                <select name="grupo_sanguineo" data-error= "Campo requerido" class="form-control" required="true">
                                                                    <option value="O-" ${examen.grupo_sanguineo == "O-" ? 'selected="selected"' : ''}>O -</option>
                                                                    <option value="O+" ${examen.grupo_sanguineo == "O+" ? 'selected="selected"' : ''}>O +</option>
                                                                    <option value="A-" ${examen.grupo_sanguineo == "A-" ? 'selected="selected"' : ''}>A -</option>
                                                                    <option value="A+" ${examen.grupo_sanguineo == "A+" ? 'selected="selected"' : ''}>A +</option>
                                                                    <option value="B-" ${examen.grupo_sanguineo == "B-" ? 'selected="selected"' : ''}>B -</option>
                                                                    <option value="B+" ${examen.grupo_sanguineo == "B+" ? 'selected="selected"' : ''}>B +</option>
                                                                    <option value="AB-" ${examen.grupo_sanguineo == "AB-" ? 'selected="selected"' : ''}>AB -</option>
                                                                    <option value="AB+" ${examen.grupo_sanguineo == "AB+" ? 'selected="selected"' : ''}>AB +</option>
                                                                </select>
                                                            </div>
                                                            <div class="help-block with-errors"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-12 text-center">
                                                        <button type="submit" class="btn btn-success btn-rounded waves-effect waves-light m-r-10"><i class="fa fa-edit m-r-5"></i>Actualizar</button>
                                                    </div>
                                                </div>
                                            </form>
                                        </c:when>
                                        <c:otherwise>
                                            <form data-toggle="validator" action="${pageContext.request.contextPath}/historias.do" method="POST">
                                                <input type="hidden" name="op" value="insertar_examen_fisico"/>
                                                <input type="hidden" name="id_historia_clinica" value="${historia.codigoHistoriaClinica}"/>
                                                <div class="row">
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label>Peso</label>
                                                            <div class="input-group">
                                                                <div class="input-group-addon"><i class="mdi mdi-weight-kilogram"></i></div>
                                                                <input id="peso" name="peso" data-error="Campo requerido" type="text" pattern="[0-9]+([\.,][0-9]+)?" value="" class="form-control" placeholder="Peso" onkeyup="calcular_imc();" required>
                                                                <span class="input-group-addon">Kg</span>
                                                            </div>
                                                            <div class="help-block with-errors"></div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label>Talla</label>
                                                            <div class="input-group">
                                                                <div class="input-group-addon"><i class="mdi mdi-ruler"></i></div>
                                                                <input id="talla" name="talla" data-error= "Campo requerido" type="text" pattern="[0-9]+([\.,][0-9]+)?" value="" class="form-control" placeholder="Talla" onkeyup="calcular_imc();" required>
                                                                <span class="input-group-addon">m</span>
                                                            </div>
                                                            <div class="help-block with-errors"></div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label>IMC</label>
                                                            <div class="input-group">
                                                                <div class="input-group-addon"><i class="ti-file"></i></div>
                                                                <input id="imc" name="imc" data-error= "Campo requerido" type="text" pattern="[0-9]+([\.,][0-9]+)?" value="" class="form-control" placeholder="Índice de masa corporal" readonly="true">
                                                                <span class="input-group-addon">Kg/m2</span>
                                                            </div>
                                                            <div class="help-block with-errors"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label>Presión arterial máxima</label>
                                                            <div class="input-group">
                                                                <div class="input-group-addon"><i class="fa fa-stethoscope"></i></div>
                                                                <input id="presion_arterial_maxima" name="presion_arterial_maxima" data-error= "Campo requerido" pattern="[0-9]+([\.,][0-9]+)?" value="" class="form-control" placeholder="Presión arterial máxima" onkeyup="calcular_frecuencia_respiratoria();" required>
                                                                <span class="input-group-addon">mmHg</span>
                                                            </div>
                                                            <div class="help-block with-errors"></div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label>Presión arterial mínima</label>
                                                            <div class="input-group">
                                                                <div class="input-group-addon"><i class="fa fa-stethoscope"></i></div>
                                                                <input id="presion_arterial_minima" name="presion_arterial_minima" data-error= "Campo requerido" pattern="[0-9]+([\.,][0-9]+)?" value="" class="form-control" placeholder="Presión arterial mínima" onkeyup="calcular_frecuencia_respiratoria();" required>
                                                                <span class="input-group-addon">mmHg</span>
                                                            </div>
                                                            <div class="help-block with-errors"></div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label>Frecuencia respiratoria</label>
                                                            <div class="input-group">
                                                                <div class="input-group-addon"><i class="fa fa-stethoscope"></i></div>
                                                                <input id="frecuencia_respiratoria" name="frecuencia_respiratoria" data-error="Campo requerido" type="text" value="" class="form-control" placeholder="Frecuencia respiratoria" readonly="true">
                                                                <span class="input-group-addon">mmHg</span>
                                                            </div>
                                                            <div class="help-block with-errors"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label>Pulso</label>
                                                            <div class="input-group">
                                                                <div class="input-group-addon"><i class="mdi mdi-heart-pulse"></i></div>
                                                                <input name="pulso" data-error="Campo requerido" type="number" value="" class="form-control" placeholder="Pulso cardíaco" required>
                                                                <span class="input-group-addon">lat/min</span>
                                                            </div>
                                                            <div class="help-block with-errors"></div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label>Grupo sanguíneo</label>
                                                            <div class="input-group">
                                                                <div class="input-group-addon"><i class="fa fa-heart"></i></div>
                                                                <select name="grupo_sanguineo" data-error= "Campo requerido" class="form-control" required="true">
                                                                    <option value="" selected>Selecione una opción</option>
                                                                    <option value="O-">O -</option>
                                                                    <option value="O+">O +</option>
                                                                    <option value="A-">A -</option>
                                                                    <option value="A+">A +</option>
                                                                    <option value="B-">B -</option>
                                                                    <option value="B+">B +</option>
                                                                    <option value="AB-">AB -</option>
                                                                    <option value="AB+">AB +</option>
                                                                </select>
                                                            </div>
                                                            <div class="help-block with-errors"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-12 text-center">
                                                        <button type="submit" class="btn btn-success btn-rounded waves-effect waves-light m-r-10"><i class="fa fa-edit m-r-5"></i>Guardar</button>
                                                    </div>
                                                </div>
                                            </form>
                                        </c:otherwise>
                                    </c:choose>
                                </div>                                   
                            </div>
                        </div>
                    </div>
                </div>

                <div id="odontograma" class="tab-pane active">
                    <ul class="nav nav-tabs tabs customtab">
                        <li class="active tab">
                            <a href="#listado_odontograma" data-toggle="tab"> <span class="visible-xs"><i class="fa fa-home"></i></span> <span class="hidden-xs">Historial de Odontogramas</span> </a>
                        </li>
                    </ul>
                    <br>
                    <div class="col-sm-12 col-xs-12 col-md-12 text-right">
                        <a type="button" class="btn btn-info btn-rounded" data-toggle="modal" data-target="#nuevo_odontograma">Nuevo odontograma</a>
                    </div>
                    <div id="nuevo_odontograma" class="modal fade in" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                    <center>
                                        <h3 class="box-title m-b-0" id="myModalLabel">Nuevo Odontograma</h3>
                                    </center>
                                </div>
                                <div class="modal-body">
                                    <form>
                                        <div class="row">
                                            <div class="col-md-12 text-center">
                                                <div class="form-group">
                                                    <h4>¿Cómo desea crear el odontograma?</h4>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6 text-center">
                                                <a type="button" class="btn btn-info btn-rounded" href="${pageContext.request.contextPath}/historias.do?op=odontograma&id=${historia.codigoHistoriaClinica}">En blanco</a>
                                            </div>
                                            <div class="col-md-6 text-center">
                                                <a type="button" class="btn btn-info btn-rounded" href="${pageContext.request.contextPath}/historias.do?op=obtenerAnteriorOdontograma&id=${ultimoOdontogramaHistoria}">Basado en el anterior</a>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                    <br>
                    <div class="tab-content">
                        <div class="tab-pane active" id="historial_citas">
                            <div class="row">
                                <div class="col-lg-12">
                                    <table id="data_odontogramas" class="display" style="width:100%">
                                        <thead>
                                            <tr>
                                                <th>Id</th>
                                                <th>Fecha</th>
                                                <th>Observaciones</th>
                                                <th>Operaciones</th>
                                            </tr>
                                        </thead>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>        

                <div id="examen_radiografico" class="tab-pane">
                    <ul class="nav nav-tabs tabs customtab">
                        <li class="active tab">
                            <a href="#listado_radiografias" data-toggle="tab"> <span class="visible-xs"><i class="fa fa-home"></i></span> <span class="hidden-xs">Historial de Radiografías</span> </a>
                        </li>
                    </ul>
                    <br>
                    <div class="col-sm-12 col-xs-12 col-md-12 text-right">
                        <a href="javascript:crearRadiografia()" class="btn btn-info btn-rounded">Nueva radiografía</a>
                    </div>
                    <div id="ver_radiografia" class="modal fade in" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                    <center>
                                        <h3 class="box-title m-b-0" id="myModalLabel">Radiografía</h3>
                                    </center>
                                </div>
                                <div class="modal-body">
                                    <div class="row el-element-overlay m-b-40">
                                        <div class="col-lg-3 col-md-4 col-sm-6 col-xs-12">
                                            <div class="white-box">
                                                <div class="el-card-item">
                                                    <div class="el-card-avatar el-overlay-1"> <img id="img_radiografia" src=""/>
                                                        <div class="el-overlay">
                                                            <ul class="el-info">
                                                                <li><a id="href_radiografia" class="btn default btn-outline image-popup-vertical-fit" href="#"><i class="icon-magnifier"></i></a></li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="modal_crearRadiografia" class="modal fade in" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                        <div class="modal-dialog modal-lg">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button id="btn_cerrar_modal" type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                    <center>
                                        <h3 class="box-title m-b-0" id="myModalLabel">Nueva Radiografía</h3>
                                    </center>
                                </div>
                                <div class="modal-body">
                                    <form data-toggle="validator" id="radiografiaFormNew" method="POST" enctype="multipart/form-data">
                                        <input type="hidden" name="id_historia_clinica" value="${historia.codigoHistoriaClinica}"/>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label>Foto</label>
                                                    <div class="input-group">
                                                        <div class="input-group-addon"><i class="ti-user"></i></div>
                                                        <input name="foto" data-error="Campo requerido" type="file" class="form-control" required>
                                                    </div>
                                                    <div class="help-block with-errors"></div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label>Observaciones</label>
                                                    <textarea name="observaciones" data-error= "Campo requerido" rows="3" class="form-control" required></textarea>
                                                    <div class="help-block with-errors"></div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12 text-center">
                                                <button id="btn_new_radiografia" data-loading-text='<i class="fa fa-spin fa-spinner"></i> Espere...' type="submit" class="btn btn-success btn-rounded waves-effect waves-light m-r-10">Guardar</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="modal_editarRadiografia" class="modal fade in" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                        <div class="modal-dialog modal-lg">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button id="btn_cerrar_modal" type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                    <center>
                                        <h3 class="box-title m-b-0" id="myModalLabel">Editar Radiografía</h3>
                                    </center>
                                </div>
                                <div class="modal-body">
                                    <form data-toggle="validator" id="radiografiaFormEdit" method="POST" enctype="multipart/form-data">
                                        <input type="hidden" name="update_id_historia_clinica" value="${historia.codigoHistoriaClinica}"/>
                                        <input type="hidden" name="update_id_examen_radiografico" id="update_id_examen_radiografico" value=""/>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label>Foto</label>
                                                    <div class="input-group">
                                                        <div class="input-group-addon"><i class="ti-user"></i></div>
                                                        <input name="update_foto" id="update_foto" type="file" class="form-control">
                                                    </div>
                                                    <div class="help-block with-errors"></div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label>Observaciones</label>
                                                    <textarea name="update_observaciones" id="update_observaciones" data-error= "Campo requerido" rows="3" class="form-control" required></textarea>
                                                    <div class="help-block with-errors"></div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12 text-center">
                                                <button id="btn_update_radiografia" data-loading-text='<i class="fa fa-spin fa-spinner"></i> Espere...' type="submit" class="btn btn-success btn-rounded waves-effect waves-light m-r-10">Guardar</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                    <br>
                    <div class="tab-content">
                        <div class="tab-pane active" id="historial_radiografias">
                            <div class="row">
                                <div class="col-lg-12">
                                    <table id="data_radiografias" class="display" style="width:100%">
                                        <thead>
                                            <tr>
                                                <th>Id</th>
                                                <th>Foto</th>
                                                <th>Observaciones</th>
                                                <th>Operaciones</th>
                                            </tr>
                                        </thead>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div id="video_tratamiento" class="tab-pane">
                    <ul class="nav nav-tabs tabs customtab">
                        <li class="active tab">
                            <a href="#listado_videos" data-toggle="tab"> <span class="visible-xs"><i class="fa fa-home"></i></span> <span class="hidden-xs">Historial de Videos</span> </a>
                        </li>
                    </ul>
                    <br>
                    <div class="col-sm-12 col-xs-12 col-md-12 text-right">
                        <a href="javascript:crearVideo()" class="btn btn-info btn-rounded">Nuevo video</a>
                    </div>
                    <div id="ver_video" class="modal fade in" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                    <center>
                                        <h3 class="box-title m-b-0" id="myModalLabel">Video</h3>
                                    </center>
                                </div>
                                <div class="modal-body">
                                    <video 
                                        id="video-js"
                                        class="video-js"
                                        controls
                                        preload="auto"
                                        width="640"
                                        height="264"
                                        data-setup="{}"
                                        >
                                        <p class="vjs-no-js">
                                            Error
                                            <a href="https://videojs.com/html5-video-support/" target="_blank">Soporte HTML5 video</a>
                                        </p>
                                    </video>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="modal_crearVideo" class="modal fade in" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                        <div class="modal-dialog modal-lg">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button id="btn_cerrar_modal" type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                    <center>
                                        <h3 class="box-title m-b-0" id="myModalLabel">Nuevo Video</h3>
                                    </center>
                                </div>
                                <div class="modal-body">
                                    <form data-toggle="validator" id="videoFormNew" method="POST" enctype="multipart/form-data">
                                        <input type="hidden" name="id_historia_clinica" value="${historia.codigoHistoriaClinica}"/>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label>Nombre</label>
                                                    <input name="nombre" data-error="Campo requerido" type="text" class="form-control" required>
                                                    <div class="help-block with-errors"></div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label>Video</label>
                                                    <div class="input-group">
                                                        <div class="input-group-addon"><i class="ti-user"></i></div>
                                                        <input name="ruta" data-error="Campo requerido" type="file" class="form-control" required>
                                                    </div>
                                                    <div class="help-block with-errors"></div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label>Observaciones</label>
                                                    <textarea name="observaciones" data-error= "Campo requerido" rows="3" class="form-control" required></textarea>
                                                    <div class="help-block with-errors"></div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12 text-center">
                                                <button id="btn_new_video" data-loading-text='<i class="fa fa-spin fa-spinner"></i> Espere...' type="submit" class="btn btn-success btn-rounded waves-effect waves-light m-r-10">Guardar</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="modal_editarVideo" class="modal fade in" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                        <div class="modal-dialog modal-lg">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button id="btn_cerrar_modal" type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                    <center>
                                        <h3 class="box-title m-b-0" id="myModalLabel">Editar Video</h3>
                                    </center>
                                </div>
                                <div class="modal-body">
                                    <form data-toggle="validator" id="videoFormEdit" method="POST" enctype="multipart/form-data">
                                        <input type="hidden" name="update_id_historia_clinica" value="${historia.codigoHistoriaClinica}"/>
                                        <input type="hidden" name="update_id_video" id="update_id_video" value=""/>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label>Nombre</label>
                                                    <input name="update_nombre" id="update_nombre" data-error="Campo requerido" type="text" class="form-control" required>
                                                    <div class="help-block with-errors"></div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label>Foto</label>
                                                    <div class="input-group">
                                                        <div class="input-group-addon"><i class="ti-user"></i></div>
                                                        <input name="update_ruta" id="update_ruta" type="file" class="form-control">
                                                    </div>
                                                    <div class="help-block with-errors"></div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label>Observaciones</label>
                                                    <textarea name="update_observaciones_video" id="update_observaciones_video" data-error= "Campo requerido" rows="3" class="form-control" required></textarea>
                                                    <div class="help-block with-errors"></div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12 text-center">
                                                <button id="btn_update_video" data-loading-text='<i class="fa fa-spin fa-spinner"></i> Espere...' type="submit" class="btn btn-success btn-rounded waves-effect waves-light m-r-10">Guardar</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                    <br>
                    <div class="tab-content">
                        <div class="tab-pane active" id="historial_videos">
                            <div class="row">
                                <div class="col-lg-12">
                                    <table id="data_videos" class="display" style="width:100%">
                                        <thead>
                                            <tr>
                                                <th>Id</th>
                                                <th>Nombre</th>
                                                <th>Observaciones</th>
                                                <th>Operaciones</th>
                                            </tr>
                                        </thead>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div id="presupuesto" class="tab-pane">
                    <ul class="nav nav-tabs tabs customtab">
                        <li class="active tab">
                            <a href="#listado_presupuestos" data-toggle="tab"> <span class="visible-xs"><i class="fa fa-home"></i></span> <span class="hidden-xs">Historial de Presupuesto</span> </a>
                        </li>
                    </ul>
                    <br>
                    <div class="col-sm-12 col-xs-12 col-md-12 text-right">
                        <a href="javascript:crearPresupuesto()" class="btn btn-info btn-rounded">Nuevo Presupuesto</a>
                    </div>
                    <div id="modal_crearPresupuesto" class="modal fade in" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                        <div class="modal-dialog modal-lg">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button id="btn_cerrar_modal" type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                    <center>
                                        <h3 class="box-title m-b-0" id="myModalLabel">Nuevo Presupuesto</h3>
                                    </center>
                                </div>
                                <div class="modal-body">
                                    <form data-toggle="validator" id="presupuestoFormNew" method="POST">
                                        <input type="hidden" id="presupuesto_id_historia_clinica" name="id_historia_clinica" value="${historia.codigoHistoriaClinica}"/>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label>Médico</label>
                                                    <div class="input-group">
                                                        <div class="input-group-addon"><i class="fa fa-heart-o"></i></div>
                                                        <select id="presupuesto_id_medico" name="id_medico" class="form-control" style="width: 100%" required="">
                                                            <option value="" selected="true">Seleccione una opción</option>
                                                            <c:forEach items="${requestScope.listaMedicos}" var="medico">
                                                                <option value="${medico.idMedico}">${medico.nombresMedico} ${medico.apellidosMedico}</option>
                                                            </c:forEach>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label>Asunto</label>
                                                    <div class="input-group">
                                                        <div class="input-group-addon"><i class="fa fa-info-circle"></i></div>
                                                        <input id="asunto" name="asunto" data-error="Campo requerido" type="text" class="form-control" style="text-transform:uppercase" required>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label>Odontogramas</label>
                                                    <div class="input-group">
                                                        <div class="input-group-addon"><i class="fa fa-life-ring"></i></div>
                                                        <select id="select2_odontogramas" name="id_odontograma" class="form-control" style="width: 100%" required="true">
                                                            <option value="" selected="true">Seleccione un odontograma</option>
                                                            <c:forEach items="${requestScope.listaOdontogramas}" var="odontograma">
                                                                <option value="${odontograma.codigoOdontograma}">${odontograma.codigoOdontograma} -> ${odontograma.date_created}</option>
                                                            </c:forEach>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label>Tipo de pago</label>
                                                    <div class="input-group">
                                                        <div class="input-group-addon"><i class="fa fa-money"></i></div>
                                                        <select id="tipo_pago" name="tipo_pago" class="form-control" required="true">
                                                            <option value="" selected="true">Seleccione un tipo de pago</option>
                                                            <option value="contado">Al contado</option>
                                                            <option value="cuotas">En cuotas</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label>Total</label>
                                                    <div class="input-group">
                                                        <div class="input-group-addon">S/</div>
                                                        <input id="total" name="total" data-error="Campo requerido" type="text" class="form-control" value="0.00" required="true" readonly="true">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <table id="data_servicio" class="display">
                                            <thead>
                                                <tr>
                                                    <th>Pieza</th>
                                                    <th>Hallazgo</th>
                                                    <th>Tratamiento</th>
                                                    <th style="text-align: center">Cantidad</th>
                                                    <th>Precio</th>
                                                    <th>Total</th>
                                                    <th>Acciones</th>
                                                </tr>
                                            </thead>
                                        </table>
                                        <div class="row">
                                            <div class="col-md-12 text-center">
                                                <button id="btn_new_presupuesto" data-loading-text='<i class="fa fa-spin fa-spinner"></i> Espere...' type="submit" class="btn btn-success btn-rounded waves-effect waves-light m-r-10">Guardar</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div id="modal_editarPresupuesto" class="modal fade in" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                        <div class="modal-dialog modal-lg">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button id="btn_cerrar_modal" type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                    <center>
                                        <h3 class="box-title m-b-0" id="myModalLabel">Editar Presupuesto</h3>
                                    </center>
                                </div>
                                <div class="modal-body">
                                    <form data-toggle="validator" id="presupuestoFormEdit" method="POST">
                                        <input type="hidden" id="update_id_presupuesto"/>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label>Médico</label>
                                                    <div class="input-group">
                                                        <div class="input-group-addon"><i class="fa fa-heart-o"></i></div>
                                                        <select id="update_presupuesto_id_medico" name="id_medico" class="form-control" style="width: 100%" required="">
                                                            <option value="" selected="true">Seleccione una opción</option>
                                                            <c:forEach items="${requestScope.listaMedicos}" var="medico">
                                                                <option value="${medico.idMedico}">${medico.nombresMedico} ${medico.apellidosMedico}</option>
                                                            </c:forEach>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label>Asunto</label>
                                                    <div class="input-group">
                                                        <div class="input-group-addon"><i class="fa fa-info-circle"></i></div>
                                                        <input id="update_asunto" name="asunto" data-error="Campo requerido" type="text" class="form-control" style="text-transform:uppercase" required>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label>Odontogramas</label>
                                                    <div class="input-group">
                                                        <div class="input-group-addon"><i class="fa fa-life-ring"></i></div>
                                                        <select id="update_select2_odontogramas" name="id_odontograma" class="form-control" style="width: 100%" required="true">
                                                            <option value="" selected="true">Seleccione un odontograma</option>
                                                            <c:forEach items="${requestScope.listaOdontogramas}" var="odontograma">
                                                                <option value="${odontograma.codigoOdontograma}">${odontograma.codigoOdontograma} -> ${odontograma.date_created}</option>
                                                            </c:forEach>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label>Tipo de pago</label>
                                                    <div class="input-group">
                                                        <div class="input-group-addon"><i class="fa fa-money"></i></div>
                                                        <select id="update_tipo_pago" name="tipo_pago" class="form-control" required="true">
                                                            <option value="" selected="true">Seleccione un tipo de pago</option>
                                                            <option value="contado">Al contado</option>
                                                            <option value="cuotas">En cuotas</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label>Total</label>
                                                    <div class="input-group">
                                                        <div class="input-group-addon">S/</div>
                                                        <input id="update_total" name="total" data-error="Campo requerido" type="text" class="form-control" value="0.00" required="true" readonly="true">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <table id="update_data_servicio" class="display">
                                            <thead>
                                                <tr>
                                                    <th>Pieza</th>
                                                    <th>Hallazgo</th>
                                                    <th>Tratamiento</th>
                                                    <th style="text-align: center">Cantidad</th>
                                                    <th>Precio</th>
                                                    <th>Total</th>
                                                    <th>Acciones</th>
                                                </tr>
                                            </thead>
                                        </table>
                                        <div class="row">
                                            <div class="col-md-12 text-center">
                                                <button id="btn_edit_presupuesto" data-loading-text='<i class="fa fa-spin fa-spinner"></i> Espere...' type="submit" class="btn btn-success btn-rounded waves-effect waves-light m-r-10">Actualizar</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>

                    <br>
                    <div class="tab-content">
                        <div class="tab-pane active" id="historial_presupuestos">
                            <div class="row">
                                <div class="col-lg-12">
                                    <table id="data_presupuestos" class="display" style="width:100%">
                                        <thead>
                                            <tr>
                                                <th>Id</th>
                                                <th>Asunto</th>
                                                <th>Fecha</th>
                                                <th>Total</th>
                                                <th>Tipo pago</th>
                                                <th>Estado</th>
                                                <th>Operaciones</th>
                                            </tr>
                                        </thead>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div> 

                <div id="citas" class="tab-pane">
                    <ul class="nav nav-tabs tabs customtab">
                        <li class="active tab">
                            <a href="#historial_citas" data-toggle="tab"> <span class="visible-xs"><i class="fa fa-home"></i></span> <span class="hidden-xs">Historial de citas</span> </a>
                        </li>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane active" id="historial_citas">
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="white-box">
                                        <table id="data_citas" class="display" style="width:100%">
                                            <thead>
                                                <tr>
                                                    <th> Id </th>
                                                    <th> Tratamiento </th>
                                                    <th> Médico </th>
                                                    <th> Fecha </th>
                                                    <th> Estado </th>
                                                </tr>
                                            </thead>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>

<script>

    var data_anamnesis_edit = $("#formeditAnamnesis").serializeArray();
    console.log(data_anamnesis_edit);

    $('input[type=radio][name=' + data_anamnesis_edit[3].name + ']').change(function () {
        if ($('input:radio[name=' + data_anamnesis_edit[3].name + ']:checked').val() == 's') {
            $('textarea[name=' + data_anamnesis_edit[4].name + ']').prop('disabled', false);
        } else {
            $('textarea[name=' + data_anamnesis_edit[4].name + ']').prop('disabled', true);
        }
        return false;
    });

    $('input[type=radio][name=' + data_anamnesis_edit[5].name + ']').change(function () {
        if ($('input:radio[name=' + data_anamnesis_edit[5].name + ']:checked').val() == 's') {
            $('textarea[name=' + data_anamnesis_edit[6].name + ']').prop('disabled', false);
        } else {
            $('textarea[name=' + data_anamnesis_edit[6].name + ']').prop('disabled', true);
        }
        return false;
    });

    $('input[type=radio][name=' + data_anamnesis_edit[7].name + ']').change(function () {
        if ($('input:radio[name=' + data_anamnesis_edit[7].name + ']:checked').val() == 's') {
            $('textarea[name=' + data_anamnesis_edit[8].name + ']').prop('disabled', false);
        } else {
            $('textarea[name=' + data_anamnesis_edit[8].name + ']').prop('disabled', true);
        }
        return false;
    });

    $('input[type=radio][name=' + data_anamnesis_edit[9].name + ']').change(function () {
        if ($('input:radio[name=' + data_anamnesis_edit[9].name + ']:checked').val() == 's') {
            $('textarea[name=' + data_anamnesis_edit[10].name + ']').prop('disabled', false);
        } else {
            $('textarea[name=' + data_anamnesis_edit[10].name + ']').prop('disabled', true);
        }
        return false;
    });

    $('input[type=radio][name=' + data_anamnesis_edit[11].name + ']').change(function () {
        if ($('input:radio[name=' + data_anamnesis_edit[11].name + ']:checked').val() == 's') {
            $('textarea[name=' + data_anamnesis_edit[12].name + ']').prop('disabled', false);
        } else {
            $('textarea[name=' + data_anamnesis_edit[12].name + ']').prop('disabled', true);
        }
        return false;
    });

    $('input[type=radio][name=' + data_anamnesis_edit[13].name + ']').change(function () {
        if ($('input:radio[name=' + data_anamnesis_edit[13].name + ']:checked').val() == 's') {
            $('textarea[name=' + data_anamnesis_edit[14].name + ']').prop('disabled', false);
        } else {
            $('textarea[name=' + data_anamnesis_edit[14].name + ']').prop('disabled', true);
        }
        return false;
    });

    $('input[type=radio][name=' + data_anamnesis_edit[15].name + ']').change(function () {
        if ($('input:radio[name=' + data_anamnesis_edit[15].name + ']:checked').val() == 's') {
            $('textarea[name=' + data_anamnesis_edit[16].name + ']').prop('disabled', false);
        } else {
            $('textarea[name=' + data_anamnesis_edit[16].name + ']').prop('disabled', true);
        }
        return false;
    });

    $('input[type=radio][name=' + data_anamnesis_edit[17].name + ']').change(function () {
        if ($('input:radio[name=' + data_anamnesis_edit[17].name + ']:checked').val() == 's') {
            $('textarea[name=' + data_anamnesis_edit[18].name + ']').prop('disabled', false);
        } else {
            $('textarea[name=' + data_anamnesis_edit[18].name + ']').prop('disabled', true);
        }
        return false;
    });

    $('input[type=radio][name=' + data_anamnesis_edit[19].name + ']').change(function () {
        if ($('input:radio[name=' + data_anamnesis_edit[19].name + ']:checked').val() == 's') {
            $('textarea[name=' + data_anamnesis_edit[20].name + ']').prop('disabled', false);
        } else {
            $('textarea[name=' + data_anamnesis_edit[20].name + ']').prop('disabled', true);
        }
        return false;
    });

    $('input[type=radio][name=' + data_anamnesis_edit[21].name + ']').change(function () {
        if ($('input:radio[name=' + data_anamnesis_edit[21].name + ']:checked').val() == 's') {
            $('textarea[name=' + data_anamnesis_edit[22].name + ']').prop('disabled', false);
        } else {
            $('textarea[name=' + data_anamnesis_edit[22].name + ']').prop('disabled', true);
        }
        return false;
    });

    for (var i = 3; i < data_anamnesis_edit.length - 1; i++) {
        if (data_anamnesis_edit[i].value == 's') {
            var name = data_anamnesis_edit[i + 1].name;
            $('textarea[name=' + name + ']').prop('disabled', false);
        } else {
            var name = data_anamnesis_edit[i + 1].name;
            $('textarea[name=' + name + ']').prop('disabled', true);
        }
    }

    function calcular_imc() {
        var total = 0;
        var peso = parseFloat(document.getElementById('peso').value);
        var talla = parseFloat(document.getElementById('talla').value);

        if (isNaN(peso) || isNaN(talla)) {
            total = 0;
        } else {
            total = (peso / (talla * talla));
            total = Math.round(total * 100) / 100;
        }
        document.getElementById('imc').value = total.toFixed(2);
    }

    function calcular_frecuencia_respiratoria() {
        var total = 0;
        var max = parseFloat(document.getElementById('presion_arterial_maxima').value);
        var min = parseFloat(document.getElementById('presion_arterial_minima').value);

        if (isNaN(max) || isNaN(min)) {
            total = 0;
        } else {
            total = max / min;
            total = Math.round(total * 100) / 100;
        }

        document.getElementById('frecuencia_respiratoria').value = total.toFixed(2);

    }

</script>