<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <jsp:include page="/templates/head.jsp"/>
    </head>

    <body class="fix-header">
        <div class="preloader">
            <svg class="circular" viewBox="25 25 50 50">
            <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10" />
            </svg>
        </div>
        <div id="wrapper">
            <jsp:include page="/templates/header.jsp" flush="true" /> 
            <jsp:include page="/templates/navbar.jsp" flush="true" />
            <div id="page-wrapper">
                <div class="container-fluid">
                    <jsp:include page="/views/HistoriaClinica/historias.jsp" flush="true"/>
                    <jsp:include page="/templates/right-sidebar.jsp" flush="true"/>
                </div>
                <jsp:include page="/templates/footer.jsp" flush="true"/>
            </div>
        </div>
        <jsp:include page="/templates/scripts.jsp" flush="true"/>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.0/js/i18n/es.js" defer></script>
    </body>
</html>

<script>
    $(document).ready(function () {
        
        $.fn.select2.defaults.set('language', 'es');
        $.fn.select2.defaults.set("theme", "bootstrap");
        $('#paciente').select2({
            minimumInputLength: 2
        });
        
        $.extend($.fn.dataTable.defaults, {
            responsive: true
        });
        
        $("#data_historias").DataTable({
            "processing": true,
            "language": {
                "url": "//cdn.datatables.net/plug-ins/1.10.24/i18n/Spanish.json"
            },
            "ajax": {
                "url": "historias.do?op=listarHistoriaClinicaJson",
                "type": "POST",
                "dataSrc": ""
            },
            "columns": [
                {
                    "data": "codigoHistoriaClinica",
                    "width": "6%"
                },
                {"data": "paciente.nombrePaciente"},
                {"data": "paciente.documentoPaciente"},
                {"data": "paciente.telefonoPaciente"},
                {"data": "paciente.nacionalidadPaciente"},
                {
                    "data": "fecha_created",
                    render: function (data, type) {
                        var dateSplit = data.split(' ');
                        var newdate = dateSplit[0].split('-');
                        var newhour = dateSplit[1].split('.');
                        return type === "display" || type === "filter" ?
                                newdate[2] + '/' + newdate[1] + '/' + newdate[0] + ' ' + newhour[0] : data;
                    }
                },
                {
                    "data": null,
                    "className": "dt-center",
                    "render": function (data) {
                        return  '<a href="javascript:verHistoria(' + data.codigoHistoriaClinica + ')" class="text-inverse p-r-10" data-toggle="tooltip" title="" data-original-title="Visualizar"><i class="fa fa-eye"></i></a>' +
                                '<a href="javascript:editarHistoria(' + data.codigoHistoriaClinica + ')" class="text-inverse p-r-10" data-toggle="tooltip" title="" data-original-title="Editar"><i class="ti-marker-alt text-info"></i></a>' +
                                '<a href="javascript:eliminarHistoria(' + data.codigoHistoriaClinica + ')" class="text-inverse" title="" data-toggle="tooltip" data-original-title="Eliminar"><i class="ti-trash text-danger"></i></a>';
                    },
                    "orderable": false
                }
            ],
            "order": [[0, "desc"]]
        });

    });

    function verHistoria(id)
    {

    }

    function editarHistoria(id)
    {
        location.href = "historias.do?op=obtener&id=" + id;
    }

    function eliminarHistoria(id)
    {

    }
</script>

