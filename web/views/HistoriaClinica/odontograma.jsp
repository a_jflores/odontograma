<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="java.util.*"%>
<!DOCTYPE html>
<html lang="es" ng-app="app">
    <head>
        <jsp:include page="/templates/head.jsp" flush="true" />
        <link href="${pageContext.request.contextPath}/css/estilosOdontograma.css" rel="stylesheet" type="text/css"/>
        <script src="${pageContext.request.contextPath}/scripts/angular.js"></script>
        <script type="text/javascript" src="${pageContext.request.contextPath}/scripts/modulos/app.js"></script>
        <script type="text/javascript" src="${pageContext.request.contextPath}/scripts/directivas/opcionescanvas.js"></script>
        <script>
            window.onload = function () {
                if (window.jQuery) {
                    console.log("JQuery cargado");
                } else {
                    console.log("JQuery no ha argado");
                }
            }
        </script>
    </head>
    <body class="fix-header">
        <div class="preloader">
            <svg class="circular" viewBox="25 25 50 50">
            <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10" />
            </svg>
        </div>
        <div id="wrapper">
            <jsp:include page="/templates/header.jsp" flush="true" /> 
            <jsp:include page="/templates/navbar.jsp" flush="true" />
            <div id="page-wrapper">
                <div class="container-fluid">
                    <div class="row bg-title">
                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                            <h4 class="page-title">Odontograma</h4>
                        </div>
                        <div class="col-lg-8 col-sm-8 col-md-8 col-xs-12">
                            <button class="right-side-toggle waves-effect waves-light btn-info btn-circle pull-right m-l-20"><i class="ti-settings text-white"></i></button>
                            <ol class="breadcrumb">
                                <li><a href="${pageContext.request.contextPath}/index.jsp">Dashboard</a></li>
                                <li class="active">Odontograma</li>
                            </ol>
                        </div>
                        <input type="hidden" id="id_historia_clinica" value="${historia.codigoHistoriaClinica}"/>
                    </div>
                    <div>
                        <odontogramageneral></odontogramageneral>
                    </div>
                    <jsp:include page="/templates/right-sidebar.jsp" flush="true"/>
                </div>
                <jsp:include page="/templates/footer.jsp" flush="true"/>
            </div>
        </div>
        <jsp:include page="/templates/scripts.jsp" flush="true"/>
        <!-- Angular Modulos-->

        <!-- Angular Controladores-->
        <script type="text/javascript" src="${pageContext.request.contextPath}/scripts/controladores/controller.js"></script>
        <!--Angular Directives-->
        <script type="text/javascript" src="${pageContext.request.contextPath}/scripts/directivas/canvasodontograma.js"></script>

        <script type="text/javascript" src="${pageContext.request.contextPath}/scripts/directivas/odontogramaGeneral.js"></script>
        <script type="text/javascript" src="${pageContext.request.contextPath}/scripts/directivas/odontogramaUp.js"></script>
        <script type="text/javascript" src="${pageContext.request.contextPath}/scripts/directivas/odontogramaDown.js"></script>
        <script type="text/javascript" src="${pageContext.request.contextPath}/scripts/directivas/placaUp.js"></script>
        <script type="text/javascript" src="${pageContext.request.contextPath}/scripts/directivas/placaDown.js"></script>

        <script type="text/javascript" src="${pageContext.request.contextPath}/scripts/directivas/diente-18.js"></script>
        <script type="text/javascript" src="${pageContext.request.contextPath}/scripts/directivas/diente-17.js"></script>
        <script type="text/javascript" src="${pageContext.request.contextPath}/scripts/directivas/diente-16.js"></script>
        <script type="text/javascript" src="${pageContext.request.contextPath}/scripts/directivas/diente-15.js"></script>
        <script type="text/javascript" src="${pageContext.request.contextPath}/scripts/directivas/diente-14.js"></script>
        <script type="text/javascript" src="${pageContext.request.contextPath}/scripts/directivas/diente-13.js"></script>
        <script type="text/javascript" src="${pageContext.request.contextPath}/scripts/directivas/diente-12.js"></script>
        <script type="text/javascript" src="${pageContext.request.contextPath}/scripts/directivas/diente-11.js"></script>
        <script type="text/javascript" src="${pageContext.request.contextPath}/scripts/directivas/diente-21.js"></script>
        <script type="text/javascript" src="${pageContext.request.contextPath}/scripts/directivas/diente-22.js"></script>
        <script type="text/javascript" src="${pageContext.request.contextPath}/scripts/directivas/diente-23.js"></script>
        <script type="text/javascript" src="${pageContext.request.contextPath}/scripts/directivas/diente-24.js"></script>
        <script type="text/javascript" src="${pageContext.request.contextPath}/scripts/directivas/diente-25.js"></script>
        <script type="text/javascript" src="${pageContext.request.contextPath}/scripts/directivas/diente-26.js"></script>
        <script type="text/javascript" src="${pageContext.request.contextPath}/scripts/directivas/diente-27.js"></script>
        <script type="text/javascript" src="${pageContext.request.contextPath}/scripts/directivas/diente-28.js"></script>

        <script type="text/javascript" src="${pageContext.request.contextPath}/scripts/directivas/diente-48.js"></script>
        <script type="text/javascript" src="${pageContext.request.contextPath}/scripts/directivas/diente-47.js"></script>
        <script type="text/javascript" src="${pageContext.request.contextPath}/scripts/directivas/diente-46.js"></script>
        <script type="text/javascript" src="${pageContext.request.contextPath}/scripts/directivas/diente-45.js"></script>
        <script type="text/javascript" src="${pageContext.request.contextPath}/scripts/directivas/diente-44.js"></script>
        <script type="text/javascript" src="${pageContext.request.contextPath}/scripts/directivas/diente-43.js"></script>
        <script type="text/javascript" src="${pageContext.request.contextPath}/scripts/directivas/diente-42.js"></script>
        <script type="text/javascript" src="${pageContext.request.contextPath}/scripts/directivas/diente-41.js"></script>
        <script type="text/javascript" src="${pageContext.request.contextPath}/scripts/directivas/diente-31.js"></script>
        <script type="text/javascript" src="${pageContext.request.contextPath}/scripts/directivas/diente-32.js"></script>
        <script type="text/javascript" src="${pageContext.request.contextPath}/scripts/directivas/diente-33.js"></script>
        <script type="text/javascript" src="${pageContext.request.contextPath}/scripts/directivas/diente-34.js"></script>
        <script type="text/javascript" src="${pageContext.request.contextPath}/scripts/directivas/diente-35.js"></script>
        <script type="text/javascript" src="${pageContext.request.contextPath}/scripts/directivas/diente-36.js"></script>
        <script type="text/javascript" src="${pageContext.request.contextPath}/scripts/directivas/diente-37.js"></script>
        <script type="text/javascript" src="${pageContext.request.contextPath}/scripts/directivas/diente-38.js"></script>

        <script type="text/javascript" src="${pageContext.request.contextPath}/scripts/directivas/diente-55.js"></script>
        <script type="text/javascript" src="${pageContext.request.contextPath}/scripts/directivas/diente-54.js"></script>
        <script type="text/javascript" src="${pageContext.request.contextPath}/scripts/directivas/diente-53.js"></script>
        <script type="text/javascript" src="${pageContext.request.contextPath}/scripts/directivas/diente-52.js"></script>
        <script type="text/javascript" src="${pageContext.request.contextPath}/scripts/directivas/diente-51.js"></script>
        <script type="text/javascript" src="${pageContext.request.contextPath}/scripts/directivas/diente-61.js"></script>
        <script type="text/javascript" src="${pageContext.request.contextPath}/scripts/directivas/diente-62.js"></script>
        <script type="text/javascript" src="${pageContext.request.contextPath}/scripts/directivas/diente-63.js"></script>
        <script type="text/javascript" src="${pageContext.request.contextPath}/scripts/directivas/diente-64.js"></script>
        <script type="text/javascript" src="${pageContext.request.contextPath}/scripts/directivas/diente-65.js"></script>

        <script type="text/javascript" src="${pageContext.request.contextPath}/scripts/directivas/diente-85.js"></script>
        <script type="text/javascript" src="${pageContext.request.contextPath}/scripts/directivas/diente-84.js"></script>
        <script type="text/javascript" src="${pageContext.request.contextPath}/scripts/directivas/diente-83.js"></script>
        <script type="text/javascript" src="${pageContext.request.contextPath}/scripts/directivas/diente-82.js"></script>
        <script type="text/javascript" src="${pageContext.request.contextPath}/scripts/directivas/diente-81.js"></script>
        <script type="text/javascript" src="${pageContext.request.contextPath}/scripts/directivas/diente-71.js"></script>
        <script type="text/javascript" src="${pageContext.request.contextPath}/scripts/directivas/diente-72.js"></script>
        <script type="text/javascript" src="${pageContext.request.contextPath}/scripts/directivas/diente-73.js"></script>
        <script type="text/javascript" src="${pageContext.request.contextPath}/scripts/directivas/diente-74.js"></script>
        <script type="text/javascript" src="${pageContext.request.contextPath}/scripts/directivas/diente-75.js"></script>
    </body>
</html>