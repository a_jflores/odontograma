<!-- jQuery -->
    <script src="${pageContext.request.contextPath}/plugins/bower_components/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap Core JavaScript -->
    <script src="${pageContext.request.contextPath}/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- Menu Plugin JavaScript -->
    <script src="${pageContext.request.contextPath}/plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.js"></script>
    <!--slimscroll JavaScript -->
    <script src="${pageContext.request.contextPath}/js/jquery.slimscroll.js"></script>
    <!--Wave Effects -->
    <script src="${pageContext.request.contextPath}/js/waves.js"></script>
    <!-- Magnific popup JavaScript -->
    <script src="${pageContext.request.contextPath}/plugins/bower_components/Magnific-Popup-master/dist/jquery.magnific-popup.min.js"></script>
    <script src="${pageContext.request.contextPath}/plugins/bower_components/Magnific-Popup-master/dist/jquery.magnific-popup-init.js"></script>
    <!--Counter js -->
    <script src="${pageContext.request.contextPath}/plugins/bower_components/waypoints/lib/jquery.waypoints.js"></script>
    <script src="${pageContext.request.contextPath}/plugins/bower_components/counterup/jquery.counterup.min.js"></script>
    <!--Morris JavaScript -->
    <script src="${pageContext.request.contextPath}/plugins/bower_components/raphael/raphael-min.js"></script>
    <script src="${pageContext.request.contextPath}/plugins/bower_components/morrisjs/morris.js"></script>
    <!-- chartist chart -->
    <script src="${pageContext.request.contextPath}/plugins/bower_components/chartist-js/dist/chartist.min.js"></script>
    <script src="${pageContext.request.contextPath}/plugins/bower_components/chartist-plugin-tooltip-master/dist/chartist-plugin-tooltip.min.js"></script>
    <!-- Calendar JavaScript -->
    <script src="${pageContext.request.contextPath}/plugins/bower_components/moment/moment.js"></script>
    <script src='${pageContext.request.contextPath}/plugins/bower_components/calendar/dist/fullcalendar.min.js'></script>
    <script src="${pageContext.request.contextPath}/plugins/bower_components/calendar/dist/cal-init.js"></script>
    <!-- Sweet-Alert  -->
    <script src="${pageContext.request.contextPath}/plugins/bower_components/sweetalert/sweetalert.min.js"></script>
    <script src="${pageContext.request.contextPath}/plugins/bower_components/sweetalert/jquery.sweet-alert.custom.js"></script>
    <!-- Custom Theme JavaScript -->
    <script src="${pageContext.request.contextPath}/js/custom.min.js"></script>
    <script src="${pageContext.request.contextPath}/js/validator.js"></script>
    
    <script src="${pageContext.request.contextPath}/plugins/bower_components/datatables/jquery.dataTables.min.js"></script>
    <script src="${pageContext.request.contextPath}/plugins/bower_components/datatables-responsive/js/dataTables.responsive.js"></script>
    <script src="https://cdn.datatables.net/plug-ins/1.10.25/api/sum().js"></script>
    <!-- Footable -->
    <script src="${pageContext.request.contextPath}/plugins/bower_components/footable/js/footable.all.min.js"></script>
    <script src="${pageContext.request.contextPath}/plugins/bower_components/bootstrap-select/bootstrap-select.min.js" type="text/javascript"></script>
    <!--FooTable init-->
    <script src="${pageContext.request.contextPath}/js/footable-init.js"></script>
    <!--Style Switcher -->
    <script src="${pageContext.request.contextPath}/plugins/bower_components/styleswitcher/jQuery.style.switcher.js"></script>
    <!-- Custom tab JavaScript -->
    <script src="${pageContext.request.contextPath}/js/cbpFWTabs.js"></script>
    <script type="text/javascript">
    (function() {
        [].slice.call(document.querySelectorAll('.sttabs')).forEach(function(el) {
            new CBPFWTabs(el);
        });
    })();
    </script>
    <script src="${pageContext.request.contextPath}/plugins/bower_components/toast-master/js/jquery.toast.js"></script>
    <script src="${pageContext.request.contextPath}/js/chat.js"></script>
    <script type="text/javascript">
        jQuery.event.special.touchstart = {
            setup: function( _, ns, handle ) {
                this.addEventListener("touchstart", handle, { passive: !ns.includes("noPreventDefault") });
            }
        };
    </script>
