<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<c:set var="ctx" value="${pageContext.request.contextPath}"/>
<c:set var="pagina" value="${requestScope['javax.servlet.forward.request_uri']}" />
<div class="navbar-default sidebar" role="navigation">
    <div class="sidebar-nav slimscrollsidebar">
        <div class="sidebar-head">
            <h3><span class="fa-fw open-close"><i class="ti-menu hidden-xs"></i><i class="ti-close visible-xs"></i></span>
                <span class="hide-menu">
                    <img src="/odontograma/plugins/images/img-logo.png" alt="icono" class="dark-logo" width="35" height="35"> ProImplant
                </span>
            </h3>
        </div>
        <ul class="nav" id="side-menu">
            <li class="user-pro">
                <a href="#" class="waves-effect"><img src="${pageContext.request.contextPath}/plugins/images/users/varun.jpg" alt="user-img" class="img-circle"> <span class="hide-menu"> Steve Gection<span class="fa arrow"></span></span>
                </a>
                <ul class="nav nav-second-level collapse" aria-expanded="false" style="height: 0px;">
                    <li><a href="javascript:void(0)"><i class="ti-user"></i> <span class="hide-menu">My Profile</span></a></li>
                    <li><a href="javascript:void(0)"><i class="ti-wallet"></i> <span class="hide-menu">My Balance</span></a></li>
                    <li><a href="javascript:void(0)"><i class="ti-email"></i> <span class="hide-menu">Inbox</span></a></li>
                    <li><a href="javascript:void(0)"><i class="ti-settings"></i> <span class="hide-menu">Account Setting</span></a></li>
                    <li><a href="javascript:void(0)"><i class="fa fa-power-off"></i> <span class="hide-menu">Logout</span></a></li>
                </ul>
            </li>
            <li><a href="${ctx}/index.jsp" class="waves-effect ${pagina.endsWith('/') ? 'active' : ''}"><i class="mdi mdi-av-timer fa-fw" data-icon="v"></i> <span class="hide-menu">Dashboard</span></a></li>
            <li><a href="javascript:void(0);" class="waves-effect ${pagina.endsWith('/pacientes.do') ? 'active' : ''}"><i class="mdi mdi-account fa-fw"></i> <span class="hide-menu">Pacientes<span class="fa arrow"></span></span></a>
                <ul class="nav nav-second-level">
                    <li><a href="${pageContext.request.contextPath}/pacientes.do?op=listar"><i class="mdi mdi-file fa-fw"></i><span class="hide-menu">Lista Pacientes</span></a></li>
                </ul>
            </li>
            <li><a href="javascript:void(0);" class="waves-effect ${pagina.endsWith('/historias.do') ? 'active' : ''}"><i class="mdi mdi-clipboard-text fa-fw"></i> <span class="hide-menu">Historia Cl�nica<span class="fa arrow"></span></span></a>
                <ul class="nav nav-second-level">
                    <li><a href="${pageContext.request.contextPath}/historias.do?op=listar"><i class="ti-comments-smiley fa-fw"></i><span class="hide-menu">Historiales cl�nicos</span></a></li>
                </ul>
            </li>
            <li class="devider"></li>
        </ul>
    </div>
</div>