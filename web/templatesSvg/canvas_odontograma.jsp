<svg width="50" height="50" viewBox="0 0 270 100" class="{{info.tipoDiente}}" id="{{info.id}}">
    <g transform="translate(135,65)">
        <circle pareja="0" cx="0" cy="0" r="105" estado="0" value="6" class="corona"/>
        <circle pareja="0" cx="0" cy="0" r="105" estado="0" value="6" class="coronatemporal"/>
        <path pareja="0" d="M -35 -35 l -35 -35 A 99 99 0 0 1 70-70 l -35 35 A 50 50 0 0 0 -35 -35 z" estado="0" value="1" class="diente"/>
        <path pareja="0" d="M 35 -35 70 -70 A 99 99 0 0 1 70 70 l -35 -35 A 50 50 0 0 0 35 -35 z" estado="0" value="2" class="diente"/>
        <path pareja="0" d="M 35 35 l 35 35 A 99 99 0 0 1-70 70 l 35 -35 A 50 50 0 0 0 35 35 z" estado="0" value="3" class="diente"/>
        <path pareja="0" d="M -35 35 l-35 35 A 99 99 0 0 1 -70-70 l 35 35 A 50 50 0 0 0 -35 35 z" estado="0" value="4" class="diente"/>
        <path pareja="0" d="m -50 0 a 50 50 0 1 0 100 0 a 50 50 0 1 0 -100 0z" estado="0" value="5" class="diente"/>
        <path pareja="0" class="diastema1" value="6" d="m -97 -100 a 200 200 0 0 0 0 200"/>
        <path pareja="0" class="diastema2" value="6" d="m 97 -100 a 200 200 0 0 1 0 200"/>
        <path pareja="0" class="edentulo_total" value="6" d="m -135 0 l 300 0"/>
        <path pareja="0" class="protesis_total" value="6" d="m -135 -20 l 300 0 m -300 40 l 300 0"/>
    </g>
</svg>