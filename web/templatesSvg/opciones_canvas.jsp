<!-- <input type="button" value="ver" id="ver"/>
<input type="button" value="Agregar" id="agregar"/>
<input type="button" value="limpiar" id="limpiar"/>
<br><br>
<input type="radio" id="Decidua" name="tipo" value="1" checked />Adultos
<input type="radio" id="Ni�os" name="tipo" value="2" />Ni�os
<input type="radio" id="Mixta" name="tipo" value="3" />Mixta -->

<!-- <table border="1" align="center"> -->
<!-- <tr>
        <th>Amalgama</th>
        <th>Caries</th>
        <th>Endodoncia</th>
        <th>Ausente</th>
        <th>Resina</th>
        <th>Implante</th>
        <th>Sellante</th>
        <th>Corona</th>
        <th>Normal</th>
</tr>
<tr>

</tr>
        <td><center><div class="color" value="1" style="background-color:red;width:20px;height:20px"></div></center></td>
        <td><center><div class="color" value="2" style="background-color:yellow;width:20px;height:20px"></div></center></td>
        <td><center><div class="color" value="3" style="background-color:orange;width:20px;height:20px"></div></center></td>
        <td><center><div class="color" value="4" style="background-color:tomato;width:20px;height:20px"></div></center></td>
        <td><center><div class="color" value="5" style="background-color:#CC6600;width:20px;height:20px"></div></center></td>
        <td><center><div class="color" value="6" style="background-color:#CC66CC;width:20px;height:20px"></div></center></td>
        <td><center><div class="color" value="7" style="background-color:green;width:20px;height:20px"></div></center></td>
        <td><center><div class="color" value="8" style="background-color:blue;width:20px;height:20px"></div></center></td>
        <td><center><div class="color" value="9" style="background-color:black;width:20px;height:20px"></div></center></td>
<tr> -->

<!-- </table> -->

<style>

    /*    .dropdown-submenu {
            position: relative;
        }
    
        .dropdown-submenu > .dropdown-menu {
            top: 0;
            left: 1%;
            margin-top: -6px;
            margin-left: -86%;
            -webkit-border-radius: 0 6px 6px 6px;
            -moz-border-radius: 0 6px 6px;
            border-radius: 0 6px 6px 6px;
        }
    
        .dropdown-submenu:hover > .dropdown-menu {
            display: block;
        }
        .dropdown-submenu:hover > a:after {
            border-left-color: #fff;
        }
    
        .dropdown-submenu.pull-left {
            float: none;
        }
    
        .dropdown-submenu.pull-left > .dropdown-menu {
            left: -100%;
            margin-left: 10px;
            -webkit-border-radius: 6px 0 6px 6px;
            -moz-border-radius: 6px 0 6px 6px;
            border-radius: 6px 0 6px 6px;
        }
        .tabla-hover td:hover{
            cursor: pointer;
            background-color: #cadeec;
        }*/

    .nav-tab-vertical {
        border-color:transparent;
        width:100%;
    }

    .nav-tab-vertical>li {
        float: none;
        margin-bottom: 0px;
        margin-right: 0px;
    }

    .nav-tab-vertical > li > a { 
        color:black;
    }

    .dropdown-menu>.active>a {
        background-color: #2cabe3;
    }

    .nav-tab-vertical > li.active > a,
    .nav-tab-vertical > li.active > a:focus,
    .nav-tab-vertical > li.active > a:hover {
        background-color:#2cabe3;
        color:#000;
    }

    /* .nav-tab-vertical > li > a:hover{
            background-color: #ecf1f3 !important;
            color:#000;
    }  */

</style>

<ul class="nav nav-tabs tabs customtab">
    <li class="active tab">
        <a href="#home" data-toggle="tab"> <span class="visible-xs"><i class="fa fa-home"></i></span> <span class="hidden-xs">1</span> </a>
    </li>
    <li class="tab">
        <a href="#profile" data-toggle="tab"> <span class="visible-xs"><i class="fa fa-user"></i></span> <span class="hidden-xs">2</span> </a>
    </li>
    <li class="tab">
        <a href="#settings" data-toggle="tab" aria-expanded="false"> <span class="visible-xs"><i class="fa fa-cog"></i></span> <span class="hidden-xs">3</span> </a>
    </li>
    <button data-toggle="tooltip" data-placement="right" data-original-title="Guardar Odontograma" id="agregar" class="waves-effect waves-light btn-info btn-circle pull-right m-l-20"><i class="fa fa-save text-white"></i></button>
    <button data-toggle="tooltip" data-placement="right" data-original-title="Borrar" class="waves-effect waves-light color btn-info btn-circle pull-right m-l-20" value="0"><i class="fa fa-eraser text-white"></i></button>               
</ul>

<div class="tab-content">
    <div class="tab-pane active" id="home">
        <div class="row">
            <div class="col-lg-12">
                <div class="white-box" style="padding-left: 0px;padding-right: 0px;padding-top: 0px;padding-bottom: 0px;margin-left: 0px;margin-right: 0px;margin-top: 0px;margin-bottom: 0px;border-left: 0px;border-right: 0px;">

                    <ul class="nav nav-tab-vertical">
                        <li class="active">
                            <a class="color" value="1"  role="tab" data-toggle="tab" href="#">
                                <img src="plugins/images/odontograma/Carie.svg" alt="user-img" width="15px" height="15px" class="img-circle">
                                <b>Caries</b>
                            </a>
                        </li>
                        <li class="dropdown">
                            <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                                <img src="plugins/images/odontograma/Restauraci�n.svg" alt="user-img" width="15px" height="15px" class="img-circle">
                                <b>Restauraci�n Buena</b>
                                <span class="caret"></span>
                            </a>
                            <ul class="dropdown-menu">
                                <li>
                                    <a class="color" value="2" href="#"  role="tab" data-toggle="tab"><strong>AM</strong> &nbsp;  Amalgama</a>
                                </li>
                                <li >
                                    <a class="color" value="3" href="#"  role="tab" data-toggle="tab"><strong>R</strong> &nbsp;  Resina</a>
                                </li>
                                <li >
                                    <a class="color" value="4" href="#"  role="tab" data-toggle="tab"><strong>IV</strong> &nbsp;  Ion�mero de Vidrio</a>
                                </li>
                                <li >
                                    <a class="color" value="5" href="#"  role="tab" data-toggle="tab"><strong>IM</strong> &nbsp;  Incrustaci�n Met�lica</a>
                                </li>
                                <li >
                                    <a class="color" value="6" href="#"  role="tab" data-toggle="tab"><strong>IE</strong> &nbsp;  Incrustacion Est�tica</a>
                                </li>
                            </ul>
                        </li>
                        <li class="dropdown">
                            <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                                <img src="plugins/images/odontograma/Restauraci�nMala.svg" alt="user-img" width="15px" height="15px" class="img-circle">
                                <b>Restauraci�n Mala</b>
                                <span class="caret"></span>
                            </a>
                            <ul class="dropdown-menu">
                                <li class="dropdown-submenu">
                                    <a class="color" value="7" href="#"  role="tab" data-toggle="tab"><strong>AM</strong> &nbsp;  Amalgama</a>
                                </li>
                                <li class="dropdown-submenu">
                                    <a class="color" value="8" href="#"  role="tab" data-toggle="tab"><strong>R</strong> &nbsp;  Resina</a>
                                </li>
                                <li class="dropdown-submenu">
                                    <a class="color" value="9" href="#"  role="tab" data-toggle="tab"><strong>IV</strong> &nbsp;  Ion�mero de Vidrio</a>
                                </li>
                                <li class="dropdown-submenu">
                                    <a class="color" value="10" href="#"  role="tab" data-toggle="tab"><strong>IM</strong> &nbsp;  Incrustaci�n Met�lica</a>
                                </li>
                                <li class="dropdown-submenu">
                                    <a class="color" value="11" href="#"  role="tab" data-toggle="tab"><strong>IE</strong> &nbsp;  Incrustacion Est�tica</a>
                                </li>
                            </ul>
                        </li>
                        <li>
                            <a class="color" value="12"  role="tab" data-toggle="tab" href="#">
                                <img src="plugins/images/odontograma/Restauraci�n-temporal.svg" alt="rt-img" width="15px" height="15px" class="img-circle">
                                <b>Restauraci�n temporal</b>
                            </a>
                        </li>
                        <li class="dropdown">
                            <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                                <img src="plugins/images/odontograma/TratamientoPulpar-bueno.svg" width="15px" height="15px" class="img-circle">
                                <b>Trat. Pulpar Bueno</b>
                                <span class="caret"></span>
                            </a>
                            <ul class="dropdown-menu">
                                <li class="dropdown-submenu">
                                    <a class="color" value="13" href="#" role="tab" data-toggle="tab"><strong>TC</strong> &nbsp;  Conductos</a>
                                </li>
                                <li class="dropdown-submenu">
                                    <a class="color" value="14" href="#" role="tab" data-toggle="tab"><strong>PC</strong> &nbsp;  Pulpectom�a</a>
                                </li>
                                <li class="dropdown-submenu">
                                    <a class="color" value="15" href="#" role="tab" data-toggle="tab"><strong>PP</strong> &nbsp;  Pulpotomia</a>
                                </li>
                            </ul>
                        </li>
                        <li class="dropdown">
                            <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                                <img src="plugins/images/odontograma/TratamientoPulpar-malo.svg" width="15px" height="15px" class="img-circle">
                                <b>Trat. Pulpar Malo</b>
                                <span class="caret"></span>
                            </a>
                            <ul class="dropdown-menu">
                                <li class="dropdown-submenu">
                                    <a class="color" value="16" href="#" role="tab" data-toggle="tab"><strong>TC</strong> &nbsp;  Conductos</a>
                                </li>
                                <li class="dropdown-submenu">
                                    <a class="color" value="17" href="#" role="tab" data-toggle="tab"><strong>PC</strong> &nbsp;  Pulpectom�a</a>
                                </li>
                                <li class="dropdown-submenu">
                                    <a class="color" value="18" href="#" role="tab" data-toggle="tab"><strong>PP</strong> &nbsp;  Pulpotomia</a>
                                </li>
                            </ul>
                        </li>
                        <li>
                            <a class="color" value="19" role="tab" data-toggle="tab" href="#">
                                <img src="plugins/images/odontograma/Ausente.svg" width="15px" height="15px" class="img-circle">
                                <b>Diente ausente</b>
                            </a>
                        </li>
                        <li>
                            <a class="color" value="20" role="tab" data-toggle="tab" href="#">
                                <img src="plugins/images/odontograma/Extraer.svg" width="15px" height="15px" class="img-circle">
                                <b>Diente por extraer</b>
                            </a>
                        </li>
                        <li>
                            <a class="color" value="21" role="tab" data-toggle="tab" href="#">
                                <img src="plugins/images/odontograma/Fractura.svg" alt="user-img" width="15px" height="15px" class="img-circle">
                                <b>Fractura</b>
                            </a>
                        </li>
                        <li>
                            <a class="color" value="22" role="tab" data-toggle="tab" href="#">
                                <img src="plugins/images/odontograma/RemanenteRadicular.svg" width="15px" height="15px" class="img-circle">
                                <b>Remanente radicular</b>
                            </a>
                        </li>
                    </ul>

                    <!-- <table id="demo-foo-pagination" class="table m-b-0 toggle-arrow-tiny tabla-hover" data-page-size="5">
                            <tbody>
                                    <tr>
                                            <td>
                                                    <div>
                                                            <a class="dropdown-toggle" type="button" data-toggle="dropdown">
                                                                    <img src="plugins/images/odontograma/Carie.svg" width="25px" height="25px">
                                                                    Caries
                                                            </a>
                                                    </div>
                                            </td>
                                    </tr>
                                    <tr>
                                            <td>															
                                                    <div class="dropdown">
                                                            <a class="dropdown-toggle" type="button" data-toggle="dropdown" aria-expanded="false">
                                                                    <img src="plugins/images/odontograma/Restauraci�n.svg" width="25px" height="25px">
                                                                    Restauraci�n
                                                                    <span class="caret"></span>
                                                            </a>
                                                            <ul class="dropdown-menu multi-level dropdown-menu-left">
                                                                    <li class="dropdown-submenu">
                                                                            <a class="color" value="11" href="#"><strong>AM</strong> &nbsp;  Amalgama</a>
                                                                    </li>
                                                                    <li class="dropdown-submenu">
                                                                            <a class="color" value="11" href="#"><strong>R</strong> &nbsp;  Resina</a>
                                                                    </li>
                                                                    <li class="dropdown-submenu">
                                                                            <a class="color" value="11" href="#"><strong>IV</strong> &nbsp;  Ion�mero de Vidrio</a>
                                                                    </li>
                                                                    <li class="dropdown-submenu">
                                                                            <a class="color" value="11" href="#"><strong>IM</strong> &nbsp;  Incrustaci�n Met�lica</a>
                                                                    </li>
                                                                    <li class="dropdown-submenu">
                                                                            <a class="color" value="11" href="#"><strong>IE</strong> &nbsp;  Incrustacion Est�tica</a>
                                                                    </li>
                                                            </ul>
                                                    </div>
                                            </td>
                                    </tr>
                                    <tr>
                                            <td>															
                                                    <div class="dropdown">
                                                            <a class="dropdown-toggle" type="button" data-toggle="dropdown" aria-expanded="false">
                                                                    <img src="plugins/images/odontograma/Restauraci�nMala.svg" width="25px" height="25px">
                                                                    Restauraci�n defectuosa
                                                                    <span class="caret"></span>
                                                            </a>
                                                            <ul class="dropdown-menu multi-level dropdown-menu-left">
                                                                    <li class="dropdown-submenu">
                                                                            <a class="color" value="18" href="#"><strong>AM</strong> &nbsp;  Amalgama</a>
                                                                    </li>
                                                                    <li class="dropdown-submenu">
                                                                            <a class="color" value="18" href="#"><strong>R</strong> &nbsp;  Resina</a>
                                                                    </li>
                                                                    <li class="dropdown-submenu">
                                                                            <a class="color" value="18" href="#"><strong>IV</strong> &nbsp;  Ion�mero de Vidrio</a>
                                                                    </li>
                                                                    <li class="dropdown-submenu">
                                                                            <a class="color" value="18" href="#"><strong>IM</strong> &nbsp;  Incrustaci�n Met�lica</a>
                                                                    </li>
                                                                    <li class="dropdown-submenu">
                                                                            <a class="color" value="18" href="#"><strong>IE</strong> &nbsp;  Incrustacion Est�tica</a>
                                                                    </li>
                                                            </ul>
                                                    </div>
                                            </td>
                                    </tr>
                                    <tr>
                                            <td>
                                                    <div class="color" value="12">
                                                            <a class="dropdown-toggle" type="button" data-toggle="dropdown">
                                                                    <img src="plugins/images/odontograma/Restauraci�n-temporal.svg" width="25px" height="25px">
                                                                    Restauraci�n temporal
                                                            </a>
                                                    </div>
                                            </td>
                                    </tr>
                                    <tr>
                                            <td>
                                                    <div class="dropdown">
                                                            <a class="dropdown-toggle" type="button" data-toggle="dropdown" aria-expanded="false">
                                                                    <img src="plugins/images/odontograma/TratamientoPulpar-bueno.svg" width="25px" height="25px">
                                                                    Tratamiento Pulpar
                                                                    <span class="caret"></span>
                                                            </a>
                                                            <ul class="dropdown-menu multi-level dropdown-menu-left">
                                                                    <li class="dropdown-submenu">
                                                                            <a href="#" data-original-title="" title=""><i class="fa fa-fw fa-caret-left" style="margin:0px;margin-left:-15px;"></i> <strong>TC</strong> &nbsp;  Conductos</a>
                                                                            <ul class="dropdown-menu dropdown-menu-left" style="left:-24px;">
                                                                                    <li><a class="color" value="15" href="#"><i class="fa fa-smile-o fa-1" aria-hidden="true" style="color:blue"></i>  Buen Estado</a></li>
                                                                                    <li><a class="color" value="16" href="#"> <i class="fa fa-frown-o fa-1" aria-hidden="true" style="color:#FF0000"></i>  Mal Estado</a></li>
                                                                            </ul>
                                                                    </li>
                                                                    <li class="dropdown-submenu">
                                                                            <a href="#" data-original-title="" title=""><i class="fa fa-fw fa-caret-left" style="margin:0px;margin-left:-15px;"></i> <strong>PC</strong> &nbsp;  Pulpectom�a</a>
                                                                            <ul class="dropdown-menu dropdown-menu-left" style="left:-24px;">
                                                                                    <li><a class="color" value="15" href="#"><i class="fa fa-smile-o fa-1" aria-hidden="true" style="color:blue"></i>  Buen Estado</a></li>
                                                                                    <li><a class="color" value="16" href="#"> <i class="fa fa-frown-o fa-1" aria-hidden="true" style="color:#FF0000"></i>  Mal Estado</a></li>
                                                                            </ul>
                                                                    </li>
                                                                    <li class="dropdown-submenu">
                                                                            <a href="#" data-original-title="" title=""><i class="fa fa-fw fa-caret-left" style="margin:0px;margin-left:-15px;"></i> <strong>PP</strong> &nbsp;  Pulpotomia</a>
                                                                            <ul class="dropdown-menu dropdown-menu-left" style="left:-24px;">
                                                                                    <li><a class="color" value="15" href="#"><i class="fa fa-smile-o fa-1" aria-hidden="true" style="color:blue"></i>  Buen Estado</a></li>
                                                                                    <li><a class="color" value="16" href="#"> <i class="fa fa-frown-o fa-1" aria-hidden="true" style="color:#FF0000"></i>  Mal Estado</a></li>
                                                                            </ul>
                                                                    </li>

                                                            </ul>
                                                    </div>
                                            </td>
                                    </tr>
                                    <tr>
                                            <td>
                                                    <div class="color" value="4">
                                                            <a class="dropdown-toggle" type="button" data-toggle="dropdown">
                                                                    <img src="plugins/images/odontograma/Ausente.svg" width="25px" height="25px">
                                                                    Diente ausente
                                                            </a>
                                                    </div>
                                            </td>
                                    </tr>
                                    <tr>
                                            <td>
                                                    <div class="color" value="5">
                                                            <a class="dropdown-toggle" type="button" data-toggle="dropdown">
                                                                    <img src="plugins/images/odontograma/Extraer.svg" width="25px" height="25px">
                                                                    Diente por extraer
                                                            </a>
                                                    </div>
                                            </td>
                                    </tr>
                            </tbody>
                    </table> -->
                </div>
            </div>
        </div>
    </div>
    <div class="tab-pane" id="profile">
        <div class="row">
            <div class="col-lg-12">
                <div class="white-box" style="padding-left: 0px;padding-right: 0px;padding-top: 0px;padding-bottom: 0px;margin-left: 0px;margin-right: 0px;margin-top: 0px;margin-bottom: 0px;border-left: 0px;border-right: 0px;">

                    <ul class="nav nav-tab-vertical">
                        <li class="dropdown">
                            <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                                <img src="plugins/images/odontograma/CoronaBuena.svg" width="15px" height="15px" class="img-circle">
                                <b>Corona Definitva</b>
                                <span class="caret"></span>
                            </a>
                            <ul class="dropdown-menu">
                                <li class="dropdown-submenu">
                                    <a class="color" value="23" href="#"  role="tab" data-toggle="tab"><strong>CM</strong> &nbsp;  Corona met�lica</a>
                                </li>
                                <li class="dropdown-submenu">
                                    <a class="color" value="24" href="#"  role="tab" data-toggle="tab"><strong>CF</strong> &nbsp;  Corona Fenestrada</a>
                                </li>
                                <li class="dropdown-submenu">
                                    <a class="color" value="25" href="#"  role="tab" data-toggle="tab"><strong>CMC</strong> &nbsp;  Corona metal cer�mica</a>
                                </li>
                                <li class="dropdown-submenu">
                                    <a class="color" value="26" href="#"  role="tab" data-toggle="tab"><strong>CV</strong> &nbsp;  Corona Veneer</a>
                                </li>
                                <li class="dropdown-submenu">
                                    <a class="color" value="27" href="#"  role="tab" data-toggle="tab"><strong>CJ</strong> &nbsp;  Corona Jacket</a>
                                </li>
                            </ul>
                        </li>
                        <li>
                            <a class="color" value="28" role="tab" data-toggle="tab" href="#">
                                <img src="plugins/images/odontograma/CoronaMala.svg" width="15px" height="15px" class="img-circle">
                                <b>Corona Temporal</b>
                            </a>
                        </li>
                        <li>
                            <a class="color" value="29" role="tab" data-toggle="tab" href="#">
                                <img src="plugins/images/odontograma/Diastema.svg" width="15px" height="15px" class="img-circle">
                                <b>Diastema</b>
                            </a>
                        </li>
                        <li class="dropdown">
                            <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                                <img src="plugins/images/odontograma/ImplanteBueno.svg" width="15px" height="15px" class="img-circle">
                                <b>Implante</b>
                                <span class="caret"></span>
                            </a>
                            <ul class="dropdown-menu">
                                <li><a class="color" value="30" href="#"  role="tab" data-toggle="tab"><i class="fa fa-smile-o fa-1" aria-hidden="true" style="color:blue"></i>  Buen Estado</a></li>
                                <li><a class="color" value="31" href="#"  role="tab" data-toggle="tab"> <i class="fa fa-frown-o fa-1" aria-hidden="true" style="color:#FF0000"></i>  Mal Estado</a></li>
                            </ul>
                        </li>
                        <li class="dropdown">
                            <a class="color" value="32"  role="tab" data-toggle="tab" href="#">
                                <img src="plugins/images/odontograma/Fusion.svg" alt="user-img" width="15px" height="15px" class="img-circle">
                                <b>Fusi�n</b>
                            </a>
                        </li>
                        <li class="dropdown">
                            <a class="color" value="33"  role="tab" data-toggle="tab" href="#">
                                <img src="plugins/images/odontograma/Geminacion.svg" alt="user-img" width="15px" height="15px" class="img-circle">
                                <b>Geminaci�n</b>
                            </a>
                        </li>
                        <li class="dropdown">
                            <a class="color" value="34"  role="tab" data-toggle="tab" href="#">
                                <img src="plugins/images/odontograma/Clavija.svg" alt="user-img" width="15px" height="15px" class="img-circle">
                                <b>Clavija</b>
                            </a>
                        </li>
                        <li class="dropdown">
                            <a class="color" value="35"  role="tab" data-toggle="tab" href="#">
                                <img src="plugins/images/odontograma/Supernumerario.svg" alt="user-img" width="15px" height="15px" class="img-circle">
                                <b>Supernumerario</b>
                            </a>
                        </li>
                        <li class="dropdown">
                            <a class="color" value="36"  role="tab" data-toggle="tab" href="#">
                                <img src="plugins/images/odontograma/Transposicion.svg" alt="user-img" width="15px" height="15px" class="img-circle">
                                <b>Transposici�n</b>
                            </a>
                        </li>
                        <!-- <li class="dropdown">
                                <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                                        <img src="plugins/images/odontograma/ProtesisFija.png" width="25px" class="img-circle">
                                        <b class="hidden-xs">Protesis Fija</b>
                                        <span class="caret"></span>
                                </a>
                                <ul class="dropdown-menu">
                                        <li><a id="testeo" onclick="myfunction();" class="color" value="22" href="#"  role="tab" data-toggle="tab" onclick="ProtesisFija();"><i class="fa fa-smile-o fa-1" aria-hidden="true" style="color:blue"></i>  Buen Estado</a></li>
                                        <li><a class="color" value="22" href="#"  role="tab" data-toggle="tab"> <i class="fa fa-frown-o fa-1" aria-hidden="true" style="color:#FF0000"></i>  Mal Estado</a></li>
                                </ul>
                        </li> -->
                    </ul>

                    <!-- <table id="demo-foo-pagination" class="table m-b-0 toggle-arrow-tiny tabla-hover">
                            <tbody>
                                    <tr>
                                            <td>
                                                    <div class="dropdown">
                                                            <a class="dropdown-toggle" type="button" data-toggle="dropdown" style="padding:0px 5px;" aria-expanded="false">
                                                                    <img src="plugins/images/odontograma/Movilidad.png">
                                                                    Movilidad
                                                                    <span class="caret"></span>
                                                            </a>
                                                            <ul class="dropdown-menu dropdown-menu-right">
                                                                    <li><a href="#">  <strong>M1</strong>&nbsp;Movilidad 1 </a></li>
                                                                    <li><a href="#">  <strong>M2</strong>&nbsp;Movilidad 2 </a></li>
                                                                    <li><a href="#">  <strong>M3</strong>&nbsp;Movilidad 3 </a></li>
                                                            </ul>
                                                    </div>
                                            </td>
                                    </tr>
                                    <tr>
                                            <td>
                                                    <div class="dropdown">
                                                            <a class="dropdown-toggle" type="button" data-toggle="dropdown" style="padding:0px 5px;" aria-expanded="false">
                                                                    <img src="plugins/images/odontograma/ProtesisFija.png">
                                                                    Pr�tesis Fija
                                                                    <span class="caret"></span>
                                                            </a>
                                                            <ul class="dropdown-menu dropdown-menu-right">
                                                                    <li><a href="#"><i class="fa fa-smile-o fa-1" aria-hidden="true" style="color:blue"></i>  Buen Estado</a></li>
                                                                    <li><a href="#"> <i class="fa fa-frown-o fa-1" aria-hidden="true" style="color:#FF0000"></i>  Mal Estado</a></li>
                                                            </ul>
                                                    </div>
                                            </td>
                                    </tr>
                                    <tr>
                                            <td>
                                                    <div class="color" value="14">
                                                            <a class="dropdown-toggle" type="button" data-toggle="dropdown">
                                                                    <img src="plugins/images/odontograma/RemanenteRadicular.svg" width="25px" height="25px">
                                                                    Remanente radicular
                                                            </a>
                                                    </div>
                                            </td>
                                    </tr>

                                    <tr>
                                            <td>
                                                    <div class="dropdown">
                                                            <a class="dropdown-toggle" type="button" data-toggle="dropdown" aria-expanded="false">
                                                                    <img src="plugins/images/odontograma/CoronaBuena.svg" width="25px" height="25px">
                                                                    Corona Definitva
                                                                    <span class="caret"></span>
                                                            </a>
                                                            <ul class="dropdown-menu  multi-level dropdown-menu-left">
                                                                    <li class="dropdown-submenu">
                                                                            <a href="#" data-original-title="" title=""><i class="fa fa-fw fa-caret-left" style="margin:0px;margin-left:-15px;"></i> <strong>CM</strong> &nbsp;  Corona met�lica</a>
                                                                            <ul class="dropdown-menu dropdown-menu-left" style="left:46px;">
                                                                                    <li><a class="color" value="8" href="#"><i class="fa fa-smile-o fa-1" aria-hidden="true" style="color:blue"></i>  Buen Estado</a></li>
                                                                                    <li><a class="color" value="10" href="#"> <i class="fa fa-frown-o fa-1" aria-hidden="true" style="color:#FF0000"></i>  Mal Estado</a></li>
                                                                            </ul>
                                                                    </li>
                                                                    <li class="dropdown-submenu">
                                                                            <a href="#" data-original-title="" title=""><i class="fa fa-fw fa-caret-left" style="margin:0px;margin-left:-15px;"></i> <strong>CF</strong> &nbsp;  Corona Fenestrada</a>
                                                                            <ul class="dropdown-menu dropdown-menu-left" style="left:46px;">
                                                                                    <li><a class="color" value="8" href="#"><i class="fa fa-smile-o fa-1" aria-hidden="true" style="color:blue"></i>  Buen Estado</a></li>
                                                                                    <li><a class="color" value="10" href="#"> <i class="fa fa-frown-o fa-1" aria-hidden="true" style="color:#FF0000"></i>  Mal Estado</a></li>
                                                                            </ul>
                                                                    </li>
                                                                    <li class="dropdown-submenu">
                                                                            <a href="#" data-original-title="" title=""><i class="fa fa-fw fa-caret-left" style="margin:0px;margin-left:-15px;"></i> <strong>CMC</strong> &nbsp;  Corona metal cer�mica</a>
                                                                            <ul class="dropdown-menu dropdown-menu-left" style="left:46px;">
                                                                                    <li><a class="color" value="8" href="#"><i class="fa fa-smile-o fa-1" aria-hidden="true" style="color:blue"></i>  Buen Estado</a></li>
                                                                                    <li><a class="color" value="10" href="#"> <i class="fa fa-frown-o fa-1" aria-hidden="true" style="color:#FF0000"></i>  Mal Estado</a></li>
                                                                            </ul>
                                                                    </li>
                                                                    <li class="dropdown-submenu">
                                                                            <a href="#" data-original-title="" title=""><i class="fa fa-fw fa-caret-left" style="margin:0px;margin-left:-15px;"></i> <strong>CV</strong> &nbsp;  Corona Veneer</a>
                                                                            <ul class="dropdown-menu dropdown-menu-left" style="left:46px;">
                                                                                    <li><a class="color" value="8" href="#"><i class="fa fa-smile-o fa-1" aria-hidden="true" style="color:blue"></i>  Buen Estado</a></li>
                                                                                    <li><a class="color" value="10" href="#"> <i class="fa fa-frown-o fa-1" aria-hidden="true" style="color:#FF0000"></i>  Mal Estado</a></li>
                                                                            </ul>
                                                                    </li>
                                                                    <li class="dropdown-submenu">
                                                                            <a href="#" data-original-title="" title=""><i class="fa fa-fw fa-caret-left" style="margin:0px;margin-left:-15px;"></i> <strong>CJ</strong> &nbsp;  Corona Jacket</a>
                                                                            <ul class="dropdown-menu dropdown-menu-left" style="left:46px;">
                                                                                    <li><a class="color" value="8" href="#"><i class="fa fa-smile-o fa-1" aria-hidden="true" style="color:blue"></i>  Buen Estado</a></li>
                                                                                    <li><a class="color" value="10" href="#"> <i class="fa fa-frown-o fa-1" aria-hidden="true" style="color:#FF0000"></i>  Mal Estado</a></li>
                                                                            </ul>
                                                                    </li>
                                                            </ul>
                                                    </div>
                                            </td>
                                    </tr>
                                    <tr>
                                            <td>
                                                    <div class="color" value="10">
                                                            <a class="dropdown-toggle" type="button" data-toggle="dropdown">
                                                                    <img src="plugins/images/odontograma/CoronaMala.svg" width="25px" height="25px">
                                                                    Corona Temporal
                                                            </a>
                                                    </div>
                                            </td>
                                    </tr>
                                    <tr>
                                            <td>
                                                    <div class="color" value="19">
                                                            <a class="dropdown-toggle" type="button" data-toggle="dropdown">
                                                                    <img src="plugins/images/odontograma/Diastema.svg" width="25px" height="25px">
                                                                    Diastema
                                                            </a>
                                                    </div>
                                            </td>
                                    </tr>
                                    <tr>
                                            <td>
                                                    <div class="dropdown">
                                                            <a class="dropdown-toggle" type="button" data-toggle="dropdown" aria-expanded="false">
                                                                    <img src="plugins/images/odontograma/ImplanteDental.svg" width="25px" height="25px">
                                                                    Implante
                                                                    <span class="caret"></span>
                                                            </a>
                                                            <ul class="dropdown-menu dropdown-menu-right">
                                                                    <li><a href="#" class="color" value="20"><i class="fa fa-smile-o fa-1" aria-hidden="true" style="color:blue"></i>  Buen Estado</a></li>
                                                                    <li><a href="#" class="color" value="21"> <i class="fa fa-frown-o fa-1" aria-hidden="true" style="color:#FF0000"></i>  Mal Estado</a></li>
                                                            </ul>
                                                    </div>
                                            </td>
                                    </tr>
                            </tbody>
                    </table> -->

                </div>
            </div>
        </div>
    </div>
    <div class="tab-pane" id="settings">
        <div class="row">
            <div class="col-lg-12">
                <ul class="nav nav-tab-vertical">
                    <li class="dropdown">
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                            <img src="plugins/images/odontograma/AparatoOrtodonticoFijo.svg" width="15px" height="15px" class="img-circle">
                            <b>Ap. Ortod�ntico fijo</b>
                            <span class="caret"></span>
                        </a>
                        <ul class="dropdown-menu">
                            <li><a class="color" value="37" href="#"  role="tab" data-toggle="tab"><i class="fa fa-smile-o fa-1" aria-hidden="true" style="color:blue"></i>  Buen Estado</a></li>
                            <li><a class="color" value="38" href="#"  role="tab" data-toggle="tab"> <i class="fa fa-frown-o fa-1" aria-hidden="true" style="color:#FF0000"></i>  Mal Estado</a></li>
                        </ul>
                    </li>
                    <li class="dropdown">
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                            <img src="plugins/images/odontograma/AparatoOrtodonticoRemovible.svg" width="15px" height="15px" class="img-circle">
                            <b>Ap. Ortod�ntico remo</b>
                            <span class="caret"></span>
                        </a>
                        <ul class="dropdown-menu">
                            <li><a class="color" value="39" href="#"  role="tab" data-toggle="tab"><i class="fa fa-smile-o fa-1" aria-hidden="true" style="color:blue"></i>  Buen Estado</a></li>
                            <li><a class="color" value="40" href="#"  role="tab" data-toggle="tab"> <i class="fa fa-frown-o fa-1" aria-hidden="true" style="color:#FF0000"></i>  Mal Estado</a></li>
                        </ul>
                    </li>
                    <li class="dropdown">
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                            <img src="plugins/images/odontograma/ProtesisFijaBuena.svg" width="15px" height="15px" class="img-circle">
                            <b>Pr�tesis fija Buena</b>
                            <span class="caret"></span>
                        </a>
                        <ul class="dropdown-menu">
                            <li><a class="color" value="41" href="#"  role="tab" data-toggle="tab">Puente izquierdo</a></li>
                            <li><a class="color" value="42" href="#"  role="tab" data-toggle="tab">Puente derecho</a></li>
                            <li><a class="color" value="43" href="#"  role="tab" data-toggle="tab">Pilar</a></li>
                            <li><a class="color" value="58" href="#"  role="tab" data-toggle="tab">Puente total</a></li>
                        </ul>
                    </li>
                    <li class="dropdown">
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                            <img src="plugins/images/odontograma/ProtesisFijaMala.svg" width="15px" height="15px" class="img-circle">
                            <b>Pr�tesis fija Mala</b>
                            <span class="caret"></span>
                        </a>
                        <ul class="dropdown-menu">
                            <li><a class="color" value="44" href="#"  role="tab" data-toggle="tab">Puente izquierdo</a></li>
                            <li><a class="color" value="45" href="#"  role="tab" data-toggle="tab">Puente derecho</a></li>
                            <li><a class="color" value="46" href="#"  role="tab" data-toggle="tab">Pilar</a></li>
                            <li><a class="color" value="59" href="#"  role="tab" data-toggle="tab">Puente total</a></li>
                        </ul>
                    </li>
                    <li class="dropdown">
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                            <img src="plugins/images/odontograma/ProtesisRemovible.svg" width="15px" height="15px" class="img-circle">
                            <b>Pr�tesis removible</b>
                            <span class="caret"></span>
                        </a>
                        <ul class="dropdown-menu">
                            <li><a class="color" value="47" href="#"  role="tab" data-toggle="tab"><i class="fa fa-smile-o fa-1" aria-hidden="true" style="color:blue"></i>  Buen Estado</a></li>
                            <li><a class="color" value="48" href="#"  role="tab" data-toggle="tab"> <i class="fa fa-frown-o fa-1" aria-hidden="true" style="color:#FF0000"></i>  Mal Estado</a></li>
                        </ul>
                    </li>
                    <li class="dropdown">
                        <a class="color" value="49"  role="tab" data-toggle="tab" href="#">
                            <img src="plugins/images/odontograma/DienteExtruido.svg" alt="user-img" width="15px" height="15px" class="img-circle">
                            <b>Diente extru�do</b>
                        </a>
                    </li>
                    <li class="dropdown">
                        <a class="color" value="50"  role="tab" data-toggle="tab" href="#">
                            <img src="plugins/images/odontograma/DienteIntruido.svg" alt="user-img" width="15px" height="15px" class="img-circle">
                            <b>Diente intru�do</b>
                        </a>
                    </li>
                    <li class="dropdown">
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                            <img src="plugins/images/odontograma/Giroversion.svg" width="15px" height="15px" class="img-circle">
                            <b>Giroversi�n</b>
                            <span class="caret"></span>
                        </a>
                        <ul class="dropdown-menu">
                            <li><a class="color" value="51" href="#"  role="tab" data-toggle="tab">Mesial</a></li>
                            <li><a class="color" value="52" href="#"  role="tab" data-toggle="tab">Distal</a></li>
                        </ul>
                    </li>
                    <li class="dropdown">
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                            <img src="plugins/images/odontograma/Migracion.svg" width="15px" height="15px" class="img-circle">
                            <b>Migraci�n</b>
                            <span class="caret"></span>
                        </a>
                        <ul class="dropdown-menu">
                            <li><a class="color" value="53" href="#"  role="tab" data-toggle="tab">Mesial</a></li>
                            <li><a class="color" value="54" href="#"  role="tab" data-toggle="tab">Distal</a></li>
                        </ul>
                    </li>
                    <li class="dropdown">
                        <a class="color" value="55"  role="tab" data-toggle="tab" href="#">
                            <img src="plugins/images/odontograma/EdentuloTotal.svg" alt="user-img" width="15px" height="15px" class="img-circle">
                            <b>Ed�ntulo total</b>
                        </a>
                    </li>
                    <li class="dropdown">
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                            <img src="plugins/images/odontograma/ProtesisTotal.svg" width="15px" height="15px" class="img-circle">
                            <b>Pr�tesis total</b>
                            <span class="caret"></span>
                        </a>
                        <ul class="dropdown-menu">
                            <li><a class="color" value="56" href="#" role="tab" data-toggle="tab"><i class="fa fa-smile-o fa-1" aria-hidden="true" style="color:blue"></i>  Buen Estado</a></li>
                            <li><a class="color" value="57" href="#" role="tab" data-toggle="tab"><i class="fa fa-frown-o fa-1" aria-hidden="true" style="color:#FF0000"></i>  Mal Estado</a></li>
                        </ul>
                    </li>
                </ul>
            </div>                                    
        </div>
    </div>
</div>
<!--Jquery/Javascrip-->
<script type="text/javascript" src="scripts/jquery-odontograma.js"></script>
