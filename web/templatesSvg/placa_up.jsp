<svg width="40" height="40" viewBox="0 0 40 40" id="placa_up{{info.id}}" class="{{info.tipoDiente}}">
    <g transform="translate(0,0)">
        <path pareja="0" class="extruido" value="6" d="m 18 6 l 0 15 l -3 0 l 5 6 l 5 -6 l -3 0 l 0 -15 z "/>
        <path pareja="0" class="intruido" value="6" d="m 18 13 l 0 15 l 4 0 l 0 -15 l 3 0 l -5 -6 l -5 6 l 3 0 z "/>
        <path pareja="0" class="giroversion_1" value="6" d="m 5 5 a 7 5 0 0 0 30 0 l 2 4 m -2 -4 l -3 3 m 11 0 "/>
        <path pareja="0" class="giroversion_2" value="6" d="m 5 5 a 7 5 0 1 0 30 0 m -30 0 l 3 2 m -3 -2 l -2 3 "/>
        <path pareja="0" class="migracion_1" value="6" d="m 10 10 l 15 0 l 0 -3 l 5 5 l -5 5 l 0 -3 l -15 0 z"/>
        <path pareja="0" class="migracion_2" value="6" d="m 31 10 l -15 0 l 0 -3 l -5 5 l 5 5 l 0 -3 l 15 0 z"/>
    </g>
</svg>