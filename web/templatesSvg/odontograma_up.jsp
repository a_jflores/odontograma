<svg width="50" height="50" viewBox="0 0 50 50" id="up{{info.id}}" class="{{info.tipoDiente}}">
    <g transform="translate(-20,0)">
        <text class="numeracion" x="41" y="35" style="font-family: Times New Roman;
               font-size  : 10;
               stroke-width: 1.1;
               stroke     : black;
               fill       : black;">{{info.numero}}
        </text>
        <path pareja="0" class="clavija" value="6" d="m 30 38 l 30 0 l -15 -20 z"/>
        <ellipse pareja="0" class="fusion1" value="6" cx="60" cy="33" rx="25" ry="10"/>
        <ellipse pareja="0" class="fusion2" value="6" cx="85" cy="33" rx="25" ry="10"/>
        <ellipse pareja="0" class="fusion3" value="6" cx="33" cy="33" rx="25" ry="10"/>
        <ellipse pareja="0" class="fusion4" value="6" cx="6" cy="33" rx="25" ry="10"/>
        <circle pareja="0" class="geminacion" value="6" cx="46" cy="32" r="10"/>
        <circle pareja="0" class="supernumerario1" value="6" cx="20" cy="39" r="10"/>
        <circle pareja="0" class="supernumerario2" value="6" cx="70" cy="39" r="10"/>
        <text pareja="0" class="supernumerario3" value="6" x="66" y="44" style="font-family: Times New Roman;
               font-size  : 14;
               stroke-width: 1.1;
               stroke     : blue;
               fill       : blue;">S
        </text>
        <text pareja="0" class="supernumerario4" value="6" x="16" y="44" style="font-family: Times New Roman;
               font-size  : 14;
               stroke-width: 1.1;
               stroke     : blue;
               fill       : blue;">S
        </text>
        
        <path pareja="0" class="transposicion1" value="6" d="m 43 25 a 15 13 0 0 1 50 0 l -5 -4 m 5 4 l 4 -4"/>
        <path pareja="0" class="transposicion2" value="6" d="m 52 20 a 15 13 0 0 1 50 10 m -50 -10 l -2 -4 m 6 2 l -4 2"/>
        <path pareja="0" class="transposicion3" value="6" d="m -9 30 a 15 13 0 0 1 50 -10 m 0 0 l 1 -5 m -1 5 l -4 -3"/>
        <path pareja="0" class="transposicion4" value="6" d="m 0 25 a 15 13 0 0 1 50 0 m -50 0 l -5 -5 m 10 0 l -5 5"/>
        
        <path pareja="0" class="ortodontico_fijo1" value="6" d="m 41 13 l 10 0 l 0 10 l -10 0 z m 10 5 l -35 0 m 30 -5 l 0 10"/>
        <path pareja="0" class="ortodontico_fijo2" value="6" d="m 41 13 l 10 0 l 0 10 l -10 0 z m 35 5 l -35 0 m 5 -5 l 0 10"/>
        <path pareja="0" class="ortodontico_fijo3" value="6" d="m 20 18 l 50 0"/>
        
        <path pareja="0" class="ortodontico_remo" value="6" d="m 20 20 l 5 -10 l 5 10 l 5 -10 l 5 10 l 5 -10 l 5 10 l 5 -10 l 5 10 l 5 -10 l 5 10"/>
        
        <path pareja="0" class="puente_1" value="6" d="m 20 10 l 25 0"/>
        <path pareja="0" class="puente_2" value="6" d="m 45 10 l 25 0"/>
        <path pareja="0" class="pilar" value="6" d="m 45 10 l 0 15"/>
        <path pareja="0" class="puente_total" value="6" d="m 20 10 l 50 0"/>
        
        <path pareja="0" class="protesis_remo" value="6" d="m 20 10 l 50 0 m -50 5 l 50 0 l 0 1 l -50 0 l 0 -1 z "/>

    </g>
</svg>
