<style>
    table { 
        border-spacing: 0px;
    }
    td {
        padding: 0px;
        text-align: center;
        font-size:12px;
        width: 30px;
    }
    
    .scalado {
        zoom: 55%;
        transform: scale(1.5,0.9);
    }
</style>

<div ng-controller="dientes">
    <div class="row">
        <div class="col-md-9 col-xs-12">
            <div class="white-box">
                <div class="table-responsive">
                    <center>
                        <table border="0">
                            <colgroup>
                                <col>
                                <col>
                                <col>
                                <col>
                                <col>
                                <col>
                                <col>
                                <col style="border-right: 1px solid gray;">
                                <col>
                                <col>
                                <col>
                                <col>
                                <col>
                                <col>
                                <col>
                            </colgroup>
                            <tbody>
                                <tr>
                                    <td style="border: 1px solid gray;"> CC</td>
                                    <td style="border: 1px solid gray;"> </td>
                                    <td style="border: 1px solid gray;"> </td>
                                    <td style="border: 1px solid gray;"> </td>
                                    <td style="border: 1px solid gray;"> </td>
                                    <td style="border: 1px solid gray;"> </td>
                                    <td style="border: 1px solid gray;"> </td>
                                    <td style="border: 1px solid gray;"> </td>
                                    <td style="border: 1px solid gray;"> </td>
                                    <td style="border: 1px solid gray;"> </td>
                                    <td style="border: 1px solid gray;"> </td>
                                    <td style="border: 1px solid gray;"> </td>
                                    <td style="border: 1px solid gray;"> </td>
                                    <td style="border: 1px solid gray;"> </td>
                                    <td style="border: 1px solid gray;"> </td>
                                    <td style="border: 1px solid gray;"> </td>
                                </tr>
                                <tr>
                                    <td style="text-align: center;" ng-repeat="i in numeracion_arriba"><odontogramaup info="i"></odontogramaup></td>
                                </tr>
                                <tr>
                                    <td><diente18></diente18></td>
                                    <td><diente17></diente17></td>
                                    <td><diente16></diente16></td>
                                    <td><diente15></diente15></td>
                                    <td><diente14></diente14></td>
                                    <td><diente13></diente13></td>
                                    <td><diente12></diente12></td> 
                                    <td><diente11></diente11></td>
                                    <td><diente21></diente21></td>
                                    <td><diente22></diente22></td> 
                                    <td><diente23></diente23></td>
                                    <td><diente24></diente24></td>
                                    <td><diente25></diente25></td>
                                    <td><diente26></diente26></td>
                                    <td><diente27></diente27></td>
                                    <td><diente28></diente28></td>
                                </tr>
                                <tr>
                                    <td style="text-align: center;" ng-repeat="i in adultoArriba"><canvasodontograma info="i"></canvasodontograma></td>
                                </tr>
                                <tr>
                                    <td style="text-align: center;" ng-repeat="i in numeracion_arriba"><placaup info="i"></placaup></td>
                                </tr>
                                <tr>
                                    <td colspan="3"></td>
                                    <td style="border: 1px solid gray;"> CC</td>
                                    <td style="border: 1px solid gray;"> </td>
                                    <td style="border: 1px solid gray;"> </td>
                                    <td style="border: 1px solid gray;"> </td>
                                    <td style="border: 1px solid gray;"> </td>
                                    <td style="border: 1px solid gray;"> </td>
                                    <td style="border: 1px solid gray;"> </td>
                                    <td style="border: 1px solid gray;"> </td>
                                    <td style="border: 1px solid gray;"> </td>
                                    <td style="border: 1px solid gray;"> </td>
                                    <td colspan="3"></td>
                                </tr>
                                <tr>
                                    <td colspan="3"></td>
                                    <td style="text-align: center;" ng-repeat="i in ninoArriba"><odontogramaup info="i"></odontogramaup></td>
                                    <td colspan="3"></td>
                                </tr>
                                <tr>
                                    <td colspan="3"></td>
                                    <td><diente55></diente55></td>
                                    <td><diente54></diente54></td>
                                    <td><diente53></diente53></td>
                                    <td><diente52></diente52></td> 
                                    <td><diente51></diente51></td>
                                    <td><diente61></diente61></td>
                                    <td><diente62></diente62></td> 
                                    <td><diente63></diente63></td>
                                    <td><diente64></diente64></td>
                                    <td><diente65></diente65></td>
                                    <td colspan="3"></td>
                                </tr>
                                <tr>
                                    <td colspan="3"></td>
                                    <td style="text-align: center;" info="i" ng-repeat="i in ninoArriba"><canvasodontograma info="i"></canvasodontograma></td>
                                    <td colspan="3"></td>
                                </tr>
                                <tr>
                                    <td colspan="3"></td>
                                    <td style="text-align: center;" ng-repeat="i in ninoArriba"><placaup info="i"></placaup></td>
                                    <td colspan="3"></td>
                                </tr>
                                <tr>
                                    <td colspan="3"></td>
                                    <td style="text-align: center;" ng-repeat="i in ninoAbajo"><placadown info="i"></placadown></td>
                                    <td colspan="3"></td>
                                </tr>
                                <tr>
                                    <td colspan="3"></td>
                                    <td style="text-align: center;" info="i" ng-repeat="i in ninoAbajo"><canvasodontograma info="i"></canvasodontograma></td>
                                    <td colspan="3"></td>
                                </tr>
                                <tr>
                                    <td colspan="3"></td>
                                    <td><diente85></diente85></td>
                                    <td><diente84></diente84></td>
                                    <td><diente83></diente83></td>
                                    <td><diente82></diente82></td> 
                                    <td><diente81></diente81></td>
                                    <td><diente71></diente71></td>
                                    <td><diente72></diente72></td> 
                                    <td><diente73></diente73></td>
                                    <td><diente74></diente74></td>
                                    <td><diente75></diente75></td>
                                    <td colspan="3"></td>
                                </tr>
                                <tr>
                                    <td colspan="3"></td>
                                    <td style="text-align: center;" ng-repeat="i in ninoAbajo"><odontogramadown info="i"></odontogramadown></td>
                                    <td colspan="3"></td>
                                </tr>
                                <tr>
                                    <td colspan="3"></td>
                                    <td style="border: 1px solid gray;"> CC</td>
                                    <td style="border: 1px solid gray;"> </td>
                                    <td style="border: 1px solid gray;"> </td>
                                    <td style="border: 1px solid gray;"> </td>
                                    <td style="border: 1px solid gray;"> </td>
                                    <td style="border: 1px solid gray;"> </td>
                                    <td style="border: 1px solid gray;"> </td>
                                    <td style="border: 1px solid gray;"> </td>
                                    <td style="border: 1px solid gray;"> </td>
                                    <td style="border: 1px solid gray;"> </td>
                                    <td colspan="3"></td>
                                </tr>
                                <tr>
                                    <td style="text-align: center;" ng-repeat="i in numeracion_abajo"><placadown info="i"></placadown></td>
                                </tr>
                                <tr>
                                    <td style="text-align: center;" info="i" ng-repeat="i in adultoAbajo"><canvasodontograma info="i"></canvasodontograma></td>
                                </tr>
                                <tr>
                                    <td><diente48></diente48></td>
                                    <td><diente47></diente47></td>
                                    <td><diente46></diente46></td>
                                    <td><diente45></diente45></td>
                                    <td><diente44></diente44></td>
                                    <td><diente43></diente43></td>
                                    <td><diente42></diente42></td> 
                                    <td><diente41></diente41></td>
                                    <td><diente31></diente31></td>
                                    <td><diente32></diente32></td> 
                                    <td><diente33></diente33></td>
                                    <td><diente34></diente34></td>
                                    <td><diente35></diente35></td>
                                    <td><diente36></diente36></td>
                                    <td><diente37></diente37></td>
                                    <td><diente38></diente38></td>
                                </tr>
                                <tr>
                                    <td style="text-align: center;" ng-repeat="i in numeracion_abajo"><odontogramadown info="i"></odontogramadown></td>
                                </tr>
                                <tr>
                                    <td style="border: 1px solid gray;">CC </td>
                                    <td style="border: 1px solid gray;"> </td>
                                    <td style="border: 1px solid gray;"> </td>
                                    <td style="border: 1px solid gray;"> </td>
                                    <td style="border: 1px solid gray;"> </td>
                                    <td style="border: 1px solid gray;"> </td>
                                    <td style="border: 1px solid gray;"> </td>
                                    <td style="border: 1px solid gray;"> </td>
                                    <td style="border: 1px solid gray;"> </td>
                                    <td style="border: 1px solid gray;"> </td>
                                    <td style="border: 1px solid gray;"> </td>
                                    <td style="border: 1px solid gray;"> </td>
                                    <td style="border: 1px solid gray;"> </td>
                                    <td style="border: 1px solid gray;"> </td>
                                    <td style="border: 1px solid gray;"> </td>
                                    <td style="border: 1px solid gray;"> </td>
                                </tr>
                            </tbody>
                        </table>
                    </center>
                </div>
                <br>
                <label>Observaciones</label>
                <textarea id="observaciones" class="form-control" rows="3"></textarea>
            </div>
        </div>
        <div class="col-md-3 col-xs-12">
            <div class="white-box">
                <opcionescanvas></opcionescanvas>
            </div>
        </div>
    </div>
</div>