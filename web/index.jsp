<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="java.util.*"%>
<!DOCTYPE html>
<html lang="es">
<head>
    <jsp:include page="/templates/head.jsp" flush="true"/>
</head>
<body class="fix-header">
    <div class="preloader">
        <svg class="circular" viewBox="25 25 50 50">
            <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10" />
        </svg>
    </div>
    <div id="wrapper">
        <jsp:include page="/templates/header.jsp" flush="true"/> 
        <jsp:include page="/templates/navbar.jsp" flush="true"/>
        <div id="page-wrapper">
            <div class="container-fluid">
                <jsp:include page="/dashboard.jsp"/>
                <jsp:include page="/templates/right-sidebar.jsp" flush="true"/>
            </div>
            <jsp:include page="/templates/footer.jsp" flush="true"/>
        </div>
    </div>
    <jsp:include page="/templates/scripts.jsp" flush="true"/>
</body>
</html>
