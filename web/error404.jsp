<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>  
<html lang="es">
<head>
    <jsp:include page="/templates/head.jsp" flush="true" />
</head>
<body>
<section id="wrapper" class="error-page">
  <div class="error-box">
    <div class="error-body text-center">
      <h1 class="text-danger">404</h1>
      <h3 class="text-uppercase">Página no encontrada!</h3>
      <p class="text-muted m-t-30 m-b-30">PARECE ESTAR TRATANDO DE ENCONTRAR SU CAMINO A CASA</p>
      <a href="${pageContext.request.contextPath}/index.jsp" class="btn btn-danger btn-rounded waves-effect waves-light m-b-40">Regresar</a> </div>
  </div>
</section>
</body>
</html>
