app.directive('odontogramaup', function () {
    return {
        restrict: 'E',
        scope: {
            info: "=",
        },
        templateUrl: 'templatesSvg/odontograma_up.jsp'}
});