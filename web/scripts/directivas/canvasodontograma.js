app.directive('canvasodontograma', function() { 
  return { 
    restrict: 'E', 
    scope: { 
      info:"=",
    }, 
    templateUrl: 'templatesSvg/canvas_odontograma.jsp' 
  }; 
});