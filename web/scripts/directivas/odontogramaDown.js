app.directive('odontogramadown', function () {
    return {
        restrict: 'E',
        scope: {
            info: "=",
        },
        templateUrl: 'templatesSvg/odontograma_down.jsp'}
});