app.controller('dientes', ['$scope', function ($scope) {
        var jsonArmado;

        var adultoArriba = [];
        for (var i = 1; i < 17; i++) {
            if (i > 3 && i < 14) {
                jsonArmado = {id: i, tipoDiente: 'decidua mixta'};
                adultoArriba.push(jsonArmado);
            } else
            {
                jsonArmado = {id: i, tipoDiente: 'decidua'};
                adultoArriba.push(jsonArmado);
            }
        }
        $scope.adultoArriba = adultoArriba;

        var adultoAbajo = [];
        for (var i = 17; i < 33; i++) {
            if (i > 19 && i < 30) {
                jsonArmado = {id: i, tipoDiente: 'decidua mixta'};
                adultoAbajo.push(jsonArmado);
            } else
            {
                jsonArmado = {id: i, tipoDiente: 'decidua'};
                adultoAbajo.push(jsonArmado);
            }
        }
        $scope.adultoAbajo = adultoAbajo;

        var ninoArriba = [];
        var j = 55;
        var k = 61;
        for (var i = 33; i < 43; i++) {
            if (i > 37) {
                jsonArmado = {id: i, numero: k, tipoDiente: 'nino arriba'};
                ninoArriba.push(jsonArmado);
                k++;
            } else {
                jsonArmado = {id: i, numero: j, tipoDiente: 'nino arriba'};
                ninoArriba.push(jsonArmado);
                j--;
            }
        }
        $scope.ninoArriba = ninoArriba;

        var ninoAbajo = [];
        var j = 85;
        var k = 71;
        for (var i = 43; i < 53; i++) {
            if (i > 47) {
                jsonArmado = {id: i, numero: k, tipoDiente: 'nino arriba'};
                ninoAbajo.push(jsonArmado);
                k++;
            } else {
                jsonArmado = {id: i, numero: j, tipoDiente: 'nino arriba'};
                ninoAbajo.push(jsonArmado);
                j--;
            }
        }
        $scope.ninoAbajo = ninoAbajo;

//        var dienteAdulto = [];
//        for (var i = 53; i < 56; i++) {
//            jsonArmado = {id: i, tipoDiente: 'diente adulto'};
//            dienteAdulto.push(jsonArmado);
//
//        }
//        $scope.dienteAdulto = dienteAdulto;

        /**************************************************************************/

        var adultoUp = [];
        for (var i = 56; i < 72; i++) {
            if (i > 58 && i < 69) {
                jsonArmado = {id: i, tipoDiente: 'adulto up'};
                adultoUp.push(jsonArmado);
            } else
            {
                jsonArmado = {id: i, tipoDiente: 'decidua'};
                adultoUp.push(jsonArmado);
            }
        }
        $scope.adultoUp = adultoUp;


        /**************************************************************************/
        var numeracion_arriba = [];
        var k = 1;

        for (var i = 18; i > 10; i--) {
            jsonArmado = {id: k, numero: i, tipoDiente: 'arribaDiente'};
            numeracion_arriba.push(jsonArmado);
            k++;
        }
        for (var j = 21; j < 29; j++) {
            jsonArmado = {id: k, numero: j, tipoDiente: 'arribaDiente'};
            numeracion_arriba.push(jsonArmado);
            k++;
        }
        $scope.numeracion_arriba = numeracion_arriba;

        var numeracion_abajo = [];
        var k = 17;

        for (var i = 48; i > 40; i--) {
            jsonArmado = {id: k, numero: i, tipoDiente: 'abajoDiente'};
            numeracion_abajo.push(jsonArmado);
            k++;
        }

        for (var j = 31; j < 39; j++) {
            jsonArmado = {id: k, numero: j, tipoDiente: 'abajoDiente'};
            numeracion_abajo.push(jsonArmado);
            k++;
        }
        $scope.numeracion_abajo = numeracion_abajo;

    }]);