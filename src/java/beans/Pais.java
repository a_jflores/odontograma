package beans;

public class Pais {
    private int idPais;
    private String nacionalidadPais;
    private String nombrePais;
    
    public Pais(){
        this.idPais = 0;
        this.nacionalidadPais = "";
        this.nombrePais = "";
    }

    public int getIdPais() {
        return idPais;
    }

    public void setIdPais(int idPais) {
        this.idPais = idPais;
    }

    public String getNacionalidadPais() {
        return nacionalidadPais;
    }

    public void setNacionalidadPais(String nacionalidadPais) {
        this.nacionalidadPais = nacionalidadPais;
    }

    public String getNombrePais() {
        return nombrePais;
    }

    public void setNombrePais(String nombrePais) {
        this.nombrePais = nombrePais;
    }
    
    
}
