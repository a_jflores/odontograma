package beans;

public class ExamenRadiografico {
    private int id_examen_radiografico;
    private HistoriaClinica historiaclinica;
    private String foto;
    private String observaciones;
    
    public ExamenRadiografico(){
        this.id_examen_radiografico = 0;
        this.historiaclinica = null;
        this.foto = null;
        this.observaciones = "";
    }

    public ExamenRadiografico(int id_examen_radiografico, HistoriaClinica historiaclinica, String foto, String observaciones) {
        this.id_examen_radiografico = id_examen_radiografico;
        this.historiaclinica = historiaclinica;
        this.foto = foto;
        this.observaciones = observaciones;
    }

    public int getId_examen_radiografico() {
        return id_examen_radiografico;
    }

    public void setId_examen_radiografico(int id_examen_radiografico) {
        this.id_examen_radiografico = id_examen_radiografico;
    }

    public HistoriaClinica getHistoriaclinica() {
        return historiaclinica;
    }

    public void setHistoriaclinica(HistoriaClinica historiaclinica) {
        this.historiaclinica = historiaclinica;
    }

    public String getFoto() {
        return foto;
    }

    public void setFoto(String foto) {
        this.foto = foto;
    }

    public String getObservaciones() {
        return observaciones;
    }

    public void setObservaciones(String observaciones) {
        this.observaciones = observaciones;
    }
    
}
