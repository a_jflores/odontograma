package beans;

public class Medico {
    private int idMedico;
    private String nombresMedico;
    private String apellidosMedico;
    private String dniMedico;
    private String tituloMedico;
    private String colegiaturaMedico;
    private String celularMedico;

    public int getIdMedico() {
        return idMedico;
    }

    public void setIdMedico(int idMedico) {
        this.idMedico = idMedico;
    }

    public String getNombresMedico() {
        return nombresMedico;
    }

    public void setNombresMedico(String nombresMedico) {
        this.nombresMedico = nombresMedico;
    }

    public String getApellidosMedico() {
        return apellidosMedico;
    }

    public void setApellidosMedico(String apellidosMedico) {
        this.apellidosMedico = apellidosMedico;
    }

    public String getDniMedico() {
        return dniMedico;
    }

    public void setDniMedico(String dniMedico) {
        this.dniMedico = dniMedico;
    }

    public String getTituloMedico() {
        return tituloMedico;
    }

    public void setTituloMedico(String tituloMedico) {
        this.tituloMedico = tituloMedico;
    }

    public String getColegiaturaMedico() {
        return colegiaturaMedico;
    }

    public void setColegiaturaMedico(String colegiaturaMedico) {
        this.colegiaturaMedico = colegiaturaMedico;
    }

    public String getCelularMedico() {
        return celularMedico;
    }

    public void setCelularMedico(String celularMedico) {
        this.celularMedico = celularMedico;
    }
    
}
