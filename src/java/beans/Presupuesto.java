package beans;

public class Presupuesto {

    private int id_presupuesto;
    private HistoriaClinica historia;
    private Medico medico;
    private int id_clinica;
    private Odontograma odontograma;
    private String asunto;
    private String fecha;
    private Double total;
    private String tipo_pago;
    private String estado;

    public Presupuesto() {
        this.id_presupuesto = 0;
        this.historia = null;
        this.medico = null;
        this.id_clinica = 0;
        this.odontograma = null;
        this.asunto = "";
        this.fecha = "";
        this.total = 0.0;
        this.tipo_pago = "";
        this.estado = "";
    }

    public Presupuesto(int id_presupuesto, HistoriaClinica historia, Medico medico, int id_clinica, Odontograma odontograma, String asunto, String fecha, Double total, String tipo_pago, String estado) {
        this.id_presupuesto = id_presupuesto;
        this.historia = historia;
        this.medico = medico;
        this.id_clinica = id_clinica;
        this.odontograma = odontograma;
        this.asunto = asunto;
        this.fecha = fecha;
        this.total = total;
        this.tipo_pago = tipo_pago;
        this.estado = estado;
    }

    public int getId_presupuesto() {
        return id_presupuesto;
    }

    public void setId_presupuesto(int id_presupuesto) {
        this.id_presupuesto = id_presupuesto;
    }

    public HistoriaClinica getHistoria() {
        return historia;
    }

    public void setHistoria(HistoriaClinica historia) {
        this.historia = historia;
    }

    public Medico getMedico() {
        return medico;
    }

    public void setMedico(Medico medico) {
        this.medico = medico;
    }

    public int getId_clinica() {
        return id_clinica;
    }

    public void setId_clinica(int id_clinica) {
        this.id_clinica = id_clinica;
    }

    public Odontograma getOdontograma() {
        return odontograma;
    }

    public void setOdontograma(Odontograma odontograma) {
        this.odontograma = odontograma;
    }
    
    public String getAsunto() {
        return asunto;
    }

    public void setAsunto(String asunto) {
        this.asunto = asunto.toUpperCase();
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public Double getTotal() {
        return total;
    }

    public void setTotal(Double total) {
        this.total = total;
    }

    public String getTipo_pago() {
        return tipo_pago;
    }

    public void setTipo_pago(String tipo_pago) {
        this.tipo_pago = tipo_pago;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

}
