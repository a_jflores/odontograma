package beans;

public class HistoriaClinica {
    private int codigoHistoriaClinica;
    private int codigoPaciente;
    private Paciente paciente;
    private String fecha_created;
    
    public HistoriaClinica(){
        this.codigoHistoriaClinica = 0;
        this.codigoPaciente = 0;
        this.paciente = null;
        this.fecha_created = "";
    }

    public HistoriaClinica(int codigoHistoriaClinica, int codigoPaciente, Paciente paciente, String fecha_created) {
        this.codigoHistoriaClinica = codigoHistoriaClinica;
        this.codigoPaciente = codigoPaciente;
        this.paciente = paciente;
        this.fecha_created = fecha_created;
    }

    public int getCodigoHistoriaClinica() {
        return codigoHistoriaClinica;
    }

    public void setCodigoHistoriaClinica(int codigoHistoriaClinica) {
        this.codigoHistoriaClinica = codigoHistoriaClinica;
    }

    public int getCodigoPaciente() {
        return codigoPaciente;
    }

    public void setCodigoPaciente(int codigoPaciente) {
        this.codigoPaciente = codigoPaciente;
    }

    public Paciente getPaciente() {
        return paciente;
    }

    public void setPaciente(Paciente paciente) {
        this.paciente = paciente;
    }

    public String getFecha_created() {
        return fecha_created;
    }

    public void setFecha_created(String fecha_created) {
        this.fecha_created = fecha_created;
    }
    
}
