package beans;

public class Video {

    private int id_video;
    private HistoriaClinica historia;
    private String nombre;
    private String ruta;
    private String observaciones;

    public Video() {
        this.id_video = 0;
        this.historia = null;
        this.nombre = "";
        this.ruta = "";
        this.observaciones = "";
    }

    public Video(int id_video, HistoriaClinica historia, String nombre, String ruta, String observaciones) {
        this.id_video = id_video;
        this.historia = historia;
        this.nombre = nombre;
        this.ruta = ruta;
        this.observaciones = observaciones;
    }

    public int getId_video() {
        return id_video;
    }

    public void setId_video(int id_video) {
        this.id_video = id_video;
    }

    public HistoriaClinica getHistoria() {
        return historia;
    }

    public void setHistoria(HistoriaClinica historia) {
        this.historia = historia;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getRuta() {
        return ruta;
    }

    public void setRuta(String ruta) {
        this.ruta = ruta;
    }

    public String getObservaciones() {
        return observaciones;
    }

    public void setObservaciones(String observaciones) {
        this.observaciones = observaciones;
    }

}
