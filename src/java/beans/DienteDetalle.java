package beans;

public class DienteDetalle {

    private int id_detalleDiente;
    private int id_odontograma;
    private int id_diente;
    private int id_cara;
    private int id_hallazgo;
    private int pareja;
    private Odontograma odontograma;
    private Diente diente;
    private CaraDental cara;
    private HallazgoDental hallazgo;

    public DienteDetalle() {
        this.id_detalleDiente = 0;
        this.id_odontograma = 0;
        this.id_diente = 0;
        this.id_cara = 0;
        this.id_hallazgo = 0;
        this.pareja = 0;
        this.odontograma = null;
        this.diente = null;
        this.cara = null;
        this.hallazgo = null;
    }

    public DienteDetalle(int id_detalleDiente, int id_odontograma, int id_diente, int id_cara, int id_hallazgo, int pareja, Odontograma odontograma, Diente diente, CaraDental cara, HallazgoDental hallazgo) {
        this.id_detalleDiente = id_detalleDiente;
        this.id_odontograma = id_odontograma;
        this.id_diente = id_diente;
        this.id_cara = id_cara;
        this.id_hallazgo = id_hallazgo;
        this.pareja = pareja;
        this.odontograma = odontograma;
        this.diente = diente;
        this.cara = cara;
        this.hallazgo = hallazgo;
    }

    public int getId_detalleDiente() {
        return id_detalleDiente;
    }

    public void setId_detalleDiente(int id_detalleDiente) {
        this.id_detalleDiente = id_detalleDiente;
    }

    public int getId_odontograma() {
        return id_odontograma;
    }

    public void setId_odontograma(int id_odontograma) {
        this.id_odontograma = id_odontograma;
    }

    public int getId_diente() {
        return id_diente;
    }

    public void setId_diente(int id_diente) {
        this.id_diente = id_diente;
    }

    public int getId_cara() {
        return id_cara;
    }

    public void setId_cara(int id_cara) {
        this.id_cara = id_cara;
    }

    public int getId_hallazgo() {
        return id_hallazgo;
    }

    public void setId_hallazgo(int id_hallazgo) {
        this.id_hallazgo = id_hallazgo;
    }

    public int getPareja() {
        return pareja;
    }

    public void setPareja(int pareja) {
        this.pareja = pareja;
    }

    public Odontograma getOdontograma() {
        return odontograma;
    }

    public void setOdontograma(Odontograma odontograma) {
        this.odontograma = odontograma;
    }

    public Diente getDiente() {
        return diente;
    }

    public void setDiente(Diente diente) {
        this.diente = diente;
    }

    public CaraDental getCara() {
        return cara;
    }

    public void setCara(CaraDental cara) {
        this.cara = cara;
    }

    public HallazgoDental getHallazgo() {
        return hallazgo;
    }

    public void setHallazgo(HallazgoDental hallazgo) {
        this.hallazgo = hallazgo;
    }

}
