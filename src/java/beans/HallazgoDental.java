package beans;

public class HallazgoDental {
    private int id_hallazgoDental;
    private String nombre;
    private String tipo;
    private String detalle;
    
    public HallazgoDental(){
        this.id_hallazgoDental = 0;
        this.nombre = "";
        this.tipo = "";
        this.detalle ="";
    }

    public HallazgoDental(int id_hallazgoDental, String nombre, String tipo, String detalle) {
        this.id_hallazgoDental = id_hallazgoDental;
        this.nombre = nombre;
        this.tipo = tipo;
        this.detalle = detalle;
    }

    public int getId_hallazgoDental() {
        return id_hallazgoDental;
    }

    public void setId_hallazgoDental(int id_hallazgoDental) {
        this.id_hallazgoDental = id_hallazgoDental;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public String getDetalle() {
        return detalle;
    }

    public void setDetalle(String detalle) {
        this.detalle = detalle;
    }
    
}
