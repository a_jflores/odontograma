package beans;

public class Anamnesis {

    private int codigoAnamnesis;
    private HistoriaClinica historiaClinica;
    private String diabetes;
    private String hipertension;
    private String hemorragia;
    private String alergia;
    private String desmayo;
    private String hepatica;
    private String fiebre_reumatica;
    private String angina;
    private String miocardio;
    private String embarazo;
    private String detalle_diabetes;
    private String detalle_hipertension;
    private String detalle_hemorragia;
    private String detalle_alergia;
    private String detalle_desmayo;
    private String detalle_hepatica;
    private String detalle_fiebre_reumatica;
    private String detalle_angina;
    private String detalle_miocardio;
    private String detalle_embarazo;
    private String alerta_diabetes;
    private String alerta_hipertension;
    private String alerta_hemorragia;
    private String alerta_alergia;
    private String alerta_desmayo;
    private String alerta_hepatica;
    private String alerta_fiebre_reumatica;
    private String alerta_angina;
    private String alerta_miocardio;
    private String alerta_embarazo;

    public Anamnesis() {
        this.codigoAnamnesis = 0;
        this.historiaClinica = null;
        this.diabetes = "";
        this.hipertension = "";
        this.hemorragia = "";
        this.alergia = "";
        this.desmayo = "";
        this.hepatica = "";
        this.fiebre_reumatica = "";
        this.angina = "";
        this.miocardio = "";
        this.embarazo = "";
        this.detalle_diabetes = "";
        this.detalle_hipertension = "";
        this.detalle_hemorragia = "";
        this.detalle_alergia = "";
        this.detalle_desmayo = "";
        this.detalle_hepatica = "";
        this.detalle_fiebre_reumatica = "";
        this.detalle_angina = "";
        this.detalle_miocardio = "";
        this.detalle_embarazo = "";
        this.alerta_diabetes = "";
        this.alerta_hipertension = "";
        this.alerta_hemorragia = "";
        this.alerta_alergia = "";
        this.alerta_desmayo = "";
        this.alerta_hepatica = "";
        this.alerta_fiebre_reumatica = "";
        this.alerta_angina = "";
        this.alerta_miocardio = "";
        this.alerta_embarazo = "";
        this.alerta_diabetes = "";
    }

    public int getCodigoAnamnesis() {
        return codigoAnamnesis;
    }

    public void setCodigoAnamnesis(int codigoAnamnesis) {
        this.codigoAnamnesis = codigoAnamnesis;
    }

    public HistoriaClinica getHistoriaClinica() {
        return historiaClinica;
    }

    public void setHistoriaClinica(HistoriaClinica historiaClinica) {
        this.historiaClinica = historiaClinica;
    }

    public String getDiabetes() {
        return diabetes;
    }

    public void setDiabetes(String diabetes) {
        this.diabetes = diabetes;
    }

    public String getHipertension() {
        return hipertension;
    }

    public void setHipertension(String hipertension) {
        this.hipertension = hipertension;
    }

    public String getHemorragia() {
        return hemorragia;
    }

    public void setHemorragia(String hemorragia) {
        this.hemorragia = hemorragia;
    }

    public String getAlergia() {
        return alergia;
    }

    public void setAlergia(String alergia) {
        this.alergia = alergia;
    }

    public String getDesmayo() {
        return desmayo;
    }

    public void setDesmayo(String desmayo) {
        this.desmayo = desmayo;
    }

    public String getHepatica() {
        return hepatica;
    }

    public void setHepatica(String hepatica) {
        this.hepatica = hepatica;
    }

    public String getFiebre_reumatica() {
        return fiebre_reumatica;
    }

    public void setFiebre_reumatica(String fiebre_reumatica) {
        this.fiebre_reumatica = fiebre_reumatica;
    }

    public String getAngina() {
        return angina;
    }

    public void setAngina(String angina) {
        this.angina = angina;
    }

    public String getMiocardio() {
        return miocardio;
    }

    public void setMiocardio(String miocardo) {
        this.miocardio = miocardo;
    }

    public String getEmbarazo() {
        return embarazo;
    }

    public void setEmbarazo(String embarazo) {
        this.embarazo = embarazo;
    }

    public String getDetalle_diabetes() {
        return detalle_diabetes;
    }

    public void setDetalle_diabetes(String detalle_diabetes) {
        this.detalle_diabetes = detalle_diabetes;
    }

    public String getDetalle_hipertension() {
        return detalle_hipertension;
    }

    public void setDetalle_hipertension(String detalle_hipertension) {
        this.detalle_hipertension = detalle_hipertension;
    }

    public String getDetalle_hemorragia() {
        return detalle_hemorragia;
    }

    public void setDetalle_hemorragia(String detalle_hemorragia) {
        this.detalle_hemorragia = detalle_hemorragia;
    }

    public String getDetalle_alergia() {
        return detalle_alergia;
    }

    public void setDetalle_alergia(String detalle_alergia) {
        this.detalle_alergia = detalle_alergia;
    }

    public String getDetalle_desmayo() {
        return detalle_desmayo;
    }

    public void setDetalle_desmayo(String detalle_desmayo) {
        this.detalle_desmayo = detalle_desmayo;
    }

    public String getDetalle_hepatica() {
        return detalle_hepatica;
    }

    public void setDetalle_hepatica(String detalle_hepatica) {
        this.detalle_hepatica = detalle_hepatica;
    }

    public String getDetalle_fiebre_reumatica() {
        return detalle_fiebre_reumatica;
    }

    public void setDetalle_fiebre_reumatica(String detalle_fiebre_reumatica) {
        this.detalle_fiebre_reumatica = detalle_fiebre_reumatica;
    }

    public String getDetalle_angina() {
        return detalle_angina;
    }

    public void setDetalle_angina(String detalle_angina) {
        this.detalle_angina = detalle_angina;
    }

    public String getDetalle_miocardio() {
        return detalle_miocardio;
    }

    public void setDetalle_miocardio(String detalle_miocardio) {
        this.detalle_miocardio = detalle_miocardio;
    }

    public String getDetalle_embarazo() {
        return detalle_embarazo;
    }

    public void setDetalle_embarazo(String detalle_embarazo) {
        this.detalle_embarazo = detalle_embarazo;
    }

    public String getAlerta_diabetes() {
        return alerta_diabetes;
    }

    public void setAlerta_diabetes(String alerta_diabetes) {
        this.alerta_diabetes = alerta_diabetes;
    }

    public String getAlerta_hipertension() {
        return alerta_hipertension;
    }

    public void setAlerta_hipertension(String alerta_hipertension) {
        this.alerta_hipertension = alerta_hipertension;
    }

    public String getAlerta_hemorragia() {
        return alerta_hemorragia;
    }

    public void setAlerta_hemorragia(String alerta_hemorragia) {
        this.alerta_hemorragia = alerta_hemorragia;
    }

    public String getAlerta_alergia() {
        return alerta_alergia;
    }

    public void setAlerta_alergia(String alerta_alergia) {
        this.alerta_alergia = alerta_alergia;
    }

    public String getAlerta_desmayo() {
        return alerta_desmayo;
    }

    public void setAlerta_desmayo(String alerta_desmayo) {
        this.alerta_desmayo = alerta_desmayo;
    }

    public String getAlerta_hepatica() {
        return alerta_hepatica;
    }

    public void setAlerta_hepatica(String alerta_hepatica) {
        this.alerta_hepatica = alerta_hepatica;
    }

    public String getAlerta_fiebre_reumatica() {
        return alerta_fiebre_reumatica;
    }

    public void setAlerta_fiebre_reumatica(String alerta_fiebre_reumatica) {
        this.alerta_fiebre_reumatica = alerta_fiebre_reumatica;
    }

    public String getAlerta_angina() {
        return alerta_angina;
    }

    public void setAlerta_angina(String alerta_angina) {
        this.alerta_angina = alerta_angina;
    }

    public String getAlerta_miocardio() {
        return alerta_miocardio;
    }

    public void setAlerta_miocardio(String alerta_miocardio) {
        this.alerta_miocardio = alerta_miocardio;
    }

    public String getAlerta_embarazo() {
        return alerta_embarazo;
    }

    public void setAlerta_embarazo(String alerta_embarazo) {
        this.alerta_embarazo = alerta_embarazo;
    }
    
}
