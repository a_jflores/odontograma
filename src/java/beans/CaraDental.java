package beans;

public class CaraDental {
    private int codigoCaraDental;
    private String observacion;
    
    public CaraDental(){
       this.codigoCaraDental = 0;
       this.observacion = "";
    }

    public CaraDental(int codigoCaraDental) {
        this.codigoCaraDental = codigoCaraDental;
    }

    public int getCodigoCaraDental() {
        return codigoCaraDental;
    }

    public void setCodigoCaraDental(int codigoCaraDental) {
        this.codigoCaraDental = codigoCaraDental;
    }

    public String getObservacion() {
        return observacion;
    }

    public void setObservacion(String observacion) {
        this.observacion = observacion;
    }
    
}
