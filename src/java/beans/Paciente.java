package beans;

import java.time.LocalDate;
import java.time.Period;
import java.time.format.DateTimeFormatter;

public class Paciente {

    private String codigoPaciente;
    private String idClinica;
    private String nombrePaciente;
    private String nacionalidadPaciente;
    private String documentoPaciente;
    private String domicilioPaciente;
    private String sexoPaciente;
    private String fechanacimientoPaciente;
    private String emailPaciente;
    private String telefonoPaciente;
    private String recomendacionPaciente;
    private String estadocivilPaciente;
    private String hospedajePaciente;
    private String edadPaciente;

    public Paciente() {
        this.codigoPaciente = "";
        this.idClinica = "";
        this.nombrePaciente = "";
        this.nacionalidadPaciente = "";
        this.documentoPaciente = "";
        this.domicilioPaciente = "";
        this.sexoPaciente = "";
        this.fechanacimientoPaciente = "";
        this.emailPaciente = "";
        this.telefonoPaciente = "";
        this.recomendacionPaciente = "";
        this.estadocivilPaciente = "";
        this.hospedajePaciente = "";
        this.edadPaciente = "";
    }

    public Paciente(String codigoPaciente, String idClinica, String nombrePaciente, String nacionalidadPaciente, String documentoPaciente, String domicilioPaciente, String sexoPaciente, String fechanacimientoPaciente, String emailPaciente, String telefonoPaciente, String recomendacionPaciente, String estadocivilPaciente, String hospedajePaciente, String edadPaciente) {
        this.codigoPaciente = codigoPaciente;
        this.idClinica = idClinica;
        this.nombrePaciente = nombrePaciente;
        this.nacionalidadPaciente = nacionalidadPaciente;
        this.documentoPaciente = documentoPaciente;
        this.domicilioPaciente = domicilioPaciente;
        this.sexoPaciente = sexoPaciente;
        this.fechanacimientoPaciente = fechanacimientoPaciente;
        this.emailPaciente = emailPaciente;
        this.telefonoPaciente = telefonoPaciente;
        this.recomendacionPaciente = recomendacionPaciente;
        this.estadocivilPaciente = estadocivilPaciente;
        this.hospedajePaciente = hospedajePaciente;
        this.edadPaciente = edadPaciente;
    }

    public String getCodigoPaciente() {
        return codigoPaciente;
    }

    public void setCodigoPaciente(String codigoPaciente) {
        this.codigoPaciente = codigoPaciente;
    }

    public String getIdClinica() {
        return idClinica;
    }

    public void setIdClinica(String idClinica) {
        this.idClinica = idClinica;
    }

    public String getNombrePaciente() {
        return nombrePaciente;
    }

    public void setNombrePaciente(String nombrePaciente) {
        this.nombrePaciente = nombrePaciente;
    }

    public String getNacionalidadPaciente() {
        return nacionalidadPaciente;
    }

    public void setNacionalidadPaciente(String nacionalidadPaciente) {
        this.nacionalidadPaciente = nacionalidadPaciente;
    }

    public String getDocumentoPaciente() {
        return documentoPaciente;
    }

    public void setDocumentoPaciente(String documentoPaciente) {
        this.documentoPaciente = documentoPaciente;
    }

    public String getDomicilioPaciente() {
        return domicilioPaciente;
    }

    public void setDomicilioPaciente(String domicilioPaciente) {
        this.domicilioPaciente = domicilioPaciente;
    }

    public String getSexoPaciente() {
        return sexoPaciente;
    }

    public void setSexoPaciente(String sexoPaciente) {
        this.sexoPaciente = sexoPaciente;
    }

    public String getFechanacimientoPaciente() {
        return fechanacimientoPaciente;
    }

    public void setFechanacimientoPaciente(String fechanacimientoPaciente) {
        this.fechanacimientoPaciente = fechanacimientoPaciente;
    }

    public String getEmailPaciente() {
        return emailPaciente;
    }

    public void setEmailPaciente(String emailPaciente) {
        this.emailPaciente = emailPaciente;
    }

    public String getTelefonoPaciente() {
        return telefonoPaciente;
    }

    public void setTelefonoPaciente(String telefonoPaciente) {
        this.telefonoPaciente = telefonoPaciente;
    }

    public String getRecomendacionPaciente() {
        return recomendacionPaciente;
    }

    public void setRecomendacionPaciente(String recomendacionPaciente) {
        this.recomendacionPaciente = recomendacionPaciente;
    }

    public String getEstadocivilPaciente() {
        return estadocivilPaciente;
    }

    public void setEstadocivilPaciente(String estadocivilPaciente) {
        this.estadocivilPaciente = estadocivilPaciente;
    }

    public String getHospedajePaciente() {
        return hospedajePaciente;
    }

    public void setHospedajePaciente(String hospedajePaciente) {
        this.hospedajePaciente = hospedajePaciente;
    }

    public String getEdadPaciente() {
        DateTimeFormatter fmt = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        LocalDate fechaNac = LocalDate.parse(fechanacimientoPaciente, fmt);
        LocalDate ahora = LocalDate.now();
        Period periodo = Period.between(fechaNac, ahora);
        int anios = periodo.getYears();
        edadPaciente = String.valueOf(anios);
        return edadPaciente;
    }

    public void setEdadPaciente(String edadPaciente) {
        this.edadPaciente = edadPaciente;
    }
}
