package beans;

public class ExamenFisico {
    private int codigoExamenFisico;
    private HistoriaClinica historiaClinica;
    private double peso;
    private double talla;
    private String grupo_sanguineo;
    private int pulso;
    private double presion_arterial_minima;
    private double presion_arterial_maxima;
    private double frecuencia_respiratoria;
    private double imc;
    
    public ExamenFisico(){
        this.codigoExamenFisico = 0;
        this.historiaClinica = null;
        this.peso = 0;
        this.talla = 0;
        this.grupo_sanguineo = "";
        this.pulso =0;
        this.presion_arterial_minima = 0;
        this.presion_arterial_maxima = 0;
        this.frecuencia_respiratoria = 0;
        this.imc = 0;
    }

    public int getCodigoExamenFisico() {
        return codigoExamenFisico;
    }

    public void setCodigoExamenFisico(int codigoExamenFisico) {
        this.codigoExamenFisico = codigoExamenFisico;
    }

    public HistoriaClinica getHistoriaClinica() {
        return historiaClinica;
    }

    public void setHistoriaClinica(HistoriaClinica historiaClinica) {
        this.historiaClinica = historiaClinica;
    }

    public double getPeso() {
        return peso;
    }

    public void setPeso(double peso) {
        this.peso = peso;
    }

    public double getTalla() {
        return talla;
    }

    public void setTalla(double talla) {
        this.talla = talla;
    }

    public String getGrupo_sanguineo() {
        return grupo_sanguineo;
    }

    public void setGrupo_sanguineo(String grupo_sanguineo) {
        this.grupo_sanguineo = grupo_sanguineo;
    }

    public int getPulso() {
        return pulso;
    }

    public void setPulso(int pulso) {
        this.pulso = pulso;
    }

    public double getPresion_arterial_minima() {
        return presion_arterial_minima;
    }

    public void setPresion_arterial_minima(double presion_arterial_minima) {
        this.presion_arterial_minima = presion_arterial_minima;
    }

    public double getPresion_arterial_maxima() {
        return presion_arterial_maxima;
    }

    public void setPresion_arterial_maxima(double presion_arterial_maxima) {
        this.presion_arterial_maxima = presion_arterial_maxima;
    }

    public double getFrecuencia_respiratoria() {
        return frecuencia_respiratoria;
    }

    public void setFrecuencia_respiratoria(double frecuencia_respiratoria) {
        this.frecuencia_respiratoria = frecuencia_respiratoria;
    }

    public double getImc() {
        return imc;
    }

    public void setImc(double imc) {
        this.imc = imc;
    }

}
