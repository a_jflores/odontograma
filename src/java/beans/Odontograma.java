package beans;

public class Odontograma {
    private int codigoOdontograma;
    private HistoriaClinica historiaClinica;
    private String observaciones;
    private String date_created;
    
    public Odontograma(){
        this.codigoOdontograma = 0;
        this.historiaClinica = null;
        this.observaciones = "";
        this.date_created = "";
    }

    public Odontograma(int codigoOdontograma, HistoriaClinica historiaClinica, String observaciones, String date_created) {
        this.codigoOdontograma = codigoOdontograma;
        this.historiaClinica = historiaClinica;
        this.observaciones = observaciones;
        this.date_created = date_created;
    }
    
    public int getCodigoOdontograma() {
        return codigoOdontograma;
    }

    public void setCodigoOdontograma(int codigoOdontograma) {
        this.codigoOdontograma = codigoOdontograma;
    }

    public HistoriaClinica getHistoriaClinica() {
        return historiaClinica;
    }

    public void setHistoriaClinica(HistoriaClinica historiaClinica) {
        this.historiaClinica = historiaClinica;
    }

    public String getObservaciones() {
        return observaciones;
    }

    public void setObservaciones(String observaciones) {
        this.observaciones = observaciones;
    }

    public String getDate_created() {
        return date_created;
    }

    public void setDate_created(String date_created) {
        this.date_created = date_created;
    }
    
}
