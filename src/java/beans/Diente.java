package beans;

public class Diente {
    private int codigoDiente;
    private int numero;
    private String descripcion;
    
    public Diente(){
        this.codigoDiente = 0;
        this.numero = 0;
        this.descripcion = "";
    }

    public Diente(int codigoDiente) {
        this.codigoDiente = codigoDiente;
    }

    public int getCodigoDiente() {
        return codigoDiente;
    }

    public void setCodigoDiente(int codigoDiente) {
        this.codigoDiente = codigoDiente;
    }

    public int getNumero() {
        return numero;
    }

    public void setNumero(int numero) {
        this.numero = numero;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }
    
}
