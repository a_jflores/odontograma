package beans;

public class Tratamiento {
    private int idTratamiento;
    private String nombreTratamiento;
    private Double precioTratamiento;

    public int getIdTratamiento() {
        return idTratamiento;
    }

    public void setIdTratamiento(int idTratamiento) {
        this.idTratamiento = idTratamiento;
    }

    public String getNombreTratamiento() {
        return nombreTratamiento;
    }

    public void setNombreTratamiento(String nombreTratamiento) {
        this.nombreTratamiento = nombreTratamiento;
    }

    public Double getPrecioTratamiento() {
        return precioTratamiento;
    }

    public void setPrecioTratamiento(Double precioTratamiento) {
        this.precioTratamiento = precioTratamiento;
    }
    
}
