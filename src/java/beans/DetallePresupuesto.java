package beans;

public class DetallePresupuesto {
   private int id_detalle_presupuesto;
   private Presupuesto presupuesto;
   private Tratamiento tratamiento;
   private int pieza;
   private String hallazgo;
   private int cantidad;
   private double precio;
   private double subtotal;
   
   public DetallePresupuesto(){
       this.id_detalle_presupuesto = 0;
       this.presupuesto = null;
       this.tratamiento = null;
       this.pieza = 0;
       this.hallazgo = "";
       this.cantidad = 0;
       this.precio = 0;
       this.subtotal = 0;
   }

    public DetallePresupuesto(int id_detalle_presupuesto, Presupuesto presupuesto, Tratamiento tratamiento, int pieza, String hallazgo, int cantidad, double precio, double subtotal) {
        this.id_detalle_presupuesto = id_detalle_presupuesto;
        this.presupuesto = presupuesto;
        this.tratamiento = tratamiento;
        this.pieza = pieza;
        this.hallazgo = hallazgo;
        this.cantidad = cantidad;
        this.precio = precio;
        this.subtotal = subtotal;
    }
   
    public int getId_detalle_presupuesto() {
        return id_detalle_presupuesto;
    }

    public void setId_detalle_presupuesto(int id_detalle_presupuesto) {
        this.id_detalle_presupuesto = id_detalle_presupuesto;
    }

    public Presupuesto getPresupuesto() {
        return presupuesto;
    }

    public void setPresupuesto(Presupuesto presupuesto) {
        this.presupuesto = presupuesto;
    }

    public Tratamiento getTratamiento() {
        return tratamiento;
    }

    public void setTratamiento(Tratamiento tratamiento) {
        this.tratamiento = tratamiento;
    }

    public int getPieza() {
        return pieza;
    }

    public void setPieza(int pieza) {
        this.pieza = pieza;
    }

    public String getHallazgo() {
        return hallazgo;
    }

    public void setHallazgo(String hallazgo) {
        this.hallazgo = hallazgo;
    }

    public int getCantidad() {
        return cantidad;
    }

    public void setCantidad(int cantidad) {
        this.cantidad = cantidad;
    }

    public double getPrecio() {
        return precio;
    }

    public void setPrecio(double precio) {
        this.precio = precio;
    }

    public double getSubtotal() {
        return subtotal;
    }

    public void setSubtotal(double subtotal) {
        this.subtotal = subtotal;
    }
    
}
