package controllers;

import beans.Anamnesis;
import beans.DienteDetalle;
import beans.ExamenFisico;
import beans.HistoriaClinica;
import beans.Odontograma;
import beans.Paciente;
import com.google.gson.Gson;
import java.io.IOException;
import java.io.PrintWriter;
import static java.lang.Integer.parseInt;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import models.AnamnesisModel;
import models.DienteDetalleModel;
import models.ExamenFisicoModel;
import models.HistoriasModel;
import models.MedicoModel;
import models.OdontogramaModel;
import models.PacientesModel;
import models.PaisesModel;
import utils.Validaciones;

@WebServlet(name = "HistoriasController", urlPatterns = {"/historias.do"})
public class HistoriasController extends HttpServlet {

    HistoriasModel modelo = new HistoriasModel();
    PacientesModel modelo2 = new PacientesModel();
    PaisesModel modelo3 = new PaisesModel();
    PacientesModel modeloRecomendaciones = new PacientesModel();
    AnamnesisModel modeloAnamnesis = new AnamnesisModel();
    OdontogramaModel modeloOdontogramas = new OdontogramaModel();
    DienteDetalleModel modeloDienteDetalle = new DienteDetalleModel();
    ExamenFisicoModel modeloExamenFisico = new ExamenFisicoModel();
    MedicoModel modeloMedico = new MedicoModel();

    ArrayList<String> listaErrores = new ArrayList<>();
    ArrayList<String> paises = new ArrayList<String>();

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();

        try {
            if (request.getParameter("op") == null) {
                listar(request, response);
                return;
            }
            String operacion = request.getParameter("op");
            switch (operacion) {
                case "listar":
                    listar(request, response);
                    break;
                case "listarHistoriaClinicaJson":
                    listarHistoriaClinicaJson(request, response);
                    break;
                case "obtener":
                    obtener(request, response);
                    break;
                case "listarOdontogramasJson":
                    listarOdontogramasJson(request, response);
                    break;
                case "odontograma":
                    odontograma(request, response);
                    break;
                case "obtenerOdontograma":
                    obtenerOdontograma(request, response);
                    break;
                case "obtenerAnteriorOdontograma":
                    obtenerAnteriorOdontograma(request, response);
                    break;
                case "obtenerDienteDetalle":
                    obtenerDienteDetalle(request, response);
                    break;
                case "insertar":
                    insertar(request, response);
                    break;
                case "insertar_examen_fisico":
                    insertar_examen_fisico(request, response);
                    break;
                case "modificar_examen_fisico":
                    modificar_examen_fisico(request, response);
                    break;
                case "modificar":
                    modificarDatosPersonales(request, response);
                    break;
                case "paises":
                    listarpaises(request, response);
                    break;
                default:
                    request.getRequestDispatcher("/error404.jsp").forward(request, response);
                    break;
            }
        } finally {
            out.close();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

    private void listar(HttpServletRequest request, HttpServletResponse response) {
        try {
            request.setAttribute("listaHistorias", modelo.listarHistorias());
            request.setAttribute("listaPacientes", modelo2.listarPacientes());
            request.getRequestDispatcher("/views/HistoriaClinica/listaHistorias.jsp").forward(request, response);
        } catch (SQLException | ServletException | IOException ex) {
            Logger.getLogger(HistoriasController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void obtener(HttpServletRequest request, HttpServletResponse response) {
        try {
            String id_historia_clinica = request.getParameter("id");
            HistoriaClinica miHistoria = modelo.obtenerHistoria(parseInt(id_historia_clinica));
            request.setAttribute("listaPaises", modelo3.listarPaises());
            request.setAttribute("listaRecomendaciones", modeloRecomendaciones.listarRecomendaciones(id_historia_clinica));
            Anamnesis miAnamnesis = modeloAnamnesis.obtenerAnamnesis(id_historia_clinica);
            ExamenFisico miExamen = modeloExamenFisico.obtenerExamenFisico(id_historia_clinica);
            request.setAttribute("listaOdontogramas", modeloOdontogramas.listarOdontogramas(id_historia_clinica));
            int conteo = modeloOdontogramas.ultimoOdontogramaHistoria(id_historia_clinica);
            request.setAttribute("listaMedicos", modeloMedico.listarMedico());

            if (miHistoria != null) {
                request.setAttribute("historia", miHistoria);
                request.setAttribute("anamnesis", miAnamnesis);
                request.setAttribute("examen", miExamen);
                request.setAttribute("ultimoOdontogramaHistoria", conteo);
                request.getRequestDispatcher("/views/HistoriaClinica/verHistoria.jsp").forward(request, response);
            } else {
                response.sendRedirect(request.getContextPath() + "/error404.jsp");
            }
        } catch (SQLException | ServletException | IOException ex) {
            Logger.getLogger(HistoriasController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void odontograma(HttpServletRequest request, HttpServletResponse response) {
        try {
            String codigo = request.getParameter("id");
            HistoriaClinica miHistoria = modelo.crearOdontograma(parseInt(codigo));
            if (miHistoria != null) {
                request.setAttribute("historia", miHistoria);
                request.getRequestDispatcher("/views/HistoriaClinica/odontograma.jsp").forward(request, response);
            } else {
                response.sendRedirect(request.getContextPath() + "/error404.jsp");
            }
        } catch (SQLException | ServletException | IOException ex) {
            Logger.getLogger(HistoriasController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void obtenerOdontograma(HttpServletRequest request, HttpServletResponse response) {
        try {
            String id_odontograma = request.getParameter("id");
            Odontograma miOdontograma = modeloOdontogramas.obtenerOdontograma(id_odontograma);
            int conteoDienteDetalle = modeloDienteDetalle.conteoDienteDetalle(id_odontograma);

            if (miOdontograma != null) {
                request.setAttribute("conteoDienteDetalle", conteoDienteDetalle);
                request.setAttribute("odontograma", miOdontograma);
                request.setAttribute("listaDienteDetalle", modeloDienteDetalle.cargarDienteDetalle(id_odontograma));
                request.getRequestDispatcher("/views/HistoriaClinica/updateOdontograma.jsp").forward(request, response);
            } else {
                response.sendRedirect(request.getContextPath() + "/error404.jsp");
            }
        } catch (ServletException | IOException | SQLException ex) {
            Logger.getLogger(HistoriasController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void obtenerDienteDetalle(HttpServletRequest request, HttpServletResponse response) {
        PrintWriter out = null;
        try {
            String id_odontograma = request.getParameter("id");
            out = response.getWriter();

            ArrayList<DienteDetalle> midienteDetalle = modeloDienteDetalle.cargarDienteDetalle(id_odontograma);
            String json = new Gson().toJson(midienteDetalle);
            out.print(json);
        } catch (IOException ex) {
            Logger.getLogger(HistoriasController.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SQLException ex) {
            Logger.getLogger(HistoriasController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void insertar(HttpServletRequest request, HttpServletResponse response) {
        try {
            listaErrores.clear();
            HistoriaClinica miHistoria = new HistoriaClinica();
            String idPaciente = request.getParameter("id_paciente");
            miHistoria.setCodigoPaciente(parseInt(idPaciente));

            if (Validaciones.isEmpty(request.getParameter("id_paciente"))) {
                listaErrores.add("El campo es obligatorio");
            }

            if (listaErrores.size() > 0) {
                request.setAttribute("historia", miHistoria);
                request.setAttribute("listaErrores", listaErrores);
                request.getRequestDispatcher("/historias.do?op=listar").forward(request, response);
            } else { //No hay errores
                if (modelo.insertarHistoria(miHistoria) > 0) {
                    int ultimaHistoria = modelo.ultimaHistoria();
                    request.getSession().setAttribute("exito", "Historia Clínica registrada exitosamente");
                    response.sendRedirect(request.getContextPath() + "/historias.do?op=obtener&id=" + ultimaHistoria);
                } else {
                    request.getSession().setAttribute("fracaso", "No se pudo registrar este Historia, ya existe.");
                    response.sendRedirect(request.getContextPath() + "/historias.do?op=listar");
                }
            }
        } catch (ServletException | IOException | SQLException ex) {
            Logger.getLogger(HistoriasController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void modificarDatosPersonales(HttpServletRequest request, HttpServletResponse response) {
        try {
            listaErrores.clear();
            HistoriaClinica miHistoria = new HistoriaClinica();
            String idHistoriaClinica = request.getParameter("id");
            miHistoria.setCodigoPaciente(parseInt(request.getParameter("id")));
            miHistoria.setPaciente(new Paciente());
            miHistoria.getPaciente().setNombrePaciente(request.getParameter("nombres"));
            miHistoria.getPaciente().setDocumentoPaciente(request.getParameter("documento"));

            if (Validaciones.isEmpty(request.getParameter("nombres"))) {
                listaErrores.add("El campo es obligatorio");
            }
            if (Validaciones.isEmpty(request.getParameter("documento"))) {
                listaErrores.add("El campo es obligatorio");
            }
            if (Validaciones.isEmpty(request.getParameter("nombres"))) {
                listaErrores.add("El campo es obligatorio");
            }

            if (listaErrores.size() > 0) {
                request.setAttribute("historia", miHistoria);
                request.setAttribute("listaErrores", listaErrores);
                request.getRequestDispatcher("/historias.do?op=obtener&id=" + idHistoriaClinica).forward(request, response);
            } else { //No hay errores
                if (modelo.modificarDatosPersonales(miHistoria) > 0) {
                    request.getSession().setAttribute("exito", "Historia Clínica registrada erequest.getRequestDispatcher(\"/historias.do?op=obtener&id=\" + idHistoriaClinica).forward(request, response);xitosamente");
                    response.sendRedirect(request.getContextPath() + "/historias.do?op=obtener&id=" + idHistoriaClinica);
                } else {
                    request.getSession().setAttribute("fracaso", "No se pudo registrar este Paciente, ya existe.");
                    response.sendRedirect(request.getContextPath() + "/historias.do?op=obtener&id=" + idHistoriaClinica);
                }
            }
        } catch (ServletException | IOException | SQLException ex) {
            Logger.getLogger(HistoriasController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void listarpaises(HttpServletRequest request, HttpServletResponse response) {
        PrintWriter out = null;
        try {
            out = response.getWriter();
            List pais = modelo3.listarPaises();
            String json = new Gson().toJson(pais);
            out.print(json);
        } catch (IOException | SQLException ex) {
            Logger.getLogger(HistoriasController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void insertar_examen_fisico(HttpServletRequest request, HttpServletResponse response) {
        try {
            listaErrores.clear();
            ExamenFisico miExamen = new ExamenFisico();
            HistoriaClinica miHistoria = new HistoriaClinica();
            String id_historia_clinica = request.getParameter("id_historia_clinica");

            miHistoria.setCodigoHistoriaClinica(Integer.parseInt(request.getParameter("id_historia_clinica")));
            miExamen.setHistoriaClinica(miHistoria);
            miExamen.setPeso(Double.parseDouble(request.getParameter("peso")));
            miExamen.setTalla(Double.parseDouble(request.getParameter("talla")));
            miExamen.setImc(Double.parseDouble(request.getParameter("imc")));
            miExamen.setPresion_arterial_maxima(Double.parseDouble(request.getParameter("presion_arterial_maxima")));
            miExamen.setPresion_arterial_minima(Double.parseDouble(request.getParameter("presion_arterial_minima")));
            miExamen.setFrecuencia_respiratoria(Double.parseDouble(request.getParameter("frecuencia_respiratoria")));
            miExamen.setPulso(Integer.parseInt(request.getParameter("pulso")));
            miExamen.setGrupo_sanguineo(request.getParameter("grupo_sanguineo"));

            if (Validaciones.isEmpty(String.valueOf(miExamen.getPeso()))) {
                listaErrores.add("El código del editorial es obligatorio");
            }

            if (listaErrores.size() > 0) {
                request.setAttribute("examen", miExamen);
                request.setAttribute("listaErrores", listaErrores);
                request.getRequestDispatcher("/historias.do?op=obtener&id=" + id_historia_clinica).forward(request, response);
            } else { //No hay errores
                if (modeloExamenFisico.insertarExamenFisico(miExamen) > 0) {
                    request.getSession().setAttribute("exito", "Registrado exitosamente");
                    request.getRequestDispatcher("/historias.do?op=obtener&id=" + id_historia_clinica).forward(request, response);
                } else {
                    request.getSession().setAttribute("fracaso", "No se pudo registrar, ya existe.");
                    request.getRequestDispatcher("/historias.do?op=obtener&id=" + id_historia_clinica).forward(request, response);
                }
            }
        } catch (ServletException | IOException | SQLException ex) {
            Logger.getLogger(HistoriasController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void modificar_examen_fisico(HttpServletRequest request, HttpServletResponse response) {
        try {
            listaErrores.clear();
            ExamenFisico miExamen = new ExamenFisico();
            HistoriaClinica miHistoria = new HistoriaClinica();
            String id_historia_clinica = request.getParameter("id_historia_clinica");

            miHistoria.setCodigoHistoriaClinica(Integer.parseInt(request.getParameter("id_historia_clinica")));
            miExamen.setHistoriaClinica(miHistoria);
            miExamen.setPeso(Double.parseDouble(request.getParameter("peso")));
            miExamen.setTalla(Double.parseDouble(request.getParameter("talla")));
            miExamen.setImc(Double.parseDouble(request.getParameter("imc")));
            miExamen.setPresion_arterial_maxima(Double.parseDouble(request.getParameter("presion_arterial_maxima")));
            miExamen.setPresion_arterial_minima(Double.parseDouble(request.getParameter("presion_arterial_minima")));
            miExamen.setFrecuencia_respiratoria(Double.parseDouble(request.getParameter("frecuencia_respiratoria")));
            miExamen.setPulso(Integer.parseInt(request.getParameter("pulso")));
            miExamen.setGrupo_sanguineo(request.getParameter("grupo_sanguineo"));

            if (Validaciones.isEmpty(String.valueOf(miExamen.getPeso()))) {
                listaErrores.add("El código del editorial es obligatorio");
            }

            if (listaErrores.size() > 0) {
                request.setAttribute("examen", miExamen);
                request.setAttribute("listaErrores", listaErrores);
                request.getRequestDispatcher("/historias.do?op=obtener&id=" + id_historia_clinica).forward(request, response);
            } else { //No hay errores
                if (modeloExamenFisico.modificarExamenFisico(miExamen) > 0) {
                    request.getSession().setAttribute("exito", "Registrado exitosamente");
                    request.getRequestDispatcher("/historias.do?op=obtener&id=" + id_historia_clinica).forward(request, response);
                } else {
                    request.getSession().setAttribute("fracaso", "No se pudo registrar, ya existe.");
                    request.getRequestDispatcher("/historias.do?op=obtener&id=" + id_historia_clinica).forward(request, response);
                }
            }
        } catch (ServletException | IOException | SQLException ex) {
            Logger.getLogger(HistoriasController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void listarOdontogramasJson(HttpServletRequest request, HttpServletResponse response) {
        PrintWriter out = null;
        try {
            out = response.getWriter();
            String id_historia_clinica = request.getParameter("id");
            List odontogramas = modeloOdontogramas.listarOdontogramas(id_historia_clinica);
            String json = new Gson().toJson(odontogramas);
            out.print(json);
        } catch (IOException | SQLException ex) {
            Logger.getLogger(HistoriasController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void obtenerAnteriorOdontograma(HttpServletRequest request, HttpServletResponse response) {
        try {
            String id_odontograma = request.getParameter("id");
            Odontograma miOdontograma = modeloOdontogramas.obtenerOdontograma(id_odontograma);
            int conteoDienteDetalle = modeloDienteDetalle.conteoDienteDetalle(id_odontograma);
            String estado = "anterior";

            if (miOdontograma != null) {
                request.setAttribute("anterior", estado);
                request.setAttribute("conteoDienteDetalle", conteoDienteDetalle);
                request.setAttribute("odontograma", miOdontograma);
                request.setAttribute("listaDienteDetalle", modeloDienteDetalle.cargarDienteDetalle(id_odontograma));
                request.getRequestDispatcher("/views/HistoriaClinica/updateOdontograma.jsp").forward(request, response);
            } else {
                response.sendRedirect(request.getContextPath() + "/error404.jsp");
            }
        } catch (ServletException | IOException | SQLException ex) {
            Logger.getLogger(HistoriasController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void listarHistoriaClinicaJson(HttpServletRequest request, HttpServletResponse response) {
        PrintWriter out = null;
        try {
            out = response.getWriter();
            List historias = modelo.listarHistorias();
            String json = new Gson().toJson(historias);
            out.print(json);
        } catch (IOException | SQLException ex) {
            Logger.getLogger(HistoriasController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
