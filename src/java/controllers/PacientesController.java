package controllers;

import beans.Paciente;
import com.google.gson.Gson;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import models.PacientesModel;

@WebServlet(name = "PacientesController", urlPatterns = {"/pacientes.do"})
public class PacientesController extends HttpServlet {

    PacientesModel modelo = new PacientesModel();
    ArrayList<String> listaErrores = new ArrayList<>();

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        try {
            if (request.getParameter("op") == null) {
                listar(request, response);
                return;
            }
            String operacion = request.getParameter("op");
            switch (operacion) {
                case "listar":
                    listar(request, response);
                    break;
                case "listarJson":
                    listarJson(request, response);
                    break;
                case "crearJson":
                    crearJson(request, response);
                    break;
                case "verJson":
                    verJson(request, response);
                    break;
                case "editarJson":
                    editarJson(request, response);
                    break;
                case "eliminarJson":
                    eliminarJson(request, response);
                    break;
                case "obtener":
                    obtener(request, response);
                    break;
                case "odontograma":
                    odontograma(request, response);
                    break;
                case "modificar":
                    modificar(request, response);
                    break;
                default:
                    request.getRequestDispatcher("/error404.jsp").forward(request, response);
                    break;
            }
        } finally {
            out.close();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

    private void listar(HttpServletRequest request, HttpServletResponse response) {
        try {
            request.setAttribute("listaPacientes", modelo.listarPacientes());
            request.getRequestDispatcher("/views/Pacientes/listaPacientes.jsp").forward(request, response);
        } catch (SQLException | ServletException | IOException ex) {
            Logger.getLogger(PacientesController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void obtener(HttpServletRequest request, HttpServletResponse response) {
        try {
            String codigo = request.getParameter("id");
            Paciente miPaciente = modelo.obtenerPaciente(codigo);
            if (miPaciente != null) {
                request.setAttribute("paciente", miPaciente);
                request.getRequestDispatcher("/views/Pacientes/verPaciente.jsp").forward(request, response);
            } else {
                response.sendRedirect(request.getContextPath() + "/error404.jsp");
            }
        } catch (SQLException | ServletException | IOException ex) {
            Logger.getLogger(PacientesController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void odontograma(HttpServletRequest request, HttpServletResponse response) {
        try {
            String codigo = request.getParameter("id");
            Paciente miPaciente = modelo.obtenerPaciente(codigo);
            if (miPaciente != null) {
                request.setAttribute("paciente", miPaciente);
                request.getRequestDispatcher("/views/Pacientes/odontograma.jsp").forward(request, response);
            } else {
                response.sendRedirect(request.getContextPath() + "/error404.jsp");
            }
        } catch (ServletException | IOException | SQLException ex) {
            Logger.getLogger(PacientesController.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    private void modificar(HttpServletRequest request, HttpServletResponse response) {
        try {
            listaErrores.clear();
            Paciente miPaciente = new Paciente();
            String idHistoria = request.getParameter("id_historia_clinica");

            miPaciente.setCodigoPaciente(request.getParameter("id_paciente"));
            miPaciente.setNombrePaciente(request.getParameter("nombres"));
            miPaciente.setNacionalidadPaciente(request.getParameter("nacionalidad"));
            miPaciente.setDocumentoPaciente(request.getParameter("documento"));
            miPaciente.setDomicilioPaciente(request.getParameter("domicilio"));
            miPaciente.setSexoPaciente(request.getParameter("sexo"));
            miPaciente.setFechanacimientoPaciente(request.getParameter("fecha_nac"));
            miPaciente.setEmailPaciente(request.getParameter("email"));
            miPaciente.setTelefonoPaciente(request.getParameter("telefono"));
            miPaciente.setRecomendacionPaciente(request.getParameter("recomendacion"));
            miPaciente.setEstadocivilPaciente(request.getParameter("estado_civil"));
            miPaciente.setHospedajePaciente(request.getParameter("hospedaje"));

            if (listaErrores.size() > 0) {
                request.setAttribute("paciente", miPaciente);
                request.setAttribute("listaErrores", listaErrores);
                request.getRequestDispatcher("/historias.do?op=obtener&id=" + idHistoria).forward(request, response);
            } else { //No hay errores
                if (modelo.modificarPaciente(miPaciente) > 0) {
                    request.getSession().setAttribute("exito", "Paciente actualizado exitosamente");
                    response.sendRedirect(request.getContextPath() + "/historias.do?op=obtener&id=" + idHistoria);
                } else {
                    request.getSession().setAttribute("fracaso", "No se pudo actualizar los datos del Paciente");
                    response.sendRedirect(request.getContextPath() + "/historias.do?op=obtener&id=" + idHistoria);
                }
            }
        } catch (ServletException | IOException | SQLException ex) {
            Logger.getLogger(PacientesController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void listarJson(HttpServletRequest request, HttpServletResponse response) {
        PrintWriter out = null;
        try {
            out = response.getWriter();
            List pacientes = modelo.listarPacientes();
            String json = new Gson().toJson(pacientes);
            out.print(json);
        } catch (IOException | SQLException ex) {
            Logger.getLogger(PacientesController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void verJson(HttpServletRequest request, HttpServletResponse response) {
        PrintWriter out = null;
        try {
            out = response.getWriter();
            String id_paciente = request.getParameter("id");
            Paciente paciente = modelo.obtenerPaciente(id_paciente);
            String json = new Gson().toJson(paciente);
            out.print(json);
        } catch (IOException | SQLException ex) {
            Logger.getLogger(PacientesController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void editarJson(HttpServletRequest request, HttpServletResponse response) {
        PrintWriter out = null;
        try {
            out = response.getWriter();
            String respuesta = "";
            
            Paciente miPaciente = new Paciente();
            miPaciente.setNombrePaciente(request.getParameter("nombres"));
            miPaciente.setNacionalidadPaciente(request.getParameter("nacionalidad"));
            miPaciente.setDocumentoPaciente(request.getParameter("documento"));
            miPaciente.setDomicilioPaciente(request.getParameter("domicilio"));
            miPaciente.setSexoPaciente(request.getParameter("sexo"));
            miPaciente.setFechanacimientoPaciente(request.getParameter("fecha_nac"));
            miPaciente.setEmailPaciente(request.getParameter("email"));
            miPaciente.setTelefonoPaciente(request.getParameter("telefono"));
            miPaciente.setEstadocivilPaciente(request.getParameter("estado_civil"));
            miPaciente.setCodigoPaciente(request.getParameter("id_paciente"));

            if (modelo.editarPaciente(miPaciente) > 0){
                respuesta = "Éxito";
            }else{
                respuesta = "Error";
            }
            out.print(respuesta);
        } catch (IOException | SQLException ex) {
            Logger.getLogger(PacientesController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void eliminarJson(HttpServletRequest request, HttpServletResponse response) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    private void crearJson(HttpServletRequest request, HttpServletResponse response) {
        PrintWriter out = null;
        try {
            out = response.getWriter();
            String respuesta = "";

            Paciente miPaciente = new Paciente();
            miPaciente.setIdClinica("2");
            miPaciente.setNombrePaciente(request.getParameter("nombres"));
            miPaciente.setNacionalidadPaciente(request.getParameter("nacionalidad"));
            miPaciente.setDocumentoPaciente(request.getParameter("documento"));
            miPaciente.setDomicilioPaciente(request.getParameter("domicilio"));
            miPaciente.setSexoPaciente(request.getParameter("sexo"));
            miPaciente.setFechanacimientoPaciente(request.getParameter("fecha_nac"));
            miPaciente.setEmailPaciente(request.getParameter("email"));
            miPaciente.setTelefonoPaciente(request.getParameter("telefono"));
            miPaciente.setEstadocivilPaciente(request.getParameter("estado_civil"));
            
            if (modelo.insertarPaciente(miPaciente) > 0){
                respuesta = "Éxito";
            }else{
                respuesta = "Error";
            }
            out.print(respuesta);
        } catch (IOException | SQLException ex) {
            Logger.getLogger(PacientesController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

}
