package controllers;

import beans.DetallePresupuesto;
import beans.HistoriaClinica;
import beans.Medico;
import beans.Odontograma;
import beans.Presupuesto;
import beans.Tratamiento;
import com.google.gson.Gson;
import java.io.IOException;
import java.io.PrintWriter;
import static java.lang.Integer.parseInt;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import models.DetallePresupuestoModel;
import models.PresupuestoModel;
import org.json.JSONArray;
import org.json.JSONObject;

@WebServlet(name = "PresupuestoController", urlPatterns = {"/presupuesto.do"})
public class PresupuestoController extends HttpServlet {

    PresupuestoModel modeloPresupuesto = new PresupuestoModel();
    DetallePresupuestoModel modeloDetallePresupuesto = new DetallePresupuestoModel();

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        try {
            String operacion = request.getParameter("op");
            switch (operacion) {
                case "listarJson":
                    listarJson(request, response);
                    break;
                case "crearJson":
                    crearJson(request, response);
                    break;
                case "anularEstado":
                    anularEstado(request, response);
                    break;
                case "activarEstado":
                    activarEstado(request, response);
                    break;
                case "obtenerJson":
                    obtenerJson(request, response);
                    break;
                case "editarJson":
                    editarJson(request, response);
                    break;
                default:
                    request.getRequestDispatcher("/error404.jsp").forward(request, response);
                    break;
            }
        } finally {
            out.close();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

    private void listarJson(HttpServletRequest request, HttpServletResponse response) {
        PrintWriter out = null;
        try {

            out = response.getWriter();
            String id_historia_clinica = request.getParameter("id");
            List presupuestos = modeloPresupuesto.listarPresupuestos(id_historia_clinica);
            String json = new Gson().toJson(presupuestos);
            out.print(json);
        } catch (Exception ex) {
            ex.printStackTrace(System.out);
        }
    }

    private void crearJson(HttpServletRequest request, HttpServletResponse response) {
        PrintWriter out = null;
        try {
            out = response.getWriter();
            String id_historia_clinica = request.getParameter("id_historia_clinica");
            String id_medico = request.getParameter("id_medico");
            String id_clinica = "2";
            String id_odontograma = request.getParameter("id_odontograma");
            String asunto = request.getParameter("asunto");
            String total = request.getParameter("total");
            String tipo_pago = request.getParameter("tipo_pago");
            String estado = "proceso";
            String array_detalle_presupuesto = request.getParameter("array_detalle_presupuesto");
            JSONArray json_detalle_presupuesto = new JSONArray(array_detalle_presupuesto);

            Presupuesto miPresupuesto = new Presupuesto();
            HistoriaClinica miHistoria = new HistoriaClinica();
            Medico miMedico = new Medico();
            Odontograma miOdontograma = new Odontograma();

            miHistoria.setCodigoHistoriaClinica(parseInt(id_historia_clinica));
            miPresupuesto.setHistoria(miHistoria);
            miMedico.setIdMedico(parseInt(id_medico));
            miPresupuesto.setMedico(miMedico);
            miPresupuesto.setId_clinica(parseInt(id_clinica));
            miOdontograma.setCodigoOdontograma(parseInt(id_odontograma));
            miPresupuesto.setOdontograma(miOdontograma);
            miPresupuesto.setAsunto(asunto);
            miPresupuesto.setTotal(Double.valueOf(total));
            miPresupuesto.setTipo_pago(tipo_pago);
            miPresupuesto.setEstado(estado);

            if (modeloPresupuesto.insertarPresupuesto(miPresupuesto) > 0) {
                int ultimo_id_presupuesto = modeloPresupuesto.ultimoPresupuestoHistoria(id_historia_clinica);
                for (int i = 0; i < json_detalle_presupuesto.length(); i++) {
                    JSONObject jsonobject = json_detalle_presupuesto.getJSONObject(i);
                    int pieza = jsonobject.getInt("pieza");
                    String hallazgo = jsonobject.getString("hallazgo");
                    int id_tratamiento = jsonobject.getInt("id_tratamiento");
                    int precio = jsonobject.getInt("precio");
                    int cantidad = jsonobject.getInt("cantidad");
                    int subtotal = jsonobject.getInt("subtotal");

                    DetallePresupuesto miDetallePresupuesto = new DetallePresupuesto();
                    Presupuesto presupuesto = new Presupuesto();
                    Tratamiento tratamiento = new Tratamiento();
                    presupuesto.setId_presupuesto(ultimo_id_presupuesto);
                    tratamiento.setIdTratamiento(id_tratamiento);
                    miDetallePresupuesto.setPresupuesto(presupuesto);
                    miDetallePresupuesto.setTratamiento(tratamiento);
                    miDetallePresupuesto.setPieza(pieza);
                    miDetallePresupuesto.setHallazgo(hallazgo);
                    miDetallePresupuesto.setCantidad(cantidad);
                    miDetallePresupuesto.setPrecio(precio);
                    miDetallePresupuesto.setSubtotal(subtotal);
                    modeloDetallePresupuesto.insertarDetallePresupuesto(miDetallePresupuesto);
                }
                response.setStatus(200);
                out.print("Presupuesto guardado");
            } else {
                response.setStatus(400);
                out.print("Ocurrió un error");
            }
            out.flush();

        } catch (Exception ex) {
            ex.printStackTrace(System.out);
        }
    }

    private void anularEstado(HttpServletRequest request, HttpServletResponse response) {
        PrintWriter out = null;
        try {
            out = response.getWriter();
            String id_presupuesto = request.getParameter("id");

            Presupuesto miPresupuesto = new Presupuesto();
            miPresupuesto.setId_presupuesto(Integer.parseInt(id_presupuesto));
            miPresupuesto.setEstado("anulado");

            if (modeloPresupuesto.updateEstadoPresupuesto(miPresupuesto) > 0) {
                response.setStatus(200);
                out.print("Presupuesto anulado");
            } else {
                response.setStatus(400);
                out.print("Ocurrió un error");
            }
            out.flush();
        } catch (Exception ex) {
            ex.printStackTrace(System.out);
        }
    }

    private void activarEstado(HttpServletRequest request, HttpServletResponse response) {
        PrintWriter out = null;
        try {
            out = response.getWriter();
            String id_presupuesto = request.getParameter("id");

            Presupuesto miPresupuesto = new Presupuesto();
            miPresupuesto.setId_presupuesto(Integer.parseInt(id_presupuesto));
            miPresupuesto.setEstado("proceso");

            if (modeloPresupuesto.updateEstadoPresupuesto(miPresupuesto) > 0) {
                response.setStatus(200);
                out.print("Presupuesto activado");
            } else {
                response.setStatus(400);
                out.print("Ocurrió un error");
            }
            out.flush();
        } catch (Exception ex) {
            ex.printStackTrace(System.out);
        }
    }

    private void editarJson(HttpServletRequest request, HttpServletResponse response) {
        PrintWriter out = null;
        try {
            out = response.getWriter();
            String id_presupuesto = request.getParameter("id_presupuesto");
            String id_medico = request.getParameter("id_medico");
            String id_odontograma = request.getParameter("id_odontograma");
            String asunto = request.getParameter("asunto");
            String total = request.getParameter("total");
            String tipo_pago = request.getParameter("tipo_pago");
            String update_array_detalle_presupuesto = request.getParameter("update_array_detalle_presupuesto");

            JSONArray json_detalle_presupuesto = new JSONArray(update_array_detalle_presupuesto);
            Presupuesto miPresupuesto = new Presupuesto();
            Medico miMedico = new Medico();
            Odontograma miOdontograma = new Odontograma();

            miMedico.setIdMedico(parseInt(id_medico));
            miPresupuesto.setMedico(miMedico);
            miOdontograma.setCodigoOdontograma(parseInt(id_odontograma));
            miPresupuesto.setOdontograma(miOdontograma);
            miPresupuesto.setAsunto(asunto);
            miPresupuesto.setTotal(Double.valueOf(total));
            miPresupuesto.setTipo_pago(tipo_pago);
            miPresupuesto.setId_presupuesto(parseInt(id_presupuesto));

            if (modeloPresupuesto.editarPresupuesto(miPresupuesto) > 0) {
                modeloDetallePresupuesto.eliminarDetallePresupuesto(id_presupuesto);
                for (int i = 0; i < json_detalle_presupuesto.length(); i++) {
                    JSONObject jsonobject = json_detalle_presupuesto.getJSONObject(i);
                    int pieza = jsonobject.getInt("pieza");
                    String hallazgo = jsonobject.getString("hallazgo");
                    int id_tratamiento = jsonobject.getInt("id_tratamiento");
                    int precio = jsonobject.getInt("precio");
                    int cantidad = jsonobject.getInt("cantidad");
                    int subtotal = jsonobject.getInt("subtotal");

                    DetallePresupuesto miDetallePresupuesto = new DetallePresupuesto();
                    Presupuesto presupuesto = new Presupuesto();
                    Tratamiento tratamiento = new Tratamiento();
                    tratamiento.setIdTratamiento(id_tratamiento);
                    miDetallePresupuesto.setTratamiento(tratamiento);
                    miDetallePresupuesto.setPieza(pieza);
                    miDetallePresupuesto.setHallazgo(hallazgo);
                    miDetallePresupuesto.setCantidad(cantidad);
                    miDetallePresupuesto.setPrecio(precio);
                    miDetallePresupuesto.setSubtotal(subtotal);
                    presupuesto.setId_presupuesto(parseInt(id_presupuesto));
                    miDetallePresupuesto.setPresupuesto(presupuesto);
                    modeloDetallePresupuesto.insertarDetallePresupuesto(miDetallePresupuesto);
                }
                response.setStatus(200);
                out.print("Presupuesto guardado");
            } else {
                response.setStatus(400);
                out.print("Ocurrió un error");
            }
            out.flush();

        } catch (Exception ex) {
            ex.printStackTrace(System.out);
        }
    }

    private void obtenerJson(HttpServletRequest request, HttpServletResponse response) {
        PrintWriter out = null;
        try {
            out = response.getWriter();
            String id_presupuesto = request.getParameter("id");
            Presupuesto presupuesto = modeloPresupuesto.obtenerPresupuesto(id_presupuesto);
            String json = new Gson().toJson(presupuesto);
            out.print(json);
        } catch (Exception ex) {
            ex.printStackTrace(System.out);
        }
    }

}
