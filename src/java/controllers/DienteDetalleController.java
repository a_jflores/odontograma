package controllers;

import beans.DienteDetalle;
import beans.HistoriaClinica;
import beans.Odontograma;
import com.google.gson.Gson;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import models.DienteDetalleModel;
import models.OdontogramaModel;
import org.json.JSONArray;
import org.json.JSONObject;

@WebServlet(name = "DienteDetalleController", urlPatterns = {"/dientedetalle.do"})
public class DienteDetalleController extends HttpServlet {

    DienteDetalleModel modeloDienteDetalle = new DienteDetalleModel();
    OdontogramaModel modeloOdontograma = new OdontogramaModel();

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        try {
            String operacion = request.getParameter("op");
            switch (operacion) {
                case "guardarOdontograma":
                    guardarOdontograma(request, response);
                    break;
                case "actualizarOdontograma":
                    actualizarOdontograma(request, response);
                    break;
                case "obtenerAnomalia":
                    obtenerAnomalia(request, response);
                    break;
                default:
                    request.getRequestDispatcher("/error404.jsp").forward(request, response);
                    break;
            }
        } finally {
            out.close();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

    private void guardarOdontograma(HttpServletRequest request, HttpServletResponse response) {

        try {
            PrintWriter out = response.getWriter();
            String jsonArrayString = request.getParameter("array");
            String id_historia_clinica = request.getParameter("id_hc");
            String observaciones = request.getParameter("observaciones");
            JSONArray jsonarray = new JSONArray(jsonArrayString);

            Odontograma miOdontograma = new Odontograma();
            HistoriaClinica miHistoriaClinica = new HistoriaClinica();
            miHistoriaClinica.setCodigoHistoriaClinica(Integer.parseInt(id_historia_clinica));
            miOdontograma.setHistoriaClinica(miHistoriaClinica);
            miOdontograma.setObservaciones(observaciones);

            if (!jsonArrayString.equals("[]")) {
                if (modeloOdontograma.insertarOdontograma(miOdontograma) > 0) {
                    int conteo = modeloOdontograma.ultimoOdontograma();
                    for (int i = 0; i < jsonarray.length(); i++) {
                        JSONObject jsonobject = jsonarray.getJSONObject(i);
                        String diente = jsonobject.getString("diente");
                        String cara = jsonobject.getString("cara");
                        String estado = jsonobject.getString("estado");
                        String pareja = jsonobject.getString("pareja");

                        DienteDetalle miDienteDetalle = new DienteDetalle();
                        miDienteDetalle.setId_odontograma(conteo);
                        miDienteDetalle.setId_diente(Integer.parseInt(diente));
                        miDienteDetalle.setId_cara(Integer.parseInt(cara));
                        miDienteDetalle.setId_hallazgo(Integer.parseInt(estado));
                        miDienteDetalle.setPareja(Integer.parseInt(pareja));
                        modeloDienteDetalle.insertarDienteDetalle(miDienteDetalle);
                    }
                    out.print(jsonArrayString);
                    //request.getSession().setAttribute("exito", "Registrado exitosamente");
                } else {
                    out.print("error insertar");
                    //request.getSession().setAttribute("fracaso", "No se pudo registrar, ya existe.");
                }
            } else {
                out.print("vacio");
            }

        } catch (SQLException | IOException ex) {
            Logger.getLogger(DienteDetalleController.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    private void actualizarOdontograma(HttpServletRequest request, HttpServletResponse response) {
        try {
            PrintWriter out = response.getWriter();
            String jsonArrayString = request.getParameter("array");
            String id_odontograma = request.getParameter("id_odontograma");
            String observaciones = request.getParameter("observaciones");
            JSONArray jsonarray = new JSONArray(jsonArrayString);

            Odontograma miOdontograma = new Odontograma();
            miOdontograma.setCodigoOdontograma(Integer.parseInt(id_odontograma));
            miOdontograma.setObservaciones(observaciones);

            if (!jsonArrayString.equals("[]")) {
                modeloOdontograma.modificarOdontograma(miOdontograma);
                if (modeloDienteDetalle.eliminarDienteDetalle(id_odontograma) > 0) {
                    for (int i = 0; i < jsonarray.length(); i++) {
                        JSONObject jsonobject = jsonarray.getJSONObject(i);
                        String diente = jsonobject.getString("diente");
                        String cara = jsonobject.getString("cara");
                        String estado = jsonobject.getString("estado");
                        String pareja = jsonobject.getString("pareja");

                        DienteDetalle miDienteDetalle = new DienteDetalle();
                        miDienteDetalle.setId_odontograma(Integer.parseInt(id_odontograma));
                        miDienteDetalle.setId_diente(Integer.parseInt(diente));
                        miDienteDetalle.setId_cara(Integer.parseInt(cara));
                        miDienteDetalle.setId_hallazgo(Integer.parseInt(estado));
                        miDienteDetalle.setPareja(Integer.parseInt(pareja));
                        modeloDienteDetalle.insertarDienteDetalle(miDienteDetalle);
                    }
                    out.print(jsonArrayString);
                } else {
                    out.print("error eliminar");
                    request.setAttribute("fracaso", "No se puede eliminar");
                }
            } else {
                out.print("vacio");
            }

        } catch (SQLException | IOException ex) {
            Logger.getLogger(DienteDetalleController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void obtenerAnomalia(HttpServletRequest request, HttpServletResponse response) {
        PrintWriter out = null;
        try {
            out = response.getWriter();
            String codigo = request.getParameter("id");
            List diente_detalle = modeloDienteDetalle.obtenerAnomalias(codigo);
            String json = new Gson().toJson(diente_detalle);
            out.print(json);
        } catch (Exception e) {
            e.printStackTrace(System.out);
        }
    }

}
