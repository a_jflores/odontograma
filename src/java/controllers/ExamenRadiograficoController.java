package controllers;

import beans.ExamenRadiografico;
import beans.HistoriaClinica;
import com.google.gson.Gson;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import static java.lang.Integer.parseInt;
import java.nio.file.FileAlreadyExistsException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;
import models.ExamenRadiograficoModel;

@MultipartConfig
@WebServlet(name = "ExamenRadiograficoController", urlPatterns = {"/examenradiografico.do"})
public class ExamenRadiograficoController extends HttpServlet {

    ExamenRadiograficoModel modeloExamenRadiografico = new ExamenRadiograficoModel();
    ArrayList<String> lista = new ArrayList<>();

    private String pathFiles = "C:\\Users\\";
    private File uploads = new File(pathFiles);
    private String[] extens = {".ico", ".png", ".jpg", ".jpeg"};

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        try {
            String operacion = request.getParameter("op");
            switch (operacion) {
                case "listarJson":
                    listarJson(request, response);
                    break;
                case "verJson":
                    verJson(request, response);
                    break;
                case "eliminarJson":
                    eliminarJson(request, response);
                    break;
                case "agregarRadiografia":
                    agregarRadiografia(request, response);
                    break;
                case "editarRadiografia":
                    editarRadiografia(request, response);
                    break;
                default:
                    request.getRequestDispatcher("/error404.jsp").forward(request, response);
                    break;
            }
        } finally {
            out.close();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

    private void listarJson(HttpServletRequest request, HttpServletResponse response) {
        PrintWriter out = null;
        try {
            out = response.getWriter();
            String id_historia_clinica = request.getParameter("id");
            List pacientes = modeloExamenRadiografico.listarExamenRadiografico(id_historia_clinica);
            String json = new Gson().toJson(pacientes);
            out.print(json);
        } catch (IOException | SQLException ex) {
            Logger.getLogger(ExamenRadiograficoController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void verJson(HttpServletRequest request, HttpServletResponse response) {
        PrintWriter out = null;
        try {
            out = response.getWriter();
            String id_examen_radiografico = request.getParameter("id");
            ExamenRadiografico examenRadiografico = modeloExamenRadiografico.obtenerExamenRadiografico(id_examen_radiografico);
            String json = new Gson().toJson(examenRadiografico);
            out.print(json);
        } catch (IOException | SQLException ex) {
            Logger.getLogger(ExamenRadiograficoController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void agregarRadiografia(HttpServletRequest req, HttpServletResponse response) {
        PrintWriter out = null;
        try {
            out = response.getWriter();
            String id_historia_clinica = req.getParameter("id_historia_clinica");
            String observaciones = req.getParameter("observaciones");
            Part part = req.getPart("foto");

            if (part == null) {
                System.out.println("No ha seleccionado un archivo");
                response.setStatus(400);
                out.print("Archivo vacío");
                return;
            }

            if (isExtension(part.getSubmittedFileName(), extens)) {
                String photo = saveFile(part, uploads);
                ExamenRadiografico examen = new ExamenRadiografico();
                HistoriaClinica historiaclinica = new HistoriaClinica();
                historiaclinica.setCodigoHistoriaClinica(parseInt(id_historia_clinica));
                examen.setHistoriaclinica(historiaclinica);
                examen.setFoto(photo);
                examen.setObservaciones(observaciones);
                modeloExamenRadiografico.insertarExamenRadiografico(examen);
                response.setStatus(200);
                out.print("Imagen guardada");
            } else {
                response.setStatus(400);
                out.print("Formato inválido");
            }
            out.flush();

        } catch (Exception e) {
            e.printStackTrace(System.out);
        }
    }

    private String saveFile(Part part, File pathUploads) {
        String pathAbsolute = "";
        try {
            Path path = Paths.get(part.getSubmittedFileName());
            String fileName = path.getFileName().toString();
            InputStream input = part.getInputStream();

            if (input != null) {
                //File file = new File(pathUploads, fileName);
                //pathAbsolute = file.getAbsolutePath();
                File file = new File(getServletContext().getRealPath("/public/img/"), fileName);
                pathAbsolute = getServletContext().getContextPath() + "/public/img/" + file.getName();
                Files.copy(input, file.toPath());
            }
        } catch (FileAlreadyExistsException e) {
            //Existe archivo
        } catch (IOException e) {
            e.printStackTrace();
        }

        return pathAbsolute;
    }

    private boolean isExtension(String fileName, String[] extensions) {
        for (String et : extensions) {
            if (fileName.toLowerCase().endsWith(et)) {
                return true;
            }
        }
        return false;
    }

    private void editarRadiografia(HttpServletRequest req, HttpServletResponse response) {
        PrintWriter out = null;
        try {
            out = response.getWriter();
            String id_historia_clinica = req.getParameter("update_id_historia_clinica");
            String id_examen_radiografico = req.getParameter("update_id_examen_radiografico");
            String observaciones = req.getParameter("update_observaciones");
            Part part = req.getPart("update_foto");
            InputStream inputStream = part.getInputStream();

            if (inputStream.available() == 0) {
                System.out.println("File vacío");
                ExamenRadiografico examen = new ExamenRadiografico();
                HistoriaClinica historiaclinica = new HistoriaClinica();
                historiaclinica.setCodigoHistoriaClinica(parseInt(id_historia_clinica));
                examen.setHistoriaclinica(historiaclinica);
                examen.setId_examen_radiografico(parseInt(id_examen_radiografico));
                examen.setObservaciones(observaciones);
                modeloExamenRadiografico.editExamRadiograph(examen);
                response.setStatus(200);
                out.print("Datos actualizados");
            } else {
                if (isExtension(part.getSubmittedFileName(), extens)) {
                    String photo = saveFile(part, uploads);
                    ExamenRadiografico examen = new ExamenRadiografico();
                    HistoriaClinica historiaclinica = new HistoriaClinica();
                    historiaclinica.setCodigoHistoriaClinica(parseInt(id_historia_clinica));
                    examen.setHistoriaclinica(historiaclinica);
                    examen.setId_examen_radiografico(parseInt(id_examen_radiografico));
                    examen.setFoto(photo);
                    examen.setObservaciones(observaciones);
                    modeloExamenRadiografico.editarExamenRadiografico(examen);
                    response.setStatus(200);
                    out.print("Datos actualizados");
                } else {
                    response.setStatus(400);
                    out.print("Formato inválido");
                }
            }
            out.flush();
            
        } catch (Exception e) {
            e.printStackTrace(System.out);
        }
    }

    private void eliminarJson(HttpServletRequest request, HttpServletResponse response) {
        PrintWriter out = null;
        try {
            out = response.getWriter();
            String respuesta = "";
            String id_examen_radiografico = request.getParameter("id");

            if (modeloExamenRadiografico.eliminarExamenRadiografico(id_examen_radiografico) > 0) {
                respuesta = "Éxito";
            } else {
                respuesta = "Error";
            }
            out.print(respuesta);

        } catch (IOException | SQLException ex) {
            Logger.getLogger(ExamenRadiograficoController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

}
