package controllers;

import com.google.gson.Gson;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;
import models.DetallePresupuestoModel;
import models.PresupuestoModel;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.export.JRPdfExporter;
import net.sf.jasperreports.engine.util.JRLoader;
import net.sf.jasperreports.export.SimpleExporterInput;
import net.sf.jasperreports.export.SimpleOutputStreamExporterOutput;
import net.sf.jasperreports.export.SimplePdfExporterConfiguration;

@WebServlet(name = "DetallePresupuestoController", urlPatterns = {"/detallepresupuesto.do"})
public class DetallePresupuestoController extends HttpServlet {

    DetallePresupuestoModel modeloDetallePresupuesto = new DetallePresupuestoModel();
    PresupuestoModel modeloPresupuesto = new PresupuestoModel();

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        ServletOutputStream out = response.getOutputStream();
        try {
            String operacion = request.getParameter("op");
            switch (operacion) {
                case "crearJson":
                    crearJson(request, response);
                    break;
                case "reporte":
                    reporte(request, response);
                    break;
                case "obtenerJson":
                    obtenerJson(request, response);
                    break;
                default:
                    request.getRequestDispatcher("/error404.jsp").forward(request, response);
                    break;
            }
        } finally {
            out.flush();
            out.close();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

    private void crearJson(HttpServletRequest request, HttpServletResponse response) {
        PrintWriter out = null;
        try {
            out = response.getWriter();
            String id_historia_clinica = request.getParameter("id_historia_clinica");
            String json_array = request.getParameter("array_detalle_presupuesto");

            int ultimo_id_presupuesto = modeloPresupuesto.ultimoPresupuestoHistoria(id_historia_clinica);

//            for (int i = 0; i < jsonarray.length(); i++) {
//                JSONObject jsonobject = jsonarray.getJSONObject(i);
//                int id_tratamiento = jsonobject.getInt("id_tratamiento");
//
//                DetallePresupuesto miDetallePresupuesto = new DetallePresupuesto();
//                Presupuesto presupuesto = new Presupuesto();
//                Tratamiento tratamiento = new Tratamiento();
//                presupuesto.setId_presupuesto(ultimo_id_presupuesto);
//                tratamiento.setIdTratamiento(id_tratamiento);
//                miDetallePresupuesto.setPresupuesto(presupuesto);
//                miDetallePresupuesto.setTratamiento(tratamiento);
//                modeloDetallePresupuesto.insertarDetallePresupuesto(miDetallePresupuesto);
//            }
            out.flush();
            out.close();

        } catch (Exception ex) {
            ex.printStackTrace(System.out);
        }
    }

    private void reporte(HttpServletRequest request, HttpServletResponse response) throws IOException {
        Connection conexion = null;
        ServletOutputStream out = response.getOutputStream();
        String pathReporte = "";
        String logo = "";
        String url = "";
        int id_presupuesto = Integer.parseInt(request.getParameter("id"));
        try {
            response.setHeader("Content-Disposition", "inline; filename=\"report1.pdf\";");
            response.setHeader("Cache-Control", "no-cache");
            response.setHeader("Pragma", "no-cache");
            response.setDateHeader("Expires", 0);
            response.setContentType("application/pdf");

            Context init = new InitialContext();
            Context context = (Context) init.lookup("java:comp/env");
            DataSource dataSource = (DataSource) context.lookup("jdbc/mysql");
            conexion = dataSource.getConnection();

            pathReporte = getServletContext().getRealPath("/reportesJasper/report1.jasper");
            logo = getServletContext().getRealPath("/reportesJasper/img/img-logo.png");
            url = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+""+request.getContextPath();

            Map parametros = new HashMap();
            parametros.put("id_presupuesto", id_presupuesto);
            parametros.put("logo", logo);
            parametros.put("url", url);

            JasperReport reporte = (JasperReport) JRLoader.loadObjectFromFile(pathReporte);
            JasperPrint jasperPrint = JasperFillManager.fillReport(reporte, parametros, conexion);
            JRPdfExporter exporter = new JRPdfExporter();
            exporter.setExporterInput(new SimpleExporterInput(jasperPrint));
            exporter.setExporterOutput(new SimpleOutputStreamExporterOutput(out));
            SimplePdfExporterConfiguration configuration = new SimplePdfExporterConfiguration();
            exporter.setConfiguration(configuration);
            exporter.exportReport();
            exporter.exportReport();
        } catch (Exception ex) {
            ex.printStackTrace(System.out);
        } finally {
            try {
                out.flush();
                out.close();
                conexion.close();
            } catch (SQLException ex) {
                Logger.getLogger(DetallePresupuestoController.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    private void obtenerJson(HttpServletRequest request, HttpServletResponse response) throws IOException {
        ServletOutputStream out = response.getOutputStream();
        try {
            String id_presupuesto = request.getParameter("id");
            List detalle_presupuesto = modeloDetallePresupuesto.listarDetallePresupuesto(id_presupuesto);            
            String json = new Gson().toJson(detalle_presupuesto);
            out.print(json);
            out.flush();
            out.close();

        } catch (Exception ex) {
            ex.printStackTrace(System.out);
        }
    }
}
