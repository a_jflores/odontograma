package controllers;

import beans.HistoriaClinica;
import beans.Video;
import com.google.gson.Gson;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import static java.lang.Integer.parseInt;
import java.nio.file.FileAlreadyExistsException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.SQLException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;
import models.VideoModel;

@MultipartConfig
@WebServlet(name = "VideoController", urlPatterns = {"/video.do"})
public class VideoController extends HttpServlet {

    VideoModel modeloVideo = new VideoModel();
    private String pathFiles = "C:\\Users\\";
    private File uploads = new File(pathFiles);
    private String[] extens = {".mp4", ".avi", ".3gp", ".m4a"};

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        try {
            String operacion = request.getParameter("op");
            switch (operacion) {
                case "listarJson":
                    listarJson(request, response);
                    break;
                case "crearJson":
                    crearJson(request, response);
                    break;
                case "verJson":
                    verJson(request, response);
                    break;
                case "editarJson":
                    editarJson(request, response);
                    break;
                case "eliminarJson":
                    eliminarJson(request, response);
                    break;
                default:
                    request.getRequestDispatcher("/error404.jsp").forward(request, response);
                    break;
            }
        } finally {
            out.close();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

    private String saveFile(Part part, File pathUploads) {
        String pathAbsolute = "";
        try {
            Path path = Paths.get(part.getSubmittedFileName());
            String fileName = path.getFileName().toString();
            InputStream input = part.getInputStream();

            if (input != null) {
                File file = new File(getServletContext().getRealPath("/public/video/"), fileName);
                pathAbsolute = getServletContext().getContextPath() + "/public/video/" + file.getName();
                Files.copy(input, file.toPath());
            }
        } catch (FileAlreadyExistsException e) {
            //Existe archivo
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return pathAbsolute;
    }

    private boolean isExtension(String fileName, String[] extensions) {
        for (String et : extensions) {
            if (fileName.toLowerCase().endsWith(et)) {
                return true;
            }
        }
        return false;
    }

    private void listarJson(HttpServletRequest request, HttpServletResponse response) {
        PrintWriter out = null;
        try {
            out = response.getWriter();
            String id_historia_clinica = request.getParameter("id");
            List videos = modeloVideo.listarVideo(id_historia_clinica);
            String json = new Gson().toJson(videos);
            out.print(json);
        } catch (IOException | SQLException ex) {
            Logger.getLogger(VideoController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void crearJson(HttpServletRequest request, HttpServletResponse response) {
        PrintWriter out = null;
        try {
            out = response.getWriter();
            String id_historia_clinica = request.getParameter("id_historia_clinica");
            String nombre = request.getParameter("nombre");
            Part part = request.getPart("ruta");
            String observaciones = request.getParameter("observaciones");

            if (part == null) {
                System.out.println("No ha seleccionado un archivo");
                response.setStatus(400);
                out.print("Vacío");
                return;
            }

            if (isExtension(part.getSubmittedFileName(), extens)) {
                String photo = saveFile(part, uploads);
                Video video = new Video();
                HistoriaClinica historiaclinica = new HistoriaClinica();
                historiaclinica.setCodigoHistoriaClinica(parseInt(id_historia_clinica));
                video.setHistoria(historiaclinica);
                video.setNombre(nombre);
                video.setRuta(photo);
                video.setObservaciones(observaciones);
                modeloVideo.insertarVideo(video);
                response.setStatus(200);
                out.print("Video guardado");
            } else {
                response.setStatus(400);
                out.print("Formato inválido");
            }
            out.flush();

        } catch (Exception ex) {
            ex.printStackTrace(System.out);
        }
    }

    private void verJson(HttpServletRequest request, HttpServletResponse response) {
        PrintWriter out = null;
        try {
            out = response.getWriter();
            String id_video_radiografico = request.getParameter("id");
            Video video = modeloVideo.obtenerVideo(id_video_radiografico);
            String json = new Gson().toJson(video);
            out.print(json);
        } catch (IOException | SQLException ex) {
            Logger.getLogger(VideoController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void editarJson(HttpServletRequest request, HttpServletResponse response) {
        PrintWriter out = null;
        try {
            out = response.getWriter();
            String id_historia_clinica = request.getParameter("update_id_historia_clinica");
            String id_video = request.getParameter("update_id_video");
            String nombre = request.getParameter("update_nombre");
            Part part = request.getPart("update_ruta");
            String observaciones = request.getParameter("update_observaciones_video");
            InputStream inputStream = part.getInputStream();
            if (inputStream.available() == 0) {
                System.out.println("File vacío");
                Video video = new Video();
                HistoriaClinica historiaclinica = new HistoriaClinica();
                historiaclinica.setCodigoHistoriaClinica(parseInt(id_historia_clinica));
                video.setHistoria(historiaclinica);
                video.setId_video(parseInt(id_video));
                video.setNombre(nombre);
                video.setObservaciones(observaciones);
                modeloVideo.editarVideosinRuta(video);
                response.setStatus(200);
                out.print("Datos actualizados");
            } else {
                if (isExtension(part.getSubmittedFileName(), extens)) {
                    String photo = saveFile(part, uploads);
                    Video video = new Video();
                    HistoriaClinica historiaclinica = new HistoriaClinica();
                    historiaclinica.setCodigoHistoriaClinica(parseInt(id_historia_clinica));
                    video.setHistoria(historiaclinica);
                    video.setId_video(parseInt(id_video));
                    video.setNombre(nombre);
                    video.setRuta(photo);
                    video.setObservaciones(observaciones);
                    modeloVideo.editarVideo(video);
                    response.setStatus(200);
                    out.print("Datos actualizados");
                } else {
                    response.setStatus(400);
                    out.print("Formato inválido");
                }
            }
            out.flush();

        } catch (Exception ex) {
            ex.printStackTrace(System.out);
        }
    }

    private void eliminarJson(HttpServletRequest request, HttpServletResponse response) {
        PrintWriter out = null;
        try {
            out = response.getWriter();
            String respuesta = "";
            String id_video = request.getParameter("id");

            if (modeloVideo.eliminarVideo(id_video) > 0) {
                respuesta = "Éxito";
            } else {
                respuesta = "Error";
            }
            out.print(respuesta);
            out.flush();
        } catch (IOException | SQLException ex) {
            Logger.getLogger(VideoController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

}
