package controllers;

import beans.DienteDetalle;
import beans.Odontograma;
import com.google.gson.Gson;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import models.DienteDetalleModel;
import models.OdontogramaModel;

@WebServlet(name = "OdontogramasController", urlPatterns = {"/odontogramas.do"})
public class OdontogramasController extends HttpServlet {

    OdontogramaModel modeloOdontogramas = new OdontogramaModel();
    DienteDetalleModel modeloDienteDetalle = new DienteDetalleModel();

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        try {
            String operacion = request.getParameter("op");
            switch (operacion) {
                case "obtenerOdontograma":
                    obtenerOdontograma(request, response);
                    break;
                case "obtenerDienteDetalle":
                    obtenerDienteDetalle(request, response);
                    break;
                default:
                    request.getRequestDispatcher("/error404.jsp").forward(request, response);
                    break;
            }
        } finally {
            out.close();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

    private void obtenerOdontograma(HttpServletRequest request, HttpServletResponse response) {
        try {
            String id_odontograma = request.getParameter("id");
            Odontograma miOdontograma = modeloOdontogramas.obtenerOdontograma(id_odontograma);

            if (miOdontograma != null) {
                request.setAttribute("odontograma", miOdontograma);
                request.setAttribute("listaDienteDetalle", modeloDienteDetalle.cargarDienteDetalle(id_odontograma));
                request.getRequestDispatcher("/views/HistoriaClinica/updateOdontograma.jsp").forward(request, response);
            } else {
                response.sendRedirect(request.getContextPath() + "/error404.jsp");
            }
        } catch (ServletException | IOException | SQLException ex) {
            Logger.getLogger(OdontogramasController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void obtenerDienteDetalle(HttpServletRequest request, HttpServletResponse response) {
        PrintWriter out = null;
        try {
            String id_odontograma = request.getParameter("id");
            out = response.getWriter();

            ArrayList<DienteDetalle> midienteDetalle = modeloDienteDetalle.cargarDienteDetalle(id_odontograma);
            String json = new Gson().toJson(midienteDetalle);
            out.print(json);
        } catch (IOException ex) {
            Logger.getLogger(OdontogramasController.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SQLException ex) {
            Logger.getLogger(OdontogramasController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
