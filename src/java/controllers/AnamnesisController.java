/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;

import beans.Anamnesis;
import beans.HistoriaClinica;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import models.AnamnesisModel;
import utils.Validaciones;

@WebServlet(name = "AnamnesisController", urlPatterns = {"/anamnesis.do"})
public class AnamnesisController extends HttpServlet {

    AnamnesisModel modelo = new AnamnesisModel();
    ArrayList<String> listaErrores = new ArrayList<>();

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        try {

            String operacion = request.getParameter("op");
            switch (operacion) {
                case "cuestionario_1_insertar":
                    insertar_cuestionario_1(request, response);
                    break;
                case "cuestionario_1_update":
                    cuestionario_1_update(request, response);
                    break;
                default:
                    request.getRequestDispatcher("/error404.jsp").forward(request, response);
                    break;
            }
        } finally {
            out.close();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

    private void insertar_cuestionario_1(HttpServletRequest request, HttpServletResponse response) {
        try {
            listaErrores.clear();
            String idHistoriaClinica = request.getParameter("id_historia_clinica");

            Anamnesis miAnamnesis = new Anamnesis();
            HistoriaClinica historia = new HistoriaClinica();

            historia.setCodigoHistoriaClinica(Integer.parseInt(idHistoriaClinica));
            miAnamnesis.setHistoriaClinica(historia);
            miAnamnesis.setDiabetes(request.getParameter("diabetes"));
            miAnamnesis.setHipertension(request.getParameter("hipertension_arterial"));
            miAnamnesis.setHemorragia(request.getParameter("hemorragia"));
            miAnamnesis.setAlergia(request.getParameter("alergia"));
            miAnamnesis.setDesmayo(request.getParameter("desmayo"));
            miAnamnesis.setHepatica(request.getParameter("hepatica"));
            miAnamnesis.setFiebre_reumatica(request.getParameter("fiebre_reumatica"));
            miAnamnesis.setAngina(request.getParameter("angina_pecho"));
            miAnamnesis.setMiocardio(request.getParameter("miocardio"));
            miAnamnesis.setEmbarazo(request.getParameter("embarazo"));
            miAnamnesis.setDetalle_diabetes(request.getParameter("detalle_diabetes"));
            miAnamnesis.setDetalle_hipertension(request.getParameter("detalle_hipertension_arterial"));
            miAnamnesis.setDetalle_hemorragia(request.getParameter("detalle_hemorragia"));
            miAnamnesis.setDetalle_alergia(request.getParameter("detalle_alergia"));
            miAnamnesis.setDetalle_desmayo(request.getParameter("detalle_desmayo"));
            miAnamnesis.setDetalle_hepatica(request.getParameter("detalle_hepatica"));
            miAnamnesis.setDetalle_fiebre_reumatica(request.getParameter("detalle_fiebre_reumatica"));
            miAnamnesis.setDetalle_angina(request.getParameter("detalle_angina_pecho"));
            miAnamnesis.setDetalle_miocardio(request.getParameter("detalle_miocardio"));
            miAnamnesis.setDetalle_embarazo(request.getParameter("detalle_embarazo"));
            miAnamnesis.setAlerta_diabetes(request.getParameter("alerta_diabetes"));
            miAnamnesis.setAlerta_hipertension(request.getParameter("alerta_hipertension_arterial"));
            miAnamnesis.setAlerta_hemorragia(request.getParameter("alerta_hemorragia"));
            miAnamnesis.setAlerta_alergia(request.getParameter("alerta_alergia"));
            miAnamnesis.setAlerta_desmayo(request.getParameter("alerta_desmayo"));
            miAnamnesis.setAlerta_hepatica(request.getParameter("alerta_hepatica"));
            miAnamnesis.setAlerta_fiebre_reumatica(request.getParameter("alerta_fiebre_reumatica"));
            miAnamnesis.setAlerta_angina(request.getParameter("alerta_angina_pecho"));
            miAnamnesis.setAlerta_miocardio(request.getParameter("alerta_miocardio"));
            miAnamnesis.setAlerta_embarazo(request.getParameter("alerta_embarazo"));

            if (listaErrores.size() > 0) {
                request.setAttribute("anamnesis", miAnamnesis);
                request.setAttribute("listaErrores", listaErrores);
                request.getRequestDispatcher("/historias.do?op=obtener&id=" + idHistoriaClinica).forward(request, response);
            } else { //No hay errores
                if (modelo.insertarAnamnesis(miAnamnesis) > 0) {
                    request.getSession().setAttribute("exito", "Cuestionario registrado exitosamente");
                    response.sendRedirect(request.getContextPath() + "/historias.do?op=obtener&id=" + idHistoriaClinica);
                } else {
                    request.getSession().setAttribute("fracaso", "No se pudo registrar este Cuestionario, ya existe.");
                    response.sendRedirect(request.getContextPath() + "/historias.do?op=obtener&id=" + idHistoriaClinica);
                }
            }
        } catch (ServletException | IOException | SQLException ex) {
            Logger.getLogger(AnamnesisController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void cuestionario_1_update(HttpServletRequest request, HttpServletResponse response) {
        try {
            listaErrores.clear();
            String idHistoriaClinica = request.getParameter("id_historia_clinica");

            Anamnesis miAnamnesis = new Anamnesis();
            HistoriaClinica historia = new HistoriaClinica();

            historia.setCodigoHistoriaClinica(Integer.parseInt(idHistoriaClinica));
            miAnamnesis.setHistoriaClinica(historia);
            miAnamnesis.setDiabetes(request.getParameter("diabetes"));
            miAnamnesis.setHipertension(request.getParameter("hipertension_arterial"));
            miAnamnesis.setHemorragia(request.getParameter("hemorragia"));
            miAnamnesis.setAlergia(request.getParameter("alergia"));
            miAnamnesis.setDesmayo(request.getParameter("desmayo"));
            miAnamnesis.setHepatica(request.getParameter("hepatica"));
            miAnamnesis.setFiebre_reumatica(request.getParameter("fiebre_reumatica"));
            miAnamnesis.setAngina(request.getParameter("angina_pecho"));
            miAnamnesis.setMiocardio(request.getParameter("miocardio"));
            miAnamnesis.setEmbarazo(request.getParameter("embarazo"));
            miAnamnesis.setDetalle_diabetes(request.getParameter("detalle_diabetes"));
            miAnamnesis.setDetalle_hipertension(request.getParameter("detalle_hipertension_arterial"));
            miAnamnesis.setDetalle_hemorragia(request.getParameter("detalle_hemorragia"));
            miAnamnesis.setDetalle_alergia(request.getParameter("detalle_alergia"));
            miAnamnesis.setDetalle_desmayo(request.getParameter("detalle_desmayo"));
            miAnamnesis.setDetalle_hepatica(request.getParameter("detalle_hepatica"));
            miAnamnesis.setDetalle_fiebre_reumatica(request.getParameter("detalle_fiebre_reumatica"));
            miAnamnesis.setDetalle_angina(request.getParameter("detalle_angina_pecho"));
            miAnamnesis.setDetalle_miocardio(request.getParameter("detalle_miocardio"));
            miAnamnesis.setDetalle_embarazo(request.getParameter("detalle_embarazo"));
            miAnamnesis.setAlerta_diabetes(request.getParameter("alerta_diabetes"));
            miAnamnesis.setAlerta_hipertension(request.getParameter("alerta_hipertension_arterial"));
            miAnamnesis.setAlerta_hemorragia(request.getParameter("alerta_hemorragia"));
            miAnamnesis.setAlerta_alergia(request.getParameter("alerta_alergia"));
            miAnamnesis.setAlerta_desmayo(request.getParameter("alerta_desmayo"));
            miAnamnesis.setAlerta_hepatica(request.getParameter("alerta_hepatica"));
            miAnamnesis.setAlerta_fiebre_reumatica(request.getParameter("alerta_fiebre_reumatica"));
            miAnamnesis.setAlerta_angina(request.getParameter("alerta_angina_pecho"));
            miAnamnesis.setAlerta_miocardio(request.getParameter("alerta_miocardio"));
            miAnamnesis.setAlerta_embarazo(request.getParameter("alerta_embarazo"));

            if (listaErrores.size() > 0) {
                request.setAttribute("anamnesis", miAnamnesis);
                request.setAttribute("listaErrores", listaErrores);
                request.getRequestDispatcher("/historias.do?op=obtener&id=" + idHistoriaClinica).forward(request, response);
            } else { //No hay errores
                if (modelo.modificarCuestionario1(miAnamnesis) > 0) {
                    request.getSession().setAttribute("exito", "Cuestionario actualizado exitosamente");
                    response.sendRedirect(request.getContextPath() + "/historias.do?op=obtener&id=" + idHistoriaClinica);
                } else {
                    request.getSession().setAttribute("fracaso", "No se pudo registrar este Cuestionario");
                    response.sendRedirect(request.getContextPath() + "/historias.do?op=obtener&id=" + idHistoriaClinica);
                }
            }
        } catch (ServletException | IOException | SQLException ex) {
            Logger.getLogger(AnamnesisController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

}
