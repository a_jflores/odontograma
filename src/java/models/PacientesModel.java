package models;

import beans.Paciente;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

public class PacientesModel extends Conexion {

    public ArrayList<Paciente> listarPacientes() throws SQLException {
        try {
            ArrayList<Paciente> lista = new ArrayList<Paciente>();
            String sql = "SELECT * FROM pacientes";
            this.conectar();
            st = conexion.prepareStatement(sql);
            rs = st.executeQuery();
            while (rs.next()) {
                Paciente paciente = new Paciente();
                paciente.setCodigoPaciente(rs.getString("id_paciente"));
                paciente.setNombrePaciente(rs.getString("nombres"));
                paciente.setDocumentoPaciente(rs.getString("documento"));
                paciente.setNacionalidadPaciente(rs.getString("nacionalidad"));
                paciente.setTelefonoPaciente(rs.getString("telefono"));
                paciente.setDomicilioPaciente(rs.getString("domicilio"));
                paciente.setFechanacimientoPaciente(rs.getString("fecha_nac"));
                paciente.setSexoPaciente(rs.getString("sexo"));
                paciente.setEmailPaciente(rs.getString("email"));
                paciente.setEstadocivilPaciente(rs.getString("estado_civil"));
                lista.add(paciente);
            }
            this.desconectar();
            return lista;
        } catch (SQLException ex) {
            Logger.getLogger(PacientesModel.class.getName()).log(Level.SEVERE, null, ex);
            this.desconectar();
            return null;
        }
    }

    public Paciente obtenerPaciente(String codigo) throws SQLException {
        try {
            String sql = "SELECT * FROM pacientes WHERE id_paciente=?";
            this.conectar();
            st = conexion.prepareStatement(sql);
            st.setString(1, codigo);
            rs = st.executeQuery();
            if (rs.next()) {
                Paciente paciente = new Paciente();
                paciente.setCodigoPaciente(rs.getString("id_paciente"));
                paciente.setNombrePaciente(rs.getString("nombres"));
                paciente.setDocumentoPaciente(rs.getString("documento"));
                paciente.setNacionalidadPaciente(rs.getString("nacionalidad"));
                paciente.setTelefonoPaciente(rs.getString("telefono"));
                paciente.setDomicilioPaciente(rs.getString("domicilio"));
                paciente.setFechanacimientoPaciente(rs.getString("fecha_nac"));
                paciente.setSexoPaciente(rs.getString("sexo"));
                paciente.setEmailPaciente(rs.getString("email"));
                paciente.setEstadocivilPaciente(rs.getString("estado_civil"));
                this.desconectar();
                return paciente;
            }
            this.desconectar();
            return null;
        } catch (SQLException ex) {
            Logger.getLogger(PacientesModel.class.getName()).log(Level.SEVERE, null, ex);
            this.desconectar();
            return null;
        }
    }

    public int modificarPaciente(Paciente paciente) throws SQLException {
        try {
            int filasAfectadas = 0;
            String sql = "UPDATE pacientes "
                    + "SET nombres=?, "
                    + "nacionalidad=?, "
                    + "documento=?, "
                    + "domicilio=?, "
                    + "sexo=?, "
                    + "fecha_nac=?, "
                    + "email=?, "
                    + "telefono=?, "
                    + "recomendacion=?, "
                    + "estado_civil=?, "
                    + "hospedaje=? "
                    + "WHERE id_paciente=?";
            this.conectar();
            st = conexion.prepareStatement(sql);
            st.setString(1, paciente.getNombrePaciente());
            st.setString(2, paciente.getNacionalidadPaciente());
            st.setString(3, paciente.getDocumentoPaciente());
            st.setString(4, paciente.getDomicilioPaciente());
            st.setString(5, paciente.getSexoPaciente());
            st.setString(6, paciente.getFechanacimientoPaciente());
            st.setString(7, paciente.getEmailPaciente());
            st.setString(8, paciente.getTelefonoPaciente());
            st.setString(9, paciente.getRecomendacionPaciente());
            st.setString(10, paciente.getEstadocivilPaciente());
            st.setString(11, paciente.getHospedajePaciente());
            st.setString(12, paciente.getCodigoPaciente());
            filasAfectadas = st.executeUpdate();
            this.desconectar();
            return filasAfectadas;
        } catch (SQLException ex) {
            Logger.getLogger(PacientesModel.class.getName()).log(Level.SEVERE, null, ex);
            this.desconectar();
            return 0;
        }
    }

    public ArrayList<Paciente> listarRecomendaciones(String codigo) throws SQLException {
        try {
            ArrayList<Paciente> lista = new ArrayList<Paciente>();
            String sql = "SELECT \n"
                    + "DISTINCT\n"
                    + "p.id_paciente,\n"
                    + "p.nombres\n"
                    + "\n"
                    + "FROM pacientes p\n"
                    + "LEFT JOIN historia_clinica h\n"
                    + "ON h.id_paciente = p.id_paciente\n"
                    + "WHERE h.id_historia_clinica != ? or h.id_historia_clinica is null";
            this.conectar();
            st = conexion.prepareStatement(sql);
            st.setString(1, codigo);
            rs = st.executeQuery();
            while (rs.next()) {
                Paciente paciente = new Paciente();
                paciente.setCodigoPaciente(rs.getString("id_paciente"));
                paciente.setNombrePaciente(rs.getString("nombres"));
                lista.add(paciente);
            }
            this.desconectar();
            return lista;
        } catch (SQLException ex) {
            Logger.getLogger(PacientesModel.class.getName()).log(Level.SEVERE, null, ex);
            this.desconectar();
            return null;
        }
    }

    public int editarPaciente(Paciente paciente) throws SQLException {
        try {
            int filasAfectadas = 0;
            String sql = "UPDATE pacientes "
                    + "SET nombres=?, "
                    + "nacionalidad=?, "
                    + "documento=?, "
                    + "domicilio=?, "
                    + "sexo=?, "
                    + "fecha_nac=?, "
                    + "email=?, "
                    + "telefono=?, "
                    + "estado_civil=? "
                    + "WHERE id_paciente=?";
            this.conectar();
            st = conexion.prepareStatement(sql);
            st.setString(1, paciente.getNombrePaciente());
            st.setString(2, paciente.getNacionalidadPaciente());
            st.setString(3, paciente.getDocumentoPaciente());
            st.setString(4, paciente.getDomicilioPaciente());
            st.setString(5, paciente.getSexoPaciente());
            st.setString(6, paciente.getFechanacimientoPaciente());
            st.setString(7, paciente.getEmailPaciente());
            st.setString(8, paciente.getTelefonoPaciente());
            st.setString(9, paciente.getEstadocivilPaciente());
            st.setString(10, paciente.getCodigoPaciente());
            filasAfectadas = st.executeUpdate();
            this.desconectar();
            return filasAfectadas;
        } catch (SQLException ex) {
            Logger.getLogger(PacientesModel.class.getName()).log(Level.SEVERE, null, ex);
            this.desconectar();
            return 0;
        }
    }

    public int insertarPaciente(Paciente paciente) throws SQLException {
        try {
            int filasAfectadas = 0;
            String sql = "INSERT INTO pacientes "
                    + "(id_clinica, "
                    + "nombres, "
                    + "nacionalidad, "
                    + "documento, "
                    + "domicilio, "
                    + "sexo, "
                    + "fecha_nac, "
                    + "email, "
                    + "telefono, "
                    + "estado_civil) "
                    + "VALUES(?,?,?,?,?,?,?,?,?,?)";
            this.conectar();
            st = conexion.prepareStatement(sql);
            st.setString(1, paciente.getIdClinica());
            st.setString(2, paciente.getNombrePaciente());
            st.setString(3, paciente.getNacionalidadPaciente());
            st.setString(4, paciente.getDocumentoPaciente());
            st.setString(5, paciente.getDomicilioPaciente());
            st.setString(6, paciente.getSexoPaciente());
            st.setString(7, paciente.getFechanacimientoPaciente());
            st.setString(8, paciente.getEmailPaciente());
            st.setString(9, paciente.getTelefonoPaciente());
            st.setString(10, paciente.getEstadocivilPaciente());
            filasAfectadas = st.executeUpdate();
            this.desconectar();
            return filasAfectadas;
        } catch (SQLException ex) {
            Logger.getLogger(PacientesModel.class.getName()).log(Level.SEVERE, null, ex);
            this.desconectar();
            return 0;
        }
    }

}
