package models;

import beans.Medico;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class MedicoModel extends Conexion{
    public List<Medico> listarMedico() throws SQLException {
        try {
            List<Medico> lista = new ArrayList<Medico>();
            String sql = "SELECT * FROM medicos";
            this.conectar();
            st = conexion.prepareStatement(sql);
            rs = st.executeQuery();
            while (rs.next()) {
                Medico medico = new Medico();
                medico.setIdMedico(rs.getInt("id_medico"));
                medico.setNombresMedico(rs.getString("nombres"));
                medico.setApellidosMedico(rs.getString("apellidos"));
                lista.add(medico);
            }
            this.desconectar();
            return lista;
        } catch (SQLException ex) {
            Logger.getLogger(MedicoModel.class.getName()).log(Level.SEVERE, null, ex);
            this.desconectar();
            return null;
        }
    }
}
