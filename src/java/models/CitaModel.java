package models;

import beans.Cita;
import beans.Medico;
import beans.Tratamiento;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

public class CitaModel extends Conexion {

    public ArrayList<Cita> listarCitas(int codigo) throws SQLException {
        try {
            ArrayList<Cita> lista = new ArrayList<Cita>();
            String sql = "SELECT \n"
                    + "c.id_cita,\n"
                    + "t.tratamiento,\n"
                    + "m.nombres,\n"
                    + "m.apellidos,\n"
                    + "c.estado,\n"
                    + "c.fecha\n"
                    + "\n"
                    + "FROM citas c\n"
                    + "INNER JOIN tratamientos t\n"
                    + "ON t.id_tratamiento = c.id_tratamiento\n"
                    + "INNER JOIN medicos m\n"
                    + "ON m.id_medico = c.id_medico "
                    + "WHERE c.id_paciente = ?";
            this.conectar();
            st = conexion.prepareStatement(sql);
            st.setInt(1, codigo);
            rs = st.executeQuery();
            while (rs.next()) {
                Cita cita = new Cita();
                Tratamiento tratamiento = new Tratamiento();
                Medico medico = new Medico();
                cita.setIdCita(rs.getInt("id_cita"));
                tratamiento.setNombreTratamiento(rs.getString("tratamiento"));
                cita.setTratamiento(tratamiento);
                medico.setNombresMedico(rs.getString("nombres"));
                medico.setApellidosMedico(rs.getString("apellidos"));
                cita.setMedico(medico);
                cita.setEstadoCita(rs.getString("estado"));
                cita.setFechaCita(rs.getString("fecha"));
                lista.add(cita);
            }
            this.desconectar();
            return lista;
        } catch (SQLException ex) {
            Logger.getLogger(CitaModel.class.getName()).log(Level.SEVERE, null, ex);
            this.desconectar();
            return null;
        }
    }
}
