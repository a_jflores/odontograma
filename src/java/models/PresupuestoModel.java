package models;

import beans.Medico;
import beans.Odontograma;
import beans.Presupuesto;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class PresupuestoModel extends Conexion {

    public List<Presupuesto> listarPresupuestos(String id) throws SQLException {
        try {
            List<Presupuesto> lista = new ArrayList<Presupuesto>();
            String sql = "SELECT * FROM presupuesto WHERE id_historia_clinica = ?";
            this.conectar();
            st = conexion.prepareStatement(sql);
            st.setString(1, id);
            rs = st.executeQuery();
            while (rs.next()) {
                Presupuesto presupuesto = new Presupuesto();
                presupuesto.setId_presupuesto(rs.getInt("id_presupuesto"));
                presupuesto.setAsunto(rs.getString("asunto"));
                presupuesto.setFecha(rs.getString("fecha"));
                presupuesto.setTotal(rs.getDouble("total"));
                presupuesto.setTipo_pago(rs.getString("tipo_pago"));
                presupuesto.setEstado(rs.getString("estado"));
                lista.add(presupuesto);
            }
            this.desconectar();
            return lista;
        } catch (SQLException ex) {
            Logger.getLogger(PresupuestoModel.class.getName()).log(Level.SEVERE, null, ex);
            this.desconectar();
            return null;
        }
    }

    public Presupuesto obtenerPresupuesto(String codigo) throws SQLException {
        try {
            String sql = "SELECT * FROM presupuesto WHERE id_presupuesto=?";
            this.conectar();
            st = conexion.prepareStatement(sql);
            st.setString(1, codigo);
            rs = st.executeQuery();
            if (rs.next()) {
                Presupuesto presupuesto = new Presupuesto();
                Medico medico = new Medico();
                Odontograma odontograma = new Odontograma();

                presupuesto.setId_presupuesto(rs.getInt("id_presupuesto"));
                medico.setIdMedico(rs.getInt("id_medico"));
                presupuesto.setMedico(medico);
                odontograma.setCodigoOdontograma(rs.getInt("id_odontograma"));
                presupuesto.setOdontograma(odontograma);
                presupuesto.setAsunto(rs.getString("asunto"));
                presupuesto.setTotal(rs.getDouble("total"));
                presupuesto.setTipo_pago(rs.getString("tipo_pago"));
                this.desconectar();
                return presupuesto;
            }
            this.desconectar();
            return null;
        } catch (SQLException ex) {
            Logger.getLogger(PacientesModel.class.getName()).log(Level.SEVERE, null, ex);
            this.desconectar();
            return null;
        }
    }

    public int insertarPresupuesto(Presupuesto miPresupuesto) throws SQLException {
        try {
            int filasAfectadas = 0;
            String sql = "INSERT INTO presupuesto "
                    + "VALUES (null,?,?,?,?,?,CURRENT_TIME(),?,?,?)";
            this.conectar();
            st = conexion.prepareStatement(sql);
            st.setInt(1, miPresupuesto.getHistoria().getCodigoHistoriaClinica());
            st.setInt(2, miPresupuesto.getMedico().getIdMedico());
            st.setInt(3, miPresupuesto.getId_clinica());
            st.setInt(4, miPresupuesto.getOdontograma().getCodigoOdontograma());
            st.setString(5, miPresupuesto.getAsunto());
            st.setDouble(6, miPresupuesto.getTotal());
            st.setString(7, miPresupuesto.getTipo_pago());
            st.setString(8, miPresupuesto.getEstado());
            filasAfectadas = st.executeUpdate();
            this.desconectar();
            return filasAfectadas;
        } catch (SQLException ex) {
            Logger.getLogger(PresupuestoModel.class.getName()).log(Level.SEVERE, null, ex);
            this.desconectar();
            return 0;
        }
    }

    public int ultimoPresupuestoHistoria(String codigo) throws SQLException {
        try {
            int filasAfectadas = 0;
            String sql = "SELECT MAX(id_presupuesto) FROM presupuesto\n"
                    + "WHERE id_historia_clinica = ?";
            this.conectar();
            st = conexion.prepareStatement(sql);
            st.setString(1, codigo);
            rs = st.executeQuery();
            while (rs.next()) {
                filasAfectadas = rs.getInt(1);
            }
            this.desconectar();
            return filasAfectadas;
        } catch (SQLException ex) {
            Logger.getLogger(PresupuestoModel.class.getName()).log(Level.SEVERE, null, ex);
            this.desconectar();
            return 0;
        }
    }

    public int updateEstadoPresupuesto(Presupuesto miPresupuesto) throws SQLException {
        try {
            int filasAfectadas = 0;
            String sql = "UPDATE presupuesto\n"
                    + "SET estado = ?\n"
                    + "WHERE id_presupuesto = ?";
            this.conectar();
            st = conexion.prepareStatement(sql);
            st.setString(1, miPresupuesto.getEstado());
            st.setInt(2, miPresupuesto.getId_presupuesto());
            filasAfectadas = st.executeUpdate();
            this.desconectar();
            return filasAfectadas;
        } catch (SQLException ex) {
            Logger.getLogger(PresupuestoModel.class.getName()).log(Level.SEVERE, null, ex);
            this.desconectar();
            return 0;
        }
    }

    public int editarPresupuesto(Presupuesto miPresupuesto) throws SQLException {
        try {
            int filasAfectadas = 0;
            String sql = "UPDATE presupuesto "
                    + "SET id_medico = ?,"
                    + "id_odontograma = ?,"
                    + "asunto = ?,"
                    + "total = ?,"
                    + "tipo_pago = ? "
                    + "WHERE id_presupuesto = ?";
            this.conectar();
            st = conexion.prepareStatement(sql);
            st.setInt(1, miPresupuesto.getMedico().getIdMedico());
            st.setInt(2, miPresupuesto.getOdontograma().getCodigoOdontograma());
            st.setString(3, miPresupuesto.getAsunto());
            st.setDouble(4, miPresupuesto.getTotal());
            st.setString(5, miPresupuesto.getTipo_pago());
            st.setInt(6, miPresupuesto.getId_presupuesto());
            filasAfectadas = st.executeUpdate();
            this.desconectar();
            return filasAfectadas;
        } catch (SQLException ex) {
            Logger.getLogger(PresupuestoModel.class.getName()).log(Level.SEVERE, null, ex);
            this.desconectar();
            return 0;
        }
    }

}
