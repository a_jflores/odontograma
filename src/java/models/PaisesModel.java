package models;

import beans.Pais;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class PaisesModel extends Conexion{

    public List<Pais> listarPaises() throws SQLException {
        try {
            List<Pais> lista = new ArrayList<Pais>();
            String sql = "SELECT * FROM paises";
            this.conectar();
            st = conexion.prepareStatement(sql);
            rs = st.executeQuery();
            while (rs.next()) {
                Pais pais = new Pais();
                pais.setIdPais(rs.getInt("id"));
                pais.setNacionalidadPais(rs.getString("nacionalidad"));
                pais.setNombrePais(rs.getString("nombre"));
                lista.add(pais);
            }
            this.desconectar();
            return lista;
        } catch (SQLException ex) {
            Logger.getLogger(PaisesModel.class.getName()).log(Level.SEVERE, null, ex);
            this.desconectar();
            return null;
        }
    }
}
