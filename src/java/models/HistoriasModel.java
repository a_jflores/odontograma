package models;

import beans.HistoriaClinica;
import beans.Paciente;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

public class HistoriasModel extends Conexion {

    public ArrayList<HistoriaClinica> listarHistorias() throws SQLException {
        try {
            ArrayList<HistoriaClinica> lista = new ArrayList<HistoriaClinica>();
            String sql = "SELECT "
                    + "hc.id_historia_clinica, "
                    + "hc.id_paciente, "
                    + "p.nombres, "
                    + "p.documento, "
                    + "p.telefono, "
                    + "p.nacionalidad, "
                    + "hc.fecha_created "
                    + "FROM historia_clinica hc "
                    + "INNER JOIN pacientes p "
                    + "ON p.id_paciente = hc.id_paciente";
            this.conectar();
            st = conexion.prepareStatement(sql);
            rs = st.executeQuery();
            while (rs.next()) {
                HistoriaClinica historia = new HistoriaClinica();
                historia.setCodigoHistoriaClinica(rs.getInt("id_historia_clinica"));
                historia.setFecha_created(rs.getString("fecha_created"));
                //historia.setPaciente(new Paciente(rs.getString("id_paciente"),rs.getString("nombres"),rs.getString("documento")));
                historia.setPaciente(new Paciente());
                historia.getPaciente().setCodigoPaciente(rs.getString("id_paciente"));
                historia.getPaciente().setNombrePaciente(rs.getString("nombres"));
                historia.getPaciente().setDocumentoPaciente(rs.getString("documento"));
                historia.getPaciente().setTelefonoPaciente(rs.getString("telefono"));
                historia.getPaciente().setNacionalidadPaciente(rs.getString("nacionalidad"));
                lista.add(historia);
            }
            this.desconectar();
            return lista;
        } catch (SQLException ex) {
            Logger.getLogger(HistoriasModel.class.getName()).log(Level.SEVERE, null, ex);
            this.desconectar();
            return null;
        }
    }

    public HistoriaClinica obtenerHistoria(Integer codigo) throws SQLException {
        try {
            String sql = "SELECT \n"
                    + "hc.id_historia_clinica,\n"
                    + "p.id_paciente,\n"
                    + "p.nombres,\n"
                    + "p.nacionalidad,\n"
                    + "p.documento,\n"
                    + "p.domicilio,\n"
                    + "p.sexo,\n"
                    + "p.fecha_nac,\n"
                    + "p.email,\n"
                    + "p.telefono,\n"
                    + "p.recomendacion, \n"
                    + "p.estado_civil, \n"
                    + "p.hospedaje \n"
                    + "FROM historia_clinica hc\n"
                    + "INNER JOIN pacientes p\n"
                    + "ON p.id_paciente = hc.id_paciente\n"
                    + "WHERE hc.id_historia_clinica = ?";
            this.conectar();
            st = conexion.prepareStatement(sql);
            st.setInt(1, codigo);
            rs = st.executeQuery();
            if (rs.next()) {
                HistoriaClinica historia = new HistoriaClinica();
                historia.setCodigoHistoriaClinica(rs.getInt("id_historia_clinica"));
                //historia.setPaciente(new Paciente(rs.getString("id_paciente"),rs.getString("nombres"),rs.getString("documento")));
                historia.setPaciente(new Paciente());
                historia.getPaciente().setCodigoPaciente(rs.getString("id_paciente"));
                historia.getPaciente().setNombrePaciente(rs.getString("nombres"));
                historia.getPaciente().setNacionalidadPaciente(rs.getString("nacionalidad"));
                historia.getPaciente().setDocumentoPaciente(rs.getString("documento"));
                historia.getPaciente().setDomicilioPaciente(rs.getString("domicilio"));
                historia.getPaciente().setSexoPaciente(rs.getString("sexo"));
                historia.getPaciente().setFechanacimientoPaciente(rs.getString("fecha_nac"));
                historia.getPaciente().setEmailPaciente(rs.getString("email"));
                historia.getPaciente().setTelefonoPaciente(rs.getString("telefono"));
                historia.getPaciente().setRecomendacionPaciente(rs.getString("recomendacion"));
                historia.getPaciente().setEstadocivilPaciente(rs.getString("estado_civil"));
                historia.getPaciente().setHospedajePaciente(rs.getString("hospedaje"));
                this.desconectar();
                return historia;
            }
            this.desconectar();
            return null;
        } catch (SQLException ex) {
            Logger.getLogger(HistoriasModel.class.getName()).log(Level.SEVERE, null, ex);
            this.desconectar();
            return null;
        }
    }

    public int insertarHistoria(HistoriaClinica historia) throws SQLException {
        try {
            int filasAfectadas = 0;
            String sql = "INSERT INTO historia_clinica (id_paciente) VALUES(?)";
            this.conectar();
            st = conexion.prepareStatement(sql);
            st.setInt(1, historia.getCodigoPaciente());
            filasAfectadas = st.executeUpdate();
            this.desconectar();
            return filasAfectadas;
        } catch (SQLException ex) {
            Logger.getLogger(HistoriasModel.class.getName()).log(Level.SEVERE, null, ex);
            this.desconectar();
            return 0;
        }
    }

    public int ultimaHistoria() throws SQLException {
        try {
            String sql = "SELECT id_historia_clinica\n"
                    + "FROM historia_clinica \n"
                    + "ORDER BY id_historia_clinica DESC LIMIT 1";
            this.conectar();
            st = conexion.prepareStatement(sql);
            rs = st.executeQuery();
            int id_historia = 0;
            while (rs.next()) {
                id_historia = rs.getInt(1);
            }
            this.desconectar();
            return id_historia;
        } catch (SQLException ex) {
            Logger.getLogger(DashboardModel.class.getName()).log(Level.SEVERE, null, ex);
            this.desconectar();
            return 0;
        }
    }

    public int modificarDatosPersonales(HistoriaClinica historia) throws SQLException {
        try {
            int filasAfectadas = 0;
            String sql = "UPDATE pacientes SET nombres=?,"
                    + "documento=? WHERE id_paciente=?";
            this.conectar();
            st = conexion.prepareStatement(sql);
            st.setString(1, historia.getPaciente().getNombrePaciente());
            st.setString(2, historia.getPaciente().getDocumentoPaciente());
            st.setString(3, historia.getPaciente().getCodigoPaciente());
            filasAfectadas = st.executeUpdate();
            this.desconectar();
            return filasAfectadas;
        } catch (SQLException ex) {
            Logger.getLogger(HistoriasModel.class.getName()).log(Level.SEVERE, null, ex);
            this.desconectar();
            return 0;
        }
    }

    public HistoriaClinica crearOdontograma(Integer codigo) throws SQLException {
        try {
            String sql = "SELECT \n"
                    + "hc.id_historia_clinica,\n"
                    + "p.id_paciente,\n"
                    + "p.nombres,\n"
                    + "p.nacionalidad,\n"
                    + "p.documento,\n"
                    + "p.domicilio,\n"
                    + "p.sexo,\n"
                    + "p.fecha_nac,\n"
                    + "p.email,\n"
                    + "p.telefono,\n"
                    + "p.recomendacion, \n"
                    + "p.estado_civil, \n"
                    + "p.hospedaje \n"
                    + "FROM historia_clinica hc\n"
                    + "INNER JOIN pacientes p\n"
                    + "ON p.id_paciente = hc.id_paciente\n"
                    + "WHERE hc.id_historia_clinica = ?";
            this.conectar();
            st = conexion.prepareStatement(sql);
            st.setInt(1, codigo);
            rs = st.executeQuery();
            if (rs.next()) {
                HistoriaClinica historia = new HistoriaClinica();
                historia.setCodigoHistoriaClinica(rs.getInt("id_historia_clinica"));
                //historia.setPaciente(new Paciente(rs.getString("id_paciente"),rs.getString("nombres"),rs.getString("documento")));
                historia.setPaciente(new Paciente());
                historia.getPaciente().setCodigoPaciente(rs.getString("id_paciente"));
                historia.getPaciente().setNombrePaciente(rs.getString("nombres"));
                historia.getPaciente().setNacionalidadPaciente(rs.getString("nacionalidad"));
                historia.getPaciente().setDocumentoPaciente(rs.getString("documento"));
                historia.getPaciente().setDomicilioPaciente(rs.getString("domicilio"));
                historia.getPaciente().setSexoPaciente(rs.getString("sexo"));
                historia.getPaciente().setFechanacimientoPaciente(rs.getString("fecha_nac"));
                historia.getPaciente().setEmailPaciente(rs.getString("email"));
                historia.getPaciente().setTelefonoPaciente(rs.getString("telefono"));
                historia.getPaciente().setRecomendacionPaciente(rs.getString("recomendacion"));
                historia.getPaciente().setEstadocivilPaciente(rs.getString("estado_civil"));
                historia.getPaciente().setHospedajePaciente(rs.getString("hospedaje"));
                this.desconectar();
                return historia;
            }
            this.desconectar();
            return null;
        } catch (SQLException ex) {
            Logger.getLogger(HistoriasModel.class.getName()).log(Level.SEVERE, null, ex);
            this.desconectar();
            return null;
        }
    }
}
