package models;

import beans.Tratamiento;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class TratamientoModel extends Conexion {

    public List<Tratamiento> listarTratamientos() throws SQLException {
        try {
            List<Tratamiento> lista = new ArrayList<Tratamiento>();
            String sql = "SELECT * FROM tratamientos";
            this.conectar();
            st = conexion.prepareStatement(sql);
            rs = st.executeQuery();
            while (rs.next()) {
                Tratamiento tratamiento = new Tratamiento();
                tratamiento.setIdTratamiento(rs.getInt("id_tratamiento"));
                tratamiento.setNombreTratamiento(rs.getString("tratamiento"));
                tratamiento.setPrecioTratamiento(rs.getDouble("precio"));
                lista.add(tratamiento);
            }
            this.desconectar();
            return lista;
        } catch (SQLException ex) {
            Logger.getLogger(TratamientoModel.class.getName()).log(Level.SEVERE, null, ex);
            this.desconectar();
            return null;
        }
    }
}
