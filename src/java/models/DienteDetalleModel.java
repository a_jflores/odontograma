package models;

import beans.Diente;
import beans.DienteDetalle;
import beans.HallazgoDental;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import static models.Conexion.conexion;

public class DienteDetalleModel extends Conexion {

    public ArrayList<DienteDetalle> cargarDienteDetalle(String codigo) throws SQLException {
        try {
            ArrayList<DienteDetalle> lista = new ArrayList<DienteDetalle>();
            String sql = "SELECT \n"
                    + "id_diente,\n"
                    + "id_cara,\n"
                    + "id_hallazgo,\n"
                    + "pareja\n"
                    + "FROM diente_detalle\n"
                    + "WHERE id_odontograma = ?";
            this.conectar();
            st = conexion.prepareStatement(sql);
            st.setString(1, codigo);
            rs = st.executeQuery();
            while (rs.next()) {
                DienteDetalle diente_detalle = new DienteDetalle();
                diente_detalle.setId_diente(rs.getInt("id_diente"));
                diente_detalle.setId_cara(rs.getInt("id_cara"));
                diente_detalle.setId_hallazgo(rs.getInt("id_hallazgo"));
                diente_detalle.setPareja(rs.getInt("pareja"));
                lista.add(diente_detalle);
            }
            this.desconectar();
            return lista;
        } catch (SQLException ex) {
            Logger.getLogger(DienteDetalleModel.class.getName()).log(Level.SEVERE, null, ex);
            this.desconectar();
            return null;
        }
    }

    public int insertarDienteDetalle(DienteDetalle diente_detalle) throws SQLException {
        try {
            int filasAfectadas = 0;
            String sql = "INSERT INTO diente_detalle "
                    + "(id_odontograma,"
                    + "id_diente,"
                    + "id_cara,"
                    + "id_hallazgo,"
                    + "pareja) "
                    + "VALUES (?,?,?,?,?)";
            this.conectar();
            st = conexion.prepareStatement(sql);
            st.setInt(1, diente_detalle.getId_odontograma());
            st.setInt(2, diente_detalle.getId_diente());
            st.setInt(3, diente_detalle.getId_cara());
            st.setInt(4, diente_detalle.getId_hallazgo());
            st.setInt(5, diente_detalle.getPareja());
            filasAfectadas = st.executeUpdate();
            this.desconectar();
            return filasAfectadas;
        } catch (SQLException ex) {
            Logger.getLogger(DienteDetalleModel.class.getName()).log(Level.SEVERE, null, ex);
            this.desconectar();
            return 0;
        }
    }

    public int modificarDienteDetalle(DienteDetalle diente_detalle) throws SQLException {
        try {
            int filasAfectadas = 0;
            String sql = "INSERT INTO diente_detalle "
                    + "(id_odontograma,"
                    + "id_diente,"
                    + "id_cara,"
                    + "id_hallazgo,"
                    + "pareja) "
                    + "VALUES (?,?,?,?,?)";
            this.conectar();
            st = conexion.prepareStatement(sql);
            st.setInt(1, diente_detalle.getId_odontograma());
            st.setInt(2, diente_detalle.getId_diente());
            st.setInt(3, diente_detalle.getId_cara());
            st.setInt(4, diente_detalle.getId_hallazgo());
            st.setInt(5, diente_detalle.getPareja());
            filasAfectadas = st.executeUpdate();
            this.desconectar();
            return filasAfectadas;
        } catch (SQLException ex) {
            Logger.getLogger(DienteDetalleModel.class.getName()).log(Level.SEVERE, null, ex);
            this.desconectar();
            return 0;
        }
    }

    public int eliminarDienteDetalle(String codigo) throws SQLException {
        try {
            int filasAfectadas = 0;
            String sql = "DELETE FROM diente_detalle WHERE id_odontograma=?";
            this.conectar();
            st = conexion.prepareStatement(sql);
            st.setString(1, codigo);
            filasAfectadas = st.executeUpdate();
            this.desconectar();
            return filasAfectadas;
        } catch (SQLException ex) {
            Logger.getLogger(DienteDetalleModel.class.getName()).log(Level.SEVERE, null, ex);
            this.desconectar();
            return 0;
        }
    }

    public int conteoDienteDetalle(String codigo) throws SQLException {
        try {
            int filasAfectadas = 0;
            String sql = "SELECT\n"
                    + "count(*)\n"
                    + "FROM diente_detalle \n"
                    + "WHERE id_odontograma = ?";
            this.conectar();
            st = conexion.prepareStatement(sql);
            st.setString(1, codigo);
            rs = st.executeQuery();
            while (rs.next()) {
                filasAfectadas = rs.getInt(1);
            }
            this.desconectar();
            return filasAfectadas;
        } catch (SQLException ex) {
            Logger.getLogger(DienteDetalleModel.class.getName()).log(Level.SEVERE, null, ex);
            this.desconectar();
            return 0;
        }
    }

    public List<DienteDetalle> obtenerAnomalias(String codigo) throws SQLException {
        try {
            List<DienteDetalle> lista = new ArrayList<DienteDetalle>();
            String sql = "SELECT\n"
                    + "hd.nombre,\n"
                    + "d.numero_diente\n"
                    + "FROM diente_detalle dd\n"
                    + "INNER JOIN hallazgo_dental hd\n"
                    + "ON hd.id_hallazgo = dd.id_hallazgo\n"
                    + "INNER JOIN diente d\n"
                    + "ON d.id_diente = dd.id_diente\n"
                    + "WHERE dd.id_odontograma = ? AND hd.estado = 'malo'";
            this.conectar();
            st = conexion.prepareStatement(sql);
            st.setString(1, codigo);
            rs = st.executeQuery();
            while (rs.next()) {
                DienteDetalle diente_detalle = new DienteDetalle();
                Diente diente = new Diente();
                HallazgoDental hallazgo = new HallazgoDental();
                
                hallazgo.setNombre(rs.getString("nombre"));
                diente_detalle.setHallazgo(hallazgo);
                diente.setNumero(rs.getInt("numero_diente"));
                diente_detalle.setDiente(diente);
                lista.add(diente_detalle);
            }
            this.desconectar();
            return lista;
        } catch (SQLException ex) {
            Logger.getLogger(DienteDetalleModel.class.getName()).log(Level.SEVERE, null, ex);
            this.desconectar();
            return null;
        }
    }

}
