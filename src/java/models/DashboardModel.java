package models;

import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class DashboardModel extends Conexion{
    
    public int conteoHistorias() throws SQLException{
        try {
            String sql = "SELECT COUNT(id_historia_clinica) FROM historia_clinica";
            this.conectar();
            st = conexion.prepareStatement(sql);
            rs = st.executeQuery();
            int conteo = 0;
            while(rs.next()){
                conteo = rs.getInt(1);
            }
            this.desconectar();
            return conteo;
        } catch (SQLException ex) {
            Logger.getLogger(DashboardModel.class.getName()).log(Level.SEVERE, null, ex);
            this.desconectar();
            return 0;
        }
    }
    
    public int conteoPacientes() throws SQLException{
        try {
            String sql = "SELECT COUNT(id_paciente) FROM pacientes";
            this.conectar();
            st = conexion.prepareStatement(sql);
            rs = st.executeQuery();
            int conteo = 0;
            while(rs.next()){
                conteo = rs.getInt(1);
            }
            this.desconectar();
            return conteo;
        } catch (SQLException ex) {
            Logger.getLogger(DashboardModel.class.getName()).log(Level.SEVERE, null, ex);
            this.desconectar();
            return 0;
        }
    }
    
    public int conteoCitas() throws SQLException{
        try {
            String sql = "SELECT COUNT(id_cita) FROM citas";
            this.conectar();
            st = conexion.prepareStatement(sql);
            rs = st.executeQuery();
            int conteo = 0;
            while(rs.next()){
                conteo = rs.getInt(1);
            }
            this.desconectar();
            return conteo;
        } catch (SQLException ex) {
            Logger.getLogger(DashboardModel.class.getName()).log(Level.SEVERE, null, ex);
            this.desconectar();
            return 0;
        }
    }
    
    public int conteoUsuarios() throws SQLException{
        try {
            String sql = "SELECT COUNT(id_usuario) FROM usuarios";
            this.conectar();
            st = conexion.prepareStatement(sql);
            rs = st.executeQuery();
            int conteo = 0;
            while(rs.next()){
                conteo = rs.getInt(1);
            }
            this.desconectar();
            return conteo;
        } catch (SQLException ex) {
            Logger.getLogger(DashboardModel.class.getName()).log(Level.SEVERE, null, ex);
            this.desconectar();
            return 0;
        }
    }
    
    public int conteoMedicos() throws SQLException{
        try {
            String sql = "SELECT COUNT(id_medico) FROM medicos";
            this.conectar();
            st = conexion.prepareStatement(sql);
            rs = st.executeQuery();
            int conteo = 0;
            while(rs.next()){
                conteo = rs.getInt(1);
            }
            this.desconectar();
            return conteo;
        } catch (SQLException ex) {
            Logger.getLogger(DashboardModel.class.getName()).log(Level.SEVERE, null, ex);
            this.desconectar();
            return 0;
        }
    }
}
