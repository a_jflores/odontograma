package models;

import beans.ExamenRadiografico;
import beans.HistoriaClinica;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ExamenRadiograficoModel extends Conexion {

    public List<ExamenRadiografico> listarExamenRadiografico(String codigo) throws SQLException {
        try {
            List<ExamenRadiografico> lista = new ArrayList<ExamenRadiografico>();
            String sql = "SELECT * FROM examen_radiografico WHERE id_historia_clinica=?";
            this.conectar();
            st = conexion.prepareStatement(sql);
            st.setString(1, codigo);
            rs = st.executeQuery();
            while (rs.next()) {
                ExamenRadiografico examenRadiografico = new ExamenRadiografico();
                HistoriaClinica historia = new HistoriaClinica();
                examenRadiografico.setId_examen_radiografico(rs.getInt("id_examen_radiografico"));
                historia.setCodigoHistoriaClinica(rs.getInt("id_historia_clinica"));
                examenRadiografico.setHistoriaclinica(historia);
                examenRadiografico.setFoto(rs.getString("foto"));
                examenRadiografico.setObservaciones(rs.getString("observaciones"));
                lista.add(examenRadiografico);
            }
            this.desconectar();
            return lista;
        } catch (SQLException ex) {
            Logger.getLogger(ExamenRadiograficoModel.class.getName()).log(Level.SEVERE, null, ex);
            this.desconectar();
            return null;
        }
    }

    public ExamenRadiografico obtenerExamenRadiografico(String codigo) throws SQLException {
        try {
            String sql = "SELECT * FROM examen_radiografico WHERE id_examen_radiografico = ?";
            this.conectar();
            st = conexion.prepareStatement(sql);
            st.setString(1, codigo);
            rs = st.executeQuery();
            if (rs.next()) {
                ExamenRadiografico examenRadiografico = new ExamenRadiografico();
                HistoriaClinica historia = new HistoriaClinica();
                examenRadiografico.setId_examen_radiografico(rs.getInt("id_examen_radiografico"));
                historia.setCodigoHistoriaClinica(rs.getInt("id_historia_clinica"));
                examenRadiografico.setHistoriaclinica(historia);
                examenRadiografico.setFoto(rs.getString("foto"));
                examenRadiografico.setObservaciones(rs.getString("observaciones"));
                this.conectar();
                return examenRadiografico;
            }
            this.desconectar();
            return null;
        } catch (SQLException ex) {
            Logger.getLogger(ExamenRadiograficoModel.class.getName()).log(Level.SEVERE, null, ex);
            this.desconectar();
            return null;
        }
    }

    public int insertarExamenRadiografico(ExamenRadiografico examenRadiografico) throws SQLException {
        try {
            int filasAfectadas = 0;
            String sql = "INSERT INTO examen_radiografico (id_historia_clinica,foto,observaciones) VALUES(?,?,?)";
            this.conectar();
            st = conexion.prepareStatement(sql);
            st.setInt(1, examenRadiografico.getHistoriaclinica().getCodigoHistoriaClinica());
            st.setString(2, examenRadiografico.getFoto());
            st.setString(3, examenRadiografico.getObservaciones());
            filasAfectadas = st.executeUpdate();
            this.desconectar();
            return filasAfectadas;
        } catch (SQLException ex) {
            Logger.getLogger(ExamenRadiograficoModel.class.getName()).log(Level.SEVERE, null, ex);
            this.desconectar();
            return 0;
        }
    }

    public int editarExamenRadiografico(ExamenRadiografico examenRadiografico) throws SQLException {
        try {
            int filasAfectadas = 0;
            String sql = "UPDATE examen_radiografico "
                    + "SET "
                    + "foto=?, "
                    + "observaciones=? "
                    + "WHERE id_examen_radiografico=?";
            this.conectar();
            st = conexion.prepareStatement(sql);
            st.setString(1, examenRadiografico.getFoto());
            st.setString(2, examenRadiografico.getObservaciones());
            st.setInt(3, examenRadiografico.getId_examen_radiografico());
            filasAfectadas = st.executeUpdate();
            this.desconectar();
            return filasAfectadas;
        } catch (SQLException ex) {
            Logger.getLogger(ExamenRadiograficoModel.class.getName()).log(Level.SEVERE, null, ex);
            this.desconectar();
            return 0;
        }
    }

    public int addExamRadiograph(ExamenRadiografico examenRadiografico) throws SQLException {
        try {
            int rs = 0;
            String sql = "insert into examen_radiografico values(null, ?, ?, ?)";
            this.conectar();
            st = conexion.prepareStatement(sql);
            st.setInt(1, examenRadiografico.getHistoriaclinica().getCodigoHistoriaClinica());
            st.setString(2, examenRadiografico.getFoto());
            st.setString(3, examenRadiografico.getObservaciones());
            rs = st.executeUpdate();
            this.desconectar();
            return rs;
        } catch (SQLException ex) {
            Logger.getLogger(ExamenRadiograficoModel.class.getName()).log(Level.SEVERE, null, ex);
            this.desconectar();
            return 0;
        }
    }

    //Edit without photo
    public int editExamRadiograph(ExamenRadiografico examenRadiografico) throws SQLException {
        try {
            int rs = 0;
            String sql = "UPDATE examen_radiografico "
                    + "SET "
                    + "observaciones=? "
                    + "WHERE id_examen_radiografico=?";
            this.conectar();
            st = conexion.prepareStatement(sql);
            st.setString(1, examenRadiografico.getObservaciones());
            st.setInt(2, examenRadiografico.getId_examen_radiografico());
            rs = st.executeUpdate();
            this.desconectar();
            return rs;
        } catch (SQLException ex) {
            Logger.getLogger(ExamenRadiograficoModel.class.getName()).log(Level.SEVERE, null, ex);
            this.desconectar();
            return 0;
        }
    }

    public int eliminarExamenRadiografico(String codigo) throws SQLException {
        try {
            int filasAfectadas = 0;
            String sql = "DELETE FROM examen_radiografico "
                    + "WHERE id_examen_radiografico = ?";
            this.conectar();
            st = conexion.prepareStatement(sql);
            st.setString(1, codigo);
            filasAfectadas = st.executeUpdate();
            this.desconectar();
            return filasAfectadas;
        } catch (SQLException ex) {
            Logger.getLogger(ExamenRadiograficoModel.class.getName()).log(Level.SEVERE, null, ex);
            this.desconectar();
            return 0;
        }
    }
}
