package models;

import beans.HistoriaClinica;
import beans.Odontograma;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

public class OdontogramaModel extends Conexion {

    public ArrayList<Odontograma> listarOdontogramas(String codigo) throws SQLException {
        try {
            ArrayList<Odontograma> lista = new ArrayList<Odontograma>();
            String sql = "SELECT * FROM odontograma "
                    + "WHERE id_historia_clinica = ?";
            this.conectar();
            st = conexion.prepareStatement(sql);
            st.setString(1, codigo);
            rs = st.executeQuery();
            while (rs.next()) {
                Odontograma odontograma = new Odontograma();
                HistoriaClinica historia = new HistoriaClinica();

                odontograma.setCodigoOdontograma(rs.getInt("id_odontograma"));
                historia.setCodigoHistoriaClinica(rs.getInt("id_historia_clinica"));
                odontograma.setHistoriaClinica(historia);
                odontograma.setObservaciones(rs.getString("observaciones"));
                odontograma.setDate_created(rs.getString("date_created"));
                lista.add(odontograma);
            }
            this.desconectar();
            return lista;
        } catch (SQLException ex) {
            Logger.getLogger(OdontogramaModel.class.getName()).log(Level.SEVERE, null, ex);
            this.desconectar();
            return null;
        }
    }

    public Odontograma obtenerOdontograma(String codigo) throws SQLException {
        try {
            String sql = "SELECT * FROM odontograma WHERE id_odontograma=?";
            this.conectar();
            st = conexion.prepareStatement(sql);
            st.setString(1, codigo);
            rs = st.executeQuery();
            if (rs.next()) {
                Odontograma odontograma = new Odontograma();
                HistoriaClinica historia = new HistoriaClinica();

                odontograma.setCodigoOdontograma(rs.getInt("id_odontograma"));
                historia.setCodigoHistoriaClinica(rs.getInt("id_historia_clinica"));
                odontograma.setHistoriaClinica(historia);
                odontograma.setObservaciones(rs.getString("observaciones"));
                odontograma.setDate_created(rs.getString("date_created"));
                this.desconectar();
                return odontograma;
            }
            this.desconectar();
            return null;
        } catch (SQLException ex) {
            Logger.getLogger(OdontogramaModel.class.getName()).log(Level.SEVERE, null, ex);
            this.desconectar();
            return null;
        }
    }

    public int insertarOdontograma(Odontograma odontograma) throws SQLException {
        try {
            int filasAfectadas = 0;
            String sql = "INSERT INTO odontograma(id_historia_clinica,observaciones) values(?,?)";
            this.conectar();
            st = conexion.prepareStatement(sql);
            st.setInt(1, odontograma.getHistoriaClinica().getCodigoHistoriaClinica());
            st.setString(2, odontograma.getObservaciones());
            filasAfectadas = st.executeUpdate();
            this.desconectar();
            return filasAfectadas;
        } catch (SQLException ex) {
            Logger.getLogger(OdontogramaModel.class.getName()).log(Level.SEVERE, null, ex);
            this.desconectar();
            return 0;
        }
    }

    public int modificarOdontograma(Odontograma odontograma) throws SQLException {
        try {
            int filasAfectadas = 0;
            String sql = "UPDATE odontograma SET observaciones=? "
                    + "WHERE id_odontograma=?";
            this.conectar();
            st = conexion.prepareStatement(sql);
            st.setString(1, odontograma.getObservaciones());
            st.setInt(2, odontograma.getCodigoOdontograma());
            filasAfectadas = st.executeUpdate();
            this.desconectar();
            return filasAfectadas;
        } catch (SQLException ex) {
            Logger.getLogger(OdontogramaModel.class.getName()).log(Level.SEVERE, null, ex);
            this.desconectar();
            return 0;
        }
    }

    public int ultimoOdontograma() throws SQLException {
        try {
            int filasAfectadas = 0;
            String sql = "SELECT MAX(id_odontograma)\n"
                    + "FROM odontograma";
            this.conectar();
            st = conexion.prepareStatement(sql);
            rs = st.executeQuery();
            while (rs.next()) {
                filasAfectadas = rs.getInt(1);
            }
            this.desconectar();
            return filasAfectadas;
        } catch (SQLException ex) {
            Logger.getLogger(OdontogramaModel.class.getName()).log(Level.SEVERE, null, ex);
            this.desconectar();
            return 0;
        }
    }

    public int ultimoOdontogramaHistoria(String codigo) throws SQLException {
        try {
            int filasAfectadas = 0;
            String sql = "SELECT MAX(id_odontograma)\n"
                    + "FROM odontograma\n"
                    + "WHERE id_historia_clinica = ?";
            this.conectar();
            st = conexion.prepareStatement(sql);
            st.setString(1, codigo);
            rs = st.executeQuery();
            while (rs.next()) {
                filasAfectadas = rs.getInt(1);
            }
            this.desconectar();
            return filasAfectadas;
        } catch (SQLException ex) {
            Logger.getLogger(OdontogramaModel.class.getName()).log(Level.SEVERE, null, ex);
            this.desconectar();
            return 0;
        }
    }

}
