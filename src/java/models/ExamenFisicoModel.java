package models;

import beans.ExamenFisico;
import beans.HistoriaClinica;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ExamenFisicoModel extends Conexion {

    public int insertarExamenFisico(ExamenFisico examen) throws SQLException {
        try {
            int filasAfectadas = 0;
            String sql = "INSERT INTO examen_fisico "
                    + "(id_historia_clinica,"
                    + "peso,"
                    + "talla,"
                    + "grupo_sanguineo,"
                    + "pulso,"
                    + "presion_arterial_maxima,"
                    + "presion_arterial_minima,"
                    + "frecuencia_respiratoria,"
                    + "imc) "
                    + "VALUES(?,?,?,?,?,?,?,?,?)";
            this.conectar();
            st = conexion.prepareStatement(sql);
            st.setInt(1, examen.getHistoriaClinica().getCodigoHistoriaClinica());
            st.setDouble(2, examen.getPeso());
            st.setDouble(3, examen.getTalla());
            st.setString(4, examen.getGrupo_sanguineo());
            st.setInt(5, examen.getPulso());
            st.setDouble(6, examen.getPresion_arterial_maxima());
            st.setDouble(7, examen.getPresion_arterial_minima());
            st.setDouble(8, examen.getFrecuencia_respiratoria());
            st.setDouble(9, examen.getImc());

            filasAfectadas = st.executeUpdate();
            this.desconectar();
            return filasAfectadas;
        } catch (SQLException ex) {
            Logger.getLogger(ExamenFisicoModel.class.getName()).log(Level.SEVERE, null, ex);
            this.desconectar();
            return 0;
        }
    }

    public ExamenFisico obtenerExamenFisico(String codigo) throws SQLException {
        try {
            String sql = "SELECT * FROM examen_fisico WHERE id_historia_clinica=?";
            this.conectar();
            st = conexion.prepareStatement(sql);
            st.setString(1, codigo);
            rs = st.executeQuery();
            if (rs.next()) {
                ExamenFisico examen = new ExamenFisico();
                HistoriaClinica historia = new HistoriaClinica();

                examen.setCodigoExamenFisico(rs.getInt("id_examen_fisico"));
                historia.setCodigoHistoriaClinica(rs.getInt("id_historia_clinica"));
                examen.setHistoriaClinica(historia);
                examen.setPeso(rs.getDouble("peso"));
                examen.setTalla(rs.getDouble("talla"));
                examen.setImc(rs.getDouble("imc"));
                examen.setPresion_arterial_maxima(rs.getDouble("presion_arterial_maxima"));
                examen.setPresion_arterial_minima(rs.getDouble("presion_arterial_minima"));
                examen.setFrecuencia_respiratoria(rs.getDouble("frecuencia_respiratoria"));
                examen.setGrupo_sanguineo(rs.getString("grupo_sanguineo"));
                examen.setPulso(rs.getInt("pulso"));
                this.desconectar();
                return examen;
            }
            this.desconectar();
            return null;
        } catch (SQLException ex) {
            Logger.getLogger(ExamenFisicoModel.class.getName()).log(Level.SEVERE, null, ex);
            this.desconectar();
            return null;
        }
    }

    public int modificarExamenFisico(ExamenFisico examen) throws SQLException {
        try {
            int filasAfectadas = 0;
            String sql = "UPDATE examen_fisico SET "
                    + "peso = ?,"
                    + "talla = ?,"
                    + "grupo_sanguineo = ?,"
                    + "pulso = ?,"
                    + "presion_arterial_maxima = ?,"
                    + "presion_arterial_minima = ?,"
                    + "frecuencia_respiratoria = ?,"
                    + "imc = ? "
                    + "WHERE id_historia_clinica = ?";
            this.conectar();
            st = conexion.prepareStatement(sql);
            st.setDouble(1, examen.getPeso());
            st.setDouble(2, examen.getTalla());
            st.setString(3, examen.getGrupo_sanguineo());
            st.setInt(4, examen.getPulso());
            st.setDouble(5, examen.getPresion_arterial_maxima());
            st.setDouble(6, examen.getPresion_arterial_minima());
            st.setDouble(7, examen.getFrecuencia_respiratoria());
            st.setDouble(8, examen.getImc());
            st.setInt(9, examen.getHistoriaClinica().getCodigoHistoriaClinica());

            filasAfectadas = st.executeUpdate();
            this.desconectar();
            return filasAfectadas;
        } catch (SQLException ex) {
            Logger.getLogger(ExamenFisicoModel.class.getName()).log(Level.SEVERE, null, ex);
            this.desconectar();
            return 0;
        }
    }
}
