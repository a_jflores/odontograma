package models;

import beans.HistoriaClinica;
import beans.Video;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class VideoModel extends Conexion {

    public List<Video> listarVideo(String codigo) throws SQLException {
        try {
            List<Video> lista = new ArrayList<Video>();
            String sql = "SELECT * FROM video WHERE id_historia_clinica=?";
            this.conectar();
            st = conexion.prepareStatement(sql);
            st.setString(1, codigo);
            rs = st.executeQuery();
            while (rs.next()) {
                Video video = new Video();
                HistoriaClinica historia = new HistoriaClinica();
                video.setId_video(rs.getInt("id_video"));
                historia.setCodigoHistoriaClinica(rs.getInt("id_historia_clinica"));
                video.setHistoria(historia);
                video.setNombre(rs.getString("nombre"));
                video.setRuta(rs.getString("ruta"));
                video.setObservaciones(rs.getString("observaciones"));
                lista.add(video);
            }
            this.desconectar();
            return lista;
        } catch (SQLException ex) {
            Logger.getLogger(VideoModel.class.getName()).log(Level.SEVERE, null, ex);
            this.desconectar();
            return null;
        }
    }

    public Video obtenerVideo(String codigo) throws SQLException {
        try {
            String sql = "SELECT * FROM video WHERE id_video = ?";
            this.conectar();
            st = conexion.prepareStatement(sql);
            st.setString(1, codigo);
            rs = st.executeQuery();
            if (rs.next()) {
                Video video = new Video();
                HistoriaClinica historia = new HistoriaClinica();
                video.setId_video(rs.getInt("id_video"));
                historia.setCodigoHistoriaClinica(rs.getInt("id_historia_clinica"));
                video.setHistoria(historia);
                video.setNombre(rs.getString("nombre"));
                video.setRuta(rs.getString("ruta"));
                video.setObservaciones(rs.getString("observaciones"));
                this.conectar();
                return video;
            }
            this.desconectar();
            return null;
        } catch (SQLException ex) {
            Logger.getLogger(VideoModel.class.getName()).log(Level.SEVERE, null, ex);
            this.desconectar();
            return null;
        }
    }

    public int insertarVideo(Video video) throws SQLException {
        try {
            int filasAfectadas = 0;
            String sql = "INSERT INTO video VALUES (null,?,?,?,?)";
            this.conectar();
            st = conexion.prepareStatement(sql);
            st.setInt(1, video.getHistoria().getCodigoHistoriaClinica());
            st.setString(2, video.getNombre());
            st.setString(3, video.getRuta());
            st.setString(4, video.getObservaciones());
            filasAfectadas = st.executeUpdate();
            this.desconectar();
            return filasAfectadas;
        } catch (SQLException ex) {
            Logger.getLogger(VideoModel.class.getName()).log(Level.SEVERE, null, ex);
            this.desconectar();
            return 0;
        }
    }

    public int editarVideo(Video video) throws SQLException {
        try {
            int filasAfectadas = 0;
            String sql = "UPDATE video "
                    + "SET "
                    + "nombre=?, "
                    + "ruta=?, "
                    + "observaciones=? "
                    + "WHERE id_video=?";
            this.conectar();
            st = conexion.prepareStatement(sql);
            st.setString(1, video.getNombre());
            st.setString(2, video.getRuta());
            st.setString(3, video.getObservaciones());
            st.setInt(4, video.getId_video());
            filasAfectadas = st.executeUpdate();
            this.desconectar();
            return filasAfectadas;
        } catch (SQLException ex) {
            Logger.getLogger(VideoModel.class.getName()).log(Level.SEVERE, null, ex);
            this.desconectar();
            return 0;
        }
    }

    public int editarVideosinRuta(Video video) throws SQLException {
        try {
            int filasAfectadas = 0;
            String sql = "UPDATE video "
                    + "SET "
                    + "nombre=?, "
                    + "observaciones=? "
                    + "WHERE id_video=?";
            this.conectar();
            st = conexion.prepareStatement(sql);
            st.setString(1, video.getNombre());
            st.setString(2, video.getObservaciones());
            st.setInt(3, video.getId_video());
            filasAfectadas = st.executeUpdate();
            this.desconectar();
            return filasAfectadas;
        } catch (Exception ex) {
            ex.printStackTrace(System.out);
            this.desconectar();
            return 0;
        }
    }

    public int eliminarVideo(String codigo) throws SQLException {
        try {
            int filasAfectadas = 0;
            String sql = "DELETE FROM video "
                    + "WHERE id_video = ?";
            this.conectar();
            st = conexion.prepareStatement(sql);
            st.setString(1, codigo);
            filasAfectadas = st.executeUpdate();
            this.desconectar();
            return filasAfectadas;
        } catch (SQLException ex) {
            Logger.getLogger(VideoModel.class.getName()).log(Level.SEVERE, null, ex);
            this.desconectar();
            return 0;
        }
    }
}
