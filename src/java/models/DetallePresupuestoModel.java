package models;

import beans.DetallePresupuesto;
import beans.Presupuesto;
import beans.Tratamiento;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class DetallePresupuestoModel extends Conexion {

    public List<DetallePresupuesto> listarDetallePresupuesto(String id) throws SQLException {
        try {
            List<DetallePresupuesto> lista = new ArrayList<DetallePresupuesto>();
            String sql = "SELECT * FROM detalle_presupuesto WHERE id_presupuesto = ?";
            this.conectar();
            st = conexion.prepareStatement(sql);
            st.setString(1, id);
            rs = st.executeQuery();
            while (rs.next()) {
                DetallePresupuesto detalle_presupuesto = new DetallePresupuesto();
                Presupuesto presupuesto = new Presupuesto();
                Tratamiento tratamiento = new Tratamiento();

                presupuesto.setId_presupuesto(rs.getInt("id_presupuesto"));
                tratamiento.setIdTratamiento(rs.getInt("id_tratamiento"));

                detalle_presupuesto.setId_detalle_presupuesto(rs.getInt("id_detalle_presupuesto"));
                detalle_presupuesto.setPresupuesto(presupuesto);
                detalle_presupuesto.setTratamiento(tratamiento);
                detalle_presupuesto.setPieza(rs.getInt("pieza"));
                detalle_presupuesto.setHallazgo(rs.getString("hallazgo"));
                detalle_presupuesto.setCantidad(rs.getInt("cantidad"));
                detalle_presupuesto.setPrecio(rs.getDouble("precio"));
                detalle_presupuesto.setSubtotal(rs.getDouble("subtotal"));
                lista.add(detalle_presupuesto);
            }
            this.desconectar();
            return lista;
        } catch (SQLException ex) {
            Logger.getLogger(DetallePresupuestoModel.class.getName()).log(Level.SEVERE, null, ex);
            this.desconectar();
            return null;
        }
    }

    public int insertarDetallePresupuesto(DetallePresupuesto miDetallePresupuesto) throws SQLException {
        try {
            int filasAfectadas = 0;
            String sql = "INSERT INTO detalle_presupuesto "
                    + "VALUES (null,?,?,?,?,?,?,?)";
            this.conectar();
            st = conexion.prepareStatement(sql);
            st.setInt(1, miDetallePresupuesto.getPresupuesto().getId_presupuesto());
            st.setInt(2, miDetallePresupuesto.getTratamiento().getIdTratamiento());
            st.setInt(3, miDetallePresupuesto.getPieza());
            st.setString(4, miDetallePresupuesto.getHallazgo());
            st.setInt(5, miDetallePresupuesto.getCantidad());
            st.setDouble(6, miDetallePresupuesto.getPrecio());
            st.setDouble(7, miDetallePresupuesto.getSubtotal());
            filasAfectadas = st.executeUpdate();
            this.desconectar();
            return filasAfectadas;
        } catch (SQLException ex) {
            Logger.getLogger(DetallePresupuestoModel.class.getName()).log(Level.SEVERE, null, ex);
            this.desconectar();
            return 0;
        }
    }

    public int editarDetallePresupuesto(DetallePresupuesto miDetallePresupuesto) throws SQLException {
        try {
            int filasAfectadas = 0;
            String sql = "UPDATE detalle_presupuesto "
                    + "SET id_tratamiento = ?, "
                    + "pieza = ?, "
                    + "hallazgo = ?, "
                    + "cantidad = ?, "
                    + "precio = ?, "
                    + "subtotal = ? "
                    + "WHERE id_presupuesto = ?";
            this.conectar();
            st = conexion.prepareStatement(sql);
            st.setInt(1, miDetallePresupuesto.getTratamiento().getIdTratamiento());
            st.setInt(2, miDetallePresupuesto.getPieza());
            st.setString(3, miDetallePresupuesto.getHallazgo());
            st.setInt(4, miDetallePresupuesto.getCantidad());
            st.setDouble(5, miDetallePresupuesto.getPrecio());
            st.setDouble(6, miDetallePresupuesto.getSubtotal());
            st.setInt(7, miDetallePresupuesto.getPresupuesto().getId_presupuesto());
            filasAfectadas = st.executeUpdate();
            this.desconectar();
            return filasAfectadas;
        } catch (SQLException ex) {
            Logger.getLogger(DetallePresupuestoModel.class.getName()).log(Level.SEVERE, null, ex);
            this.desconectar();
            return 0;
        }
    }
    
    public int eliminarDetallePresupuesto(String codigo) throws SQLException {
        try {
            int filasAfectadas = 0;
            String sql = "DELETE FROM detalle_presupuesto WHERE id_presupuesto =?";
            this.conectar();
            st = conexion.prepareStatement(sql);
            st.setString(1, codigo);
            filasAfectadas = st.executeUpdate();
            this.desconectar();
            return filasAfectadas;
        } catch (SQLException ex) {
            Logger.getLogger(DienteDetalleModel.class.getName()).log(Level.SEVERE, null, ex);
            this.desconectar();
            return 0;
        }
    }

}
