package models;

import beans.Anamnesis;
import beans.HistoriaClinica;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class AnamnesisModel extends Conexion {

    public Anamnesis obtenerAnamnesis(String codigo) throws SQLException {
        try {
            String sql = "SELECT * FROM anamnesis WHERE id_historia_clinica=?";
            this.conectar();
            st = conexion.prepareStatement(sql);
            st.setString(1, codigo);
            rs = st.executeQuery();
            if (rs.next()) {
                Anamnesis anamnesis = new Anamnesis();
                HistoriaClinica historia = new HistoriaClinica();

                anamnesis.setCodigoAnamnesis(rs.getInt("id_anamnesis"));
                historia.setCodigoHistoriaClinica(rs.getInt("id_historia_clinica"));
                anamnesis.setHistoriaClinica(historia);
                anamnesis.setDiabetes(rs.getString("diabetes"));
                anamnesis.setHipertension(rs.getString("hipertension_arterial"));
                anamnesis.setHemorragia(rs.getString("hemorragia"));
                anamnesis.setAlergia(rs.getString("alergia"));
                anamnesis.setDesmayo(rs.getString("desmayo"));
                anamnesis.setHepatica(rs.getString("hepatica"));
                anamnesis.setFiebre_reumatica(rs.getString("fiebre_reumatica"));
                anamnesis.setAngina(rs.getString("angina_pecho"));
                anamnesis.setMiocardio(rs.getString("miocardio"));
                anamnesis.setEmbarazo(rs.getString("embarazo"));
                anamnesis.setDetalle_diabetes(rs.getString("detalle_diabetes"));
                anamnesis.setDetalle_hipertension(rs.getString("detalle_hipertension_arterial"));
                anamnesis.setDetalle_hemorragia(rs.getString("detalle_hemorragia"));
                anamnesis.setDetalle_alergia(rs.getString("detalle_alergia"));
                anamnesis.setDetalle_desmayo(rs.getString("detalle_desmayo"));
                anamnesis.setDetalle_hepatica(rs.getString("detalle_hepatica"));
                anamnesis.setDetalle_fiebre_reumatica(rs.getString("detalle_fiebre_reumatica"));
                anamnesis.setDetalle_angina(rs.getString("detalle_angina_pecho"));
                anamnesis.setDetalle_miocardio(rs.getString("detalle_miocardio"));
                anamnesis.setDetalle_embarazo(rs.getString("detalle_embarazo"));
                anamnesis.setAlerta_diabetes(rs.getString("alerta_diabetes"));
                anamnesis.setAlerta_hipertension(rs.getString("alerta_hipertension_arterial"));
                anamnesis.setAlerta_hemorragia(rs.getString("alerta_hemorragia"));
                anamnesis.setAlerta_alergia(rs.getString("alerta_alergia"));
                anamnesis.setAlerta_desmayo(rs.getString("alerta_desmayo"));
                anamnesis.setAlerta_hepatica(rs.getString("alerta_hepatica"));
                anamnesis.setAlerta_fiebre_reumatica(rs.getString("alerta_fiebre_reumatica"));
                anamnesis.setAlerta_angina(rs.getString("alerta_angina_pecho"));
                anamnesis.setAlerta_miocardio(rs.getString("alerta_miocardio"));
                anamnesis.setAlerta_embarazo(rs.getString("alerta_embarazo"));
                this.desconectar();
                return anamnesis;
            }
            this.desconectar();
            return null;
        } catch (SQLException ex) {
            Logger.getLogger(AnamnesisModel.class.getName()).log(Level.SEVERE, null, ex);
            this.desconectar();
            return null;
        }
    }

    public int insertarAnamnesis(Anamnesis anamnesis) throws SQLException {
        try {
            int filasAfectadas = 0;
            String sql = "INSERT INTO anamnesis VALUES(null,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
            this.conectar();
            st = conexion.prepareStatement(sql);
            st.setInt(1, anamnesis.getHistoriaClinica().getCodigoHistoriaClinica());
            st.setString(2, anamnesis.getDiabetes());
            st.setString(3, anamnesis.getHipertension());
            st.setString(4, anamnesis.getHemorragia());
            st.setString(5, anamnesis.getAlergia());
            st.setString(6, anamnesis.getDesmayo());
            st.setString(7, anamnesis.getHepatica());
            st.setString(8, anamnesis.getFiebre_reumatica());
            st.setString(9, anamnesis.getAngina());
            st.setString(10, anamnesis.getMiocardio());
            st.setString(11, anamnesis.getEmbarazo());
            st.setString(12, anamnesis.getDetalle_diabetes());
            st.setString(13, anamnesis.getDetalle_hipertension());
            st.setString(14, anamnesis.getDetalle_hemorragia());
            st.setString(15, anamnesis.getDetalle_alergia());
            st.setString(16, anamnesis.getDetalle_desmayo());
            st.setString(17, anamnesis.getDetalle_hepatica());
            st.setString(18, anamnesis.getDetalle_fiebre_reumatica());
            st.setString(19, anamnesis.getDetalle_angina());
            st.setString(20, anamnesis.getDetalle_miocardio());
            st.setString(21, anamnesis.getDetalle_embarazo());
            st.setString(22, anamnesis.getAlerta_diabetes());
            st.setString(23, anamnesis.getAlerta_hipertension());
            st.setString(24, anamnesis.getAlerta_hemorragia());
            st.setString(25, anamnesis.getAlerta_alergia());
            st.setString(26, anamnesis.getAlerta_desmayo());
            st.setString(27, anamnesis.getAlerta_hepatica());
            st.setString(28, anamnesis.getAlerta_fiebre_reumatica());
            st.setString(29, anamnesis.getAlerta_angina());
            st.setString(30, anamnesis.getAlerta_miocardio());
            st.setString(31, anamnesis.getAlerta_embarazo());
            filasAfectadas = st.executeUpdate();
            this.desconectar();
            return filasAfectadas;
        } catch (SQLException ex) {
            Logger.getLogger(AnamnesisModel.class.getName()).log(Level.SEVERE, null, ex);
            this.desconectar();
            return 0;
        }
    }

    public int modificarCuestionario1(Anamnesis anamnesis) throws SQLException {
        try {
            int filasAfectadas = 0;
            String sql = "UPDATE anamnesis "
                    + "SET "
                    + "diabetes=?, "
                    + "hipertension_arterial=?, "
                    + "hemorragia=?, "
                    + "alergia=?, "
                    + "desmayo=?, "
                    + "hepatica=?, "
                    + "fiebre_reumatica=?, "
                    + "angina_pecho=?, "
                    + "miocardio=?, "
                    + "embarazo=?, "
                    + "detalle_diabetes=?, "
                    + "detalle_hipertension_arterial=?, "
                    + "detalle_hemorragia=?, "
                    + "detalle_alergia=?, "
                    + "detalle_desmayo=?, "
                    + "detalle_hepatica=?, "
                    + "detalle_fiebre_reumatica=?, "
                    + "detalle_angina_pecho=?, "
                    + "detalle_miocardio=?, "
                    + "detalle_embarazo=?, "
                    + "alerta_diabetes=?, "
                    + "alerta_hipertension_arterial=?, "
                    + "alerta_hemorragia=?, "
                    + "alerta_alergia=?, "
                    + "alerta_desmayo=?, "
                    + "alerta_hepatica=?, "
                    + "alerta_fiebre_reumatica=?, "
                    + "alerta_angina_pecho=?, "
                    + "alerta_miocardio=?, "
                    + "alerta_embarazo=? "
                    + "WHERE id_historia_clinica=?";
            this.conectar();
            st = conexion.prepareStatement(sql);
            st.setString(1, anamnesis.getDiabetes());
            st.setString(2, anamnesis.getHipertension());
            st.setString(3, anamnesis.getHemorragia());
            st.setString(4, anamnesis.getAlergia());
            st.setString(5, anamnesis.getDesmayo());
            st.setString(6, anamnesis.getHepatica());
            st.setString(7, anamnesis.getFiebre_reumatica());
            st.setString(8, anamnesis.getAngina());
            st.setString(9, anamnesis.getMiocardio());
            st.setString(10, anamnesis.getEmbarazo());
            st.setString(11, anamnesis.getDetalle_diabetes());
            st.setString(12, anamnesis.getDetalle_hipertension());
            st.setString(13, anamnesis.getDetalle_hemorragia());
            st.setString(14, anamnesis.getDetalle_alergia());
            st.setString(15, anamnesis.getDetalle_desmayo());
            st.setString(16, anamnesis.getDetalle_hepatica());
            st.setString(17, anamnesis.getDetalle_fiebre_reumatica());
            st.setString(18, anamnesis.getDetalle_angina());
            st.setString(19, anamnesis.getDetalle_miocardio());
            st.setString(20, anamnesis.getDetalle_embarazo());
            st.setString(21, anamnesis.getAlerta_diabetes());
            st.setString(22, anamnesis.getAlerta_hipertension());
            st.setString(23, anamnesis.getAlerta_hemorragia());
            st.setString(24, anamnesis.getAlerta_alergia());
            st.setString(25, anamnesis.getAlerta_desmayo());
            st.setString(26, anamnesis.getAlerta_hepatica());
            st.setString(27, anamnesis.getAlerta_fiebre_reumatica());
            st.setString(28, anamnesis.getAlerta_angina());
            st.setString(29, anamnesis.getAlerta_miocardio());
            st.setString(30, anamnesis.getAlerta_embarazo());
            st.setInt(31, anamnesis.getHistoriaClinica().getCodigoHistoriaClinica());
            filasAfectadas = st.executeUpdate();
            this.desconectar();
            return filasAfectadas;
        } catch (SQLException ex) {
            Logger.getLogger(AnamnesisModel.class.getName()).log(Level.SEVERE, null, ex);
            this.desconectar();
            return 0;
        }
    }
}
